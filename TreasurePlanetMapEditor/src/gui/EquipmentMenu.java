package gui;

import java.awt.Window;
import java.awt.event.WindowEvent;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import data.world.Sailor;
import data.world.Weapon;
import gui.elements.JCheckBoxList;

/**
 * A menu to manipulate the available equipment and crew
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class EquipmentMenu extends GenericDialog {

	private final JCheckBox[]	weaponsChecks	= new JCheckBox[Weapon.weapons.size ()];
	private final JCheckBox[]	sailorsChecks	= new JCheckBox[Sailor.sailors.size ()];


	/**
	 * Creates a new {@link EquipmentMenu}
	 *
	 * @param parent the parent window
	 */
	public EquipmentMenu (Window parent) {
		super (parent, "equipment", 420, 300);
		setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);

		for (int i = 0, n = Weapon.weapons.size (); i < n; i++) {
			weaponsChecks[i] = new JCheckBox (Weapon.weapons.get (i).getName (), Weapon.selected.get (i));
		}
		for (int i = 0, n = Sailor.sailors.size (); i < n; i++) {
			sailorsChecks[i] = new JCheckBox (Sailor.sailors.get (i).getName (), Sailor.selected.get (i));
		}


		JLabel lNElementsWY = new JLabel ();
		JCheckBoxList lsWeaponsY = new JCheckBoxList (lNElementsWY, "Weapons", weaponsChecks);

		JScrollPane sWeaponY = new JScrollPane ();
		sWeaponY.setBounds (12, 12, 185, 247);
		sWeaponY.setColumnHeaderView (lNElementsWY);
		sWeaponY.setViewportView (lsWeaponsY);


		JLabel lNElementsSY = new JLabel ();
		JCheckBoxList lsSaylorsY = new JCheckBoxList (lNElementsSY, "Sailors", sailorsChecks);

		JScrollPane sSaylorsY = new JScrollPane ();
		sSaylorsY.setBounds (217, 12, 185, 247);
		sSaylorsY.setColumnHeaderView (lNElementsSY);
		sSaylorsY.setViewportView (lsSaylorsY);


		setLayout (null);
		add (sWeaponY);
		add (sSaylorsY);

		setVisible (true);
	}


	@Override
	public void windowClosing (WindowEvent e) {
		for (int i = 0, n = weaponsChecks.length; i < n; i++) {
			Weapon.selected.set (i, weaponsChecks[i].isSelected ());
		}
		for (int i = 0, n = Sailor.sailors.size (); i < n; i++) {
			Sailor.selected.set (i, sailorsChecks[i].isSelected ());
		}
		dispose ();
	}

}
