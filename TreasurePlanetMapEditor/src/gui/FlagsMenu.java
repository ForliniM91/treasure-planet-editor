package gui;

import java.awt.Window;

import data.world.Flag;
import gui.editors.FlagsMenuEditor;


/**
 * A menu to manipulate the flags
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class FlagsMenu extends EntriesListMenu <Flag> {

	/**
	 * Creates a new {@link FlagsMenu}
	 *
	 * @param parent The parent window
	 */
	public FlagsMenu (Window parent) {
		super (parent, "flag", Flag.flags, true);
	}

	@Override
	public Flag newElementEvent () throws IllegalStateException {
		FlagsMenuEditor editor = new FlagsMenuEditor (this);
		return editor.element;
	}

	@Override
	public boolean editElementEvent (Flag flag) {
		new FlagsMenuEditor (this, flag);
		return true;
	}

}
