package gui.elements;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/**
 * A panel which display an image
 *
 * @author MarcoForlini
 */
public class JImage extends JPanel {

	private static final long	serialVersionUID	= -10520949611632033L;

	/**
	 * The image of this panel
	 */
	public Image				image;

	/**
	 * Creates a new {@link JImage}
	 *
	 * @param image the image
	 */
	public JImage (Image image) {
		this.image = image;
	}

	@Override
	protected void paintComponent (Graphics g) {
		super.paintComponent (g);
		g.drawImage (image, 0, 0, null);
	}

}