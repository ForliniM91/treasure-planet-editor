package gui.elements;

import java.util.Arrays;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.ListSelectionModel;

import interfaces.JExtended;

/**
 * An extended JList
 *
 * @author MarcoForlini
 * @param <E> type of elements
 */
public class JListEx <E extends Comparable <? super E>> extends JList <E> implements JExtended <E> {

	private static final long	serialVersionUID	= 8002730612116076334L;

	private Vector <E>			elements;


	/**
	 * Creates a new {@link JListEx}
	 */
	public JListEx () {
		this (new Vector <E> (), false);
	}

	/**
	 * Creates a new empty {@link JListEx}
	 *
	 * @param data the array of elements
	 */
	public JListEx (E[] data) {
		this (new Vector<> (Arrays.asList (data)), false);
	}

	/**
	 * Creates a new empty {@link JListEx}
	 *
	 * @param data the vector of elements
	 */
	public JListEx (Vector <E> data) {
		this (data, false);
	}

	/**
	 * Creates a new empty {@link JListEx}
	 *
	 * @param data the array of elements
	 * @param sorted if true, the elements will be sorted
	 */
	public JListEx (E[] data, boolean sorted) {
		this (new Vector<> (Arrays.asList (data)), sorted);
	}

	/**
	 * Creates a new empty {@link JListEx}
	 *
	 * @param data the vector of elements
	 * @param sorted if true, the elements will be sorted
	 */
	public JListEx (Vector <E> data, boolean sorted) {
		super ();
		setVector (data, sorted);
		setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
		setVisibleRowCount (10);
	}

	@Override
	public Vector <E> getVector () {
		return elements;
	}

	@Override
	public void setVector (Vector <E> newElements, boolean sorted) {
		elements = newElements;
		if (sorted) {
			sort ();
		}
		refresh ();
	}

	@Override
	public E getSelectedElement () {
		return getSelectedValue ();
	}

	@Override
	public int getSelectedIndex () {
		return super.getSelectedIndex ();
	}

	@Override
	public void setSelectedElement (E element, boolean showElement) {
		setSelectedValue (element, showElement);
	}

	@Override
	public void setSelectedIndex (int index) {
		super.setSelectedIndex (index);
	}

	@Override
	public void setSelectedIndex (int index, boolean showElement) {
		setSelectedValue (elements.get (index), showElement);
	}

	@Override
	public void refresh (Vector <E> elements) {
		setListData (elements);
	}

}
