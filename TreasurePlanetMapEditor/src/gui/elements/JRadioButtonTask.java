package gui.elements;

import javax.swing.JRadioButton;

/**
 * Just a {@link JRadioButton} with a more complete constructor
 *
 * @author MarcoForlini
 */
public class JRadioButtonTask extends JRadioButton {

	private static final long serialVersionUID = -7696150881846791599L;

	/**
	 * Creates a new {@link JRadioButtonTask}
	 *
	 * @param name Displayed name
	 * @param descr ToolTip text
	 * @param x x position
	 * @param y y position
	 * @param width width
	 * @param height height
	 * @param selected if true, it's initially checked
	 */
	public JRadioButtonTask (String name, String descr, int x, int y, int width, int height, boolean selected) {
		super (name, selected);
		setToolTipText (descr);
		setBounds (x, y, width, height);
	}

}