package gui.elements;

import java.util.Arrays;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import interfaces.JExtended;

/**
 * An extended JComboBox
 *
 * @author MarcoForlini
 * @param <E> the type of elements
 */
public class JComboBoxEx <E extends Comparable <? super E>> extends JComboBox <E> implements JExtended <E> {

	private static final long	serialVersionUID	= -9016736239927762256L;

	private Vector <E>			elements;


	/**
	 * Creates a new {@link JComboBoxEx}
	 */
	public JComboBoxEx () {
		this (new Vector <E> (), false);
	}

	/**
	 * Creates a new {@link JComboBoxEx}
	 *
	 * @param data the array of elements
	 */
	public JComboBoxEx (E[] data) {
		this (new Vector<> (Arrays.asList (data)), false);
	}

	/**
	 * Creates a new {@link JComboBoxEx}
	 *
	 * @param data the vector of elements
	 */
	public JComboBoxEx (Vector <E> data) {
		this (data, false);
	}

	/**
	 * Creates a new {@link JComboBoxEx}
	 *
	 * @param data the array of elements
	 * @param sorted if true, the elements will be sorted
	 */
	public JComboBoxEx (E[] data, boolean sorted) {
		this (new Vector<> (Arrays.asList (data)), sorted);
	}

	/**
	 * Creates a new {@link JComboBoxEx}
	 *
	 * @param data the vector of elements
	 * @param sorted if true, the elements will be sorted
	 */
	public JComboBoxEx (Vector <E> data, boolean sorted) {
		super ();
		elements = data;
		if (sorted) {
			sort ();
		}
		refresh ();
	}

	@Override
	public Vector <E> getVector () {
		return elements;
	}

	@Override
	public void setVector (Vector <E> newElements, boolean sorted) {
		elements = new Vector<> (newElements);
		if (sorted) {
			sort ();
		}
		refresh ();
	}

	@SuppressWarnings ("unchecked")
	@Override
	public E getSelectedElement () {
		return (E) getSelectedItem ();
	}

	@Override
	public int getSelectedIndex () {
		return super.getSelectedIndex ();
	}

	@Override
	public void setSelectedElement (E element, boolean showElement) {
		setSelectedItem (element);
	}

	@Override
	public void setSelectedIndex (int index) {
		super.setSelectedIndex (index);
	}

	@Override
	public void setSelectedIndex (int index, boolean showElement) {
		super.setSelectedIndex (index);
	}

	@Override
	public void refresh (Vector <E> elements) {
		setModel (new DefaultComboBoxModel<> (elements));
	}

}
