package gui.elements;

import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import data.world.Entity;
import logic.Core;

/**
 * A panel which contains a field ID and a button to check it.
 *
 * @author MarcoForlini
 * @param <T> The {@link Entity}
 */
public class IDPanel <T extends Entity> extends JPanel {

	private static final long	serialVersionUID	= 5442101494988225144L;

	/** ID of the element */
	private final JTextField	tID					= new JTextField ();


	/**
	 * Creates a new {@link IDPanel}
	 *
	 * @param element The element
	 * @param x The x position
	 * @param y The y position
	 */
	public IDPanel (T element, int x, int y) {
		setBounds (x, y, 374, 23);
		setLayout (null);

		JLabel lPlayerID = new JLabel ("ID");
		lPlayerID.setToolTipText ("A unique ID");
		lPlayerID.setBounds (0, 4, 59, 14);
		lPlayerID.setLabelFor (tID);
		add (lPlayerID);

		if (element != null) {
			tID.setText (element.getID ());
		}
		tID.setToolTipText ("A unique ID");
		tID.setBounds (69, 1, 218, 20);
		add (tID);

		JButton bCheckID = new JButton ("Check ID");
		bCheckID.addActionListener (e -> Core.checkID (tID.getText (), element, true));
		bCheckID.setToolTipText ("Check if the ID is valid and if it isn't already used");
		bCheckID.setMargin (new Insets (2, 5, 2, 5));
		bCheckID.setBounds (297, 0, 77, 23);
		add (bCheckID);

		setVisible (true);
	}


	/**
	 * Gets the ID
	 *
	 * @return the ID
	 */
	public String getID () {
		return tID.getText ();
	}

}
