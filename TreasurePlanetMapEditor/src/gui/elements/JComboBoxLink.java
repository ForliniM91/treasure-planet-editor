package gui.elements;

import java.util.Vector;

import javax.swing.JComboBox;

import data.world.WorldObjectInstance;

/**
 * A JComboBox linked to another JComboBox
 *
 * @author MarcoForlini
 * @param <E> the type of elements
 */
public class JComboBoxLink <E> extends JComboBox <E> {

	private static final long		serialVersionUID	= -7847433684607647173L;

	private JComboBox <WorldObjectInstance>	link;


	/**
	 * Creates a new {@link JComboBoxLink}
	 */
	public JComboBoxLink () {
		super ();
	}

	/**
	 * Creates a new {@link JComboBoxLink}
	 *
	 * @param items the array of elements
	 */
	public JComboBoxLink (E[] items) {
		super (items);
	}

	/**
	 * Creates a new {@link JComboBoxLink}
	 *
	 * @param items the vector of elements
	 */
	public JComboBoxLink (Vector <E> items) {
		super (items);
	}

	/**
	 * Get the linked JComboBox
	 *
	 * @return the linked JComboBox
	 */
	public JComboBox <WorldObjectInstance> getLink () {
		return link;
	}

	/**
	 * Link the given JComboBox
	 *
	 * @param link the JComboBox to link
	 */
	public void setLink (JComboBox <WorldObjectInstance> link) {
		this.link = link;
	}

}
