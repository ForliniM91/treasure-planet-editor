package gui.elements;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;


/**
 * A {@link JList} containing JCheckBox
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("synthetic-access")
public class JCheckBoxList extends JList <JCheckBox> {

	private static final long	serialVersionUID	= 5172611500246384391L;
	private static final Border	noFocusBorder		= new EmptyBorder (1, 1, 1, 1);

	private final JLabel		lNElements;
	private JCheckBox[]			listData;

	/**
	 * Creates a new {@link JCheckBoxList}
	 *
	 * @param lNElements The JLabel to update
	 * @param prefix The prefix for the JLabel
	 * @param listData The array of elements
	 */
	public JCheckBoxList (JLabel lNElements, String prefix, JCheckBox[] listData) {
		this.lNElements = lNElements;
		setListData (listData);
		lNElements.setOpaque (true);
		lNElements.setHorizontalAlignment (SwingConstants.CENTER);
		lNElements.setBackground (Color.LIGHT_GRAY);
		updateLabel (prefix);

		setCellRenderer (new CellRenderer ());
		addMouseListener (new MouseAdapter () {
			@Override
			public void mousePressed (MouseEvent e) {
				if (e.getX () < 20) {
					int index = locationToIndex (e.getPoint ());
					if (index >= 0) {
						JCheckBox selected = getModel ().getElementAt (index);
						selected.doClick ();
						updateLabel (prefix);
						repaint ();
					}
				}
			}
		});
		addKeyListener (new KeyAdapter () {
			@Override
			public void keyPressed (KeyEvent e) {
				if (e.getKeyCode () == KeyEvent.VK_SPACE) {
					JCheckBox selected = getSelectedValue ();
					if (selected != null) {
						selected.doClick ();
						updateLabel (prefix);
						repaint ();
					}
				}
			}
		});

		setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
	}

	@Override
	public void setListData (JCheckBox[] listData) {
		this.listData = listData;
		super.setListData (listData);
	}

	/**
	 * Update the linked label
	 *
	 * @param prefix prefix of the label
	 */
	private void updateLabel (String prefix) {
		lNElements.setText (prefix + ": " + Arrays.stream (listData).parallel ().filter (x -> x.isSelected ()).count ());
	}

	private class CellRenderer implements ListCellRenderer <JCheckBox> {
		@Override
		public Component getListCellRendererComponent (JList <? extends JCheckBox> list, JCheckBox checkBox, int index, boolean isSelected, boolean cellHasFocus) {
			checkBox.setBackground (isSelected ? getSelectionBackground () : getBackground ());
			checkBox.setForeground (isSelected ? getSelectionForeground () : getForeground ());
			checkBox.setEnabled (isEnabled ());
			checkBox.setFont (getFont ());
			checkBox.setFocusPainted (false);
			checkBox.setBorderPainted (true);
			checkBox.setBorder (isSelected ? UIManager.getBorder ("List.focusCellHighlightBorder") : noFocusBorder);
			return checkBox;
		}
	}

}