package gui.elements;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;

import interfaces.EntityInterface;

/**
 * A listener for a double click event in a {@link JList}
 *
 * @author MarcoForlini
 * @param <T> Type of entity
 */
public class DoubleClickList <T extends EntityInterface <? super T>> implements MouseListener {

	private final Runnable handler;

	/**
	 * Creates a new {@link DoubleClickList}
	 *
	 * @param handler Handle the double click event
	 */
	public DoubleClickList (Runnable handler) {
		this.handler = handler;
	}


	@Override
	@SuppressWarnings ("unchecked")
	public void mouseClicked (MouseEvent e) {
		if (e.getClickCount () == 2) {
			JList <T> lsList = (JList <T>) e.getSource ();
			if (lsList.getSelectedIndex () >= 0 && !lsList.getSelectedValue ().isSpecial ()) {
				handler.run ();
			}
		}
	}

	@Override
	public void mousePressed (MouseEvent e) {/* Do nothing */}

	@Override
	public void mouseReleased (MouseEvent e) {/* Do nothing */}

	@Override
	public void mouseEntered (MouseEvent e) {/* Do nothing */}

	@Override
	public void mouseExited (MouseEvent e) {/* Do nothing */}
}