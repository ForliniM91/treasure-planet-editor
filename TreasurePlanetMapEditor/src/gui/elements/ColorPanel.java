package gui.elements;

import java.awt.Color;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import entities.GameColor;

/**
 * A panel which contains a field ID and a button to check it.
 *
 * @author MarcoForlini
 */
public class ColorPanel extends JPanel {

	private static final long	serialVersionUID	= -5889200608756588567L;

	/** Choose color button */
	private final JButton		lChooseColor		= new JButton ("Choose color");

	/**
	 * Creates a new {@link ColorPanel}
	 *
	 * @param name Name to display
	 * @param firstColor The first color
	 * @param defaultColor The default color for the default button
	 * @param hasDefault if true, show the default button, else hide it
	 * @param x The x position
	 * @param y The y position
	 */
	public ColorPanel (String name, Color firstColor, Color defaultColor, boolean hasDefault, int x, int y) {
		setBounds (x, y, 374, 23);
		setLayout (null);

		JLabel lColor = new JLabel (name);
		lColor.setToolTipText ("Click on the button to choose the color");
		lColor.setBounds (0, 4, 59, 14);
		lColor.setLabelFor (lChooseColor);
		add (lColor);

		lChooseColor.setToolTipText ("Click to choose the color");
		lChooseColor.setBackground (firstColor);
		lChooseColor.setForeground (GameColor.getOpposite (firstColor));
		lChooseColor.setOpaque (true);
		lChooseColor.setBounds (69, 1, 131, 20);
		lChooseColor.addActionListener (e -> {
			Color c = JColorChooser.showDialog (this, "Choose a color", lChooseColor.getBackground ());
			if (c != null) {
				lChooseColor.setBackground (c);
				lChooseColor.setForeground (GameColor.getOpposite (c));
			}
		});
		add (lChooseColor);

		JButton bRandom = new JButton ("Random");
		bRandom.setToolTipText ("Assign a random color");
		bRandom.setMargin (new Insets (2, 5, 2, 5));
		bRandom.setBounds (210, 0, 77, 23);
		bRandom.addActionListener (e -> {
			Color c = new GameColor ();
			lChooseColor.setBackground (c);
			lChooseColor.setForeground (GameColor.getOpposite (c));
		});
		add (bRandom);

		if (hasDefault) {
			JButton bDefault = new JButton ("Default");
			bDefault.setToolTipText ("Assign the default color");
			bDefault.setMargin (new Insets (2, 5, 2, 5));
			bDefault.setBounds (297, 0, 77, 23);
			if (defaultColor == null) {
				defaultColor = firstColor;
			}
			final Color defaultColor2 = defaultColor;
			bDefault.addActionListener (e -> {
				lChooseColor.setBackground (defaultColor2);
				lChooseColor.setForeground (GameColor.getOpposite (defaultColor2));
			});
			add (bDefault);
		}

		setVisible (true);
	}

	/**
	 * Get the chosen color
	 *
	 * @return the chosen color
	 */
	public Color getColor () {
		return lChooseColor.getBackground ();
	}

}
