package gui.elements;

import java.awt.Color;

import data.world.ColorableEntity;
import data.world.Entity;
import entities.GameColor;

/**
 * A panel which contains a field ID and a button to check it.
 *
 * @author MarcoForlini
 * @param <T> The {@link Entity}
 */
public class ColorPanelEntity <T extends ColorableEntity> extends ColorPanel {

	private static final long serialVersionUID = -2741655217572200910L;


	/**
	 * Creates a new {@link ColorPanelEntity}
	 *
	 * @param name Name to display
	 * @param element The element
	 * @param lsList The list of elements
	 * @param defaultColor a default color. If not null, it's always the starting color
	 * @param colors The array of colors
	 * @param newElement true if it's a new element
	 * @param x The x position
	 * @param y The y position
	 */
	public ColorPanelEntity (String name, T element, JListEx <T> lsList, Color defaultColor, Color[] colors, boolean newElement, int x, int y) {
		super (name, getDefaultColor (element, lsList, defaultColor, colors), null, true, x, y);
	}


	private static <E extends ColorableEntity> Color getDefaultColor (E element, JListEx <E> lsList, Color defaultColor, Color[] colors) {
		if (defaultColor != null) {
			return defaultColor;
		} else if (element == null) {
			if (colors != null && lsList.getSelectedIndex () < colors.length) {
				return colors[lsList.getLength ()];
			} else {
				return new GameColor ();
			}
		} else if (lsList.getSelectedIndex () < colors.length) {
			return colors[lsList.getSelectedIndex ()];
		} else {
			return lsList.getSelectedValue ().getColor ();
		}
	}

}
