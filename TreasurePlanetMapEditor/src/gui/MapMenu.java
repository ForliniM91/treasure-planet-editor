package gui;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import data.world.Point2D;
import data.world.Settings;


/**
 * A menu to manipulate the map
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class MapMenu extends GenericDialog {

	private Point2D position = new Point2D (0, 0);

	/**
	 * Creates a new {@link MapMenu}
	 *
	 * @param parent the parent window
	 */
	public MapMenu (Window parent) {
		super (parent, "Map editor", 960, 640);
		setLayout (null);

		JScrollPane sMap = new JScrollPane ();
		sMap.setBounds (12, 12, 800, 560);
		sMap.getVerticalScrollBar ().setUnitIncrement (10);
		sMap.getVerticalScrollBar ().setBlockIncrement (100);
		sMap.getHorizontalScrollBar ().setUnitIncrement (10);
		sMap.getHorizontalScrollBar ().setBlockIncrement (100);
		add (sMap);

		JLabel lDesc = new JLabel ("Position:");
		JPanel map = new JPanel ();
		map.addMouseMotionListener ((MouseMotionListeneCursor) (MouseEvent e) -> {
			Point2D position = new Point2D (e.getX (), e.getY ());
			setPosition (position);
			lDesc.setText (position.toString ());
		});
		map.setPreferredSize (new Dimension (Settings.width, Settings.depth));
		sMap.setViewportView (map);

		lDesc.setBounds (512, 572, 300, 16);
		lDesc.setHorizontalAlignment (SwingConstants.RIGHT);
		add (lDesc);

		setVisible (true);
	}


	/**
	 * Returns the position of the cursor
	 *
	 * @return the position of the cursor
	 */
	public Point2D getPosition () {
		return position;
	}

	/**
	 * Sets the position of the cursor
	 *
	 * @param position the new position of the cursor
	 */
	public void setPosition (Point2D position) {
		this.position = position;
	}


	@FunctionalInterface
	private interface MouseMotionListeneCursor extends MouseMotionListener {
		@Override
		default void mouseDragged (MouseEvent e) {/* Do nothing */}
	}

}
