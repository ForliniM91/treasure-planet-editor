package gui;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import data.world.Settings;
import logic.Core;


/**
 * The main menu
 *
 * @author MarcoForlini
 */
@SuppressWarnings ({ "synthetic-access", "serial" })
public class MainMenu extends JFrame implements WindowListener {

	private JTextField			tMapID		= new JTextField ();
	private JComboBox <String>	cbMapName	= new JComboBox<> ();
	private JComboBox <String>	cbMapDesc	= new JComboBox<> ();
	private JTextField			tSeed		= new JTextField ();
	{
		tMapID.getDocument ().addDocumentListener (new DocumentListener () {
			@Override
			public void removeUpdate (DocumentEvent e) {
				Settings.mapID = tMapID.getText ();
			}

			@Override
			public void insertUpdate (DocumentEvent e) {
				Settings.mapID = tMapID.getText ();
			}

			@Override
			public void changedUpdate (DocumentEvent e) {
				Settings.mapID = tMapID.getText ();
			}
		});

		cbMapName.addItemListener (e -> {
			if (e.getStateChange () == ItemEvent.SELECTED) {
				Settings.mapName = e.getItem ().toString ();
			}
		});

		cbMapDesc.addItemListener (e -> {
			if (e.getStateChange () == ItemEvent.SELECTED) {
				Settings.mapDesc = e.getItem ().toString ();
			}
		});

		tSeed.getDocument ().addDocumentListener (new DocumentListener () {
			@Override
			public void removeUpdate (DocumentEvent e) {
				Settings.seed = tSeed.getText ();
			}

			@Override
			public void insertUpdate (DocumentEvent e) {
				Settings.seed = tSeed.getText ();
			}

			@Override
			public void changedUpdate (DocumentEvent e) {
				Settings.seed = tSeed.getText ();
			}
		});
	}




	/**
	 * Creates a new {@link MainMenu}
	 */
	public MainMenu () {
		addWindowListener (this);
		setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);
		setSize (450, 450);
		setLocationRelativeTo (null);
		setLayout (null);
		setResizable (false);

		JLabel lMapID = new JLabel ("Map ID");
		lMapID.setToolTipText ("Type a map identifier");
		lMapID.setBounds (10, 11, 59, 14);
		add (lMapID);

		JLabel lblMapName = new JLabel ("Map name");
		lblMapName.setToolTipText ("Select the string containing the name of the map");
		lblMapName.setBounds (10, 36, 59, 14);
		add (lblMapName);

		JLabel lblMapDesc = new JLabel ("Map desc");
		lblMapDesc.setToolTipText ("Select the string containing the description of the map");
		lblMapDesc.setBounds (10, 61, 59, 14);
		add (lblMapDesc);

		tMapID.setBounds (79, 8, 248, 20);
		tMapID.setColumns (10);
		add (tMapID);

		JButton bCheckID = new JButton ("Check ID");
		bCheckID.setToolTipText ("Check if the map ID is valid and if it isn't already used");
		bCheckID.setMargin (new Insets (2, 5, 2, 5));
		bCheckID.setBounds (337, 7, 97, 23);
		add (bCheckID);

		cbMapName.setModel (new DefaultComboBoxModel<> (Core.mapNames));
		cbMapName.setMaximumRowCount (10);
		cbMapName.setBounds (79, 33, 248, 20);
		add (cbMapName);

		JButton bNewMapName = new JButton ("New name");
		bNewMapName.setToolTipText ("Add a new map name to the list");
		bNewMapName.setMargin (new Insets (2, 5, 2, 5));
		bNewMapName.setBounds (337, 32, 97, 23);
		add (bNewMapName);

		cbMapDesc.setModel (new DefaultComboBoxModel<> (Core.mapDesc));
		cbMapDesc.setMaximumRowCount (10);
		cbMapDesc.setBounds (79, 58, 248, 20);
		add (cbMapDesc);

		JButton bNewMapDesc = new JButton ("New desc");
		bNewMapDesc.setToolTipText ("Add a new map description to the list");
		bNewMapDesc.setMargin (new Insets (2, 5, 2, 5));
		bNewMapDesc.setBounds (337, 57, 97, 23);
		add (bNewMapDesc);

		JLabel lSeed = new JLabel ("Seed");
		lSeed.setToolTipText ("Random seed used in the map");
		lSeed.setBounds (10, 86, 59, 14);
		add (lSeed);

		tSeed.setEditable (false);
		tSeed.setColumns (10);
		tSeed.setBounds (79, 83, 248, 20);
		add (tSeed);

		JButton bRandomSeed = new JButton ("Generate seed");
		bRandomSeed.addActionListener (e -> tSeed.setText ("" + Core.RANDOM.nextInt ()));
		bRandomSeed.setToolTipText ("Generate a random seed");
		bRandomSeed.setMargin (new Insets (2, 5, 2, 5));
		bRandomSeed.setBounds (337, 82, 97, 23);
		add (bRandomSeed);

		JButton bWorldSettings = new JButton ("World settings");
		bWorldSettings.setToolTipText ("Let you define the world settings");
		bWorldSettings.addActionListener (e -> new SettingsMenu (this));
		bWorldSettings.setBounds (10, 119, 424, 30);
		bWorldSettings.setMargin (new Insets (2, 10, 2, 10));
		add (bWorldSettings);

		JLabel lGameSettings = new JLabel ("Game settings");
		lGameSettings.setHorizontalAlignment (SwingConstants.CENTER);
		lGameSettings.setBounds (10, 160, 110, 14);
		add (lGameSettings);

		JButton bTeams = new JButton ("Teams");
		bTeams.addActionListener (e -> new TeamsMenu (this));
		bTeams.setToolTipText ("Define the teams for the map");
		bTeams.setBounds (10, 182, 110, 30);
		add (bTeams);

		JButton bPlayers = new JButton ("Players");
		bPlayers.addActionListener (e -> new PlayersMenu (this));
		bPlayers.setToolTipText ("Define the players for the map");
		bPlayers.setBounds (10, 214, 110, 30);
		add (bPlayers);

		JButton bAlliances = new JButton ("Alliances");
		bAlliances.setToolTipText ("Define the alliances for players");
		bAlliances.addActionListener (e -> new AlliancesMenu (this));
		bAlliances.setBounds (10, 246, 110, 30);
		add (bAlliances);

		JButton bEquipment = new JButton ("Equipment");
		bEquipment.addActionListener (e -> new EquipmentMenu (this));
		bEquipment.setToolTipText ("Define the available equipment for the fleet");
		bEquipment.setBounds (10, 278, 110, 30);
		add (bEquipment);

		JLabel lEvents = new JLabel ("Manage events");
		lEvents.setHorizontalAlignment (SwingConstants.CENTER);
		lEvents.setBounds (324, 160, 110, 14);
		add (lEvents);

		JButton bFlags = new JButton ("Flags");
		bFlags.addActionListener (e -> new FlagsMenu (this));
		bFlags.setToolTipText ("Define flags (i.e. boolean variables) for triggers");
		bFlags.setBounds (324, 182, 110, 30);
		add (bFlags);

		JButton bTimers = new JButton ("Timers");
		bTimers.addActionListener (e -> new TimersMenu (this));
		bTimers.setToolTipText ("Define timers for triggers");
		bTimers.setBounds (324, 214, 110, 30);
		add (bTimers);

		JButton bDialogs = new JButton ("Dialogs");
		bDialogs.addActionListener (e -> new DialogsMenu (this));
		bDialogs.setToolTipText ("Define dialogs");
		bDialogs.setBounds (324, 246, 110, 30);
		add (bDialogs);

		JLabel lBuildMap = new JLabel ("Build Map");
		lBuildMap.setHorizontalAlignment (SwingConstants.CENTER);
		lBuildMap.setBounds (167, 160, 110, 14);
		add (lBuildMap);

		JButton bMap = new JButton ("Map");
		bMap.setToolTipText ("Place ships, islands, asteroids, solar storms...");
		bMap.addActionListener (e -> new MapMenu (this));
		bMap.setBounds (167, 182, 110, 30);
		add (bMap);

		JButton bStarMap = new JButton ("Starmap");
		bStarMap.setToolTipText ("Manage the starmap");
		bStarMap.addActionListener (e -> new StarMapMenu (this));
		bStarMap.setBounds (167, 214, 110, 30);
		add (bStarMap);

		JButton bObjectives = new JButton ("Objectives");
		bObjectives.addActionListener (e -> new ObjectivesMenu (this));
		bObjectives.setToolTipText ("Define objectives");
		bObjectives.setBounds (167, 246, 110, 30);
		add (bObjectives);

		JButton bJournal = new JButton ("Journal");
		// bJournal.addActionListener (e -> new JournalMenu(this));
		bJournal.setToolTipText ("Define the journal entries");
		bJournal.setBounds (167, 278, 110, 30);
		add (bJournal);

		JButton bTriggers = new JButton ("Triggers");
		bTriggers.addActionListener (e -> new TriggersMenu (this));
		bTriggers.setToolTipText ("Manage the triggers as Conditions->Actions.");
		bTriggers.setBounds (324, 278, 110, 30);
		add (bTriggers);

		JButton bHelp = new JButton ("HELP");
		// bHelp.addActionListener (e -> goToMenu (Core.helpMenu));
		bHelp.setMargin (new Insets (2, 5, 2, 5));
		bHelp.setFont (new Font ("Tahoma", Font.PLAIN, 30));
		bHelp.setBounds (10, 350, 110, 60);
		add (bHelp);

		setVisible (true);
	}



	@Override
	public void windowOpened (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowClosing (WindowEvent e) {
		/*
		 * if (JOptionPane.showConfirmDialog(getFrame(), "Close the editor?", "Exit", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) != 0){
		 * return;
		 * }
		 * setDefaultCloseOperation(JEXIT_ON_CLOSE);
		 */
		dispose ();
	}

	@Override
	public void windowClosed (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowIconified (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowDeiconified (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowActivated (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowDeactivated (WindowEvent e) {/* Do nothing */}
}
