package gui;

import java.awt.Dimension;
import java.awt.Window;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import data.world.Action;
import entities.ActionCategory;
import entities.ActionType;
import entities.Field;
import entities.VarType;
import logic.Core;

/**
 * A menu to manipulate the actions
 *
 * @author MarcoForlini
 */
@SuppressWarnings ({ "synthetic-access", "serial" })
public class ActionsMenu extends TriggerSubMenu <ActionType, ActionCategory, Action> {

	private final JPanel						pnFields		= new JPanel ();
	private final JScrollPane					spScroll		= new JScrollPane ();
	private final JComboBox <ActionCategory>	cbCategories	= new JComboBox<> ();
	private final JComboBox <ActionType>		cbTypes			= new JComboBox<> ();
	private final JTextArea						lDescription	= new JTextArea ("");


	/**
	 * Creates a new {@link ActionsMenu}
	 *
	 * @param parent the parent window
	 */
	public ActionsMenu (Window parent) {
		super (parent, "action", 19);
		initialize (false);
		buildMenu ((ActionType) cbTypes.getSelectedItem ());
		pnFields.setPreferredSize (null);
		setVisible (true);
	}

	/**
	 * Creates a new {@link ActionsMenu}
	 *
	 * @param parent the parent window
	 * @param action the action
	 */
	public ActionsMenu (Window parent, Action action) {
		super (parent, "action", 19, action);

		initialize (true);
		int i = buildMenu (action);
		if (i > 0) {
			pnFields.setPreferredSize (new Dimension (724, jLabels[i - 1].getY () + 25));
		} else {
			pnFields.setPreferredSize (null);
		}
		setVisible (true);
	}


	@Override
	public void initialize (boolean newElement) {
		spScroll.getVerticalScrollBar ().setUnitIncrement (5);
		spScroll.setHorizontalScrollBarPolicy (ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spScroll.setBounds (10, 61, 724, 312);
		add (spScroll);

		pnFields.setLayout (null);
		spScroll.setViewportView (pnFields);

		JLabel lCategory = new JLabel ("Category");
		lCategory.setToolTipText ("Select the category for the action");
		lCategory.setBounds (10, 11, 175, 14);
		add (lCategory);

		cbCategories.setToolTipText ("Select the category for the action");
		cbCategories.setModel (new DefaultComboBoxModel<> (ActionCategory.values ()));
		cbCategories.setBounds (195, 8, 519, 20);
		cbCategories.addPopupMenuListener (new PopupMenuListenerCategory ());
		add (cbCategories);

		JLabel lAction = new JLabel ("Action");
		lAction.setToolTipText ("Select the action");
		lAction.setBounds (10, 36, 175, 14);
		add (lAction);
		add (cbTypes);

		cbTypes.addPopupMenuListener (new PopupMenuListenerAction ());
		cbTypes.setToolTipText ("Select the action");
		cbTypes.setModel (new DefaultComboBoxModel<> (ActionCategory.values ()[0].getTypes ()));
		cbTypes.setMaximumRowCount (20);
		cbTypes.setBounds (195, 33, 519, 20);

		for (int i = 0; i < jLabels.length; i++) {
			jLabels[i] = new JLabel ("NONE " + i);
			jLabels[i].setBounds (10, 11 + (i * 25), 175, 14);
			jLabels[i].setVisible (false);
			getMainPanel ().add (jLabels[i]);
		}

		lDescription.setLineWrap (true);
		lDescription.setWrapStyleWord (true);
		lDescription.setEditable (false);
		lDescription.setBorder (new BevelBorder (BevelBorder.LOWERED, null, null, null, null));
		lDescription.setToolTipText ("Description for the selected action");
		lDescription.setBounds (10, 384, 704, 60);
		add (lDescription);

		JButton bSave = new JButton ("Save");
		bSave.addActionListener (e -> onSaveEvent ());
		bSave.setToolTipText ("Save");
		bSave.setBounds (10, 455, 150, 23);
		add (bSave);

		JButton bCancel = new JButton ("Cancel");
		bCancel.addActionListener (e -> dispose ());
		bCancel.setToolTipText ("Cancel the current operation");
		bCancel.setBounds (170, 455, 150, 23);
		add (bCancel);

		JButton bReset = new JButton ("Reset");
		bReset.addActionListener (e -> {
			cbCategories.setSelectedItem (0);
			cbTypes.setModel (new DefaultComboBoxModel<> (((ActionCategory) cbCategories.getSelectedItem ()).getTypes ()));
			int i = buildMenu ((ActionType) cbTypes.getSelectedItem ());
			pnFields.setPreferredSize (new Dimension (574, jLabels[i - 1].getY () + 25));
		});
		bReset.setToolTipText ("Restore all fields to their default value");
		bReset.setBounds (564, 455, 150, 23);
		add (bReset);
	}


	@Override
	protected JPanel getMainPanel () {
		return pnFields;
	}

	@Override
	protected JTextArea getDescription () {
		return lDescription;
	}

	@Override
	public void setComboBox (ActionCategory category, ActionType type) {
		if (cbCategories.getSelectedItem () != category) {
			cbCategories.setSelectedItem (category);
		}
		if (cbTypes.getSelectedItem () != type) {
			cbTypes.setSelectedItem (type);
		}
	}


	@Override
	public int buildMenu (Action element) {
		SETCOMBO: {
			for (ActionCategory category : ActionCategory.values ()) {
				ActionType[] types = category.getTypes ();
				if (types == null) {
					continue;
				}

				for (ActionType type : types) {
					if (type == element.getElementType ()) {
						setComboBox (category, type);
						break SETCOMBO;
					}
				}
			}
			throw new InternalError ();
		}
		return super.buildMenu (element);
	}




	private void onSaveEvent () {
		Field[] fields = ((ActionType) cbTypes.getSelectedItem ()).getFields ();
		Object[] values;
		if (fields.length > 0) {
			values = new Object[fields.length];
			for (int i = 0; i < fields.length; i++) {
				if (jFields[i] instanceof JTextField) {
					if (fields[i].getUsedDomain () == VarType.INT && Core.getInt ((JTextField) jFields[i]) != null) {
						JOptionPane.showMessageDialog (this, "You must type an integer value for the field \"" + fields[i].getName () + '\"', "Missing/wrong values", JOptionPane.ERROR_MESSAGE);
						return;
					} else if (fields[i].getUsedDomain () != VarType.INT && Core.getFloat ((JTextField) jFields[i]) != null) {
						JOptionPane.showMessageDialog (this, "You must type a value for the field \"" + fields[i].getName () + '\"', "Missing/wrong values", JOptionPane.ERROR_MESSAGE);
						return;
					}
					values[i] = ((JTextField) jFields[i]).getText ();
				} else if (jFields[i] instanceof JComboBox) {
					if (((JComboBox <?>) jFields[i]).getSelectedIndex () == -1) {
						JOptionPane.showMessageDialog (this, "You must choice a value for the field \"" + fields[i].getName () + "\""
								+ "\nIf there are no choices in the list, then the problem is one of these:\n1)You have not created any item/entity for this list\n2)The editor failed in reading the game data (admin priviledges or similar). Post the problem on the download page and wait for the fix\n3)The programmer sucks. Post the problem and the insults on the download page and pray for the fix (if I'm able to fix it...)\n4)Your operating system or game hate you. Post the problem and the insults on the Microsoft/Apple/Disney forums and get banned.", "Missing/wrong values", JOptionPane.ERROR_MESSAGE);
						return;
					}
					values[i] = ((JComboBox <?>) jFields[i]).getSelectedItem ();
				} else {
					values[i] = ((JCheckBox) jFields[i]).isSelected ();
				}

			}
		} else {
			values = null;
		}

		if (element == null) {
			element = new Action ((ActionType) cbTypes.getSelectedItem (), values);
		} else {
			element.setValues ((ActionType) cbTypes.getSelectedItem (), values);
		}
		dispose ();
	}



	private class PopupMenuListenerCategory implements PopupMenuListener {
		@Override
		public void popupMenuWillBecomeInvisible (PopupMenuEvent e) {
			if (rebuild) {
				cbTypes.setModel (new DefaultComboBoxModel<> (((ActionCategory) cbCategories.getSelectedItem ()).getTypes ()));
				int i = buildMenu ((ActionType) cbTypes.getSelectedItem ());
				if (i > 0) {
					pnFields.setPreferredSize (new Dimension (724, jLabels[i - 1].getY () + 25));
				} else {
					pnFields.setPreferredSize (null);
				}
			}
		}

		@Override
		public void popupMenuWillBecomeVisible (PopupMenuEvent e) {
			rebuild = true;
		}

		@Override
		public void popupMenuCanceled (PopupMenuEvent e) {
			rebuild = false;
		}
	}

	private class PopupMenuListenerAction implements PopupMenuListener {
		@Override
		public void popupMenuWillBecomeInvisible (PopupMenuEvent e) {
			if (rebuild) {
				int i = buildMenu ((ActionType) cbTypes.getSelectedItem ());
				if (i > 0) {
					pnFields.setPreferredSize (new Dimension (724, jLabels[i - 1].getY () + 25));
				} else {
					pnFields.setPreferredSize (null);
				}
			}
		}

		@Override
		public void popupMenuWillBecomeVisible (PopupMenuEvent e) {
			rebuild = true;
		}

		@Override
		public void popupMenuCanceled (PopupMenuEvent e) {
			rebuild = false;
		}
	}

}
