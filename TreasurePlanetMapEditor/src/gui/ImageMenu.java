package gui;

import java.awt.Color;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import gui.elements.JImage;
import logic.Core;

/**
 * A menu to view images
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class ImageMenu extends GenericDialog {

	private final JPanel			pnList;
	private final JImage			jImage;
	private final JList <String>	lsImageList;
	private String					path	= null;
	private String[]				images	= null;

	/**
	 * Creates a new {@link ImageMenu}
	 *
	 * @param parent The parent window
	 * @param images The array of paths for the images
	 */
	public ImageMenu (Window parent, String[] images) {
		super (parent, "Image viewer", 497, 300);

		pnList = new JPanel ();
		pnList.setBounds (10, 11, 471, 249);
		pnList.setLayout (null);
		add (pnList);

		JScrollPane scrollPane = new JScrollPane ();
		scrollPane.setBounds (0, 0, 224, 208);
		pnList.add (scrollPane);

		JLabel lNElements = new JLabel ("Images: 0");
		lNElements.setOpaque (true);
		lNElements.setHorizontalAlignment (SwingConstants.CENTER);
		lNElements.setBackground (Color.LIGHT_GRAY);
		scrollPane.setColumnHeaderView (lNElements);

		lsImageList = new JList<> ();
		lsImageList.addListSelectionListener (e -> setSelectedImage (lsImageList.getSelectedValue ()));
		lsImageList.addPropertyChangeListener (evt -> updateLabel (lNElements));
		lsImageList.setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
		lsImageList.setToolTipText ("Select an image");
		lsImageList.setVisibleRowCount (10);
		scrollPane.setViewportView (lsImageList);

		jImage = new JImage (null);
		jImage.setBounds (234, 11, 225, 225);
		pnList.add (jImage);

		JButton bDone = new JButton ("Done");
		bDone.setBounds (10, 214, 204, 23);
		pnList.add (bDone);
		bDone.setToolTipText ("Close this menu");
		bDone.setMargin (new Insets (2, 5, 2, 5));

		setVisible (true);
	}


	private void setSelectedImage (String image) {
		jImage.image = Core.getImage (path, image);
	}

	/**
	 * Returns the selected image
	 *
	 * @return the selected image
	 */
	public Image getSelectedImage () {
		return jImage.image;
	}


	private void updateLabel (JLabel lNElements) {
		lNElements.setText ("Images: " + images.length);
	}

	/**
	 * Sets the path with the images
	 *
	 * @param path The path
	 * @param images The images names
	 */
	public void setImages (String path, String[] images) {
		this.images = images;
	}

}
