package gui;

import java.awt.Window;

import data.world.Player;
import gui.editors.PlayersMenuEditor;

/**
 * A menu to manipulate the players
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class PlayersMenu extends EntriesListMenu <Player> {

	/**
	 * Creates a new {@link PlayersMenu}
	 *
	 * @param parent The parent window
	 */
	public PlayersMenu (Window parent) {
		super (parent, "player", Player.players, true);
	}


	@Override
	public Player newElementEvent () {
		PlayersMenuEditor editor = new PlayersMenuEditor (this);
		return editor.element;
	}

	@Override
	public boolean editElementEvent (Player player) {
		new PlayersMenuEditor (this, player);
		return true;
	}

}
