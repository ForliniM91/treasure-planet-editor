package gui;

import java.awt.Color;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import gui.elements.DoubleClickList;
import gui.elements.JListEx;
import interfaces.EntityInterface;
import logic.Core;

/**
 * A window which show a list of elements and the commands to manipulate them
 *
 * @author MarcoForlini
 * @param <T> Type of elements
 */
@SuppressWarnings ("serial")
public abstract class EntriesListMenu <T extends EntityInterface <? super T>> extends GenericDialog {

	private static ImageIcon	ICON_MOVE_UP	= new ImageIcon (TeamsMenu.class.getResource ("/javax/swing/plaf/metal/icons/sortUp.png"));
	private static ImageIcon	ICON_MOVE_DOWN	= new ImageIcon (TeamsMenu.class.getResource ("/javax/swing/plaf/metal/icons/sortDown.png"));

	/** List of elements */
	public final JListEx <T>	lsList;

	/** Name of the elements */
	public final String			elemName;



	/**
	 * Creates a new {@link EntriesListMenu}
	 *
	 * @param parent The parent window
	 * @param elemName Name of the element
	 * @param elements List of elements
	 * @param allowDuplicate <code>true</code> to allow the duplicate button, <code>false</code> otherwise
	 * @wbp.parser.constructor
	 */
	public EntriesListMenu (Window parent, String elemName, Vector <T> elements, boolean allowDuplicate) {
		this (parent, elemName, elements, allowDuplicate, 420, 300);
	}

	/**
	 * Creates a new {@link EntriesListMenu}
	 *
	 * @param parent The parent window
	 * @param elemName Name of the element
	 * @param elements List of elements
	 * @param allowDuplicate <code>true</code> to allow the duplicate button, <code>false</code> otherwise
	 * @param width the width of the window
	 * @param height the height of the window
	 */
	@SuppressWarnings ("unchecked")
	public EntriesListMenu (Window parent, String elemName, Vector <T> elements, boolean allowDuplicate, int width, int height) {
		super (parent, Core.firstUpper (elemName), width, height);
		this.elemName = elemName;

		setLayout (null);

		JScrollPane spList = new JScrollPane ();
		spList.setBounds (10, 10, width - 196, height - 51);
		add (spList);

		JLabel lNElements = new JLabel ();
		lNElements.setBackground (Color.LIGHT_GRAY);
		lNElements.setOpaque (true);
		lNElements.setHorizontalAlignment (SwingConstants.CENTER);
		spList.setColumnHeaderView (lNElements);

		lsList = new JListEx<> (elements);
		lsList.addPropertyChangeListener (evt -> lNElements.setText (Core.firstUpper (elemName) + "s: " + lsList.getLength ()));
		lsList.addMouseListener (new DoubleClickList<> (this::editElementEvent));
		lsList.setToolTipText ("Select an element in the list");
		spList.setViewportView (lsList);

		JButton bNew = new JButton ("New " + elemName);
		bNew.setToolTipText ("Create a new " + elemName);
		bNew.setBounds (width - 176, 21, 150, 23);
		bNew.addActionListener (e -> {
			T newElement = newElementEvent ();
			if (newElement != null) {
				lsList.add (newElement);
			}
			lNElements.setText (Core.firstUpper (elemName) + "s: " + lsList.getLength ());
		});
		add (bNew);

		JButton bEdit = new JButton ("Edit " + elemName);
		bEdit.setToolTipText ("Edit the selected " + elemName);
		bEdit.setBounds (width - 176, 46, 150, 23);
		bEdit.addActionListener (e -> {
			T element = lsList.getSelectedValue ();
			if (element == null) {
				JOptionPane.showMessageDialog (this, "Select an element in the list, then click edit", "How to edit an element", JOptionPane.INFORMATION_MESSAGE);
			} else if (editElementEvent (element)) {
				SwingUtilities.invokeLater (lsList::repaint);
			}
		});
		add (bEdit);

		int posY = 71;
		if (allowDuplicate) {
			JButton bDuplicate = new JButton ("Duplicate " + elemName);
			bDuplicate.setToolTipText ("Duplicate the selected " + elemName);
			bDuplicate.setBounds (width - 176, 71, 150, 23);
			bDuplicate.addActionListener (e -> {
				T element = lsList.getSelectedValue ();
				if (element == null) {
					JOptionPane.showMessageDialog (this, "Select an element in the list, then click the duplicate button", "How to duplicate an element", JOptionPane.INFORMATION_MESSAGE);
				} else if (element.isSpecial ()) {
					JOptionPane.showMessageDialog (this, "You can't duplicate this special element!", "Don't touch!", JOptionPane.ERROR_MESSAGE);
				} else {
					element = (T) element.clone ();
					if (element == null) {
						JOptionPane.showMessageDialog (this, "The maximum index (64) has been reached for the duplicates. Rename them, then retry", "How to duplicate an element", JOptionPane.WARNING_MESSAGE);
					} else {
						lsList.add (element, lsList.getSelectedIndex () + 1);
					}
				}
			});
			add (bDuplicate);
			posY += 25;
		}

		JButton bDelete = new JButton ("Delete " + elemName);
		bDelete.setBounds (width - 176, posY, 150, 23);
		bDelete.setMargin (new Insets (2, 5, 2, 5));
		bDelete.setToolTipText ("Delete the selected element from the list");
		bDelete.addActionListener (e -> {
			T element = lsList.getSelectedValue ();
			if (element == null) {
				JOptionPane.showMessageDialog (this, "Select an element in the list, then click delete", "How to delete an element", JOptionPane.INFORMATION_MESSAGE);
			} else if (element.isSpecial ()) {
				JOptionPane.showMessageDialog (this, "You can't delete this special element!", "Don't touch!", JOptionPane.ERROR_MESSAGE);
			} else {
				deleteElementEvent (e, element);
			}
		});
		add (bDelete);

		JButton bListUp = new JButton ("");
		bListUp.setToolTipText ("Move up the selected element");
		bListUp.addActionListener (e -> {
			T element = lsList.getSelectedValue ();
			if (element == null) {
				JOptionPane.showMessageDialog (this, "Select an element in the list, then click the up arrow", "How to move up an element", JOptionPane.INFORMATION_MESSAGE);
			} else if (element.isSpecial ()) {
				JOptionPane.showMessageDialog (this, "You can't move this special element!", "Don't touch!", JOptionPane.ERROR_MESSAGE);
			} else {
				int selected = lsList.getSelectedIndex ();
				if (selected > 0 && !lsList.get (selected - 1).isSpecial ()) {
					lsList.moveUp (selected);
					lsList.setSelectedIndex (selected - 1);
				}
			}
		});
		bListUp.setIcon (ICON_MOVE_UP);
		bListUp.setBounds (width - 176, 202, 30, 23);
		add (bListUp);

		JButton bListDown = new JButton ("");
		bListDown.setToolTipText ("Move down the selected element");
		bListDown.addActionListener (e -> {
			T element = lsList.getSelectedValue ();
			if (element == null) {
				JOptionPane.showMessageDialog (this, "Select an element in the list, then click the down arrow", "How to move down an element", JOptionPane.INFORMATION_MESSAGE);
			} else if (element.isSpecial ()) {
				JOptionPane.showMessageDialog (this, "You can't move this special element!", "Don't touch!", JOptionPane.ERROR_MESSAGE);
			} else {
				int selected = lsList.getSelectedIndex ();
				if (selected < lsList.getLength () - 1 && !lsList.get (selected + 1).isSpecial ()) {
					lsList.moveDown (selected);
					lsList.setSelectedIndex (selected + 1);
				}
			}
		});
		bListDown.setIcon (ICON_MOVE_DOWN);
		bListDown.setBounds (width - 176, 225, 30, 23);
		add (bListDown);

		setVisible (true);
	}


	/**
	 * Creates and return a new element
	 *
	 * @return the new element
	 */
	public abstract T newElementEvent ();

	/**
	 * Edit the given element
	 *
	 * @param element the element to edit
	 * @return <code>true</code> if succeed, <code>false</code> otherwise
	 */
	public abstract boolean editElementEvent (T element);

	private boolean editElementEvent () {
		return editElementEvent (lsList.getSelectedElement ());
	}

	/**
	 * Delete the given element
	 *
	 * @param e The event
	 * @param element The element to delete
	 * @return <code>true</code> if succeed, <code>false</code> otherwise
	 */
	public boolean deleteElementEvent (ActionEvent e, T element) {
		if ((e.getModifiers () & ActionEvent.CTRL_MASK) != 0 ||
				JOptionPane.showConfirmDialog (this, "Delete " + element + "?", "Confirm",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
			lsList.erase (element);
			return true;
		}
		return false;
	}

	/**
	 * Gets the list of elements
	 *
	 * @return the list of elements
	 */
	public JListEx <T> getList () {
		return lsList;
	}

}
