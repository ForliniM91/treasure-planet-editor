package gui;

import java.awt.Window;

import data.world.Alliance;
import gui.editors.AlliancesMenuEditor;
import logic.Core;

/**
 * A menu to manipulate the alliances
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class AlliancesMenu extends EntriesListMenu <Alliance> {

	/**
	 * Creates a new {@link TeamsMenu}
	 *
	 * @param parent The parent window
	 */
	public AlliancesMenu (Window parent) {
		super (parent, "alliance", Alliance.alliances, false);
	}

	@Override
	public Alliance newElementEvent () {
		try {
			AlliancesMenuEditor editor = new AlliancesMenuEditor (this);
			return editor.element;
		} catch (IllegalStateException e) {
			Core.printError (this, e.getMessage (), "Error");
			return null;
		}
	}

	@Override
	public boolean editElementEvent (Alliance element) {
		new AlliancesMenuEditor (this, element);
		return true;
	}

}
