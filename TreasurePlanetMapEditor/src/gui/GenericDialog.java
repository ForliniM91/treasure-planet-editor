package gui;

import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JDialog;
import javax.swing.WindowConstants;

import logic.Core;

/**
 * A JDialog with some default values and settings
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public abstract class GenericDialog extends JDialog implements WindowListener {

	/**
	 * Creates a new {@link GenericDialog}
	 *
	 * @param parent The parent window
	 * @param title The title
	 * @param width The width
	 * @param height The height
	 */
	public GenericDialog (Window parent, String title, int width, int height) {
		super (parent, ModalityType.DOCUMENT_MODAL);
		setTitle (Core.firstUpper (title));
		setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);
		setSize (width, height);
		setLocationRelativeTo (parent);
		setResizable (false);
		addWindowListener (this);
	}


	@Override
	public void windowOpened (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowClosing (WindowEvent e) {
		dispose ();
	}

	@Override
	public void windowClosed (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowIconified (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowDeiconified (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowActivated (WindowEvent e) {/* Do nothing */}

	@Override
	public void windowDeactivated (WindowEvent e) {/* Do nothing */}

}
