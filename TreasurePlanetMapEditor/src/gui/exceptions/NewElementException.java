package gui.exceptions;

/**
 * An exception thrown when creating an object
 *
 * @author MarcoForlini
 */
public class NewElementException extends IllegalStateException {

	private static final long serialVersionUID = 7704167911407832313L;


	/**
	 * Constructs an {@link NewElementException} with no detail message.
	 * A detail message is a String that describes this particular exception.
	 */
	public NewElementException () {
		super ();
	}

	/**
	 * Constructs an {@link NewElementException} with the specified detail
	 * message. A detail message is a String that describes this particular
	 * exception.
	 *
	 * @param s the String that contains a detailed message
	 */
	public NewElementException (String s) {
		super (s);
	}

	/**
	 * Constructs a new exception with the specified detail message and
	 * cause.
	 * <p>
	 * Note that the detail message associated with <code>cause</code> is
	 * <i>not</i> automatically incorporated in this exception's detail
	 * message.
	 *
	 * @param message the detail message (which is saved for later retrieval
	 *            by the {@link Throwable#getMessage()} method).
	 * @param cause the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). (A <tt>null</tt> value
	 *            is permitted, and indicates that the cause is nonexistent or
	 *            unknown.)
	 * @since 1.5
	 */
	public NewElementException (String message, Throwable cause) {
		super (message, cause);
	}

	/**
	 * Constructs a new exception with the specified cause and a detail
	 * message of <tt>(cause==null ? null : cause.toString())</tt> (which
	 * typically contains the class and detail message of <tt>cause</tt>).
	 * This constructor is useful for exceptions that are little more than
	 * wrappers for other throwables (for example, {@link
	 * java.security.PrivilegedActionException}).
	 *
	 * @param cause the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). (A <tt>null</tt> value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.)
	 * @since 1.5
	 */
	public NewElementException (Throwable cause) {
		super (cause);
	}

}
