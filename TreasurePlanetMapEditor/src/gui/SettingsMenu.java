package gui;

import java.awt.Window;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import data.world.Settings;
import gui.elements.ColorPanel;
import logic.Core;


/**
 * A menu to manipulate the map settings
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class SettingsMenu extends GenericDialog {

	private static final String			hystoricalWarning		= "So this is an \"Hystorical map\".\n"
			+ "Maybe you noticed: in hystorical maps you can't customize your fleet.\n"
			+ "This is how the game works, it's not my fault and I can't fix it.\n"
			+ "You simply start with a fixed fleet and play; that fleet is defined in the map itself.\n"
			+ "This means that, in hystorical maps, you must manually assign a fleet to everybody.\n\nDON'T FORGET!!\nAssign the fleet in the \"Map section\", else players will start without ships and the map will be unplayable!";

	private final JTextField			tWidth					= new JTextField (Integer.toString (Settings.width));
	private final JTextField			tDepth					= new JTextField (Integer.toString (Settings.depth));
	private final JTextField			tHeight					= new JTextField (Integer.toString (Settings.height));
	private final JTextField			tWorldBufferSize		= new JTextField (Integer.toString (Settings.worldBufferSize));
	private final JTextField			tX						= new JTextField (Float.toString (Settings.x));
	private final JTextField			tY						= new JTextField (Float.toString (Settings.y));
	private final JTextField			tZ						= new JTextField (Float.toString (Settings.z));
	private final JComboBox <String>	cbBackground			= new JComboBox<> ();
	private final JComboBox <String>	cbStarmap				= new JComboBox<> ();
	private final ColorPanel			cpAmbientColor			= new ColorPanel ("Ambient color", Settings.ambientColor, null, false, 8, 80);
	private final ColorPanel			cpRoofColor				= new ColorPanel ("Roof color", Settings.roofColor, null, false, 8, 111);
	private final ColorPanel			cpFloorColor			= new ColorPanel ("Floor color", Settings.floorColor, null, false, 8, 142);
	private final ButtonGroup			buttonGroup				= new ButtonGroup ();
	private final JRadioButton			chCampaign				= new JRadioButton ("Campaign map", Settings.isCampaign);
	private final JRadioButton			chSkirmish				= new JRadioButton ("Skirmish map", Settings.isSkirmish);
	private final JRadioButton			chHystorical			= new JRadioButton ("Hystorical map", Settings.isHystorical);
	private final JCheckBox				ckLastMission			= new JCheckBox ("Last mission", Settings.isLastMission);
	private final JCheckBox				ckAllianceChangeAllowed	= new JCheckBox ("Alliance change allowed", Settings.isAllianceChangeAllowed);
	private final JCheckBox				ckIslandsMakeSounds		= new JCheckBox ("Islands make sounds", Settings.isIslandsMakeSounds);


	/**
	 * Creates a new {@link SettingsMenu}
	 *
	 * @param parent The parend window
	 */
	public SettingsMenu (Window parent) {
		super (parent, "World settings", 450, 450);
		setLayout (null);

		JLabel lWorldsize = new JLabel ("World Size");
		lWorldsize.setToolTipText ("Size of the world");
		lWorldsize.setBounds (8, 12, 61, 16);
		lWorldsize.setLabelFor (tWidth);
		add (lWorldsize);

		JLabel lWidth = new JLabel ("Width");
		lWidth.setToolTipText ("Width size");
		lWidth.setBounds (154, 12, 33, 16);
		lWidth.setLabelFor (tWidth);
		add (lWidth);

		tWidth.setToolTipText ("Width size");
		tWidth.setBounds (99, 10, 50, 20);
		tWidth.setColumns (4);
		add (tWidth);

		JLabel lDepth = new JLabel ("Depth");
		lDepth.setLabelFor (tDepth);
		lDepth.setToolTipText ("");
		lDepth.setBounds (260, 12, 33, 16);
		add (lDepth);

		tDepth.setToolTipText ("Depth size");
		tDepth.setColumns (4);
		tDepth.setBounds (205, 10, 50, 20);
		add (tDepth);

		JLabel lHeight = new JLabel ("Height");
		lHeight.setToolTipText ("Height size");
		lHeight.setBounds (365, 12, 36, 16);
		lHeight.setLabelFor (tHeight);
		add (lHeight);

		tHeight.setToolTipText ("Height size");
		tHeight.setColumns (4);
		tHeight.setBounds (311, 10, 50, 20);
		add (tHeight);

		JLabel lblWorldBufferSize = new JLabel ("World buffer");
		lblWorldBufferSize.setToolTipText ("Size of the buffer for this map");
		lblWorldBufferSize.setBounds (8, 40, 97, 16);
		lblWorldBufferSize.setLabelFor (tWorldBufferSize);
		add (lblWorldBufferSize);

		tWorldBufferSize.setToolTipText ("Size of the buffer for this map");
		tWorldBufferSize.setBounds (98, 38, 51, 20);
		tWorldBufferSize.setColumns (10);
		add (tWorldBufferSize);


		JLabel lLight = new JLabel ("Light orientat.");
		lLight.setToolTipText ("Light orientation from the roof");
		lLight.setBounds (8, 174, 92, 16);
		lLight.setLabelFor (tX);
		add (lLight);

		JLabel lX = new JLabel ("X");
		lX.setToolTipText ("X direction");
		lX.setBounds (154, 174, 8, 16);
		lX.setLabelFor (tX);
		add (lX);

		tX.setToolTipText ("X direction");
		tX.setColumns (8);
		tX.setBounds (99, 172, 50, 20);
		add (tX);

		JLabel lY = new JLabel ("Y");
		lY.setToolTipText ("Y direction");
		lY.setBounds (260, 174, 8, 16);
		lY.setLabelFor (tY);
		add (lY);

		tY.setToolTipText ("Y direction");
		tY.setColumns (8);
		tY.setBounds (205, 172, 50, 20);
		add (tY);

		JLabel lZ = new JLabel ("Z");
		lZ.setLabelFor (tZ);
		lZ.setToolTipText ("Z direction");
		lZ.setBounds (365, 174, 8, 16);
		add (lZ);

		tZ.setToolTipText ("Z direction");
		tZ.setColumns (8);
		tZ.setBounds (311, 172, 50, 20);
		add (tZ);

		add (cpAmbientColor);
		add (cpRoofColor);
		add (cpFloorColor);


		JLabel lBackground = new JLabel ("Background");
		lBackground.setToolTipText ("Set the space background image for this map");
		lBackground.setBounds (8, 205, 79, 16);
		lBackground.setLabelFor (cbBackground);
		add (lBackground);

		cbBackground.setToolTipText ("Set the space background image for this map");
		cbBackground.setModel (new DefaultComboBoxModel<> (Core.backgrounds));
		cbBackground.setBounds (97, 202, 302, 23);
		cbBackground.setSelectedItem (Settings.background);
		add (cbBackground);

		JLabel lStarMap = new JLabel ("Star map");
		lStarMap.setToolTipText ("Set the starmap image");
		lStarMap.setBounds (10, 255, 62, 16);
		lStarMap.setLabelFor (cbStarmap);
		add (lStarMap);

		cbStarmap.setToolTipText ("Set the starmap image");
		cbStarmap.setModel (new DefaultComboBoxModel<> (Core.starmaps));
		cbStarmap.setBounds (97, 252, 302, 23);
		cbStarmap.setSelectedItem (Settings.starMap);
		add (cbStarmap);


		chCampaign.addItemListener (e -> ckLastMission.setEnabled (chCampaign.isSelected ()));
		chCampaign.setBounds (8, 283, 110, 20);
		buttonGroup.add (chCampaign);
		add (chCampaign);

		chSkirmish.setBounds (8, 308, 110, 20);
		buttonGroup.add (chSkirmish);
		add (chSkirmish);

		chHystorical.addItemListener (e -> {
			if (chHystorical.isSelected ()) {
				JOptionPane.showMessageDialog (this, hystoricalWarning, "Hystorical map", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		chHystorical.setBounds (8, 333, 110, 20);
		buttonGroup.add (chHystorical);
		add (chHystorical);

		ckLastMission.setEnabled (false);
		ckLastMission.setToolTipText ("This is the last mission and will play the end movie when finished");
		ckLastMission.setHorizontalAlignment (SwingConstants.TRAILING);
		ckLastMission.setBounds (122, 281, 112, 24);
		add (ckLastMission);

		ckAllianceChangeAllowed.setToolTipText ("The players can change the alliances during the game");
		ckAllianceChangeAllowed.setBounds (8, 357, 162, 24);
		add (ckAllianceChangeAllowed);

		ckIslandsMakeSounds.setToolTipText ("You'll hear sound coming from the islands");
		ckIslandsMakeSounds.setBounds (8, 385, 146, 24);
		add (ckIslandsMakeSounds);

		setVisible (true);
	}

	@Override
	public void windowClosing (WindowEvent e) {
		Settings.width = Core.getInt (tWidth);
		Settings.height = Core.getInt (tHeight);
		Settings.depth = Core.getInt (tDepth);
		Settings.worldBufferSize = Core.getInt (tWorldBufferSize);
		Settings.x = Core.getFloat (tX);
		Settings.y = Core.getFloat (tY);
		Settings.z = Core.getFloat (tZ);
		Settings.background = (String) cbBackground.getSelectedItem ();
		Settings.starMap = (String) cbStarmap.getSelectedItem ();
		Settings.ambientColor = cpAmbientColor.getColor ();
		Settings.roofColor = cpRoofColor.getColor ();
		Settings.floorColor = cpFloorColor.getColor ();
		Settings.isCampaign = chCampaign.isSelected ();
		Settings.isSkirmish = chSkirmish.isSelected ();
		Settings.isHystorical = chHystorical.isSelected ();
		Settings.isLastMission = chCampaign.isSelected () && ckLastMission.isSelected ();
		Settings.isAllianceChangeAllowed = ckAllianceChangeAllowed.isSelected ();
		Settings.isIslandsMakeSounds = ckIslandsMakeSounds.isSelected ();
		super.windowClosing (e);
	}

}
