package gui;

import java.awt.Window;

import data.world.Dialog;
import gui.editors.DialogsMenuEditor;


/**
 * A menu to manipulate the dialogs
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class DialogsMenu extends EntriesListMenu <Dialog> {

	/**
	 * Creates a new {@link DialogsMenu}
	 *
	 * @param parent the parent window
	 */
	public DialogsMenu (Window parent) {
		super (parent, "dialog", Dialog.dialogs, true);
	}

	@Override
	public Dialog newElementEvent () {
		DialogsMenuEditor editor = new DialogsMenuEditor (this);
		return editor.element;
	}

	@Override
	public boolean editElementEvent (Dialog dialog) {
		new DialogsMenuEditor (this, dialog);
		return true;
	}

}
