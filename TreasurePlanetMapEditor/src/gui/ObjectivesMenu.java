package gui;

import java.awt.Window;

import data.world.ObjectiveTask;
import gui.editors.ObjectivesMenuEditor;
import logic.Core;


/**
 * A menu to manipulate the Objectives
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class ObjectivesMenu extends EntriesListMenu <ObjectiveTask> {

	/**
	 * Creates a new {@link ObjectivesMenu}
	 *
	 * @param parent the parent window
	 */
	public ObjectivesMenu (Window parent) {
		super (parent, "objective", ObjectiveTask.objectiveTasks, true);
	}

	@Override
	public ObjectiveTask newElementEvent () {
		try {
			ObjectivesMenuEditor editor = new ObjectivesMenuEditor (this);
			return editor.element;
		} catch (IllegalStateException e) {
			Core.printError (this, e.getMessage (), "Error");
			return null;
		}
	}

	@Override
	public boolean editElementEvent (ObjectiveTask element) {
		new ObjectivesMenuEditor (this, element);
		return true;
	}

}
