package gui;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import data.world.Dialog;
import data.world.Flag;
import data.world.Group;
import data.world.ObjectivePoint;
import data.world.ObjectiveTask;
import data.world.Player;
import data.world.PointSet;
import data.world.Polygon;
import data.world.Timer;
import data.world.Waypoint;
import data.world.WorldObjectInstance;
import entities.Field;
import entities.VarType;
import gui.elements.JComboBoxLink;
import interfaces.TriggerCategory;
import interfaces.TriggerElement;
import interfaces.TriggerElementType;
import logic.Core;

/**
 * A menu to manipulate TriggerElements
 *
 * @author MarcoForlini
 * @param <T> The type of trigger element
 * @param <C> The category of trigger
 * @param <E> The type element
 */
@SuppressWarnings ("serial")
public abstract class TriggerSubMenu <T extends TriggerElementType, C extends TriggerCategory <T>, E extends TriggerElement <T>> extends GenericDialog {

	private static final ActionListener	comboBoxListener	= e -> comboBoxEvent (e);

	/** The element */
	public E							element;


	protected JLabel[]					jLabels;
	protected JComponent[]				jFields;
	protected JComboBox[]				jCombos;
	protected boolean					rebuild				= false;


	/**
	 * Creates a new TriggerSubMenu
	 * 
	 * @param parent the parent window
	 * @param title the title of the window
	 * @param numFields The number of fields
	 * @wbp.parser.constructor
	 */
	public TriggerSubMenu (Window parent, String title, int numFields) {
		super (parent, title, 800, 540);
		this.element = null;
		initialize (numFields);
	}

	/**
	 * Creates a new TriggerSubMenu
	 *
	 * @param parent the parent window
	 * @param title the title of the window
	 * @param numFields The number of fields
	 */
	public TriggerSubMenu (Window parent, String title, int numFields, E element) {
		super (parent, title, 750, 540);
		this.element = element;
		initialize (numFields);
	}

	private final void initialize (int numFields) {
		jLabels = new JLabel[numFields];
		jFields = new JComponent[numFields];
		jCombos = new JComboBox[numFields];
		setLayout (null);
	}

	/**
	 * Initialize the menu
	 *
	 * @param newElement if true, this is a new element
	 */
	public abstract void initialize (boolean newElement);





	private static void comboBoxEvent (ActionEvent e) {
		JComboBoxLink <WorldObjectInstance> jCombo = (JComboBoxLink <WorldObjectInstance>) e.getSource ();
		Group group = (Group) jCombo.getSelectedItem ();
		jCombo.getLink ().setModel (new DefaultComboBoxModel<> (group.getObjects ()));
	}

	/**
	 * Build and return a new {@link JTextField}
	 *
	 * @param text The starting text
	 * @param i The index of the element
	 * @return The new {@link JTextField}
	 */
	protected JTextField getTextField (String text, int i) {
		JTextField jText = new JTextField (text);
		jText.setBounds (195, jLabels[i].getY () - 3, 505, 20);
		jText.setVisible (true);
		return jText;
	}

	/**
	 * Build and return a new {@link JCheckBox}
	 *
	 * @param text The starting text
	 * @param selected The initial value
	 * @param i The index of the element
	 * @return The new {@link JCheckBox}
	 */
	protected JCheckBox getCheckBox (String text, boolean selected, int i) {
		JCheckBox jCheck = new JCheckBox (text, selected);
		jCheck.setBounds (195, jLabels[i].getY () - 3, 505, 20);
		jCheck.setVisible (true);
		return jCheck;
	}

	/**
	 * Build and return a new {@link JComboBox}
	 *
	 * @param <T> The type of elements in the JComboBox
	 * @param items The vector of items
	 * @param i The index of the element
	 * @param linkTo The linked JComboBox, if any
	 * @param right If true, the JComboBox is placed right
	 * @return The new {@link JComboBox}
	 */
	protected <T> JComboBox <T> getComboBox (Vector <T> items, int i, JComboBox <WorldObjectInstance> linkTo, boolean right) {
		if (linkTo == null) {
			JComboBox <T> jCombo = new JComboBox<> (items);
			if (right) {
				jCombo.setBounds (450, jLabels[i].getY () - 3, 250, 20);
			} else {
				jCombo.setBounds (195, jLabels[i].getY () - 3, 505, 20);
			}
			jCombo.setVisible (true);
			return jCombo;
		} else {
			JComboBoxLink <T> jCombo = new JComboBoxLink<> (items);
			jCombo.setBounds (195, jLabels[i].getY () - 3, 250, 20);
			jCombo.setLink (linkTo);
			jCombo.setVisible (true);
			return jCombo;
		}
	}

	/**
	 * Build and return a new {@link JComboBox}
	 *
	 * @param <T> The type of elements in the JComboBox
	 * @param items The array of items
	 * @param i The index of the element
	 * @param linkTo The linked JComboBox, if any
	 * @param right If true, the JComboBox is placed right
	 * @return The new {@link JComboBox}
	 */
	protected <T> JComboBox <T> getComboBox (T[] items, int i, JComboBox <WorldObjectInstance> linkTo, boolean right) {
		return getComboBox (new Vector<> (Arrays.asList (items)), i, linkTo, right);
	}





	/**
	 * Build the menu with all fields required for the condition/action type.
	 *
	 * @param element The condition/action type
	 * @return The number of fields required
	 */
	public int buildMenu (T element) {
		Field[] fields = element.getFields ();

		if (fields == null) {
			throw new InternalError ();
		}

		cleanMenu (); // CLEAN

		getDescription ().setText (element.getDesc ());
		for (int i = 0; i < fields.length; i++) { // JLABELS
			if (fields[i].getRealDomain () != VarType.BOOLEAN) {
				jLabels[i].setText (fields[i].getName ());
				jLabels[i].setToolTipText (fields[i].getName ());
				jLabels[i].setVisible (true);
			} else {
				jLabels[i].setVisible (false);
			}
		}




		JComboBox <?> cb;
		JComboBox <WorldObjectInstance> cb2;
		JCheckBox ck;



		for (int i = 0; i < fields.length; i++) { // JFIELDS

			switch (fields[i].getUsedDomain ()) {
				case INT:
					final JTextField tx = getTextField ("0", i);
					tx.addKeyListener (new KeyAdapter () {
						@Override
						public void keyTyped (KeyEvent e) {
							if (Core.isValidNumber (tx, e, false, null, null) == false) {
								e.consume ();
							}
						}
					});
					tx.setToolTipText (fields[i].getName ());
					jFields[i] = tx;
					getMainPanel ().add (tx);
					continue;


				case FLOAT:
				case DOUBLE:
					final JTextField tx2 = getTextField ("0.000000", i);
					tx2.addKeyListener (new KeyAdapter () {
						@Override
						public void keyTyped (KeyEvent e) {
							if (Core.isValidNumber (tx2, e, true, null, null) == false) {
								e.consume ();
							}
						}
					});
					tx2.setToolTipText (fields[i].getName ());
					jFields[i] = tx2;
					getMainPanel ().add (tx2);
					continue;


				case STRING:
					if (fields[i].getRealDomain () == VarType.BOOLEAN) {
						ck = getCheckBox (fields[i].getName (), false, i);
						ck.setText (fields[i].getName ());
						ck.setToolTipText (fields[i].getName ());
						jFields[i] = ck;
						getMainPanel ().add (ck);
						continue;
					}

					if (fields[i].getRealDomain () == VarType.ID_GROUP_UNIT) {
						cb2 = getComboBox (new WorldObjectInstance[] { }, i, null, true);
						cb2.setToolTipText ("Unit");
						cb = getComboBox (Group.groups, i, cb2, false);
						cb.addActionListener (comboBoxListener);
						cb.setToolTipText (fields[i].getName ());
						jCombos[i] = cb2;
						getMainPanel ().add (cb2);
						break;
					}

					switch (fields[i].getRealDomain ()) {
						case ENUM:
							cb = getComboBox (fields[i].getValuesEnum (), i, null, false);
							break;
						case ID_STRING:
							cb = getComboBox (fields[i].getValuesIDS (), i, null, false);
							break;
						case ID_ETHERIUM:
							cb = getComboBox (WorldObjectInstance.etherium, i, null, false);
							break;
						case ID_NEBULA:
							cb = getComboBox (WorldObjectInstance.nebulas, i, null, false);
							break;
						case ID_FLAG:
							cb = getComboBox (Flag.flags, i, null, false);
							break;
						case ID_TIMER:
							cb = getComboBox (Timer.timers, i, null, false);
							break;
						case ID_TASK:
							cb = getComboBox (ObjectiveTask.objectiveTasks, i, null, false);
							break;
						case ID_DIALOG:
							cb = getComboBox (Dialog.dialogs, i, null, false);
							break;
						case ID_PLAYER:
							cb = getComboBox (Player.players, i, null, false);
							break;
						case ID_SHIP:
							cb = getComboBox (WorldObjectInstance.ships, i, null, false);
							break;
						case ID_OBJECTIVE_POINT:
							cb = getComboBox (ObjectivePoint.objectivePoints, i, null, false);
							break;
						case ID_POINT_SET:
							cb = getComboBox (PointSet.pointSets, i, null, false);
							break;
						case ID_WAYPOINTS:
							cb = getComboBox (Waypoint.waypoints, i, null, false);
							break;
						case ID_POLYGON:
							cb = getComboBox (Polygon.polygons, i, null, false);
							break;
						default:
							throw new InternalError ();
					}
					break;


				default:
					throw new InternalError ();
			}

			cb.setToolTipText (fields[i].getName ());
			jFields[i] = cb;
			getMainPanel ().add (cb);

		}

		return fields.length;

	}


	/**
	 * Build the menu with all fields required for the condition/action type, and populate them with the values from the condition/action.
	 *
	 * @param element The condition/action
	 * @return The number of fields required
	 */
	public int buildMenu (E element) {
		int num = buildMenu (element.getElementType ());
		Field[] fields = element.getElementType ().getFields ();
		Object[] values = element.getValues ();

		for (int i = 0; i < fields.length; i++) {
			if (fields[i].getRealDomain () == VarType.BOOLEAN) {
				((JCheckBox) jFields[i]).setSelected ((boolean) values[i]);
				continue;
			}
			switch (fields[i].getUsedDomain ()) {
				case INT:
				case FLOAT:
				case DOUBLE:
					((JTextField) jFields[i]).setText ((String) values[i]);
					break;
				case STRING:
					((JComboBox <?>) jFields[i]).setSelectedItem (values[i]);
					break;
				default:
					throw new InternalError ();
			}
		}

		return num;
	}



	/**
	 * Remove/hide all components from index A (included) to index B (excluded)
	 *
	 * @param fromIndex From index (included)
	 * @param toIndex To index (excluded)
	 */
	protected void cleanMenu (int fromIndex, int toIndex) {
		for (int i = fromIndex; i < toIndex; i++) {
			jLabels[i].setVisible (false);
			if (jFields[i] != null) {
				getMainPanel ().remove (jFields[i]);
				jFields[i] = null;
			}
			if (jCombos[i] != null) {
				getMainPanel ().remove (jCombos[i]);
				jCombos[i] = null;
			}
		}
		repaint ();
	}

	/**
	 * Remove/hide all components
	 */
	protected void cleanMenu () {
		cleanMenu (0, jLabels.length);
	}



	/**
	 * Return the main JPanel used in the menu
	 *
	 * @return The main JPanel.
	 */
	protected abstract JPanel getMainPanel ();

	/**
	 * Return the description JLabel used in the menu
	 *
	 * @return The description JLabel.
	 */
	protected abstract JTextArea getDescription ();

	/**
	 * Set the combobox to the arguments
	 *
	 * @param category The category for the condition/action
	 * @param type The contition/action type
	 */
	public abstract void setComboBox (C category, T type);

}
