package gui.editors;

import javax.swing.JButton;

import gui.EntriesListMenu;
import gui.GenericDialog;
import gui.elements.JListEx;
import gui.exceptions.EditElementException;
import gui.exceptions.NewElementException;
import interfaces.EntityInterface;
import logic.Core;

/**
 * A JDialog which allow to create/edit an element
 *
 * @author MarcoForlini
 * @param <T> Type of entity
 */
@SuppressWarnings ("serial")
public abstract class EntriesListMenuEditor <T extends EntityInterface <? super T>> extends GenericDialog {

	/** List of elements */
	public JListEx <T>	lsList;

	/** The element created/edited */
	public T			element;

	/** Save the element */
	public JButton		bSave	= new JButton ("Save");

	/** Cancel the operation */
	public JButton		bCancel	= new JButton ("Cancel");


	/**
	 * Creates a new {@link EntriesListMenuEditor} to create a new element
	 *
	 * @param entriesListMenu the parent window
	 * @param width The width of the window
	 * @param height The height of the window
	 * @throws NewElementException If an error happens when creating an object
	 */
	public EntriesListMenuEditor (EntriesListMenu <T> entriesListMenu, int width, int height) throws NewElementException {
		super (entriesListMenu, entriesListMenu != null ? "New " + entriesListMenu.elemName : "New", width, height);
		this.lsList = entriesListMenu != null ? entriesListMenu.lsList : null;
		element = null;
		initialize ();
	}

	/**
	 * Creates a new {@link EntriesListMenuEditor} to edit the given element
	 *
	 * @param entriesListMenu the parent window
	 * @param element The element to edit
	 * @param width The width of the window
	 * @param height The height of the window
	 * @throws EditElementException If an error happens when creating an object
	 */
	public EntriesListMenuEditor (EntriesListMenu <T> entriesListMenu, T element, int width, int height) throws EditElementException {
		super (entriesListMenu, entriesListMenu != null ? "Edit " + entriesListMenu.elemName : "Edit", width, height);
		if (element.isSpecial ()) {
			throw new EditElementException ("You can't edit this special element!");
		}
		this.lsList = entriesListMenu != null ? entriesListMenu.lsList : null;
		this.element = element;
		initialize ();
	}

	private final void initialize () {
		setLayout (null);

		bSave.addActionListener (e -> {
			try {
				saveElement ();
			} catch (NumberFormatException exc) {
				Core.printError (this, "Be careful: some fields are number, not strings!", "Error");
			} catch (Exception exc) {
				Core.printException (this, exc, true);
			}
		});
		bSave.setToolTipText ("Save the element");
		bSave.setBounds (10, 237, 150, 23);
		add (bSave);

		bCancel.addActionListener (e -> dispose ());
		bCancel.setToolTipText ("Cancel the operation");
		bCancel.setBounds (234, 237, 150, 23);
		add (bCancel);
	}



	/**
	 * Initialize the dialog
	 *
	 * @param newElement <code>true</code> if it's a new element, <code>false</code> otherwise
	 */
	public abstract void initialize (boolean newElement);

	/**
	 * Save the element.
	 * Store it into {@link EntriesListMenuEditor#element} if its a new element.
	 *
	 * @throws Exception If anything happens while saving
	 */
	public abstract void saveElement () throws Exception;

}