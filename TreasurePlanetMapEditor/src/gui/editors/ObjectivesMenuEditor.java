package gui.editors;

import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import data.world.ObjectiveTask;
import entities.TaskState;
import gui.ObjectivesMenu;
import gui.elements.IDPanel;
import gui.elements.JComboBoxEx;
import gui.elements.JRadioButtonTask;
import logic.Core;

/**
 * An editor to create/edit a {@link ObjectiveTask}
 *
 * @author MarcoForlini
 */
public class ObjectivesMenuEditor extends EntriesListMenuEditor <ObjectiveTask> {

	private IDPanel <ObjectiveTask>		idPanel;
	private final JPanel				pnState				= new JPanel ();
	private final JComboBoxEx <String>	cbObjectiveNames	= new JComboBoxEx<> ();
	private final ButtonGroup			buttonGroup			= new ButtonGroup ();
	private final JRadioButton[]		chTaskState			= new JRadioButton[] {
			new JRadioButtonTask ("Not active", "This task isn't active", 10, 11, 97, 23, true),
			new JRadioButtonTask ("Active", "This task is active", 10, 38, 97, 23, false),
			new JRadioButtonTask ("Completed", "This task is completed", 10, 65, 97, 23, false),
			new JRadioButtonTask ("Failed", "This task is failed", 10, 92, 97, 23, false)
	};


	/**
	 * Creates a new {@link ObjectivesMenuEditor} to create a new {@link ObjectiveTask}
	 *
	 * @param parent the parent window
	 */
	public ObjectivesMenuEditor (ObjectivesMenu parent) {
		super (parent, 420, 300);
		initialize (true);
		if (Core.taskNames.length == 0) {
			throw new IllegalStateException ("No names available for the objectives");
		}
		cbObjectiveNames.setSelectedIndex (0);
		chTaskState[0].setSelected (true);
		setVisible (true);
	}


	/**
	 * Creates a new {@link ObjectivesMenuEditor} to edit a {@link ObjectiveTask}
	 *
	 * @param parent the parent window
	 * @param objTask the Player to edit
	 */
	public ObjectivesMenuEditor (ObjectivesMenu parent, ObjectiveTask objTask) {
		super (parent, objTask, 420, 300);
		initialize (false);
		cbObjectiveNames.setSelectedElement (objTask.getText ());
		chTaskState[objTask.getState ().ordinal ()].setSelected (true);
		setVisible (true);
	}


	@Override
	public void initialize (boolean newElement) {
		idPanel = new IDPanel<> (element, 10, 7);
		add (idPanel);

		JLabel lObjectiveText = new JLabel ("Text");
		lObjectiveText.setToolTipText ("Select the string containing the name of the objective");
		lObjectiveText.setBounds (10, 36, 59, 14);
		add (lObjectiveText);

		lObjectiveText.setLabelFor (cbObjectiveNames);
		cbObjectiveNames.setToolTipText ("Select the string containing the name of the objective");
		cbObjectiveNames.setModel (new DefaultComboBoxModel<> (Core.taskNames));
		cbObjectiveNames.setMaximumRowCount (10);
		cbObjectiveNames.setBounds (79, 33, 218, 20);
		add (cbObjectiveNames);

		JButton bNewObjectiveName = new JButton ("New name");
		bNewObjectiveName.setToolTipText ("Add a new objective name to the list");
		bNewObjectiveName.addActionListener (e -> {
			// TODO
		});
		bNewObjectiveName.setMargin (new Insets (2, 5, 2, 5));
		bNewObjectiveName.setBounds (307, 32, 77, 23);
		add (bNewObjectiveName);

		buttonGroup.add (chTaskState[0]);
		buttonGroup.add (chTaskState[1]);
		buttonGroup.add (chTaskState[2]);
		buttonGroup.add (chTaskState[3]);

		pnState.setBounds (10, 62, 150, 148);
		pnState.setLayout (null);
		pnState.add (chTaskState[0]);
		pnState.add (chTaskState[1]);
		pnState.add (chTaskState[2]);
		pnState.add (chTaskState[3]);
		add (pnState);

		add (bSave);
		add (bCancel);
	}


	@Override
	public void saveElement () {
		String ID = idPanel.getID ();
		if (Core.checkID (ID, element, false)) {
			String name = cbObjectiveNames.getSelectedElement ();
			if (Core.checkString (name, element, lsList)) {
				TaskState select = null;
				for (int i = 0; i < chTaskState.length; i++) {
					if (chTaskState[i].isSelected ()) {
						select = TaskState.values ()[i];
						break;
					}
				}
				if (element == null) {
					element = new ObjectiveTask (ID, name, select);
				} else {
					element.setValues (ID, name, select);
				}
				dispose ();
			}
		}
	}

}