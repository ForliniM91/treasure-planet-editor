package gui.editors;

import java.util.function.Predicate;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import data.world.Alliance;
import data.world.Player;
import gui.AlliancesMenu;
import gui.elements.JComboBoxEx;

/**
 * An editor to create/edit an {@link Alliance}
 *
 * @author MarcoForlini
 */
public class AlliancesMenuEditor extends EntriesListMenuEditor <Alliance> {

	private final JComboBoxEx <Player>	cbPlayerA	= new JComboBoxEx<> (Player.players);
	private final JComboBoxEx <Player>	cbPlayerB	= new JComboBoxEx<> (Player.players);


	/**
	 * Creates a new {@link AlliancesMenuEditor} to create a new {@link Alliance}
	 *
	 * @param parent the parent window
	 */
	public AlliancesMenuEditor (AlliancesMenu parent) {
		super (parent, 420, 300);
		if (Player.players.size () < 2) {
			throw new IllegalStateException ("An alliance require 2 players");
		}
		initialize (true);
		cbPlayerA.setSelectedIndex (0);
		cbPlayerB.setSelectedIndex (1);

		if (Player.players.size () == 2) {
			JOptionPane.showMessageDialog (this, "There are only 2 players, so I'll create the alliance automatically.\nDon't forget: if the only 2 players are allied, there will be no battle!", "Error", JOptionPane.WARNING_MESSAGE);
			saveElement ();
		} else {
			setVisible (true);
		}
	}


	/**
	 * Creates a new {@link AlliancesMenuEditor} to edit a {@link Alliance}
	 *
	 * @param parent the parent window
	 * @param alliance the Alliance to edit
	 */
	public AlliancesMenuEditor (AlliancesMenu parent, Alliance alliance) {
		super (parent, alliance, 420, 300);
		initialize (false);
		cbPlayerA.setSelectedItem (alliance.getPlayerA ());
		cbPlayerB.setSelectedItem (alliance.getPlayerB ());
		setVisible (true);
	}


	@Override
	public void initialize (boolean newElement) {
		JLabel lPlayerA = new JLabel ("Player A:");
		lPlayerA.setToolTipText ("Select the player A");
		lPlayerA.setHorizontalAlignment (SwingConstants.LEFT);
		lPlayerA.setBounds (10, 11, 146, 14);
		lPlayerA.setLabelFor (cbPlayerA);
		add (lPlayerA);

		cbPlayerA.setToolTipText ("Select the player A");
		cbPlayerA.setModel (new DefaultComboBoxModel<> (Player.players));
		cbPlayerA.setMaximumRowCount (10);
		cbPlayerA.setBounds (166, 8, 218, 20);
		add (cbPlayerA);

		JLabel lPlayerB = new JLabel ("Player B:");
		lPlayerB.setToolTipText ("Select the player B");
		lPlayerB.setHorizontalAlignment (SwingConstants.LEFT);
		lPlayerB.setBounds (10, 36, 146, 14);
		lPlayerB.setLabelFor (cbPlayerB);
		add (lPlayerB);

		cbPlayerB.setToolTipText ("Select the player B");
		cbPlayerB.setModel (new DefaultComboBoxModel<> (Player.players));
		cbPlayerB.setMaximumRowCount (10);
		cbPlayerB.setBounds (166, 33, 218, 20);
		add (cbPlayerB);
	}


	@Override
	public void saveElement () {
		Player pA = (Player) cbPlayerA.getSelectedItem ();
		Player pB = (Player) cbPlayerB.getSelectedItem ();
		if (pA == pB) {
			JOptionPane.showMessageDialog (this, pA.getID () + " allied with " + pB.getID () + "?!?\nNO WAY! Use 2 different players!", "Self-alliance", JOptionPane.ERROR_MESSAGE);
		} else {
			if (element == null || pA != element.getPlayerA () || pB != element.getPlayerB ()) {
				if (element == null || pA != element.getPlayerB () || pB != element.getPlayerA ()) {
					Predicate <Alliance> matcher = (Alliance al) -> (pA == al.getPlayerA () && pB == al.getPlayerB ()) || (pA == al.getPlayerB () && pB == al.getPlayerA ());
					if (lsList.getVector ().parallelStream ().anyMatch (matcher)) {
						JOptionPane.showMessageDialog (this, "These players are already allied!", "Alliance already created", JOptionPane.ERROR_MESSAGE);
						return;
					}
				}

				if (element == null) {
					element = new Alliance (pA, pB);
				} else {
					element.setValues (pA, pB);
				}
			}
			dispose ();
		}
	}

}