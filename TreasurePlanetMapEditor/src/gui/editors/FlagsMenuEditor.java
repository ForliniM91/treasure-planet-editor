package gui.editors;

import javax.swing.JCheckBox;

import data.world.Flag;
import gui.FlagsMenu;
import gui.elements.IDPanel;
import logic.Core;

/**
 * An editor to create/edit a {@link Flag}
 *
 * @author MarcoForlini
 */
public class FlagsMenuEditor extends EntriesListMenuEditor <Flag> {

	private IDPanel <Flag>	idPanel;
	private final JCheckBox	ckValue	= new JCheckBox ("Flag value is true");


	/**
	 * Creates a new {@link FlagsMenuEditor} to create a new {@link Flag}
	 *
	 * @param parent the parent window
	 */
	public FlagsMenuEditor (FlagsMenu parent) {
		super (parent, 420, 300);
		initialize (true);
		ckValue.setSelected (false);
		setVisible (true);
	}


	/**
	 * Creates a new {@link FlagsMenuEditor} to edit a {@link Flag}
	 *
	 * @param parent the parent window
	 * @param flag the Flag to edit
	 */
	public FlagsMenuEditor (FlagsMenu parent, Flag flag) {
		super (parent, flag, 420, 300);
		initialize (false);
		ckValue.setSelected (flag.isValue ());
		setVisible (true);
	}


	@Override
	public void initialize (boolean newElement) {
		idPanel = new IDPanel<> (element, 10, 7);
		add (idPanel);

		ckValue.setToolTipText ("Check this box to make the flag true at startup");
		ckValue.setBounds (10, 32, 120, 24);
		add (ckValue);

		add (bSave);
		add (bCancel);
	}


	@Override
	public void saveElement () {
		String ID = idPanel.getID ();
		if (Core.checkID (ID, element, false)) {
			if (element == null) {
				element = new Flag (ID, ckValue.isSelected ());
			} else {
				element.setValues (ID, ckValue.isSelected ());
			}
		}

	}

}