package gui.editors;

import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import data.world.Timer;
import gui.TimersMenu;
import gui.elements.IDPanel;
import logic.Core;

/**
 * An editor to create/edit a {@link Timer}
 *
 * @author MarcoForlini
 */
public class TimersMenuEditor extends EntriesListMenuEditor <Timer> {

	private IDPanel <Timer>		idPanel;
	private final JTextField	tTime	= new JTextField ();
	private final JCheckBox		ckValue	= new JCheckBox ("Running");


	/**
	 * Creates a new {@link TimersMenuEditor} to create a new {@link Timer}
	 *
	 * @param parent the parent window
	 */
	public TimersMenuEditor (TimersMenu parent) {
		super (parent, 420, 300);
		initialize (true);
		tTime.setText ("0");
		ckValue.setSelected (false);
		setVisible (true);
	}


	/**
	 * Creates a new {@link TimersMenuEditor} to edit a {@link Timer}
	 *
	 * @param parent the parent window
	 * @param timer the Timer to edit
	 */
	public TimersMenuEditor (TimersMenu parent, Timer timer) {
		super (parent, timer, 420, 300);
		initialize (false);
		tTime.setText ("" + timer.getTime ());
		ckValue.setSelected (timer.getState ());
		setVisible (true);
	}


	@Override
	public void initialize (boolean newElement) {
		idPanel = new IDPanel<> (element, 10, 7);
		add (idPanel);

		JLabel lTime = new JLabel ("Time");
		lTime.setToolTipText ("This is the current time for this timer");
		lTime.setBounds (10, 39, 59, 14);
		lTime.setLabelFor (tTime);
		add (lTime);

		tTime.setText ("0");
		tTime.setToolTipText ("This is the current time for this timer");
		tTime.setBounds (79, 36, 218, 20);
		add (tTime);

		JButton bReset = new JButton ("Reset");
		bReset.addActionListener (e -> tTime.setText ("0"));
		bReset.setToolTipText ("Reset the current time to 0");
		bReset.setMargin (new Insets (2, 5, 2, 5));
		bReset.setBounds (307, 35, 77, 23);
		add (bReset);

		ckValue.setToolTipText ("Check this box to start the timer at startup");
		ckValue.setBounds (10, 60, 120, 24);
		add (ckValue);

		add (bSave);
		add (bCancel);
	}


	@Override
	public void saveElement () {
		String ID = idPanel.getID ();
		if (Core.checkID (ID, element, false)) {
			if (element == null) {
				element = new Timer (ID, Core.getFloat (tTime), ckValue.isSelected ());
			} else {
				element.setValues (ID, Core.getFloat (tTime), ckValue.isSelected ());
			}
		}
	}
}