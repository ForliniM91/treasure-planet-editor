package gui.editors;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

import data.world.Dialog;
import entities.GameColor;
import gui.DialogsMenu;
import gui.elements.ColorPanelEntity;
import gui.elements.IDPanel;
import gui.elements.JComboBoxEx;
import logic.Core;

/**
 * An editor to create/edit a {@link Dialog}
 *
 * @author MarcoForlini
 */
public class DialogsMenuEditor extends EntriesListMenuEditor <Dialog> {

	private static final GameColor		defaultTextColor	= new GameColor (0.788200f, 0.482300f, 0.964700f, 1.000000f);

	private IDPanel <Dialog>			idPanel;
	private final JComboBoxEx <String>	cbSpeaker			= new JComboBoxEx<> ();
	private final JComboBoxEx <String>	cbTexture			= new JComboBoxEx<> ();
	private final JCheckBox				ckShowFace			= new JCheckBox ("Show face");
	private final JComboBoxEx <String>	cbVoice				= new JComboBoxEx<> ();
	private final JComboBoxEx <String>	cbText				= new JComboBoxEx<> ();
	private ColorPanelEntity <Dialog>	colorPanel;
	private final JCheckBox				ckPlayedOnce		= new JCheckBox ("Has been played");


	/**
	 * Creates a new {@link DialogsMenuEditor} to create a new {@link Dialog}
	 *
	 * @param parent the parent window
	 */
	public DialogsMenuEditor (DialogsMenu parent) {
		super (parent, 420, 300);
		initialize (true);
		cbSpeaker.setSelectedIndex (0);
		cbTexture.setSelectedIndex (0);
		cbTexture.setEnabled (false);
		ckShowFace.setSelected (false);
		cbVoice.setSelectedIndex (0);
		cbText.setSelectedIndex (0);
		ckPlayedOnce.setSelected (false);
		setVisible (true);
	}


	/**
	 * Creates a new {@link DialogsMenuEditor} to edit a {@link Dialog}
	 *
	 * @param parent the parent window
	 * @param dialog the Dialog to edit
	 */
	public DialogsMenuEditor (DialogsMenu parent, Dialog dialog) {
		super (parent, dialog, 420, 300);
		initialize (false);
		cbSpeaker.setSelectedItem (dialog.getSpeaker ());
		cbTexture.setSelectedItem (dialog.getFaceTexture ());
		ckShowFace.setSelected (dialog.isShowHead ());
		cbTexture.setEnabled (ckShowFace.isSelected ());
		cbVoice.setSelectedItem (dialog.getVoice ());
		cbText.setSelectedItem (dialog.getText ());
		ckPlayedOnce.setSelected (dialog.isPlayedOnce ());
		setVisible (true);
	}


	@Override
	public void initialize (boolean newElement) {
		idPanel = new IDPanel<> (element, 10, 7);
		add (idPanel);

		JLabel lSpeaker = new JLabel ("Speaker");
		lSpeaker.setToolTipText ("Select the speaker for this dialog");
		lSpeaker.setBounds (10, 36, 69, 14);
		lSpeaker.setLabelFor (cbSpeaker);
		add (lSpeaker);

		cbSpeaker.setToolTipText ("Select the speaker for this dialog");
		cbSpeaker.setModel (new DefaultComboBoxModel<> (Core.dialogSpeaker));
		cbSpeaker.setMaximumRowCount (10);
		cbSpeaker.setBounds (79, 33, 218, 20);
		add (cbSpeaker);

		JLabel lTexture = new JLabel ("Face");
		lTexture.setToolTipText ("Select the image file for the face");
		lTexture.setBounds (10, 61, 59, 14);
		lTexture.setLabelFor (cbTexture);
		add (lTexture);

		cbTexture.setEnabled (false);
		cbTexture.setToolTipText ("Select the image file for the face");
		cbTexture.setModel (new DefaultComboBoxModel<> (Core.dialogTextures));
		cbTexture.setMaximumRowCount (10);
		cbTexture.setBounds (79, 58, 218, 20);
		add (cbTexture);

		ckShowFace.addActionListener (e -> cbTexture.setEnabled (ckShowFace.isSelected ()));
		ckShowFace.setToolTipText ("Show the face in the lower right corner");
		ckShowFace.setBounds (307, 57, 87, 23);
		add (ckShowFace);

		JLabel lVoice = new JLabel ("Voice file");
		lVoice.setToolTipText ("Select the sound file for this dialog");
		lVoice.setBounds (10, 86, 59, 14);
		lVoice.setLabelFor (cbVoice);
		add (lVoice);

		cbVoice.setToolTipText ("Select the sound file for this dialog");
		cbVoice.setModel (new DefaultComboBoxModel<> (Core.dialogVoices));
		cbVoice.setMaximumRowCount (10);
		cbVoice.setBounds (79, 83, 218, 20);
		add (cbVoice);

		JLabel lText = new JLabel ("Text");
		lText.setToolTipText ("Select the text for the dialog");
		lText.setBounds (10, 111, 69, 14);
		lText.setLabelFor (cbText);
		add (lText);

		cbText.setToolTipText ("Select the text for the dialog");
		cbText.setModel (new DefaultComboBoxModel<> (Core.dialogTexts));
		cbText.setMaximumRowCount (10);
		cbText.setBounds (79, 108, 218, 20);
		add (cbText);

		colorPanel = new ColorPanelEntity<> ("Text color", element, lsList, defaultTextColor, null, newElement, 10, 133);
		add (colorPanel);

		ckPlayedOnce.setToolTipText ("This dialogue has been played at least once");
		ckPlayedOnce.setBounds (10, 157, 153, 23);
		add (ckPlayedOnce);

		add (bSave);
		add (bCancel);
	}


	@Override
	public void saveElement () throws Exception {
		String ID = idPanel.getID ();
		if (Core.checkID (ID, element, false)) {
			if (element == null) {
				element = new Dialog (ID,
						(String) cbSpeaker.getSelectedItem (), ckShowFace.isSelected (), (String) cbVoice.getSelectedItem (),
						(String) cbTexture.getSelectedItem (), (String) cbText.getSelectedItem (),
						new GameColor (colorPanel.getColor ()));
			} else {
				element.setValues (ID,
						(String) cbSpeaker.getSelectedItem (), ckShowFace.isSelected (), (String) cbVoice.getSelectedItem (),
						(String) cbTexture.getSelectedItem (), (String) cbText.getSelectedItem (),
						new GameColor (colorPanel.getColor ()));
			}
			dispose ();
		}
	}

}