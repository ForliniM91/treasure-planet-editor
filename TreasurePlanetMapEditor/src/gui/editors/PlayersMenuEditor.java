package gui.editors;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JToggleButton;

import data.world.Player;
import data.world.Team;
import entities.FormationType;
import entities.GameColor;
import entities.Race;
import gui.PlayersMenu;
import gui.elements.ColorPanelEntity;
import gui.elements.IDPanel;
import gui.elements.JComboBoxEx;
import logic.Core;

/**
 * An editor to create/edit a {@link Player}
 *
 * @author MarcoForlini
 */
public class PlayersMenuEditor extends EntriesListMenuEditor <Player> {

	private IDPanel <Player>					idPanel;
	private final JComboBoxEx <Team>			cbPlayerTeam	= new JComboBoxEx<> ();
	private final JComboBoxEx <Race>			cbPlayerRace	= new JComboBoxEx<> ();
	private final JComboBoxEx <FormationType>	cbFormation		= new JComboBoxEx<> ();
	private ColorPanelEntity <Player>			colorPanel;
	private final JToggleButton					ckPlayable		= new JToggleButton ("Playable");



	/**
	 * Creates a new {@link PlayersMenuEditor} to create a new {@link Player}
	 *
	 * @param parent the parent window
	 */
	public PlayersMenuEditor (PlayersMenu parent) {
		super (parent, 420, 300);
		initialize (true);
		cbPlayerTeam.setSelectedIndex (0);
		cbPlayerRace.setSelectedIndex (0);
		cbFormation.setSelectedIndex (0);
		ckPlayable.setSelected (true);
		setVisible (true);
	}


	/**
	 * Creates a new {@link PlayersMenuEditor} to edit a {@link Player}
	 *
	 * @param parent the parent window
	 * @param player the Player to edit
	 */
	public PlayersMenuEditor (PlayersMenu parent, Player player) {
		super (parent, 420, 300);
		initialize (false);
		cbPlayerTeam.setSelectedItem (player.getTeam ());
		cbPlayerRace.setSelectedItem (player.getRace ());
		cbFormation.setSelectedItem (player.getFormation ());
		ckPlayable.setSelected (player.isPlayable ());
		setVisible (true);
	}


	@Override
	public void initialize (boolean newElement) {
		idPanel = new IDPanel<> (element, 10, 7);
		add (idPanel);

		JLabel lPlayerTeam = new JLabel ("Team");
		lPlayerTeam.setToolTipText ("Assign the player to a team");
		lPlayerTeam.setBounds (10, 36, 69, 14);
		add (lPlayerTeam);

		lPlayerTeam.setLabelFor (cbPlayerTeam);
		cbPlayerTeam.setToolTipText ("Assign the player to a team");
		cbPlayerTeam.setModel (new DefaultComboBoxModel<> (Team.teams));
		cbPlayerTeam.setMaximumRowCount (10);
		cbPlayerTeam.setBounds (79, 33, 218, 20);
		add (cbPlayerTeam);

		JLabel lRace = new JLabel ("Race");
		lRace.setToolTipText ("Select the race for this player (Ignored in Hystorical maps, if this player join a team with a locked race)");
		lRace.setBounds (10, 61, 59, 14);
		add (lRace);

		lRace.setLabelFor (cbPlayerRace);
		cbPlayerRace.setToolTipText ("Select the race for this player (Ignored in Hystorical maps, if this player join a team with a locked race)");
		cbPlayerRace.setModel (new DefaultComboBoxModel<> (Race.values ()));
		cbPlayerRace.setMaximumRowCount (10);
		cbPlayerRace.setBounds (79, 58, 218, 20);
		add (cbPlayerRace);

		JLabel lFormation = new JLabel ("Formation");
		lFormation.setToolTipText ("Select the starting formation for this player");
		lFormation.setBounds (10, 86, 59, 14);
		add (lFormation);

		lFormation.setLabelFor (cbFormation);
		cbFormation.setToolTipText ("Select the starting formation for this player");
		cbFormation.setModel (new DefaultComboBoxModel<> (FormationType.values ()));
		cbFormation.setMaximumRowCount (10);
		cbFormation.setBounds (79, 83, 218, 20);
		add (cbFormation);

		colorPanel = new ColorPanelEntity<> ("Player color", element, lsList, null, Player.colors, newElement, 10, 107);
		add (colorPanel);

		ckPlayable.setSelected (true);
		ckPlayable.setToolTipText ("Is this player playable by any human?");
		ckPlayable.setBounds (10, 136, 95, 23);
		add (ckPlayable);
	}


	@Override
	public void saveElement () {
		String ID = idPanel.getID ();
		if (Core.checkID (ID, element, false)) {
			if (element == null) {
				element = new Player (ID, (Team) cbPlayerTeam.getSelectedItem (), (Race) cbPlayerRace.getSelectedItem (),
						new GameColor (colorPanel.getColor ()), (FormationType) cbFormation.getSelectedItem (), ckPlayable.isSelected ());
			} else {
				element.setValues (ID, (Team) cbPlayerTeam.getSelectedItem (), (Race) cbPlayerRace.getSelectedItem (),
						new GameColor (colorPanel.getColor ()), (FormationType) cbFormation.getSelectedItem (), ckPlayable.isSelected ());
			}
			dispose ();
		}
	}

}