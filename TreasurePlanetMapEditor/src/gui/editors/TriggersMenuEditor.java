package gui.editors;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import data.world.Action;
import data.world.Condition;
import data.world.Trigger;
import entities.ConditionType;
import gui.ActionsMenu;
import gui.ConditionsMenu;
import gui.TriggersMenu;
import gui.elements.DoubleClickList;
import gui.elements.IDPanel;
import gui.elements.JListEx;
import logic.Core;

/**
 * An editor to create/edit a {@link Trigger}
 *
 * @author MarcoForlini
 */
public class TriggersMenuEditor extends EntriesListMenuEditor <Trigger> {

	private static final int			width			= 800;
	private static final int			height			= 600;
	private static final int			listWidth		= width - 236;
	private static final int			listHeight		= (height - 150) / 2;
	private static final int			secondButtonsY	= 2 * listHeight - 36;

	private final JScrollPane			pnConditionList	= new JScrollPane ();
	private final JScrollPane			pnActionList	= new JScrollPane ();
	private final JLabel				lNElementsC		= new JLabel ("Conditions: 0");
	private final JLabel				lNElementsA		= new JLabel ("Actions: 0");
	private final JListEx <Condition>	lsConditionList	= new JListEx<> ();
	private final JListEx <Action>		lsActionList	= new JListEx<> ();
	private IDPanel <Trigger>			idPanel;
	private final JCheckBox				ckActive		= new JCheckBox ("Active");
	private final JCheckBox				ckFireOnce		= new JCheckBox ("Fire once");


	/**
	 * Creates a new {@link TriggersMenuEditor} to create a new {@link Trigger}
	 *
	 * @param parent the parent window
	 * @wbp.parser.constructor
	 */
	public TriggersMenuEditor (TriggersMenu parent) {
		super (parent, width, height);
		initialize (true);
		ckActive.setSelected (true);
		ckFireOnce.setSelected (true);
		setVisible (true);
	}

	/**
	 * Creates a new {@link TriggersMenuEditor} to edit a {@link Trigger}
	 *
	 * @param parent the parent window
	 * @param trigger the Trigger to edit
	 */
	public TriggersMenuEditor (TriggersMenu parent, Trigger trigger) {
		super (parent, trigger, width, height);
		initialize (false);
		ckActive.setSelected (trigger.isActive ());
		ckFireOnce.setSelected (trigger.isRunOnce ());
		lsConditionList.setVector (trigger.getConditions ());
		lsActionList.setVector (trigger.getActions ());
		setVisible (true);
	}


	private void newConditionEvent () {
		ConditionsMenu editor = new ConditionsMenu (this);
		if (editor.element != null) {
			lsConditionList.add (editor.element);
			SwingUtilities.invokeLater (lsList::repaint);
		}
	}


	private void editConditionEvent () {
		Condition condition = lsConditionList.getSelectedValue ();
		if (condition == null) {
			JOptionPane.showMessageDialog (this, "Select a condition in the list, then click edit", "How to edit a condition", JOptionPane.INFORMATION_MESSAGE);
		} else {
			new ConditionsMenu (this, condition);
			SwingUtilities.invokeLater (lsList::repaint);
		}
	}


	private void newActionEvent () {
		ActionsMenu editor = new ActionsMenu (this);
		if (editor.element != null) {
			lsActionList.add (editor.element);
			SwingUtilities.invokeLater (lsList::repaint);
		}
	}


	private void editActionEvent () {
		Action action = lsActionList.getSelectedValue ();
		if (action == null) {
			JOptionPane.showMessageDialog (this, "Select an action in the list, then click edit", "How to edit an action", JOptionPane.INFORMATION_MESSAGE);
		} else {
			new ActionsMenu (this, action);
			SwingUtilities.invokeLater (lsList::repaint);
		}
	}


	@Override
	public void initialize (boolean newElement) {
		idPanel = new IDPanel<> (element, 10, 7);
		getContentPane ().add (idPanel);

		ckActive.setToolTipText ("The trigger is active/can fire? NOTE: ONLY FOR TESTING, since if it isn't active, there's no way to activate it in game.");
		ckActive.setBounds (79, 35, 80, 23);
		getContentPane ().add (ckActive);

		ckFireOnce.setToolTipText ("The trigger only fire once? If false, the trigger can fire infinite times (it fires everytime the condition is evaluated true)");
		ckFireOnce.setBounds (161, 35, 97, 23);
		getContentPane ().add (ckFireOnce);




		lNElementsC.setOpaque (true);
		lNElementsC.setHorizontalAlignment (SwingConstants.CENTER);
		lNElementsC.setBackground (Color.LIGHT_GRAY);
		lsConditionList.setToolTipText ("Select a player in the list.");
		lsConditionList.addMouseListener (new DoubleClickList<> (this::editConditionEvent));

		pnConditionList.setBounds (10, 65, listWidth, listHeight);
		pnConditionList.setColumnHeaderView (lNElementsC);
		pnConditionList.setViewportView (lsConditionList);
		getContentPane ().add (pnConditionList);

		JButton bNewCond = new JButton ("New condition");
		bNewCond.setToolTipText ("Create a new condition");
		bNewCond.setBounds (listWidth + 60, 76, 150, 23);
		bNewCond.addActionListener (e -> newConditionEvent ());
		getContentPane ().add (bNewCond);

		JButton bEditCond = new JButton ("Edit condition");
		bEditCond.setToolTipText ("Edit the selected condition");
		bEditCond.setMargin (new Insets (2, 5, 2, 5));
		bEditCond.setBounds (listWidth + 60, 102, 150, 23);
		bEditCond.addActionListener (e -> editConditionEvent ());
		getContentPane ().add (bEditCond);

		JButton bDuplicateCond = new JButton ("Duplicate condition");
		bDuplicateCond.setToolTipText ("Duplicate the selected condition");
		bDuplicateCond.setMargin (new Insets (2, 5, 2, 5));
		bDuplicateCond.setBounds (listWidth + 60, 128, 150, 23);
		bDuplicateCond.addActionListener (e -> {
			Condition selected = lsConditionList.getSelectedValue ();
			if (selected == null) {
				JOptionPane.showMessageDialog (this, "Select a condition in the list, then click the duplicate button", "How to duplicate a condition", JOptionPane.INFORMATION_MESSAGE);
			} else {
				lsConditionList.add (selected.clone ());
			}
		});
		getContentPane ().add (bDuplicateCond);

		JButton bRemoveCond = new JButton ("Delete condition");
		bRemoveCond.setToolTipText ("Delete the selected condition from the list");
		bRemoveCond.setMargin (new Insets (2, 5, 2, 5));
		bRemoveCond.setBounds (listWidth + 60, 154, 150, 23);
		bRemoveCond.addActionListener (e -> {
			Condition selected = lsConditionList.getSelectedValue ();
			if (selected == null) {
				JOptionPane.showMessageDialog (this, "Select a condition in the list, then click delete", "How to delete a condition", JOptionPane.INFORMATION_MESSAGE);
			} else if ((e.getModifiers () & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK ||
					JOptionPane.showConfirmDialog (this, "Delete " + selected + "?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
				lsConditionList.erase (selected);
			}
		});
		getContentPane ().add (bRemoveCond);

		JButton bListUpC = new JButton ("");
		bListUpC.setIcon (new ImageIcon (TriggersMenu.class.getResource ("/javax/swing/plaf/metal/icons/sortUp.png")));
		bListUpC.setToolTipText ("Move up the selected item");
		bListUpC.setBounds (listWidth + 20, 102, 30, 23);
		bListUpC.addActionListener (e -> {
			int selected = lsConditionList.getSelectedIndex ();
			if (selected < 0) {
				JOptionPane.showMessageDialog (this, "Select a condition in the list, then click the up arrow", "How to move a condition", JOptionPane.INFORMATION_MESSAGE);
			} else if (lsConditionList.moveUp (selected) != null) {
				lsConditionList.setSelectedIndex (selected - 1);
			}
		});
		getContentPane ().add (bListUpC);

		JButton bListDownC = new JButton ("");
		bListDownC.setIcon (new ImageIcon (TriggersMenu.class.getResource ("/javax/swing/plaf/metal/icons/sortDown.png")));
		bListDownC.setToolTipText ("Move down the selected item");
		bListDownC.setBounds (listWidth + 20, 128, 30, 23);
		bListDownC.addActionListener (e -> {
			int selected = lsConditionList.getSelectedIndex ();
			if (selected < 0) {
				JOptionPane.showMessageDialog (this, "Select a condition in the list, then click the down arrow", "How to move a condition", JOptionPane.INFORMATION_MESSAGE);
			} else if (lsConditionList.moveDown (selected) != null) {
				lsConditionList.setSelectedIndex (selected + 1);
			}
		});
		getContentPane ().add (bListDownC);




		lNElementsA.setOpaque (true);
		lNElementsA.setHorizontalAlignment (SwingConstants.CENTER);
		lNElementsA.setBackground (Color.LIGHT_GRAY);
		lsActionList.setToolTipText ("Select a player in the list.");
		lsActionList.addMouseListener (new DoubleClickList<> (this::editActionEvent));

		pnActionList.setBounds (10, listHeight + 74, listWidth, listHeight);
		pnActionList.setColumnHeaderView (lNElementsA);
		pnActionList.setViewportView (lsActionList);
		getContentPane ().add (pnActionList);

		JButton bNewAct = new JButton ("New action");
		bNewAct.setToolTipText ("Create a new action");
		bNewAct.setBounds (listWidth + 60, secondButtonsY, 150, 23);
		bNewAct.addActionListener (e -> newActionEvent ());
		getContentPane ().add (bNewAct);

		JButton bEditAct = new JButton ("Edit action");
		bEditAct.setToolTipText ("Edit the selected action");
		bEditAct.setMargin (new Insets (2, 5, 2, 5));
		bEditAct.setBounds (listWidth + 60, secondButtonsY + 26, 150, 23);
		bEditAct.addActionListener (e -> editActionEvent ());
		getContentPane ().add (bEditAct);

		JButton bDuplicateAct = new JButton ("Duplicate action");
		bDuplicateAct.setToolTipText ("Duplicate the selected action");
		bDuplicateAct.setMargin (new Insets (2, 5, 2, 5));
		bDuplicateAct.setBounds (listWidth + 60, secondButtonsY + 52, 150, 23);
		bDuplicateAct.addActionListener (e -> {
			Action selected = lsActionList.getSelectedValue ();
			if (selected == null) {
				JOptionPane.showMessageDialog (this, "Select an action in the list, then click the duplicate button", "How to duplicate an action", JOptionPane.INFORMATION_MESSAGE);
			} else {
				lsActionList.add (selected.clone (), lsActionList.getSelectedIndex () + 1);
			}
		});
		getContentPane ().add (bDuplicateAct);

		JButton bRemoveAct = new JButton ("Delete action");
		bRemoveAct.setToolTipText ("Delete the selected action from the list");
		bRemoveAct.setMargin (new Insets (2, 5, 2, 5));
		bRemoveAct.setBounds (listWidth + 60, secondButtonsY + 78, 150, 23);
		bRemoveAct.addActionListener (e -> {
			Action action = lsActionList.getSelectedValue ();
			int selected = lsActionList.getSelectedIndex ();
			if (action == null) {
				JOptionPane.showMessageDialog (this, "Select an action in the list, then click delete", "How to delete an action", JOptionPane.INFORMATION_MESSAGE);
			} else if ((e.getModifiers () & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK ||
					JOptionPane.showConfirmDialog (this, "Delete " + action + "?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
				lsActionList.remove (selected);
			}
		});
		getContentPane ().add (bRemoveAct);

		JButton bListUpA = new JButton ("");
		bListUpA.setIcon (new ImageIcon (TriggersMenu.class.getResource ("/javax/swing/plaf/metal/icons/sortUp.png")));
		bListUpA.setToolTipText ("Move up the selected item");
		bListUpA.setBounds (listWidth + 20, secondButtonsY + 26, 30, 23);
		bListUpA.addActionListener (e -> {
			int selected = lsActionList.getSelectedIndex ();
			if (selected < 0) {
				JOptionPane.showMessageDialog (this, "Select an action in the list, then click the up arrow", "How to move an action", JOptionPane.INFORMATION_MESSAGE);
			} else if (lsActionList.moveUp (selected) != null) {
				lsActionList.setSelectedIndex (selected - 1);
			}
		});
		getContentPane ().add (bListUpA);

		JButton bListDownA = new JButton ("");
		bListDownA.setIcon (new ImageIcon (TriggersMenu.class.getResource ("/javax/swing/plaf/metal/icons/sortDown.png")));
		bListDownA.setToolTipText ("Move down the selected item");
		bListDownA.setBounds (listWidth + 20, secondButtonsY + 52, 30, 23);
		bListDownA.addActionListener (e -> {
			int selected = lsActionList.getSelectedIndex ();
			if (selected < 0) {
				JOptionPane.showMessageDialog (this, "Select an action in the list, then click the down arrow", "How to move an action", JOptionPane.INFORMATION_MESSAGE);
			} else if (lsActionList.moveDown (selected) != null) {
				lsActionList.setSelectedIndex (selected + 1);
			}
		});
		getContentPane ().add (bListDownA);

		bSave.setLocation (bSave.getLocation ().x, 2 * listHeight + 86);
		bCancel.setLocation (bCancel.getLocation ().x, 2 * listHeight + 86);
		getContentPane ().add (bSave);
		getContentPane ().add (bCancel);

		setVisible (false);
	}


	@Override
	public void saveElement () throws Exception {
		String ID = idPanel.getID ();
		if (Core.checkID (ID, element, false)) {
			if (lsActionList.getLength () == 0) {
				JOptionPane.showMessageDialog (this, "The trigger must have at least an action", "No actions!", JOptionPane.ERROR_MESSAGE);
				return;
			} else if (lsConditionList.getLength () == 0) {
				if (JOptionPane.showConfirmDialog (this, "The trigger doesn't have a condition. Do you want a \"World Initialize\" condition so it will fire at startup?", "No conditions", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE) != 0) {
					return;
				}
				lsConditionList.add (new Condition (ConditionType.WORLD_INITIALIZE, null));
			}
			if (element == null) {
				element = new Trigger (ID, ckActive.isSelected (), ckFireOnce.isSelected (), lsConditionList.getVector (), lsActionList.getVector ());
			} else {
				element.setValues (ID, ckActive.isSelected (), ckFireOnce.isSelected (), lsConditionList.getVector (), lsActionList.getVector ());
			}
			dispose ();
		}
	}


}