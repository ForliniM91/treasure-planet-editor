package gui.editors;

import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

import data.world.Team;
import entities.Race;
import gui.TeamsMenu;
import gui.elements.IDPanel;
import gui.elements.JComboBoxEx;
import gui.exceptions.NewElementException;
import logic.Core;

/**
 * An editor to create/edit a {@link Team}
 *
 * @author MarcoForlini
 */
public class TeamsMenuEditor extends EntriesListMenuEditor <Team> {

	private IDPanel <Team>			idPanel;
	private JComboBoxEx <String>	cbTeamName	= new JComboBoxEx<> ();
	private JComboBoxEx <Race>		cbTeamRace	= new JComboBoxEx<> ();
	private JCheckBox				ckLockRace	= new JCheckBox ("Lock race");


	/**
	 * Creates a new {@link TeamsMenuEditor} to create a new {@link Team}
	 *
	 * @param parent the parent window
	 */
	public TeamsMenuEditor (TeamsMenu parent) {
		super (parent, 420, 300);
		if (TeamsMenu.teamNames.length == 0) {
			throw new NewElementException ("No available name for the teams");
		}
		initialize (true);
		cbTeamName.setSelectedIndex (0);
		cbTeamRace.setSelectedIndex (0);
		ckLockRace.setSelected (false);
		setVisible (true);
	}


	/**
	 * Creates a new {@link TeamsMenuEditor} to edit a {@link Team}
	 *
	 * @param parent the parent window
	 * @param team the Team to edit
	 */
	public TeamsMenuEditor (TeamsMenu parent, Team team) {
		super (parent, team, 420, 300);
		initialize (false);
		cbTeamName.setSelectedItem (team.getName ());
		cbTeamRace.setSelectedItem (team.getRace ());
		ckLockRace.setSelected (team.isLockRace ());
		setVisible (true);
	}


	@Override
	public void initialize (boolean newElement) {
		idPanel = new IDPanel<> (element, 10, 7);
		add (idPanel);

		JLabel lTeamName = new JLabel ("Name");
		lTeamName.setToolTipText ("Select the string containing the name of the team");
		lTeamName.setBounds (10, 36, 59, 14);
		lTeamName.setLabelFor (cbTeamName);
		add (lTeamName);

		cbTeamName.setVector (TeamsMenu.teamNames);
		cbTeamName.setToolTipText ("Select the string containing the name of the team");
		cbTeamName.setBounds (79, 33, 218, 20);
		add (cbTeamName);

		JButton bNewTeamName = new JButton ("New name");
		bNewTeamName.setToolTipText ("Add a new team name to the list");
		bNewTeamName.addActionListener (e -> {
			// TODO
		});
		bNewTeamName.setMargin (new Insets (2, 5, 2, 5));
		bNewTeamName.setBounds (307, 32, 77, 23);
		add (bNewTeamName);

		JLabel lTeamRace = new JLabel ("Race");
		lTeamRace.setToolTipText ("Select the race for this team (only effective for Hystorical maps, if \"Lock race\" is enabled)");
		lTeamRace.setBounds (10, 61, 59, 14);
		lTeamRace.setLabelFor (cbTeamRace);
		add (lTeamRace);

		cbTeamRace.setVector (Race.values ());
		cbTeamRace.setToolTipText ("Select the race for this team (only effective for Hystorical maps, if \"Lock race\" is enabled)");
		cbTeamRace.setBounds (79, 58, 218, 20);
		add (cbTeamRace);

		ckLockRace.setToolTipText ("All team members must to use the above race (only effective for Hystorical maps)");
		ckLockRace.setBounds (10, 82, 97, 23);
		add (ckLockRace);
	}


	@Override
	public void saveElement () {
		String ID = idPanel.getID ();
		boolean newElement = (element == null);
		if (Core.checkID (ID, element, false)) {
			if (Core.checkString ((String) cbTeamName.getSelectedItem (), element, lsList)) {
				if (newElement) {
					element = new Team (ID, (String) cbTeamName.getSelectedItem (), (Race) cbTeamRace.getSelectedItem (), ckLockRace.isSelected ());
				} else {
					element.setValues (ID, (String) cbTeamName.getSelectedItem (), (Race) cbTeamRace.getSelectedItem (), ckLockRace.isSelected ());
				}
				dispose ();
			}
		}
	}

}