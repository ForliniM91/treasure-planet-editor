package gui;

import java.awt.Window;

import data.world.Timer;
import gui.editors.TimersMenuEditor;

/**
 * A menu to manipulate the timers
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class TimersMenu extends EntriesListMenu <Timer> {

	/**
	 * Creates a new {@link TimersMenu}
	 *
	 * @param parent The parent window
	 */
	public TimersMenu (Window parent) {
		super (parent, "timer", Timer.timers, true);
	}

	@Override
	public Timer newElementEvent () throws IllegalStateException {
		TimersMenuEditor editor = new TimersMenuEditor (this);
		return editor.element;
	}

	@Override
	public boolean editElementEvent (Timer element) {
		new TimersMenuEditor (this, element);
		return true;
	}

}
