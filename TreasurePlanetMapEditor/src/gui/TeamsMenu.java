package gui;

import java.awt.Window;
import java.awt.event.ActionEvent;

import data.world.Team;
import gui.editors.TeamsMenuEditor;
import gui.exceptions.EditElementException;
import gui.exceptions.NewElementException;
import logic.Core;


/**
 * A menu to manipulate teams
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class TeamsMenu extends EntriesListMenu <Team> {

	/** Team defined in the map. */
	public static String[] teamNames = new String[] { "Team1", "Team2", "Team3", "Team4", "Team5", "Team6", "Team7", "Team8" };

	/**
	 * Creates a new {@link TeamsMenu}
	 *
	 * @param parent The parent window
	 */
	public TeamsMenu (Window parent) {
		super (parent, "team", Team.teams, true);
	}


	@Override
	public Team newElementEvent () {
		try {
			TeamsMenuEditor editor = new TeamsMenuEditor (this);
			return editor.element;
		} catch (NewElementException e) {
			Core.printError (this, e.getMessage (), "Error");
			return null;
		}
	}

	@Override
	public boolean editElementEvent (Team element) {
		try {
			new TeamsMenuEditor (this, element);
			return true;
		} catch (EditElementException e) {
			return false;
		}
	}

	@Override
	public boolean deleteElementEvent (ActionEvent e, Team element) {
		if (super.deleteElementEvent (e, element)) {
			Core.checkTeams (element);
			return true;
		}
		return false;
	}

}
