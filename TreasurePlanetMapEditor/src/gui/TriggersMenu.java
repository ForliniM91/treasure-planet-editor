package gui;

import java.awt.Window;

import data.world.Trigger;
import gui.editors.TriggersMenuEditor;

/**
 * A menu to manipulate the triggers
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class TriggersMenu extends EntriesListMenu <Trigger> {

	/**
	 * Creates a new {@link TriggersMenu}
	 *
	 * @param parent
	 */
	public TriggersMenu (Window parent) {
		super (parent, "trigger", Trigger.triggers, true, 600, 400);
	}

	@Override
	public Trigger newElementEvent () {
		TriggersMenuEditor editor = new TriggersMenuEditor (this);
		return editor.element;
	}

	@Override
	public boolean editElementEvent (Trigger trigger) {
		new TriggersMenuEditor (this, trigger);
		return true;
	}

}