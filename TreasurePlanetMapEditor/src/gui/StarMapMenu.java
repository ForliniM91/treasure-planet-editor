package gui;

import java.awt.Window;

/**
 * A menu to manipulate the starmap
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("serial")
public class StarMapMenu extends GenericDialog {

	/**
	 * Creates a new {@link StarMapMenu}
	 *
	 * @param parent the parent window
	 */
	public StarMapMenu (Window parent) {
		super (parent, "Star map", 420, 300);
		setVisible (true);
	}

	// TODO

}
