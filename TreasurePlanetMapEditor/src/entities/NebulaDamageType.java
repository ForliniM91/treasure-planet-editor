package entities;

/**
 * Type of damage caused by a Nebula
 *
 * @author MarcoForlini
 */
public enum NebulaDamageType {

	/** Lightning damage */
	Lightning,

	/** Wind damage */
	Wind,

	/** Meteor damage */
	Meteor

}
