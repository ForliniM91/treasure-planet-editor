package entities;

import java.awt.Color;

import interfaces.WriteCode;
import logic.Core;


/**
 * Color used by the game
 *
 * @author MarcoForlini
 */
public class GameColor extends Color implements WriteCode {

	private static final long		serialVersionUID	= -4822843177813485996L;

	/** Color used in texts */
	public static final GameColor	textColor			= new GameColor (0.784314F, 0.474510F, 0.956863F, 1F);

	/**
	 * Create a new GameColor from the RGBA values
	 *
	 * @param r Red value
	 * @param g Green value
	 * @param b Blue value
	 * @param a Alpha value
	 */
	public GameColor (float r, float g, float b, float a) {
		super (r, g, b, a);
	}

	/**
	 * Create a new random GameColor
	 */
	public GameColor () {
		super (Core.RANDOM.nextFloat (), Core.RANDOM.nextFloat (), Core.RANDOM.nextFloat ());
	}

	/**
	 * Create a new GameColor from a Color object.
	 *
	 * @param color The color object.
	 */
	public GameColor (Color color) {
		super (color.getRed (), color.getGreen (), color.getBlue (), color.getAlpha ());
	}

	/**
	 * Gets the opposite of this color
	 *
	 * @return the opposite of this color
	 */
	public GameColor getOpposite () {
		return new GameColor (255 - getRed (), 255 - getGreen (), 255 - getBlue (), getAlpha ());
	}

	/**
	 * Get the opposite of the given color
	 *
	 * @param c the color
	 * @return the opposite of the given color
	 */
	public static Color getOpposite (Color c) {
		return new Color (255 - c.getRed (), 255 - c.getGreen (), 255 - c.getBlue (), c.getAlpha ());
	}

	@Override
	public CharSequence toCode (int indentation) {
		return "( " + toFloat6 (getRed () / 255f) + ", " + toFloat6 (getGreen () / 255f) + ", " + toFloat6 (getBlue () / 255f) + ", " + toFloat6 (getAlpha () / 255f) + " )";
	}

}
