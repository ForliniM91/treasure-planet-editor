package entities;

import interfaces.FieldEnum;


/**
 * AI Stances
 * 
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum AIStance implements FieldEnum {

	AI_STANCE ("Default", "AI STANCE"),
	AGGRESSIVE ("Aggressive", "Stance_Aggressive"),
	DEFENSIVE ("Defensive", "Stance_Defensive"),
	NEUTRAL ("Neutral", "Stance_Neutral"),
	PERSISTANT ("Persistant", "Stance Persistant"),
	DUMMY ("Dummy", "Stance_Dummy"),
	CAPTAIN ("Captain human", "Captain_Human"); // ONLY FOR 'Set Group/Unit AI Captain'



	/** AI name */
	private String	name;
	/** AI code */
	private String	code;

	/**
	 * Create a new AI stance
	 *
	 * @param name AI name
	 * @param code AI code
	 */
	private AIStance (String name, String code) {
		this.name = name;
		this.code = code;
	}

	@Override
	public String toString () {
		return name;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return code;
	}

}
