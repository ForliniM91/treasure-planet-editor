package entities;

/**
 * Task state
 * 
 * @author MarcoForlini
 */
public enum TaskState {

	/** Task not yet activated */
	NOTACTIVE,

	/** Task activated */
	ACTIVE,

	/** Task completed */
	COMPLETED,

	/** Task failed */
	FAILED

}
