package entities;

import interfaces.FieldEnum;


/**
 * Formation type
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum FormationType implements FieldEnum {

	NONE ("None", "None"),
	CONVOY ("Convoy", "Convoy"),
	COLUMN ("Column", "Column"),
	DIAMOND ("Diamond", "Diamond"),
	LINEABREAST ("Line abreast", "LineAbreast"),
	ECHELONLEFT ("Echelon left", "EchelonLeft"),
	ECHELONRIGHT ("Echelon right", "EchelonRight"),
	CUSTOM ("Custom", "Custom"),
	;


	/** Formation name */
	public String	name;

	/** Formation code */
	public String	code;

	/**
	 * Create a new FormationType
	 *
	 * @param name Formation name
	 * @param code Formation code
	 */
	private FormationType (String name, String code) {
		this.name = name;
		this.code = code;
	}


	@Override
	public String toCode (int indentation) {
		return code;
	}

	@Override
	public String toString () {
		return name;
	}

}
