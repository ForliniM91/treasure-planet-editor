package entities;

import interfaces.TriggerCategory;

/**
 * The categories which group the {@link ConditionType}
 *
 * @author MarcoForlini
 */
public enum ConditionCategory implements TriggerCategory <ConditionType> {

	/** Conditions relative to the world */
	WORLD ("World", new ConditionType[] {
			ConditionType.WORLD_INITIALIZE,
			ConditionType.SKIRMISH_GAME_COMPLETE,
			ConditionType.TEAM_GAME_COMPLETE,
			ConditionType.NO_HUMAN_SHIPS,
			ConditionType.FLAG,
			ConditionType.TIMER,
			ConditionType.STARMAP_OPEN,
			ConditionType.MORTAR_EXPLODE_IN_AREA
	}),

	/** Conditions relative to the players and teams */
	PLAYER ("Player", new ConditionType[] {
			ConditionType.PLAYER_KILLED_OBJECT,
			ConditionType.PLAYER_VS_PLAYER_CAPTURE_COUNT,
			ConditionType.PLAYER_HAS_NO_LIFEBOAT,
			ConditionType.TEAM_DESTROY_SHIP,
			ConditionType.TEAM_CAPTURE_SHIP,
			ConditionType.TEAM_ENTER_VOLUME,
			ConditionType.TEAM_HAS_X_POINTS,
			ConditionType.TEAM_HAS_NO_SHIPS,
			ConditionType.NO_TEAM_HAS_SHIPS
	}),

	/** Conditions relative to the groups actions */
	GROUP_ACTION ("Group/Unit actions", new ConditionType[] {
			ConditionType.GROUP_ATTACKING_GROUP,
			ConditionType.GROUP_UNDER_ATTACK,
			ConditionType.GROUP_DESTROYED,
			ConditionType.GROUP_ENTER_VOLUME,
			ConditionType.GROUP_EXIT_VOLUME,
			ConditionType.GROUP_IS_IN_VOLUME,
			ConditionType.GROUPS_ONE_ENTER_VOLUME,
			ConditionType.GROUPS_ALL_ENTER_VOLUME,
			ConditionType.GROUP_WITHIN_NEBULA,
			ConditionType.GROUP_DOCKED,
			ConditionType.GROUP_IS_DOCKED,
			ConditionType.GROUP_IS_TOWING_SHIP
	}),

	/** Conditions relative to the groups status */
	GROUP_STATUS ("Group/Unit status", new ConditionType[] {
			ConditionType.GROUP_VITAL_DAMAGE,
			ConditionType.GROUP_DAMAGE,
			ConditionType.GROUP_FIRED_X_SHOTS,
			ConditionType.GROUP_HIT_X_TIMES,
			ConditionType.GROUP_HIT_X_TIMES_BY_PLAYER,
			ConditionType.GROUP_HIT_X_TIMES_BY_PLAYER_EQUIVALENCE,
			ConditionType.GROUP_HAS_X_MEMBER,
			ConditionType.GROUP_CONTAINS_UNIT,
			ConditionType.GROUP_CONTAINS_NO_ESSENTIAL,
			ConditionType.GROUP_TO_GROUP_DISTANCE,
			ConditionType.GROUP_FLAG_TEXTURE
	}),

	/** Conditions relative to the multimedia events */
	MULTIMEDIA ("Multimedia", new ConditionType[] {
			ConditionType.SPEECH_EVENT_PLAYER_ONCE,
			ConditionType.SPEECH_EVENT_NOT_PLAYED
	}),
	;




	/** Category name */
	private String			name;

	/** Conditions in this category */
	private ConditionType[]	cond;

	/**
	 * Create a new TriggerCategory
	 *
	 * @param name Category name
	 * @param cond Conditions in this category
	 */
	private ConditionCategory (String name, ConditionType... cond) {
		this.name = name;
		this.cond = cond;
	}

	@Override
	public ConditionType[] getTypes () {
		return cond;
	}

	@Override
	public String toString () {
		return name;
	}
}
