package entities;

import interfaces.FieldEnum;


/**
 * Skill level
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum CrewSkillLevel implements FieldEnum {

	DEFAULT ("Default", "CREW SKILL LEVEL"),
	AVERAGE ("Average", "Average"),
	GREEN ("Green", "Green"),
	ELITE ("Elite", "Elite"),
	PLAYERS ("Players", "Players"),
	JIM ("Jim", "M01JimTBoat"),
	;



	/** Skill name */
	public String	name;

	/** Skill code */
	public String	code;

	/**
	 * Create a new CrewSkillLevel
	 *
	 * @param name Skill name
	 * @param code Skill code
	 */
	private CrewSkillLevel (String name, String code) {
		this.name = name;
		this.code = code;
	}

	@Override
	public String toString () {
		return name;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return code;
	}

}
