package entities;

/**
 * Races
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum Race {

	NAVY ("Navy", 0, true),
	PIRATE ("Pirate", 2, true),
	PROCYON ("Procyon", 3, true),
	NONE ("None", 4, false),
	;



	/** Race code */
	public String	name;

	/** Game index */
	public int		index;

	/** Is playable by human players? */
	public boolean	playable;

	/**
	 * Create a new Race
	 *
	 * @param name Race name
	 * @param index Game index
	 * @param playable Is playable by human players?
	 */
	private Race (String name, int index, boolean playable) {
		this.name = name;
		this.index = index;
		this.playable = playable;
	}

	@Override
	public String toString () {
		return (!playable ? "[X] " : "") + name;
	}

}
