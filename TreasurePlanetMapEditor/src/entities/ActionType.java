package entities;

import interfaces.TriggerElementType;

/**
 * The types of actions
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum ActionType implements TriggerElementType {

	// Hidden
	SETUP_SHIP (0, "*State Init* Setup Ship",
			"Initialize ship", "Initialize a declared ship.\nUsually used with a \"World Initialize\" condition, but can be used later in the game.",
			Field.WORLD_OBJECT_ID,
			Field.SHIP_NAME,
			Field.SHIP_PATH,
			Field.FOLLOW_MODE,
			Field.AI_STANCE,
			Field.PLAYER_OWNER,
			Field.PRIMARY_SHIP,
			Field.CREW_SKILL_LEVEL,
			Field.BOARDABLE,
			Field.LOCALIZED_SHIP_NAME),
	SETUP_ISLAND (1, "*State Init* Setup Island",
			"Initialize island", "Initialize a declared island.\nUsually used with a \"World Initialize\" condition, but can be used later in the game.",
			Field.WORLD_OBJECT_ID,
			Field.COMBAT_STRENGTH,
			Field.PLAYER_OWNER,
			Field.GUNNERY_LEVEL,
			Field.AI_STANCE),
	SETUP_NEBULA (2, "*State Init* Setup Nebula",
			"Initialize nebula", "Initialize a declared nebula.\nUsually used with a \"World Initialize\" condition, but can be used later in the game.",
			Field.WORLD_OBJECT_ID,
			Field.NEW_NEBULA_NAME,
			Field.POLYGON_NAME,
			Field.LIGHTNING,
			Field.LIGHTNING_RATE,
			Field.METEORS,
			Field.METEOR_RATE,
			Field.NEBULA_CLOUD_EFFECT,
			Field.SOLAR_STORM_EFFECT,
			Field.METEOR_SHOWER_EFFECT,
			Field.NEBULA_CLOUD_POINT_SET,
			Field.SOLAR_STORM_POINT_SET,
			Field.METEOR_SHOWER_POINT_SET,
			Field.WIND,
			Field.WIND_MAGNITUDE,
			Field.ENERGY_DRAIN,
			Field.NEBULA_OCCLUSION,
			Field.WIND_RATE,
			Field.AMBIENT_SOUND_DISTANCE),
	SETUP_ETHERIUM (3, "*State Init* Setup Etherium Current",
			"Initialize etherium current", "Initialize a declared etherium current.\nUsually used with a \"World Initialize\" condition, but can be used later in the game.",
			Field.WORLD_OBJECT_ID,
			Field.ETHERIUM_NAME,
			Field.ETHERIUM_PATH),



	// Setup
	SET_ASTEROID_BELT (0, "Setup Asteroid Belt",
			"Setup asteroid belt",
			"Initialize a declared asteroid belt.\nUsually used with a \"World Initialize\" condition, but can be used later in the game.",
			Field.GROUP_NAME,
			Field.PATH_NAME,
			Field.FOLLOW_MODE,
			Field.FIND_CLOSEST_POINT,
			Field.VELOCITY_UPPER,
			Field.VELOCITY_LOWER,
			Field.TUMBLE_UPPER,
			Field.TUMBLE_LOWER),
	SET_ANIMAL_FLOCK (1, "Setup Space Animal Flock",
			"Setup space animal",
			"Initialize a declared space animal.\nUsually used with a \"World Initialize\" condition, but can be used later in the game.",
			Field.GROUP_NAME,
			Field.PATH_NAME,
			Field.FOLLOW_MODE,
			Field.FIND_CLOSEST_POINT,
			Field.VELOCITY),
	SETUP_TEAM_OBJECTIVE (2, "Setup Team Objective",
			"(Skirmish) Setup team objective",
			"Initialize a declared team objective.\nUsually used with a \"World Initialize\" condition, but can be used later in the game.",
			Field.TEAM_NAME,
			Field.OBJECTIVE_POINT,
			Field.OBJECTIVE_TASK),



	// World
	GOTO_NEXT_LEVEL (0, "Goto Next Level",
			"(Campaign) Goto next level",
			"End this mission and advande to the specified mission.\nUnknow effect if used in skirmish maps",
			Field.WORLD,
			Field.DISPLAY_LOADING_SCREEN),
	END_GAME (1, "End Game",
			"(Skirmish) End game",
			"End this game and show the players stats.\nUnknow effect if used in campaign maps, but it should terminate them, since you can't specify the next level",
			Field.USE_CUSTOM_MESSAGE,
			Field.WINNER_CUSTOM_MESSAGE,
			Field.LOSER_CUSTOM_MESSAGE,
			Field.SHOW_STAT_SCREEN),
	SET_FLAG_ACTION (2, "Set Flag Action",
			"Set flag state",
			"Set the value for the flag variable",
			Field.FLAG_NAME,
			Field.BOOLEAN_VALUE),
	START_TIMER (3, "Start Timer",
			"Start timer",
			NONE,
			Field.TIMER_NAME),
	STOP_TIMER (4, "Stop Timer",
			"Stop timer",
			NONE,
			Field.TIMER_NAME),
	SET_IS_IN_CALYAN_ABYSS (5, "Set Is In Calyan Abyss",
			"Set is in Calyan abyss",
			"Unknow effect. Only used in missions 7 and 8",
			Field.BOOLEAN),
	SET_NEBULA_TO_HOLD_OBJECT (6, "Set Nebula to Lock Objects",
			"Toggle nebula \"Lock objects\"",
			"Nebula will trap the object inside it",
			Field.TRAP),
	SET_NEBULA_FADEOUT_DISTANCE (7, "Set Nebula Fadeout Distance",
			"Set nebula fadeout distance",
			"How far you can see the nebula",
			Field.FADE_OUT_DISTANCE),
	DARK_MATTER_EXPLOSION (8, "Mission 9 - Do Dark Matter Explosion",
			"Do dark matter explosion",
			"A big explosion! Better run away...",
			Field.AFFECTED_AREA),



	// Objective
	SET_OBJECTIVE_ACTIVE (0, "Set Objective Task Active State",
			"Set objective task active",
			"Activate/deactivate a task",
			Field.OBJECTIVE_TASK,
			Field.ACTIVE_STATE),
	SET_OBJECTIVE_COMPLETE (1, "Set Objective Task Complete State",
			"Set objective task complete",
			"Marks a task as completed/not completed",
			Field.OBJECTIVE_TASK,
			Field.COMPLETE_STATE),
	SET_OBJECTIVE_FAILED (2, "Set Objective Task Failed State",
			"Set objective task failed",
			"Marks a task as failed/not failed",
			Field.OBJECTIVE_TASK,
			Field.FAILED_STATE),
	SET_OBJECTIVE_POINT (3, "Set Current Objective Point",
			"Set objective to point",
			"The yellow arrow under the ship will point to this location",
			Field.OBJECTIVE_POINT),
	SET_OBJECTIVE_POINT_TO_SHIP (4, "Set Current Objective Point On Ship",
			"Set objective to ship",
			"The yellow arrow under the shipp will point to this ship",
			Field.GROUP_NAME),
	SET_OBJECTIVE_POINT_VISIBLE_STARMAP (5, "Set Current Objective Point Visible On Starmap",
			"Set objective point visible on starmap",
			"If true, the objective is visible on the starmap",
			Field.VISIBLE_ON_STARMAP),



	// Player & team
	SET_ALLIANCE_TO (0, "Set alliance between PlayerA and PlayerB to TRUE/FALSE",
			"Set alliance",
			"Set the alliance between 2 players",
			Field.PLAYER_A,
			Field.PLAYER_B,
			Field.SET_ALLIANCE_TO),
	SET_FLEET_FORMATION_TYPE (1, "Set FleetFormation Type",
			"Set fleet formation",
			NONE,
			Field.PLAYER_NAME,
			Field.FORMATION_TYPE,
			Field.HOLD_FORMATION),
	SET_FLEET_HOLD_FIRE (2, "Set Fleet Hold Fire",
			"Fleet hold fire",
			"The fleet will not attack or defend from enemies",
			Field.PLAYER_FLEET,
			Field.HOLD_FIRE),
	ADD_VICTORY_POINTS (3, "Add Victory Points for SinglePlayer",
			"(Campaign) Add victory points",
			"Add victory points to the player.\nUnknow effect if used in multiplayer maps, since you can't specify the player",
			Field.VICTORY_POINTS),
	GRANT_TEAM_X_POINTS (4, "Grant Team X Points",
			"Give X points to the team",
			"Do not confuse with Victory Points: these score points only counts in the current map.\nYou can check for these points with the \"Team has X points\" condition",
			Field.TEAM_NAME,
			Field.POINTS),
	TEAM_X_WINS (5, "Team X Wins",
			"Team X wins",
			NONE,
			Field.TEAM_NAME),
	REMAINING_TEAMS_WINS (6, "Remaining Team Wins",
			"All remaining teams wins",
			NONE),



	// Group actions
	SET_GROUP_FOLLOW_PATH (0, "Group to follow path",
			"Follow path",
			"Group or unit follow a path",
			Field.GROUP_NAME,
			Field.PATH_NAME,
			Field.FOLLOW_MODE,
			Field.FIND_CLOSEST_POINT),
	ATTACK_GROUP (1, "GroupA to attack GroupB",
			"Attack target",
			"Group or unit attack the defined target",
			Field.ATTACKER_GROUP,
			Field.TARGET_GROUP),
	RAM_GROUP (2, "GroupA to Ram GroupB",
			"Ram target",
			"Group or unit ram the defined target",
			Field.GROUP_NAME,
			Field.GROUP_UNIT_TARGET),
	DAMAGE_GROUP_UNIT (3, "Damage Group/Unit by X percent",
			"Damage group by X percent",
			"Instantly inflict the defined percent of damage to the group or unit.",
			Field.GROUP_NAME,
			Field.PERCENT),
	DESTROY (4, "Destroy Group/Unit",
			"Destroy group/unit",
			"Instantly destroy the group or unit",
			Field.GROUP_NAME),
	TOW_SHIP (5, "Tow Ship",
			"Tow ship",
			"Group or unit tow the defined target",
			Field.DOCKING_GROUP,
			Field.TARGET_TO_TOW),
	BREAK_TOW (6, "Break Tow",
			"Break tow",
			"Break the tow between the 2 groups",
			Field.TOWER_GROUP_A,
			Field.TOWER_GROUP_B),
	DOCK_SHIP (7, "Dock Ships",
			"Dock ships",
			"Group or unit dock to the defined dockable island or ship",
			Field.DOCKING_GROUP,
			Field.TARGET_TO_DOCK_TO),
	TELEPORT_GROUP_UNIT (8,
			"Teleport Group/Unit",
			"Teleport group/unit to",
			"The group or unit is instantly teleported to the defined point",
			Field.GROUP_UNIT_NAME,
			Field.POINT_SET_NAME),
	TELEPORT_LONGBOAT (9, "Mission 9 - Teleport Longboat",
			"Teleport to the main ship",
			"The group or unit is instantly moved to the main ship",
			Field.LONGBOAT_GROUP),
	TRANSFER_GROUP_TO_GROUP (10, "Transfer Group/Unit to Group",
			"Transfer group/unit to group",
			"The group or unit is placed in another group",
			Field.GROUP_UNIT_TO_TRANSFER,
			Field.TARGET_GROUP),



	// Group status
	SET_SHIP_NAME (0, "Set Ship Name",
			"Set ship name",
			NONE,
			Field.GROUP_UNIT_NAME,
			Field.NEW_SHIP_NAME),
	SET_GROUP_UNIT_PRIMARY_SHIP (1, "Set Fleet Primary Ship",
			"Set primary ship",
			"Apparently, it does set the main ship, but the campaign usually use the \"Set group/unit Captain\" action",
			Field.GROUP_UNIT_NAME),
	SET_GROUP_UNIT_AI_CAPTAIN (2, "Set Group/Unit AI Captain",
			"Set group/unit Captain",
			"It does set the main ship, but the campaign sometimes use the \"Set primary ship\" action (which seems equivalent)",
			Field.GROUP_UNIT_NAME,
			Field.AI_STANCE), // ALWAYS 'Captain_Human'
	SET_GROUP_UNIT_OWNER (3, "Set Group/Unit Owner",
			"Set group/unit owner",
			"The group or unit will be controlled by the new owner.\nAn example is the Fast Frigate given to you in the Mission 7",
			Field.GROUP_UNIT_NAME,
			Field.NEW_OWNER),
	SET_GROUP_THROTTLE_PERCENT (4, "Set Group Throttle Percent",
			"Set group current speed (percent)",
			NONE,
			Field.GROUP_NAME,
			Field.THROTTLE),
	SET_GROUP_THROTTLE_PERCENT_MAX (5, "Set Max Throttle Percent (Max user settable)",
			"Set group max speed (percent)",
			"Set max speed for the group or unit",
			Field.GROUP_NAME,
			Field.THROTTLE),
	SET_GROUP_TOP_SPEED_PERCENT (6, "Set Ship Top Speed Percentage",
			"Set group top speed (percent)",
			"Set max speed for the group or unit\n(Duplicate for the \"Set group max speed (percent)\" action, and only used in Mission 9",
			Field.GROUP_NAME,
			Field.THROTTLE),
	SET_GROUP_VELOCITY (7, "Set Group Space Objects velocity",
			"Set group velocity",
			"Set the speed for any space object but ships (asteroids, animals...)",
			Field.GROUP_NAME,
			Field.VELOCITY),
	SET_GROUP_BOARDABLE (8, "Set Group/Unit Boardable",
			"Set group/unit is boardable",
			"If false, the group or unit can't be captured",
			Field.GROUP_NAME,
			Field.BOOLEAN),
	SET_GROUP_TOWABLE (9, "Set Group/Unit Towable",
			"Set group/unit is towable",
			"If false, the group or unit can't be towed",
			Field.GROUP_NAME,
			Field.BOOLEAN),
	SET_GROUP_MOVABLE (10, "Set Group/Unit Movable",
			"Set group/unit is movable",
			"If false, the group or unit is locked in place.\nNeither collisions, explosion or big bangs can move it.",
			Field.GROUP_NAME,
			Field.BOOLEAN),
	SEG_GROUP_COLLIDABLE (11, "SetCollidable",
			"Set group/unit is collidable",
			"If false, remove the collision from the group or unit",
			Field.GROUP_UNIT_NAME,
			Field.BOOLEAN),
	SET_GROUP_VISIBILITY (12, "Set Group/Unit Visibility",
			"Set group/unit is visible",
			"If false, the group or unit is invisible",
			Field.GROUP_UNIT_NAME,
			Field.BOOLEAN),
	SET_GROUP_VULNERABILITY (13, "Set Group/Unit Vulnerability",
			"Set group/unit is vulnerable",
			"WARNING: Basically, the unit health (and the sections health) can't drop below this value.\nThis mean that even the smallest value will make the unit invulnerable (and the sections too)",
			Field.GROUP_NAME,
			Field.INVULNERABLE_PERCENT),
	SET_GROUP_DOCKABLE (14, "Set Group/Unit Dockable",
			"Set group/unit is dockable",
			"Can make a ship dockable (Like when docking the lifeboat to the frigate in Mission 9) or make a ship or island not dockable",
			Field.GROUP_NAME,
			Field.BOOLEAN),
	TOGGLE_ISLAND_REPAIR (15, "Toggle Island Repair When Docked",
			"Set group/unit can repair",
			"If false, the island/ship will remain dockable but will not repair the docking ship",
			Field.GROUP_NAME,
			Field.REPAIR_WHEN_DOCKED),
	SET_GROUP_UNIT_ESSENTIAL (16, "Set Group/Unit Mission Essential",
			"Set group/unit is essential to mission",
			"WARNING: Destroying an essential ships won't end the game, but you can use \"Group/Unit contains no mission essential ships\" condition to check if an essential group or unit is destroyed, and eventually end the game.",
			Field.GROUP_NAME,
			Field.BOOLEAN),
	SET_GROUP_UNIT_WARNING_SHOT (17, "Set Group/Unit Warning Shot Mode",
			"Set group/unit warning shot mode",
			"Apparently, this should make all bullets do no damage.\nOnly used in Mission 5, against the fishing boats",
			Field.GROUPUNIT,
			Field.BOOLEAN),
	SET_GROUP_UNIT_CLOAK_STATE (18, "Set Group/Unit Cloak State",
			"Set group/unit is cloacked",
			"Cloak the unit like the procyon submersible",
			Field.GROUP_NAME,
			Field.CLOAK_STATE,
			Field.AI_CAN_OVERRIDE),
	SET_DOCK_TIME (19, "Set Dock Time",
			"Set dock time",
			"How much time the group or unit need to repair all damage when docking",
			Field.DOCKING_GROUP_UNIT,
			Field.DOCK_TIME),
	SET_GROUP_UNIT_BANNER_TYPE (20, "Set Ship Banner Type",
			"Set ship banner",
			NONE,
			Field.GROUP_UNIT_NAME,
			Field.BANNER_TYPE),
	SET_SHIP_FLAG_TEXTURE (21, "Set Ships Flag Texture",
			"Set ship flag image",
			"Change the flag image for the group or unit",
			Field.GROUP_NAME,
			Field.SHIP_FLAG_TEXTURE),
	SET_LIFEBOAT_CREATION (22, "Set Lifeboat Creation State",
			"Set group/unit have lifeboat",
			"If false, the group or unit will not release the lifeboats when destroyed",
			Field.GROUP_NAME,
			Field.BOOLEAN),
	SET_GROUP_UNIT_BORDER (23, "Set Group/Unit Border Zone",
			"Set group/unit border area",
			"Set a border: group or unit can't leave this area",
			Field.GROUP_UNIT_NAME,
			Field.POLYGON_NAME),
	SET_GROUP_UNIT_BORDER_RETURN_POINT (24, "Set Group/Unit Border Zone Return Point",
			"Set group/unit border area - return point",
			"If the group or unit try to trepass the border, the game will bring them back to this point",
			Field.GROUP_UNIT_NAME,
			Field.POINTSET_NAME),
	CLEAR_GROUP_UNIT_BORDER (25, "Clear Group/Unit Border Zone",
			"Clear group/unit border area",
			"Remove the border for this group or unit",
			Field.GROUP_UNIT_NAME),
	RESET_SHOTS_FIRED_COUNT (26, "Reset Shots Fired Count",
			"Reset shots fired count",
			"Reset the internal counter \"Bullets fired\"",
			Field.GROUP_UNIT_NAME),
	RESET_HIT_COUNT (27, "Reset Hit Count",
			"Reset hit count",
			"Reset the internal counter \"Bullets landed\"",
			Field.GROUP_UNIT_NAME),



	// Group AI
	SET_GROUP_UNIT_AI_STANCE (0, "Set Group/Unit AI Stance",
			"Set AI stance",
			"WARNING: Don't use for dragons.\nThey have their AI Stance action",
			Field.GROUP_UNIT_NAME,
			Field.AI_STANCE),
	SET_DRAGON_AI_STANCE (1, "Dragon - Set AI Stance",
			"Set AI stance for dragons",
			"WARNING: Only use for dragons.\nOther units have a generic AI Stance action.",
			Field.GROUP_UNIT_NAME,
			Field.AI_STANCE),
	SET_DRAGON_DAMAGE_THRESHOLD (2, "Dragon - Set Damage Threshold",
			"Set dragon damage Threshold",
			"Percent of damage you must inflict to dragons before they stop attacking and fly away",
			Field.GROUP_UNIT_NAME,
			Field.DAMAGE_THRESHOLD),
	CLEAR_ALL_AI_COMMANDS (3, "Clear all AI commands",
			null,
			NONE,
			Field.GROUP_UNIT_NAME),
	SET_GROUP_HOLD_POSITION (4, "Set Group/Unit Hold Position",
			"Set group/unit hold position",
			"Group or unit will attack and defend but will not follow the enemies",
			Field.GROUP_UNIT_NAME,
			Field.BOOLEAN),



	// Multimedia
	PLAY_MUSIC_TRACK (0, "Play Music Track",
			"Play music track",
			"Set the background music",
			Field.FILE_NAME,
			Field.CROSSFADE_TRANSICTION,
			Field.FADE_OUT_TIME,
			Field.FADE_IN_TIME,
			Field.NEW_VOlUME),
	PLAY_SPEECH_EVENT (1, "Play speech event",
			"Play a dialog",
			NONE,
			Field.SPEECH_EVENT_NAME),
	PLAY_SOUND (2, "Play Special Effect",
			"Play a sound",
			NONE,
			Field.FILE_NAME,
			Field.PLAY_AS_DIALOG),
	TOGGLE_EVENT_EFFECT (3, "Create/Release Event Effect",
			"Play/Stop effect",
			"Play a graphic effect in the defined point or stop it",
			Field.POINT_SET,
			Field.EFFECT_NAME,
			Field.BOOLEAN_STATE),
	TOGGLE_CREW_SPEECH (4, "Crew Speech - Toggle On Off",
			"Toggle crew can talk",
			"If false, mute all crew dialogs",
			Field.CREW_SPEECH_STATE),
	CREW_SPEECH_HELM_OFF_COURSE (5, "Crew Speech - Helm Off Course",
			"Play crew dialog: Helm off course",
			"The crew will say: \"Helm off course\""),



	// GUI
	OPEN_CREW_ARM_SCREEN (0, "Open Crew and Arms Screens",
			"Open crew and arms screens",
			"Let the player change the equipment and the crew.\nUnknow effect if used in multiplayer maps, since you can't specify the player"),
	OPEN_WEAPON_BAR (1, "Open Weapon Bar",
			"Open/Close weapon bar",
			"Unknow effect if used in multiplayer maps, since you can't specify the player",
			Field.OPENSTATE),
	OPEN_HUD_TEXTURE_OVERLAY (2, "Open HUD Texture Overlay",
			"Show texture overlay",
			"Show a texture on the screen.\nYou can see the effect on the tutorial map.\nUnknow effect if used in multiplayer maps, since you can't specify the player",
			Field.TEXTURE_BASE_NAME),
	CLOSE_HUD_TEXTURE_OVERLAY (3, "Close HUD Texture Overlay",
			"Remove texture overlay",
			"Remove any added texture from the screen"),
	SET_RADAR_ACTIVE (4, "Set Radar Active State",
			"Set radar active",
			"If false, it deactivate the radar, as in the first 2 missions.\nUnknow effect if used in multiplayer maps, since you can't specify the player",
			Field.RADAR_ACTIVE),
	SET_MAP_TEXT_VISIBILITY (5, "Set Map Text Visibility",
			"Set map text visibility",
			"Show/Hide a text on the starmap",
			Field.MAP_TEXT_NAME,
			Field.VISIBLE_ON_STARMAP),



	// Camera
	START_NIS (1, "NIS Start",
			"Start NIS (Non Interactive Sequence)",
			"Start a NIS.\nBlack bars appear and you can use the actions below to move the camera freely",
			Field.NIS_ALL_OBJECT_VISIBLE,
			Field.NIS_OPEN_NIS_BAR),
	END_NIS (12, "NIS End",
			"End NIS (Non Interactive Sequence)",
			"End the NIS.\nThe black bars disappear and the game continue"),
	NIS_TOGGLE_ALL_OBJECT_VISIBILE (2, "NIS Toggle All Objects Visibility",
			"(NIS) Toggle all objects visibility",
			"Makes all object in the map visibile",
			Field.NIS_ALL_OBJECT_VISIBLE),
	NIS_SET_CAMERA_PATH (3, "NIS Set Camera Path",
			"(NIS) Set camera path",
			"The camera will follow a path",
			Field.PATH_NAME,
			Field.NIS_JUMP_TO_START),
	NIS_FOCUS_CAMERA_ON_MAIN_SHIP (4, "NIS Focus on Main Ship",
			"(NIS) Focus camera on main ship",
			"The camera focus on the main ship"),
	FOCUS_CAMERA_ON_GROUP (0, "Focus Camera On Group",
			"Focus camera on group",
			"Camera will move to the specified unit",
			Field.GROUPUNIT,
			Field.DISTANCE,
			Field.RELATIVE_ANGLE,
			Field.USE_TRANSITION),
	NIS_FOCUS_CAMERA_ON_GROUP_UNIT (5, "NIS Focus camera on Group/Unit",
			"(NIS) Focus camera on group/unit",
			"The camera focus on the defined group or unit",
			Field.GROUPUNIT),
	NIS_FOCUS_CAMERA_ON_POINT (6, "NIS Focus camera on Point",
			"(Nis) Focus camera on point",
			"The camera focus on the defined point",
			Field.POINTSET_NAME),
	NIS_POSITION_CAMERA_RELATIVE_TO_OBJECT (7, "NIS Position Camera Relative to Object",
			"(NIS) Position camera relative to object",
			"The camera point to an object with the defined distance and angle",
			Field.UNIT_NAME,
			Field.DISTANCE,
			Field.NIS_ANGLE_YZ,
			Field.NIS_ANGLE_XY,
			Field.NIS_JUMP_TO_POINT),
	NIS_ATTACH_CAMERA (8, "NIS Attach Camera",
			"(NIS) Attach camera to group/unit",
			"The camera point to a group or unit and follow his movements",
			Field.GROUP_UNIT_NAME,
			Field.DISTANCE,
			Field.NIS_ANGLE_YZ,
			Field.NIS_ANGLE_XY),
	NIS_SET_CAMERA_SPEED (9, "NIS Set Camera Speed",
			"(NIS) Set camera speed",
			NONE,
			Field.NIS_CAMERA_ACCEL,
			Field.NIS_CAMERA_MAX_VEL),
	NIS_SET_TRANSITION_CAMERA_SPEED (10, "NIS Set Transition Camera Speed",
			"(NIS) Set transition camera speed",
			NONE,
			Field.NIS_CAMERA_ACCEL,
			Field.NIS_CAMERA_MAX_VEL),
	NIS_ZOOM (11, "NIS Zoom",
			"(NIS) Zoom camera",
			NONE,
			Field.NIS_FOV,
			Field.NIS_SPEED)

	;


	/** Index */
	private int		index;

	/** Game code */
	private String	code;

	/** Displayed name */
	private String	name;

	/** Description */
	private String	desc;

	/** Fields required */
	private Field[]	fields;

	/**
	 * Create a new {@link ActionType}
	 *
	 * @param name Displayed name
	 * @param code Game code
	 * @param fields Fields required
	 */
	ActionType (int index, String code, String name, String desc, Field... fields) {
		this.index = index;
		this.code = code;
		if (name != null) {
			this.name = name;
		} else {
			this.name = Character.toUpperCase (code.charAt (0)) + code.substring (1).toLowerCase ();
		}
		if (desc != null) {
			this.desc = desc;
		} else {
			this.desc = this.name;
		}
		this.fields = fields;
	}

	@Override
	public String toString () {
		return name;
	}

	@Override
	public int getIndex () {
		return index;
	}

	@Override
	public String getCode () {
		return code;
	}

	@Override
	public String getName () {
		return name;
	}

	@Override
	public String getDesc () {
		return desc;
	}

	@Override
	public Field[] getFields () {
		return fields;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return Field.TYPE.toString () + " '" + code + '\'';
	}
}
