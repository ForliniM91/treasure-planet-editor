package entities;

import interfaces.TriggerCategory;

/**
 * The categories which group the {@link ActionType}
 *
 * @author MarcoForlini
 */
public enum ActionCategory implements TriggerCategory <ActionType> {

	/** Actions used at start up */
	SETUP ("Setup", new ActionType[] {
			ActionType.SET_ASTEROID_BELT,
			ActionType.SET_ANIMAL_FLOCK,
			ActionType.SETUP_TEAM_OBJECTIVE
	}),

	/** Actions which manipulate the world or the game */
	WORLD ("World", new ActionType[] {
			ActionType.GOTO_NEXT_LEVEL,
			ActionType.END_GAME,
			ActionType.SET_FLAG_ACTION,
			ActionType.START_TIMER,
			ActionType.STOP_TIMER,
			ActionType.SET_IS_IN_CALYAN_ABYSS,
			ActionType.SET_NEBULA_TO_HOLD_OBJECT,
			ActionType.SET_NEBULA_FADEOUT_DISTANCE,
			ActionType.DARK_MATTER_EXPLOSION
	}),

	/** Actions which manipulate the objectives */
	OBJECTIVES ("Objectives", new ActionType[] {
			ActionType.SET_OBJECTIVE_ACTIVE,
			ActionType.SET_OBJECTIVE_COMPLETE,
			ActionType.SET_OBJECTIVE_FAILED,
			ActionType.SET_OBJECTIVE_POINT,
			ActionType.SET_OBJECTIVE_POINT_TO_SHIP,
			ActionType.SET_OBJECTIVE_POINT_VISIBLE_STARMAP
	}),

	/** Actions which manipulate a player */
	PLAYER ("Player", new ActionType[] {
			ActionType.SET_ALLIANCE_TO,
			ActionType.SET_FLEET_FORMATION_TYPE,
			ActionType.SET_FLEET_HOLD_FIRE,
			ActionType.ADD_VICTORY_POINTS,
			ActionType.GRANT_TEAM_X_POINTS,
			ActionType.REMAINING_TEAMS_WINS,
			ActionType.TEAM_X_WINS
	}),

	/** Actions which manipulate a group actions */
	GROUP_ACTION ("Group/Unit actions", new ActionType[] {
			ActionType.SET_GROUP_FOLLOW_PATH,
			ActionType.ATTACK_GROUP,
			ActionType.RAM_GROUP,
			ActionType.DAMAGE_GROUP_UNIT,
			ActionType.DESTROY,
			ActionType.TOW_SHIP,
			ActionType.BREAK_TOW,
			ActionType.DOCK_SHIP,
			ActionType.TELEPORT_GROUP_UNIT,
			ActionType.TELEPORT_LONGBOAT,
			ActionType.TRANSFER_GROUP_TO_GROUP
	}),

	/** Actions which manipulate a group attributes */
	GROUP_STATUS ("Group/Unit status", new ActionType[] {
			ActionType.SET_SHIP_NAME,
			ActionType.SET_GROUP_UNIT_PRIMARY_SHIP,
			ActionType.SET_GROUP_UNIT_AI_CAPTAIN,
			ActionType.SET_GROUP_UNIT_OWNER,
			ActionType.SET_GROUP_THROTTLE_PERCENT,
			ActionType.SET_GROUP_THROTTLE_PERCENT_MAX,
			ActionType.SET_GROUP_TOP_SPEED_PERCENT,
			ActionType.SET_GROUP_VELOCITY,
			ActionType.SET_GROUP_BOARDABLE,
			ActionType.SET_GROUP_TOWABLE,
			ActionType.SET_GROUP_MOVABLE,
			ActionType.SEG_GROUP_COLLIDABLE,
			ActionType.SET_GROUP_VISIBILITY,
			ActionType.SET_GROUP_VULNERABILITY,
			ActionType.SET_GROUP_DOCKABLE,
			ActionType.TOGGLE_ISLAND_REPAIR,
			ActionType.SET_GROUP_UNIT_ESSENTIAL,
			ActionType.SET_GROUP_UNIT_WARNING_SHOT,
			ActionType.SET_GROUP_UNIT_CLOAK_STATE,
			ActionType.SET_DOCK_TIME,
			ActionType.SET_GROUP_UNIT_BANNER_TYPE,
			ActionType.SET_SHIP_FLAG_TEXTURE,
			ActionType.SET_LIFEBOAT_CREATION,
			ActionType.SET_GROUP_UNIT_BORDER,
			ActionType.SET_GROUP_UNIT_BORDER_RETURN_POINT,
			ActionType.CLEAR_GROUP_UNIT_BORDER,
			ActionType.RESET_SHOTS_FIRED_COUNT,
			ActionType.RESET_HIT_COUNT
	}),

	/** Actions which manipulate a group AI */
	AI ("Artificial Intelligence", new ActionType[] {
			ActionType.SET_GROUP_UNIT_AI_STANCE,
			ActionType.SET_DRAGON_AI_STANCE,
			ActionType.SET_DRAGON_DAMAGE_THRESHOLD,
			ActionType.CLEAR_ALL_AI_COMMANDS,
			ActionType.SET_GROUP_HOLD_POSITION
	}),

	/** Actions which manipulate the music, dialogues and effects */
	MULTIMEDIA ("Multimedia", new ActionType[] {
			ActionType.PLAY_MUSIC_TRACK,
			ActionType.PLAY_SPEECH_EVENT,
			ActionType.PLAY_SOUND,
			ActionType.TOGGLE_EVENT_EFFECT,
			ActionType.TOGGLE_CREW_SPEECH,
			ActionType.CREW_SPEECH_HELM_OFF_COURSE
	}),

	/** Actions which manipulate the GUI */
	GUI ("Menu", new ActionType[] {
			ActionType.OPEN_CREW_ARM_SCREEN,
			ActionType.OPEN_WEAPON_BAR,
			ActionType.OPEN_HUD_TEXTURE_OVERLAY,
			ActionType.CLOSE_HUD_TEXTURE_OVERLAY,
			ActionType.SET_RADAR_ACTIVE,
			ActionType.SET_MAP_TEXT_VISIBILITY
	}),

	/** Actions which manipulate the camera */
	CAMERA ("Camera", new ActionType[] {
			ActionType.START_NIS,
			ActionType.END_NIS,
			ActionType.NIS_TOGGLE_ALL_OBJECT_VISIBILE,
			ActionType.NIS_SET_CAMERA_PATH,
			ActionType.NIS_FOCUS_CAMERA_ON_MAIN_SHIP,
			ActionType.FOCUS_CAMERA_ON_GROUP,
			ActionType.NIS_FOCUS_CAMERA_ON_GROUP_UNIT,
			ActionType.NIS_FOCUS_CAMERA_ON_POINT,
			ActionType.NIS_POSITION_CAMERA_RELATIVE_TO_OBJECT,
			ActionType.NIS_ATTACH_CAMERA,
			ActionType.NIS_SET_CAMERA_SPEED,
			ActionType.NIS_SET_TRANSITION_CAMERA_SPEED,
			ActionType.NIS_ZOOM
	});




	/** Category name */
	private String			name;

	/** Actions in this category */
	private ActionType[]	act;

	/**
	 * Create a new TriggerCategory
	 *
	 * @param name Category name
	 * @param act Actions in this category
	 */
	private ActionCategory (String name, ActionType... act) {
		this.name = name;
		this.act = act;
	}

	@Override
	public ActionType[] getTypes () {
		return act;
	}

	@Override
	public String toString () {
		return name;
	}
}
