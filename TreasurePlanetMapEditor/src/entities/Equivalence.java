package entities;

import interfaces.FieldEnum;


/**
 * Equivalence type
 * 
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum Equivalence implements FieldEnum {

	EQUAL ("=", "Equal To"),
	GREATER (">", "Greater Than"),
	LESSER ("<", "Less Than"),
	;



	/** Equivalence name */
	public String	name;

	/** Equivalence code */
	public String	code;

	/**
	 * Create a new Equivalence
	 *
	 * @param name Equivalence name
	 * @param code Equivalence code
	 */
	private Equivalence (String name, String code) {
		this.name = name;
		this.code = code;
	}

	@Override
	public String toString () {
		return code;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return code;
	}

}
