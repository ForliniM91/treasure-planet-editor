package entities;

import interfaces.TriggerElementType;

/**
 * The types of conditions
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum ConditionType implements TriggerElementType {

	// World
	WORLD_INITIALIZE (0, "World Initialize",
			null,
			"The map is initialized and the game is beginning.\n(in savegames, the world is already initialized, so the condition isn't true when loading a savegame"),
	SKIRMISH_GAME_COMPLETE (1, "Skirmish Game Complete",
			"Skirmish game is completed",
			"A player or team has destroyed all his enemies."),
	TEAM_GAME_COMPLETE (2, "Team Game Complete",
			"Team game is completed",
			"A team has destroyed all his enemies"),
	NO_HUMAN_SHIPS (3, "No Human Controlled Ships Remain",
			null,
			"There are no more ships controlled by a human player (either detroyed or captured)"),
	FLAG (4, "Flag Condition",
			"Flag value is",
			"Check the value for the flag variable",
			Field.FLAG_NAME,
			Field.BOOLEAN_VALUE),
	TIMER (5, "Timer Condition",
			"Timer has time",
			"Check the current time for the timer",
			Field.TIMER_NAME,
			Field.EQUIVALENCE,
			Field.TIME_IN_SECONDS),
	STARMAP_OPEN (6, "Is Starmap Open",
			"(Single player) Is starmap open",
			"True continuously until the map is closed\nUnknow effect in skirmish maps"),
	MORTAR_EXPLODE_IN_AREA (7, "Mission 9 - If Mortar explodes within Area",
			"Mortar explode in area",
			"A mortar bullet explode in the defined area",
			Field.VOLUME_NAME),


	// Player & team
	PLAYER_KILLED_OBJECT (0, "Player Killed A Object",
			"Player has killed/destroyed",
			"Player has just destroyed an object\n(the condition is true for every single object of this type)",
			Field.PLAYER_NAME,
			Field.WORLD_OBJECT_TYPE),
	PLAYER_VS_PLAYER_CAPTURE_COUNT (1, "Player Vs Player capture count",
			"Player A captured ships from Player B",
			"Player A has captured less/equal/more than X ships from Player B fleet",
			Field.PLAYER_NAME_A,
			Field.PLAYER_NAME_B,
			Field.EQUIVALENCE,
			Field.NUMBER_OF_CAPTURES),
	PLAYER_HAS_NO_LIFEBOAT (2, "Player has no lifeboats",
			null,
			"Useful when an enemy ship has been destroyed and you must capture the lifeboats",
			Field.PLAYER_NAME),
	TEAM_DESTROY_SHIP (3, "Team has destroyed a ship from Group/Unit",
			"Team destroy X ships from group/unit",
			NONE,
			Field.TEAM_NAME,
			Field.GROUP_UNIT,
			Field.NUMBER_SHIP_DESTROYED),
	TEAM_CAPTURE_SHIP (4, "Team has captured a ship from Group/Unit",
			"Team capture X ships from group/unit",
			NONE,
			Field.TEAM_NAME,
			Field.GROUP_UNIT,
			Field.NUMBER_OF_CAPTURES),
	TEAM_ENTER_VOLUME (5, "Team Member Enters Volume",
			"Team members enter in area",
			NONE,
			Field.TEAM_NAME,
			Field.VOLUME_NAME),
	TEAM_HAS_X_POINTS (6, "Team has X points",
			null,
			"Do not confuse with Victory Points: these are \"score\" points awarded with the action \"Give X points to the team\"",
			Field.TEAM_NAME,
			Field.POINTS),
	TEAM_HAS_NO_SHIPS (7, "Team X has no ships",
			null,
			NONE,
			Field.TEAM_NAME),
	NO_TEAM_HAS_SHIPS (8, "No Team Has Ships",
			null,
			NONE),




	// Group actions
	GROUP_ATTACKING_GROUP (0, "Is Group A attacking Group B",
			"Group A is attacking Group B",
			NONE,
			Field.GROUP_A_NAME,
			Field.GROUP_B_NAME),
	GROUP_UNDER_ATTACK (1, "Group Under Attack",
			"Group is under attack",
			NONE,
			Field.GROUP_NAME),
	GROUP_DESTROYED (2, "Group Destroyed",
			"Group has been killed/destroyed",
			"There are no units left in this group",
			Field.GROUP_NAME),
	GROUP_ENTER_VOLUME (3, "Enter Volume",
			"Group enter in area",
			"True everytime the group enter in the area",
			Field.GROUP_NAME,
			Field.VOLUME_NAME),
	GROUP_EXIT_VOLUME (4, "Exit Volume",
			"Group exit from area",
			"True everytime the group exit from the area",
			Field.GROUP_NAME,
			Field.VOLUME_NAME),
	GROUP_IS_IN_VOLUME (5, "Is Group In Volume",
			"Group is in area",
			"Continuously true while the group is in the area",
			Field.GROUP_NAME,
			Field.VOLUME_NAME,
			Field.ENTIRE_GROUP),
	GROUPS_ONE_ENTER_VOLUME (6, "Unit from Group enters trigger volume ( Once per Unit )",
			"A unit from the group enter in the area",
			"The condition is true everytime a unit from the group enter in the area.\n(5 units will make this condition true 5 times)",
			Field.GROUP_NAME,
			Field.VOLUME_NAME,
			Field.TEAM_NAME,
			Field.NUM_OBJECT_WAITING, // ALWAYS 0
			Field.NUM_OBJECT_ENTERED), // ALWAYS 0
	GROUPS_ALL_ENTER_VOLUME (7, "All Units from Group have entered trigger volume Once",
			"All units from group have entered in area once",
			"If all units have entered the area once in the game, this condition will be always true forever until the map ends.",
			Field.GROUP_NAME,
			Field.VOLUME_NAME,
			Field.PLAYER_NAME,
			Field.NUM_OBJECT_ENTERED), // ALWAYS 0
	GROUP_WITHIN_NEBULA (8, "Unit is Within any Nebula",
			"Group/Unit is withing any nebula",
			NONE,
			Field.UNIT_NAME),
	GROUP_DOCKED (9, "Group/Unit Docked",
			"Group/Unit has docked",
			"True when Group/Unit has docked",
			Field.DOCKERS_GROUP_UNIT_NAME,
			Field.TARGET_GROUP_UNIT_NAME),
	GROUP_IS_DOCKED (10, "Group/Unit Is Docked",
			"Group/Unit is docked",
			"Continuously true while the unit is on the dock area (no matter if is docking or just stand there)",
			Field.DOCKERS_GROUP_UNIT_NAME,
			Field.TARGET_GROUP_UNIT_NAME),
	GROUP_IS_TOWING_SHIP (11, "Is Ship In Tow",
			"Group/Unit A is towing ship B",
			NONE,
			Field.TOWING_GROUP,
			Field.TARGET_TO_TOW,
			Field.BOOLEAN),


	// Group status
	GROUP_VITAL_DAMAGE (0, "Group/Unit Vital Section has >,<,= X damage",
			"Group/Unit vital section are damaged",
			"True when the group or unit is damaged by X%.\nDoesn't include damage to sails and rudder (so, this is exactly: how much the green bar is drained)",
			Field.GROUP_UNIT_NAME,
			Field.VITAL_SECTION, // ALWAYS 'vitalToMission'
			Field.EQUIVALENCE,
			Field.DAMAGE_PERCENT),
	GROUP_DAMAGE (1, "Group/Unit has >,<,= X damage",
			"Group/Unit is damaged",
			"True when the group or unit is damaged by X%.\nInclude damage to sails and rudder.",
			Field.GROUP_UNIT_NAME,
			Field.EQUIVALENCE,
			Field.DAMAGE_PERCENT),
	GROUP_FIRED_X_SHOTS (2, "Group/Unit Fired X Shots",
			"Group/Unit fired X shots",
			NONE,
			Field.GROUP_UNIT_NAME,
			Field.NUMBER_ROUNDS_FIRED,
			Field.EQUIVALENCE),
	GROUP_HIT_X_TIMES (3, "Group/Unit Hit at least x Times",
			"Group/Unit is hit at least X times",
			"Within groups, it consided the sum of all hit, even splitted amongs different units in the group",
			Field.GROUP_UNIT_NAME,
			Field.NUMBER_TIMES_HIT),
	GROUP_HIT_X_TIMES_BY_PLAYER (4, "Player has hit Group/Unit at least x Times",
			"Group/Unit is hit at least X times by Player",
			"Within groups, it consided the sum of all hit, even splitted amongs different units in the group",
			Field.PLAYER_NAME,
			Field.GROUP_UNIT_NAME,
			Field.NUMBER_TIMES_HIT),
	GROUP_HIT_X_TIMES_BY_PLAYER_EQUIVALENCE (5, "Group/Unit Hit at least x Times by Player ( with eqivalence )",
			"Group/Unit is hit at least X times by Player (with equivalence)",
			"Within groups, it consided the sum of all hit, even splitted amongs many units in the group",
			Field.GROUP_UNIT_NAME,
			Field.NUMBER_TIMES_HIT,
			Field.EQUIVALENCE,
			Field.PLAYER_NAME),
	GROUP_HAS_X_MEMBER (6, "Group has X members",
			null,
			NONE,
			Field.GROUP_NAME,
			Field.EQUIVALENCE,
			Field.NUMBER),
	GROUP_CONTAINS_UNIT (7, "Does Group Contain Unit Name",
			"The group contais the specified unit",
			NONE,
			Field.UNIT_NAME,
			Field.EXISTS),
	GROUP_CONTAINS_NO_ESSENTIAL (8, "Group/Unit contains no mission essential ships",
			"Group/Unit contains no mission essential ships",
			"True if there are no mission essential ships in the group",
			Field.GROUP_NAME),
	GROUP_TO_GROUP_DISTANCE (9, "Group to Group Distance",
			"Distance between Group A and Group B is",
			"True if the distance between the groups is less/equal/more than the defined value",
			Field.GROUP_A_NAME,
			Field.EQUIVALENCE,
			Field.DISTANCE,
			Field.GROUP_B_NAME),
	GROUP_FLAG_TEXTURE (10, "Unit Flag Texture",
			"Group/Unit has flag image",
			"True if the ship (or all ships in the group) have this image on the flag",
			Field.UNIT_NAME,
			Field.FLAG_TYPE_SHIP,
			Field.BOOLEAN),



	// Multimedia
	SPEECH_EVENT_PLAYER_ONCE (0, "Speech Event Played Once",
			"Dialogue has been played",
			NONE,
			Field.SPEECH_EVENT_NAME),
	SPEECH_EVENT_NOT_PLAYED (1, "Speech Event Not Played Yet",
			"Dialogue has not played yet",
			NONE,
			Field.SPEECH_EVENT_NAME),

	;


	/** Index */
	public final int		index;

	/** Displayed name */
	public final String		name;

	/** Description */
	public final String		desc;

	/** Game code */
	public final String		code;

	/** Fields required */
	public final Field[]	fields;

	/**
	 * Create a new ConditionType
	 *
	 * @param code Game code
	 * @param name Displayed name
	 * @param desc Description
	 * @param fields Fields required
	 */
	ConditionType (int index, String code, String name, String desc, Field... fields) {
		this.index = index;
		this.code = code;
		if (name != null) {
			this.name = name;
		} else {
			this.name = Character.toUpperCase (code.charAt (0)) + code.substring (1).toLowerCase ();
		}
		if (desc != null) {
			this.desc = desc;
		} else {
			this.desc = this.name;
		}
		this.fields = fields;
	}

	@Override
	public String toString () {
		return name;
	}

	@Override
	public int getIndex () {
		return index;
	}

	@Override
	public String getCode () {
		return code;
	}

	@Override
	public String getName () {
		return name;
	}

	@Override
	public String getDesc () {
		return desc;
	}

	@Override
	public Field[] getFields () {
		return fields;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return Field.TYPE.toString () + " '" + code + '\'';
	}

}
