package entities;

import interfaces.FieldEnum;


/**
 * Follow mode
 * 
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum FollowMode implements FieldEnum {

	TO_END ("Follow to the end, stop", "to end"),
	LOOP ("Follow to the end, travel back to begin, repeat", "loop"),
	LOOP_TELEPORT ("Follow to the end, teleport back to begin, repeat", "teleport loop");



	/** FollowMode name */
	public String	name;
	/** FollowMode code */
	public String	code;

	/**
	 * Create a new FollowMode
	 *
	 * @param name FollowMode name
	 * @param code FollowMode code
	 */
	private FollowMode (String name, String code) {
		this.name = name;
		this.code = code;
	}

	@Override
	public String toString () {
		return name;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return code;
	}

}
