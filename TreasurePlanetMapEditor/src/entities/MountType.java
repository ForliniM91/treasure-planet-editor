package entities;

/**
 * Weapon mount type
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum MountType {

	Light ("Light  Mount"),
	Medium ("Medium Mount"),
	Heavy ("Heavy  Mount"),
	NS_Light ("Non-Selectable Light   Mount"),
	NS_Medium ("Non-Selectable Medium  Mount"),
	NS_Heavy ("Non-Selectable Heavy   Mount"), // Maybe
	NS_Special ("Non-Selectable Special Mount"),
	NS_DualLight ("Non-Selectable Dual Light  Mount"), // Maybe
	NS_DualMedium ("Non-Selectable Dual Medium Mount"),
	NS_DualHeavy ("Non-Selectable Dual Heavy  Mount"),
	;

	private String name;

	private MountType (String name) {
		this.name = name;
	}

	@Override
	public String toString () {
		return name;
	}

}
