package entities;

/**
 * Type of skills for the Crew
 *
 * @author MarcoForlini
 */
public enum SkillType {

	/** Improve the effective skills of the crew and boarding defense */
	Leadership,

	/** Improve the ship's maneuverability */
	Navigation,

	/** See distant objects */
	Spotting,

	/** Improve the ship's speed and repair speed */
	Engineering,

	/** Improve the ship's repair speed and boarding defense */
	Rigging,

	/** Improve the ship boarding attack and defense */
	Combat,

	/** Improve the precision of the weapons and boarding defense */
	Gunnery,

}
