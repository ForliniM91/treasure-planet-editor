package entities;

import java.util.Vector;

import interfaces.FieldEnum;
import interfaces.WriteCode;
import logic.Core;


/**
 * All possible fields in a trigger element
 * 
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum Field implements WriteCode {

	//@formatter:off
	TYPE					("Type of condition", "Type",										VarType.STRING,	VarType.ENUM),

	BOOLEAN					("Is it true/enabled?", "Boolean",									VarType.STRING,	VarType.BOOLEAN),
	BOOLEAN_VALUE			("Is it true/enabled?", "Boolean Value",							VarType.STRING,	VarType.BOOLEAN),
	BOOLEAN_STATE			("Enabled", "Boolean State",										VarType.STRING,	VarType.BOOLEAN),
	EXISTS	 				("Must be in the group?", "Exists?",								VarType.STRING, VarType.BOOLEAN),
	ENTIRE_GROUP			("Entire group", "Entire Group",									VarType.STRING,	VarType.BOOLEAN),
	FIND_CLOSEST_POINT		("Find closest point", "Find Closest Point",						VarType.STRING,	VarType.BOOLEAN),
	CROSSFADE_TRANSICTION	("Crossfade transition", "Crossfade transition",					VarType.STRING,	VarType.BOOLEAN),
	USE_CUSTOM_MESSAGE		("Use custom message", "Use Custom Message",						VarType.STRING,	VarType.BOOLEAN),
	SHOW_STAT_SCREEN		("Show stats screen", "Show Stats Screen",							VarType.STRING,	VarType.BOOLEAN),
	LIGHTNING				("Has lightnings", "Lightning On/Off",								VarType.STRING,	VarType.BOOLEAN),
	METEORS					("Has meteors", "Meteors On/Off",									VarType.STRING,	VarType.BOOLEAN),
	WIND					("Has burning wind", "Rotational Winds On/Off",						VarType.STRING,	VarType.BOOLEAN),
	ENERGY_DRAIN			("Energy drain", "Nebula Cloud Energy Drain On/Off",				VarType.STRING,	VarType.BOOLEAN),
	NEBULA_OCCLUSION		("Can see inside/outside the nebula", "Nebula Occlusion On/Off",	VarType.STRING,	VarType.BOOLEAN),
	PRIMARY_SHIP			("Primary ship", "Primary Ship",									VarType.STRING,	VarType.BOOLEAN),
	BOARDABLE				("Boardable", "Boardable",											VarType.STRING,	VarType.BOOLEAN),
	ACTIVE_STATE			("Active", "Active State",											VarType.STRING,	VarType.BOOLEAN),
	COMPLETE_STATE			("Complete", "Complete State",										VarType.STRING,	VarType.BOOLEAN),
	FAILED_STATE			("Failed", "Failed State",											VarType.STRING,	VarType.BOOLEAN),
	CREW_SPEECH_STATE		("Crew can talk", "Crew Speech State State",						VarType.STRING,	VarType.BOOLEAN),
	NIS_ALL_OBJECT_VISIBLE	("All objects visible", "All Objects Visible",						VarType.STRING,	VarType.BOOLEAN),
	NIS_JUMP_TO_START		("Jump to start", "Jump to start",									VarType.STRING,	VarType.BOOLEAN),
	NIS_JUMP_TO_POINT		("Jump to point", "Jump to point",									VarType.STRING,	VarType.BOOLEAN),
	NIS_OPEN_NIS_BAR		("Show black bars instantly", "Open NIS bars instantly",			VarType.STRING,	VarType.BOOLEAN),
	NIS_GUN_ACCURACY		("Use movie gun accuracy", "Use NIS Gun Accuracy",					VarType.STRING,	VarType.BOOLEAN),
	USE_TRANSITION			("Use transition", "Use Transition",								VarType.STRING,	VarType.BOOLEAN),
	SET_ALLIANCE_TO			("Set alliance to", "SET ALLIANCE TO",								VarType.STRING,	VarType.BOOLEAN),
	HOLD_FIRE				("Hold fire", "HOLD FIRE",											VarType.STRING,	VarType.BOOLEAN),
	HOLD_FORMATION			("Hold formation", "Hold Formation",								VarType.STRING,	VarType.BOOLEAN),
	TRAP					("Trap objects", "Trap",											VarType.STRING,	VarType.BOOLEAN),
	REPAIR_WHEN_DOCKED		("Repair when docked", "Repair when docked",						VarType.STRING,	VarType.BOOLEAN),
	PLAY_AS_DIALOG			("Play as a dialog", "Play As Dialog",								VarType.STRING,	VarType.BOOLEAN),
	OPENSTATE				("Is open", "OpenState",											VarType.STRING,	VarType.BOOLEAN),
	CLOAK_STATE				("Cloak state", "Cloak State",										VarType.STRING,	VarType.BOOLEAN),
	AI_CAN_OVERRIDE			("AI can override", "AI Can Override",								VarType.STRING,	VarType.BOOLEAN),
	RADAR_ACTIVE			("Radar active", "Radar Active",									VarType.STRING,	VarType.BOOLEAN),
	VISIBLE_ON_STARMAP		("Visible on starmap", "Visible On Starmap",						VarType.STRING,	VarType.BOOLEAN),
	DISPLAY_LOADING_SCREEN	("Display loading screen", "Display Loading String",				VarType.STRING,	VarType.BOOLEAN),
	PAUSE_WHEN_STARMAP_OPEN	("Pause when open starmap", "Pause when starmap opens",				VarType.STRING,	VarType.BOOLEAN),
	EQUIVALENCE		 		("Is equal/less/great than", "Equivalence",							VarType.STRING,	VarType.ENUM, Equivalence.values()),
	WORLD_OBJECT_TYPE		("World object", "World Object Type",								VarType.STRING,	VarType.ENUM, Category.values()),
	VITAL_SECTION 			("Sections", "Vital Section",										VarType.STRING,	VarType.ENUM, SectionType.values()), //TODO ONLY USED 'vitalToMission'. CHECK OTHER ONES
	AI_STANCE				("AI Stance", "AI Stance",											VarType.STRING,	VarType.ENUM, AIStance.values()),
	CREW_SKILL_LEVEL		("Crew skill level", "Crew Skill Level",							VarType.STRING,	VarType.ENUM, CrewSkillLevel.values()),
	GUNNERY_LEVEL			("Gunnery level", "Gunnery Level",									VarType.STRING,	VarType.ENUM, CrewSkillLevel.values()),
	FOLLOW_MODE				("Follow mode", "Follow Mode",										VarType.STRING,	VarType.ENUM, FollowMode.values()),
	FORMATION_TYPE			("Formation type", "Formation Type",								VarType.STRING,	VarType.ENUM, FormationType.values()),
	WORLD					("Next level", "World",												VarType.STRING,	VarType.ID_STRING, Core.worlds),
	WINNER_CUSTOM_MESSAGE	("Message for winners", "Winner - Custom Message String ID",		VarType.STRING,	VarType.ID_STRING, Core.winnerMessages),
	LOSER_CUSTOM_MESSAGE	("Message for losers", "Loser - Custom Message String ID",			VarType.STRING,	VarType.ID_STRING, Core.loserMessage),
	LOCALIZED_SHIP_NAME		("Displayed name", "Localized Ship Name",							VarType.STRING,	VarType.ID_STRING, Core.shipNames),
	SHIP_FLAG_TEXTURE		("Flag image", "Flag Texture",										VarType.STRING,	VarType.ID_STRING, Core.flagImages),
	FLAG_TYPE_SHIP			("Flag image", "Flag Type",											VarType.STRING,	VarType.ID_STRING, Core.flagImages),
	BANNER_TYPE				("Banner type", "Banner Type",										VarType.STRING,	VarType.ID_STRING, Core.bannerType),
	FILE_NAME				("Music file", "File Name",											VarType.STRING,	VarType.ID_STRING, Core.musicFiles),
	TEXTURE_BASE_NAME		("Texture path", "Texture Base Name",								VarType.STRING,	VarType.ID_STRING, Core.hudTextures),
	MAP_TEXT_NAME			("Map text", "Map Text Name",										VarType.STRING,	VarType.ID_STRING, Core.mapText),
	EFFECT_NAME				("Effect name", "Effect Name",										VarType.STRING,	VarType.ID_STRING, Core.effectNames),
	NEBULA_CLOUD_EFFECT		("Nebula cloud effect", "Nebula Cloud Effect Name",					VarType.STRING,	VarType.ID_STRING, Core.effectNames),
	SOLAR_STORM_EFFECT		("Solar storm effect", "Solar Storm Effect Name",					VarType.STRING,	VarType.ID_STRING, Core.effectNames),
	METEOR_SHOWER_EFFECT	("Meteor shower effect", "Meteor Shower Effect Name",				VarType.STRING,	VarType.ID_STRING, Core.effectNames),
	TEAM_NAME				("Team", "Team Name",												VarType.STRING,	VarType.ID_STRING, Core.usedTeamsNames),
	ETHERIUM_NAME			("Etherium current name", "Etherium Name",							VarType.STRING,	VarType.ID_ETHERIUM),
	NEW_NEBULA_NAME			("Nebula name", "New Nebula Name",									VarType.STRING,	VarType.ID_NEBULA),
	FLAG_NAME				("Flag", "Flag Name",												VarType.STRING,	VarType.ID_FLAG),
	TIMER_NAME	 			("Timer", "Timer Name",												VarType.STRING,	VarType.ID_TIMER),
	OBJECTIVE_TASK			("Objective task", "Objective Task",								VarType.STRING,	VarType.ID_TASK),
	SPEECH_EVENT_NAME		("Dialog", "Speech Event Name",										VarType.STRING,	VarType.ID_DIALOG),
	PLAYER_NAME				("Player", "Player Name",											VarType.STRING,	VarType.ID_PLAYER),
	PLAYER_NAME_A			("Player A", "Player Name A",										VarType.STRING,	VarType.ID_PLAYER),
	PLAYER_NAME_B			("Player B", "Player Name B",										VarType.STRING,	VarType.ID_PLAYER),
	PLAYER_A				("Player A", "PLAYER A",											VarType.STRING,	VarType.ID_PLAYER),
	PLAYER_B				("Player B", "PLAYER B",											VarType.STRING,	VarType.ID_PLAYER),
	PLAYER_FLEET			("Player", "PLAYER FLEET",											VarType.STRING,	VarType.ID_PLAYER),
	PLAYER_OWNER			("Owner", "Player/Owner",											VarType.STRING,	VarType.ID_PLAYER),
	NEW_OWNER				("New owner", "New Owner",											VarType.STRING,	VarType.ID_PLAYER),
	SHIP_NAME				("Ship name", "Ship Name",											VarType.STRING,	VarType.ID_SHIP),
	NEW_SHIP_NAME			("New ship name", "New Ship Name",									VarType.STRING,	VarType.ID_SHIP),
	UNIT_NAME				("Group/Unit", "Unit Name",											VarType.STRING,	VarType.ID_GROUP_UNIT),
	GROUP_UNIT				("Group/Unit", "Group/Unit",										VarType.STRING,	VarType.ID_GROUP_UNIT),
	GROUP_UNIT_NAME			("Group/Unit", "Group/Unit Name",									VarType.STRING,	VarType.ID_GROUP_UNIT),
	GROUP_NAME				("Group/Unit", "Group Name",										VarType.STRING,	VarType.ID_GROUP_UNIT),
	GROUP_A_NAME			("Group A", "GroupA Name",											VarType.STRING,	VarType.ID_GROUP_UNIT),
	GROUP_B_NAME			("Group B", "GroupB Name",											VarType.STRING,	VarType.ID_GROUP_UNIT),
	TOWER_GROUP_A			("Group A", "TowerGroupA",											VarType.STRING,	VarType.ID_GROUP_UNIT),
	TOWER_GROUP_B			("Group B", "TowerGroupB",											VarType.STRING,	VarType.ID_GROUP_UNIT),
	LONGBOAT_GROUP			("Group", "Longboat Group Name",									VarType.STRING,	VarType.ID_GROUP_UNIT),
	GROUP_UNIT_TO_TRANSFER	("Group to transfer", "Group/Unit to transfer",						VarType.STRING,	VarType.ID_GROUP_UNIT),
	TOWING_GROUP			("Towing group", "Towing Group",									VarType.STRING,	VarType.ID_GROUP_UNIT),
	TARGET_TO_TOW			("Target to tow", "Target to Tow",									VarType.STRING,	VarType.ID_GROUP_UNIT),
	DOCKERS_GROUP_UNIT_NAME	("Docker", "Dockers Group/Unit Name",								VarType.STRING,	VarType.ID_GROUP_UNIT),
	TARGET_GROUP_UNIT_NAME	("Target", "Target Group/Unit Name",								VarType.STRING,	VarType.ID_GROUP_UNIT),
	GROUPUNIT				("Group", "GroupUnit Name",											VarType.STRING,	VarType.ID_GROUP_UNIT),
	GROUP_UNIT_TARGET		("Target", "Group Unit Target",										VarType.STRING,	VarType.ID_GROUP_UNIT),
	ATTACKER_GROUP			("Attacker", "Attack Group",										VarType.STRING,	VarType.ID_GROUP_UNIT),
	TARGET_GROUP			("Target", "Target Group",											VarType.STRING,	VarType.ID_GROUP_UNIT),
	DOCKING_GROUP			("Docking group", "Docking Group",									VarType.STRING,	VarType.ID_GROUP_UNIT),
	DOCKING_GROUP_UNIT		("Docking group unit", "Docking GroupUnit",							VarType.STRING,	VarType.ID_GROUP_UNIT),
	TARGET_TO_DOCK_TO		("Dock to", "Target to Dock to",									VarType.STRING,	VarType.ID_GROUP_UNIT),
	OBJECTIVE_POINT			("Objective point", "Objective Point",								VarType.STRING,	VarType.ID_OBJECTIVE_POINT),
	NEBULA_CLOUD_POINT_SET	("Nebula cloud point set", "Nebula Cloud Point Set",				VarType.STRING,	VarType.ID_POINT_SET),
	SOLAR_STORM_POINT_SET	("Solar storm point set", "Solar Storm Point Set",					VarType.STRING,	VarType.ID_POINT_SET),
	METEOR_SHOWER_POINT_SET	("Meteor shower point set", "Meteor Shower Point Set",				VarType.STRING,	VarType.ID_POINT_SET),
	POINT_SET				("Point set", "Point set",											VarType.STRING,	VarType.ID_POINT_SET),
	POINTSET_NAME			("Point set", "Pointset Name",										VarType.STRING,	VarType.ID_POINT_SET),
	POINT_SET_NAME			("Point set", "Point Set Name",										VarType.STRING,	VarType.ID_POINT_SET),
	PATH_NAME				("Path", "Path Name",												VarType.STRING,	VarType.ID_WAYPOINTS),
	SHIP_PATH				("Ship path", "Ship Path",											VarType.STRING,	VarType.ID_WAYPOINTS),	// SPECIAL VALUES: 'PATH NAME', 'NO PATH'
	ETHERIUM_PATH			("Etherium path", "Etherium Path",									VarType.STRING,	VarType.ID_WAYPOINTS),
	VOLUME_NAME				("Area", "Volume Name",												VarType.STRING,	VarType.ID_POLYGON),
	POLYGON_NAME			("Polygon", "Polygon Name",											VarType.STRING,	VarType.ID_POLYGON),
	AFFECTED_AREA			("Area", "Affected Area",											VarType.STRING,	VarType.ID_POLYGON),

	NUMBER					("Number", "number",												VarType.INT),
	NUMBER_ROUNDS_FIRED		("Rounds fired", "Number Of Rounds Fired",							VarType.INT),
	NUMBER_TIMES_HIT		("Hits", "Number Of Times Hit",										VarType.INT),
	NUMBER_SHIP_DESTROYED	("Ships destroyed", "Number Of Ships Destroyed",					VarType.INT),
	NUMBER_OF_CAPTURES		("Captures", "Number Of Captures",									VarType.INT),
	TIME_IN_SECONDS			("Seconds", "Time in seconds",										VarType.INT),
	DISTANCE				("Distance", "Distance",											VarType.INT),
	POINTS					("Points", "Points",												VarType.INT),
	VICTORY_POINTS			("Victory points", "Victory points to be added",					VarType.INT),
	NUM_OBJECT_WAITING		("Entered", "Number of objects entered, waiting to report",			VarType.INT),	//ALWAYS 0
	NUM_OBJECT_ENTERED		("Entered and reported", "Object that have already entered - Size",	VarType.INT),	//ALWAYS 0
	WORLD_OBJECT_ID			("World object", "World Object ID",									VarType.INT),
	COMBAT_STRENGTH			("Combat strength", "Combat Strength",								VarType.INT),
	VELOCITY				("Velocity", "Velocity",											VarType.INT),

	DAMAGE_PERCENT			("Percent of damage", "Damage Percent",								VarType.FLOAT),
	DOCK_TIME				("Dock time", "Dock Time",											VarType.FLOAT),
	FADE_OUT_TIME			("Fade out time", "Fade Out Time ( secs )",							VarType.FLOAT),
	FADE_IN_TIME			("Fade in time", "Fade In Time ( secs )",							VarType.FLOAT),
	FADE_OUT_DISTANCE		("Fade out distance", "Fadeout Distance",							VarType.FLOAT),
	NEW_VOlUME				("Volume percent", "New Volume ( 0 to 1 )",							VarType.FLOAT),
	THROTTLE				("Speed percent", "Throttle Percent",								VarType.FLOAT),
	THROTTLE_SPEED			("Speed percent", "Throttle Speed",									VarType.FLOAT),
	LIGHTNING_RATE			("Lightning every X sec", "Lightning Blast Recharge Time",			VarType.FLOAT),
	METEOR_RATE				("Meteor every X sec", "Meteor Strike Recharge Time",				VarType.FLOAT),
	WIND_MAGNITUDE			("Wind strength","Wind Magnitude",									VarType.FLOAT),
	WIND_RATE				("Wind damage every X sec", "Solar Storm Wind Damage Frequency",	VarType.FLOAT),
	AMBIENT_SOUND_DISTANCE	("Ambient sound max distance", "Ambient sound max distance",		VarType.FLOAT),
	VELOCITY_UPPER			("Velocity upper (m/sec)", "Velocity Upper m/sec",					VarType.FLOAT),
	VELOCITY_LOWER			("Velocity lower (m/sec)", "Velocity Lower m/sec",					VarType.FLOAT),
	TUMBLE_UPPER			("Tumble upper (rads/sec)", "Tumble Upper Rads/sec",				VarType.FLOAT),
	TUMBLE_LOWER			("Tumble lower (rads/sec)", "Tumble Lower Rads/sec",				VarType.FLOAT),
	DAMAGE_THRESHOLD		("Damage threshold", "Damage Threshold",							VarType.FLOAT),
	INVULNERABLE_PERCENT	("Invulnerable percent", "Invulnerable Percent",					VarType.FLOAT),
	PERCENT					("Percent", "Percent",												VarType.FLOAT),
	RELATIVE_ANGLE			("Relative angle", "Relative Angle",								VarType.FLOAT),
	NIS_ANGLE_YZ			("Angle YZ", "Angle YZ",											VarType.FLOAT),
	NIS_ANGLE_XY			("Angle XY", "Angle XY",											VarType.FLOAT),
	NIS_CAMERA_ACCEL		("Acceleration", "Acceleration",									VarType.FLOAT),
	NIS_CAMERA_MAX_VEL		("Max velocity", "Max Velocity",									VarType.FLOAT),
	NIS_FOV					("FOV", "FOV",														VarType.FLOAT),
	NIS_SPEED				("Speed", "Speed",													VarType.FLOAT),
	;




	// @formatter:on


	/** Field name */
	private String			name;
	/** Field code */
	private String			code;
	/** Domain used in the game code */
	private VarType			usedDomain;
	/** Real domain for this field (only if usedDomain is STRING) */
	private VarType			realDomain;
	/** Array of possible enum values (only if realDomain is ENUM) */
	private FieldEnum[]		valuesEnum;
	/** Array of possible string values (only if realDomain is ID_STRING) */
	private Vector <String>	valuesIDS;

	/**
	 * Create a new Field
	 *
	 * @param name Field name
	 * @param code Field code
	 * @param usedDomain Domain used in the game code
	 */
	private Field (String name, String code, VarType usedDomain) {
		this.name = name;
		this.code = code;
		this.usedDomain = usedDomain;
	}

	/**
	 * Create a new Field
	 *
	 * @param name Field name
	 * @param code Field code
	 * @param usedDomain Domain used in the game code
	 * @param realDomain Real domain for this field (only if usedDomain is STRING)
	 */
	private Field (String name, String code, VarType usedDomain, VarType realDomain) {
		this (name, code, usedDomain);
		this.realDomain = realDomain;
	}

	/**
	 * Create a new Field
	 *
	 * @param name Field name
	 * @param code Field code
	 * @param usedDomain Domain used in the game code
	 * @param realDomain Real domain for this field (only if usedDomain is STRING)
	 * @param valuesIDS Array of possible string values (only if realDomain is ID_STRING)
	 */
	private Field (String name, String code, VarType usedDomain, VarType realDomain, Vector <String> valuesIDS) {
		this (name, code, usedDomain, realDomain);
		this.valuesIDS = valuesIDS;
		valuesEnum = null;
	}

	/**
	 * Create a new Field
	 *
	 * @param name Field name
	 * @param code Field code
	 * @param usedDomain Domain used in the game code
	 * @param realDomain Real domain for this field (only if usedDomain is STRING)
	 * @param valuesEnum Array of possible enum values (only if realDomain is ENUM)
	 */
	private Field (String name, String code, VarType usedDomain, VarType realDomain, FieldEnum[] valuesEnum) {
		this (name, code, usedDomain, realDomain);
		valuesIDS = null;
		this.valuesEnum = valuesEnum;
	}

	public String getName () {
		return name;
	}

	public String getCode () {
		return code;
	}

	public VarType getUsedDomain () {
		return usedDomain;
	}

	public VarType getRealDomain () {
		return realDomain;
	}

	public FieldEnum[] getValuesEnum () {
		return valuesEnum;
	}

	public Vector <String> getValuesIDS () {
		return valuesIDS;
	}

	@Override
	public String toString () {
		return code + '(' + realDomain + ')';
	}

	@Override
	public CharSequence toCode (int indentation) {
		return "\t\t\t\t" + code + ' ' + usedDomain;
	}

}
