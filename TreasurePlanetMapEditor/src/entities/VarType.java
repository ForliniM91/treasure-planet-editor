package entities;

@SuppressWarnings ("javadoc")
public enum VarType {

    // Used domains
	INT ("Int"),
	FLOAT ("Float"),
	DOUBLE ("Double"),
	STRING ("String"),

    // Real domains
	ENUM (null),
	BOOLEAN (null),
	ID_STRING (null),
	ID_ETHERIUM (null),
	ID_NEBULA (null),
	ID_FLAG (null),
	ID_TIMER (null),
	ID_TASK (null),
	ID_DIALOG (null),
	ID_PLAYER (null),
	ID_SHIP (null),
	ID_GROUP_UNIT (null),
	ID_OBJECTIVE_POINT (null),
	ID_POINT_SET (null),
	ID_WAYPOINTS (null),
	ID_POLYGON (null);



	/** Domain code */
	public String code;

	/**
	 * Create a new Domain
	 *
	 * @param code Domain code
	 */
	private VarType (String code) {
		this.code = code;
	}

	@Override
	public String toString () {
		return code;
	}

}
