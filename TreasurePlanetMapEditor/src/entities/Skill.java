package entities;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * The skill of a Crew member
 *
 * @author MarcoForlini
 */
public class Skill implements ReadCode, WriteCode {

	/** Type of skill */
	public SkillType	skillType;

	/** If true, the skill is available */
	public boolean		active;

	/** A zero or positive value, indicating the skill level */
	public int			value;


	/**
	 * Creates a new {@link Skill}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Skill (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link Skill}
	 *
	 * @param skillType Type of skill
	 * @param active If true, the skill is available
	 * @param value A zero or positive value, indicating the skill level
	 */
	public Skill (SkillType skillType, boolean active, int value) {
		this.skillType = skillType;
		this.active = active;
		this.value = value;
	}

	@Override
	public int getNumInternalLines () {
		return 2;
	}

	/**
	 * Creates a new {@link Skill}
	 *
	 * @param scanner The scanner
	 */
	@Override
	public void readCode (Scanner scanner) {
		String line = scanner.nextLine ();
		skillType = SkillType.valueOf (line.substring (1, line.indexOf (' ')));
		active = (line.substring (line.indexOf (" Bool ", 23) + 6)).equals ("True");
		line = scanner.nextLine ();
		value = Integer.parseInt (line.substring (line.indexOf (" Int ", 24), +5));
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder ("\n\t").append (skillType).append (" Skill Active Bool ").append (toBool (active))
		        .append ("\n\t").append (skillType).append (" Skill Ability Value Int ").append (value);
	}

}
