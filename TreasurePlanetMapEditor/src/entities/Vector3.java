package entities;

import interfaces.WriteCode;


/**
 * A Vector which define 3 coordinates in the space
 *
 * @author MarcoForlini
 */
public class Vector3 implements WriteCode {

	@SuppressWarnings ("javadoc")
	public float x0, x1, x2;

	/**
	 * Creates a new {@link Vector3}
	 *
	 * @param x0
	 * @param x1
	 * @param x2
	 */
	public Vector3 (float x0, float x1, float x2) {
		this.x0 = x0;
		this.x1 = x1;
		this.x2 = x2;
	}


	@Override
	public CharSequence toCode (int indentation) {
		return "Vector3( " + toFloat6 (x0) + ", " + toFloat6 (x1) + ", " + toFloat6 (x2) + " )";
	}

	@Override
	public String toString () {
		return "Vector3( " + toFloat6 (x0) + ", " + toFloat6 (x1) + ", " + toFloat6 (x2) + " )";
	}

}
