package entities;

import interfaces.FieldEnum;


/**
 * Section type
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum SectionType implements FieldEnum {

	VITALTOMISSION ("Vital to mission sections", "vitalToMission");



	/** Formation name */
	public String	name;
	/** Formation code */
	public String	code;

	/**
	 * Create a new FormationType
	 *
	 * @param name Formation name
	 * @param code Formation code
	 */
	private SectionType (String name, String code) {
		this.name = name;
		this.code = code;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return code;
	}

	@Override
	public String toString () {
		return name;
	}

}
