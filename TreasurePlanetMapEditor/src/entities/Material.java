package entities;

/**
 * Materials used in the game
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum Material {

	Hull_Wood ("Hull Wood", "Wood"),
	Hull_ReInforced_Wood ("Hull ReInforced Wood", "ReInforced"),
	Hull_Iron ("Hull Iron", "Iron"),
	Sail_Cloth ("Sail Cloth", "Cloth"),
	Wall_Stone ("Wall Stone", "Stone"),
	Dragon_Scale ("Dragon Scale", "Dragon");

	public String	name;
	public String	shortName;

	private Material (String name, String shortName) {
		this.name = name;
		this.shortName = shortName;
	}

	@Override
	public String toString () {
		return name;
	}

}
