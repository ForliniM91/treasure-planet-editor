package entities;

/**
 * Crew members species
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum Species {

	Aquanog (false, false, true, true, true, false, false),
	Arcturian (true, true, true, true, true, false, false),
	BEN (false, true, true, true, false, false, false),
	BUB (false, false, false, true, false, true, true),
	Canid (true, true, true, true, false, false, false),
	Cragorian (true, true, false, false, false, true, true),
	Densadron (false, false, true, false, true, true, true),
	Felinid (true, true, true, true, true, true, false),
	Geeorie (true, false, true, true, false, false, false),
	Human_Male,
	Human_Female,
	Macriki (false, false, false, true, true, true, true),
	Mantavor (true, true, true, false, true, true, true),
	Optoc (false, false, true, true, true, false, false),
	Procyon,
	Tuskrus (false, false, true, false, true, true, true),
	WAR_BOT (false, false, false, false, false, true, false),
	Zirrelian (false, true, true, true, true, true, true),
	;



	/** Available skills */
	public boolean[] availableSkills;

	private Species () {
		availableSkills = new boolean[] { true, true, true, true, true, true, true };
	}

	private Species (boolean leadership, boolean navigation, boolean spotting, boolean engineering, boolean rigging, boolean combat, boolean gunnery) {
		availableSkills = new boolean[] { leadership, navigation, spotting, engineering, rigging, combat, gunnery };
	}

}
