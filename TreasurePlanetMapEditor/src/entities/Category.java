package entities;

import interfaces.FieldEnum;


/**
 * Category of objects
 * 
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum Category implements FieldEnum {

	ANIMAL ("Animal"),
	ASTEROID ("Asteroid"),
	BLACK_HOLE ("BlackHole"),
	BASE ("Base"),
	DRAGON ("Dragon"),
	ISLAND ("Island"),
	MINE ("Mine"),
	NEBULA ("Nebula"),
	SHIP ("Ship"),
	TERRAIN ("Terrain"),
	TORPEDO ("Torpedo"),
	;



	/** Category name */
	private String name;

	/**
	 * Create a new Category
	 *
	 * @param name Category name
	 */
	private Category (String name) {
		this.name = name;
	}

	@Override
	public String toString () {
		return name;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return name;
	}

}
