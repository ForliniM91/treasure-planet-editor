package data.worldObjects.constData;

import entities.Material;


/**
 * Bullet's CustomInfo
 *
 * @author MarcoForlini
 */
public interface ConstDataCustomInfoBullet extends ConstDataCustomInfo {

	/** The materials */
	Material[] materials = Material.values ();

}
