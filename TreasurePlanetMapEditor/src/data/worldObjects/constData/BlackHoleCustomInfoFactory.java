package data.worldObjects.constData;

import java.util.Scanner;

import data.effects.Effect;
import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_BlackHoleCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class BlackHoleCustomInfoFactory implements ConstDataCustomInfo {

	/** Pull strength */
	public float	magnitude;

	/** Size of the black hole */
	public float	radius;

	/** Effect of vortex */
	public Effect	vortexEffectName;

	/** Max sound distance */
	public float	ambientSoundMaxDistance;

	/** Sound name */
	public String	ambientSoundName;



	/**
	 * Creates a new {@link BlackHoleCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public BlackHoleCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link BlackHoleCustomInfoFactory}
	 *
	 * @param magnitude Pull strength
	 * @param radius Size of the black hole
	 * @param vortexEffectName Effect of vortex
	 * @param ambientSoundMaxDistance Max sound distance
	 * @param ambientSoundName Sound name
	 */
	public BlackHoleCustomInfoFactory (float magnitude, float radius, Effect vortexEffectName, float ambientSoundMaxDistance, String ambientSoundName) {
		this.magnitude = magnitude;
		this.radius = radius;
		this.vortexEffectName = vortexEffectName;
		this.ambientSoundMaxDistance = ambientSoundMaxDistance;
		this.ambientSoundName = ambientSoundName;
	}


	@Override
	public final int getNumInternalLines () {
		return 5;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 5); // <number of lines> ConstData

		scanner.nextLine (); // {
		magnitude = readFloat (scanner);
		radius = readFloat (scanner);
		vortexEffectName = Effect.effects.get (readString (scanner));
		ambientSoundMaxDistance = readFloat (scanner);
		ambientSoundName = readString (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (300)
		        .append ("\n00000005 ConstData")
		        .append ("\n{")
		        .append ("\n\tMagnitude Float ").append (toFloat6 (magnitude))
		        .append ("\n\tRadius Float ").append (toFloat6 (radius))
		        .append ("\n\tVortexEffectName String '").append (vortexEffectName).append ('\'')
		        .append ("\n\tAmbientSoundMaxDistance Float ").append (ambientSoundMaxDistance)
		        .append ("\n\tAmbientSoundName String '").append (vortexEffectName).append ('\'')
		        .append ("\n}");
	}

}
