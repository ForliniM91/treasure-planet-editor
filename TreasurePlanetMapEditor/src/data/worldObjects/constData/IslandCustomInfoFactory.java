package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Race;
import entities.Vector3;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_IslandCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class IslandCustomInfoFactory implements ConstDataCustomInfo {

	/** Point */
	public Vector3	occlusionPolygonPoint0;
	/** Point */
	public Vector3	occlusionPolygonPoint1;
	/** Point */
	public Vector3	occlusionPolygonPoint2;
	/** Point */
	public Vector3	occlusionPolygonPoint3;
	/** Point */
	public Vector3	occlusionPolygonPoint4;
	/** Point */
	public Vector3	occlusionPolygonPoint5;
	/** Point */
	public Vector3	occlusionPolygonPoint6;
	/** Point */
	public Vector3	occlusionPolygonPoint7;
	/** Point */
	public Vector3	occlusionPolygonPoint8;
	/** Point */
	public Vector3	occlusionPolygonPoint9;

	/** Island owner */
	public Race		race;

	/** Do island makes sounds? */
	public boolean	hasAmbientSound;
	/** Max sound distance */
	public float	ambientSoundMaxDistance;
	/** Sound name */
	public String	ambientSoundName;

	/** Max hit points */
	public int		coreDamageSectionMaxHitPoints;

	/** Do spotters report this object? */
	public boolean	announceSighting;



	/**
	 * Creates a new {@link IslandCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public IslandCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link IslandCustomInfoFactory}
	 *
	 * @param occlusionPolygonPoint0 Point
	 * @param occlusionPolygonPoint1 Point
	 * @param occlusionPolygonPoint2 Point
	 * @param occlusionPolygonPoint3 Point
	 * @param occlusionPolygonPoint4 Point
	 * @param occlusionPolygonPoint5 Point
	 * @param occlusionPolygonPoint6 Point
	 * @param occlusionPolygonPoint7 Point
	 * @param occlusionPolygonPoint8 Point
	 * @param occlusionPolygonPoint9 Point
	 * @param race Island owner
	 * @param hasAmbientSound Do island makes sounds?
	 * @param ambientSoundMaxDistance Max sound distance
	 * @param ambientSoundName Sound name
	 * @param coreDamageSectionMaxHitPoints Max hit points
	 * @param announceSighting Do spotters report this object?
	 */
	public IslandCustomInfoFactory (Vector3 occlusionPolygonPoint0, Vector3 occlusionPolygonPoint1, Vector3 occlusionPolygonPoint2, Vector3 occlusionPolygonPoint3, Vector3 occlusionPolygonPoint4, Vector3 occlusionPolygonPoint5, Vector3 occlusionPolygonPoint6, Vector3 occlusionPolygonPoint7, Vector3 occlusionPolygonPoint8, Vector3 occlusionPolygonPoint9,
	                                Race race, boolean hasAmbientSound, float ambientSoundMaxDistance, String ambientSoundName, int coreDamageSectionMaxHitPoints, boolean announceSighting) {
		this.occlusionPolygonPoint0 = occlusionPolygonPoint0;
		this.occlusionPolygonPoint1 = occlusionPolygonPoint1;
		this.occlusionPolygonPoint2 = occlusionPolygonPoint2;
		this.occlusionPolygonPoint3 = occlusionPolygonPoint3;
		this.occlusionPolygonPoint4 = occlusionPolygonPoint4;
		this.occlusionPolygonPoint5 = occlusionPolygonPoint5;
		this.occlusionPolygonPoint6 = occlusionPolygonPoint6;
		this.occlusionPolygonPoint7 = occlusionPolygonPoint7;
		this.occlusionPolygonPoint8 = occlusionPolygonPoint8;
		this.occlusionPolygonPoint9 = occlusionPolygonPoint9;
		this.race = race;
		this.hasAmbientSound = hasAmbientSound && ambientSoundMaxDistance > 0 && ambientSoundName != null;
		this.ambientSoundMaxDistance = ambientSoundMaxDistance;
		this.ambientSoundName = ambientSoundName;
		this.coreDamageSectionMaxHitPoints = coreDamageSectionMaxHitPoints;
		this.announceSighting = announceSighting;
	}


	@Override
	public final int getNumInternalLines () {
		return hasAmbientSound ? 17 : 15;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		int n = readNumLines (scanner); // <number of lines> ConstData

		scanner.nextLine (); // {
		occlusionPolygonPoint0 = readVector (scanner);
		occlusionPolygonPoint1 = readVector (scanner);
		occlusionPolygonPoint2 = readVector (scanner);
		occlusionPolygonPoint3 = readVector (scanner);
		occlusionPolygonPoint4 = readVector (scanner);
		occlusionPolygonPoint5 = readVector (scanner);
		occlusionPolygonPoint6 = readVector (scanner);
		occlusionPolygonPoint7 = readVector (scanner);
		occlusionPolygonPoint8 = readVector (scanner);
		occlusionPolygonPoint9 = readVector (scanner);
		race = Race.valueOf (readString (scanner));
		hasAmbientSound = readBoolean (scanner);
		coreDamageSectionMaxHitPoints = readInt (scanner);
		announceSighting = readBoolean (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (1200)
		        .append ("\n").append (toChars8 (getNumInternalLines ())).append (" ConstData")
		        .append ("\n{")
		        .append ("\n\tOcclusion Polygon Point # 00 ").append (occlusionPolygonPoint0)
		        .append ("\n\tOcclusion Polygon Point # 01 ").append (occlusionPolygonPoint1)
		        .append ("\n\tOcclusion Polygon Point # 02 ").append (occlusionPolygonPoint2)
		        .append ("\n\tOcclusion Polygon Point # 03 ").append (occlusionPolygonPoint3)
		        .append ("\n\tOcclusion Polygon Point # 04 ").append (occlusionPolygonPoint4)
		        .append ("\n\tOcclusion Polygon Point # 05 ").append (occlusionPolygonPoint5)
		        .append ("\n\tOcclusion Polygon Point # 06 ").append (occlusionPolygonPoint6)
		        .append ("\n\tOcclusion Polygon Point # 07 ").append (occlusionPolygonPoint7)
		        .append ("\n\tOcclusion Polygon Point # 08 ").append (occlusionPolygonPoint8)
		        .append ("\n\tOcclusion Polygon Point # 09 ").append (occlusionPolygonPoint9)
		        .append ("\n\tOcclusion Polygon Point Count Int 0") // It's always 0
		        .append ("\n\tRace String '").append (race).append ('\'');
		if (hasAmbientSound) {
			sb.append ("\n\tHasAmbientSound Bool True")
			        .append ("\n\tAmbientSoundMaxDistance Float ").append (ambientSoundMaxDistance)
			        .append ("\n\tAmbientSoundName String '").append (ambientSoundName);
		} else {
			sb.append ("\n\tHasAmbientSound Bool False ");
		}
		return sb.append ("\n\tCore Damage Section Max HitPoints Int ").append (coreDamageSectionMaxHitPoints)
		        .append ("\n\tAnnounceSighting Bool ").append (toBool (announceSighting))
		        .append ("\n}");
	}

}
