package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypePhysics;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Vector3;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypePhysics#FT_ShipDemo}
 *
 * @author MarcoForlini
 */
public class ShipDemo implements ConstDataPhysics {

	/** Center of mass */
	public Vector3	centerOfMass;

	/** Mass */
	public float	mass;

	/** Max thrust */
	public float	maxThrust;

	/** Max speed */
	public float	maxSpeed;

	/** Rotational friction */
	public float	rotationalFriction;

	/** Max angular acceleration */
	public float	maxAngularAcceleration;





	/**
	 * Creates a new {@link ShipDemo}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public ShipDemo (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link ShipDemo}
	 *
	 * @param centerOfMass The center of mass
	 * @param mass The mass
	 * @param maxThrust The max thrust
	 * @param maxSpeed The max speed
	 * @param rotationalFriction The rotational friction
	 * @param maxAngularAcceleration The max angular acceleration
	 */
	public ShipDemo (Vector3 centerOfMass, float mass, float maxThrust, float maxSpeed, float rotationalFriction, float maxAngularAcceleration) {
		this.centerOfMass = centerOfMass;
		this.mass = mass;
		this.maxThrust = maxThrust;
		this.maxSpeed = maxSpeed;
		this.rotationalFriction = rotationalFriction;
		this.maxAngularAcceleration = maxAngularAcceleration;
	}


	@Override
	public final int getNumInternalLines () {
		return 6;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 6); // <number of lines> ConstData

		scanner.nextLine (); // {
		centerOfMass = readVector (scanner);
		mass = readFloat (scanner);
		maxThrust = readFloat (scanner);
		maxSpeed = readFloat (scanner);
		rotationalFriction = readFloat (scanner);
		maxAngularAcceleration = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (256)
		        .append ("\n00000006 ConstData")
		        .append ("\n{")
		        .append ("\n\tCenterOfMass").append (centerOfMass)
		        .append ("\n\tMass Float ").append (toFloat6 (mass))
		        .append ("\n\tMaxThrust Float ").append (toFloat6 (maxThrust))
		        .append ("\n\tMaxSpeed Float ").append (toFloat6 (maxSpeed))
		        .append ("\n\tRotationalFriction Float ").append (toFloat6 (rotationalFriction))
		        .append ("\n\tMaxAngularAcceleration Float ").append (toFloat6 (maxAngularAcceleration))
		        .append ("\n}");
	}
}
