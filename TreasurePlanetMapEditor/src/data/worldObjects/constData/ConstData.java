package data.worldObjects.constData;

import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * A block "ConstData"
 *
 * @author MarcoForlini
 */
public interface ConstData extends ReadCode, WriteCode {
	// Nothing to add
}
