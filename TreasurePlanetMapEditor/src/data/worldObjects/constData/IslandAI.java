package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeAI;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeAI#FT_IslandAI}
 *
 * @author MarcoForlini
 */
public class IslandAI implements ConstDataAI {

	/** The sight range */
	public float sightRange;





	/**
	 * Creates a new {@link IslandAI}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public IslandAI (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link IslandAI}
	 *
	 * @param sightRange The sight range
	 */
	public IslandAI (float sightRange) {
		this.sightRange = sightRange;
	}


	@Override
	public final int getNumInternalLines () {
		return 1;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 1); // <number of lines> ConstData
		scanner.nextLine (); // {
		sightRange = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (62)
		        .append ("\n00000001 ConstData")
		        .append ("\n{")
		        .append ("\n\tDefault SightRange Float ").append (toFloat6 (sightRange))
		        .append ("\n}");
	}

}
