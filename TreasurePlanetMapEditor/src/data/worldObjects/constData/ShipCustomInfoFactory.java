/**
 *
 */
package data.worldObjects.constData;

import java.util.Scanner;

import data.effects.Effect;
import data.worldObjects.elements.EngineType;
import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.GUIInfoChunk;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Race;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_ShipCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class ShipCustomInfoFactory implements ConstDataCustomInfo {

	/** The {@link GUIInfoChunk} */
	public GUIInfoChunk	guiInfoChunk;

	/** Name ID */
	public String		nameID;
	/** Race */
	public Race			race;
	/** Is a tender? */
	public boolean		isTender;
	/** Number of lifeboats */
	public int			numberOfLifeboats;
	/** Is a lifeboat? */
	public boolean		isLifeboat;
	/** Is cloakable? */
	public boolean		isCloakable;
	/** Ship size */
	public int			shipSize;
	/** Core section's max hit points */
	public int			coreDamageSectionMaxHitPoints;
	/** Explosion effect */
	public Effect		explosionEffect;

	/** Engine type */
	public EngineType	engineType;
	/** Sound of engines at emergency speed */
	public String		engineSoundNameEmergency;
	/** Sound of engines at full speed */
	public String		engineSoundNameFull;
	/** Sound of engines at half speed */
	public String		engineSoundNameHalf;

	/** Victory point cost */
	public int			victoryPointCost;
	/** Is available in skirmish/multiplayer games? */
	public boolean		availableInMultiplayer;
	/** Name ID 00 */
	public String		availableNameID00;
	/** Name ID 01 */
	public String		availableNameID01;
	/** Name ID 02 */
	public String		availableNameID02;
	/** Name ID 03 */
	public String		availableNameID03;
	/** Name ID 04 */
	public String		availableNameID04;
	/** Name ID 05 */
	public String		availableNameID05;
	/** Name ID 06 */
	public String		availableNameID06;
	/** Name ID 07 */
	public String		availableNameID07;
	/** Name ID 08 */
	public String		availableNameID08;
	/** Name ID 09 */
	public String		availableNameID09;
	/** Name ID 10 */
	public String		availableNameID10;
	/** Name ID 11 */
	public String		availableNameID11;

	/** Number of gunners (Fixed, depends on ship) */
	public int			maxNumberOfGunners;
	/** Number of captains (Fixed, always 1) */
	public int			maxNumberOfCaptains	= 1;
	/** Number of first mates */
	public int			maxNumberOfFirstMates;
	/** Number of navigators */
	public int			maxNumberOfNavigators;
	/** Number of engineers */
	public int			maxNumberOfEngineers;
	/** Number of riggers */
	public int			maxNumberOfRiggers;
	/** Number of fighters */
	public int			maxNumberOfFighters;
	/** Number of lookouts */
	public int			maxNumberOfLookouts;

	/** Repair effect */
	public Effect		repairEffect;




	/**
	 * Creates a new {@link ShipCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public ShipCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link ShipCustomInfoFactory}
	 *
	 * @param guiInfoChunk The {@link GUIInfoChunk}
	 * @param nameID Name ID
	 * @param race Race
	 * @param isTender Is a tender?
	 * @param numberOfLifeboats Number of lifeboats
	 * @param isLifeboat Is a lifeboat?
	 * @param isCloakable Is cloakable?
	 * @param shipSize Ship size
	 * @param coreDamageSectionMaxHitPoints Core section's max hit points
	 * @param explosionEffect Explosion effect
	 * @param engineType Engine type
	 * @param engineSoundNameEmergency Sound of engines at emergency speed
	 * @param engineSoundNameFull Sound of engines at full speed
	 * @param engineSoundNameHalf Sound of engines at half speed
	 * @param victoryPointCost Victory point cost
	 * @param availableInMultiplayer Is available in skirmish/multiplayer games?
	 * @param availableNameID00 Name ID 00
	 * @param availableNameID01 Name ID 01
	 * @param availableNameID02 Name ID 02
	 * @param availableNameID03 Name ID 03
	 * @param availableNameID04 Name ID 04
	 * @param availableNameID05 Name ID 05
	 * @param availableNameID06 Name ID 06
	 * @param availableNameID07 Name ID 07
	 * @param availableNameID08 Name ID 08
	 * @param availableNameID09 Name ID 09
	 * @param availableNameID10 Name ID 10
	 * @param availableNameID11 Name ID 11
	 * @param maxNumberOfGunners Number of gunners (Fixed, depends on ship)
	 * @param maxNumberOfFirstMates Number of first mates
	 * @param maxNumberOfNavigators Number of navigators
	 * @param maxNumberOfEngineers Number of engineers
	 * @param maxNumberOfRiggers Number of riggers
	 * @param maxNumberOfFighters Number of fighters
	 * @param maxNumberOfLookouts Number of lookouts
	 * @param repairEffect Repair effect
	 */
	public ShipCustomInfoFactory (GUIInfoChunk guiInfoChunk,
	                              String nameID, Race race, boolean isTender, int numberOfLifeboats, boolean isLifeboat, boolean isCloakable, int shipSize, int coreDamageSectionMaxHitPoints, Effect explosionEffect,
	                              EngineType engineType, String engineSoundNameEmergency, String engineSoundNameFull, String engineSoundNameHalf, int victoryPointCost, boolean availableInMultiplayer,
	                              String availableNameID00, String availableNameID01, String availableNameID02, String availableNameID03, String availableNameID04, String availableNameID05,
	                              String availableNameID06, String availableNameID07, String availableNameID08, String availableNameID09, String availableNameID10, String availableNameID11,
	                              int maxNumberOfGunners, int maxNumberOfFirstMates, int maxNumberOfNavigators, int maxNumberOfEngineers, int maxNumberOfRiggers, int maxNumberOfFighters, int maxNumberOfLookouts,
	                              Effect repairEffect) {
		this.guiInfoChunk = guiInfoChunk;
		this.nameID = nameID;
		this.race = race;
		this.isTender = isTender;
		this.numberOfLifeboats = numberOfLifeboats;
		this.isLifeboat = isLifeboat;
		this.isCloakable = isCloakable;
		this.shipSize = shipSize;
		this.coreDamageSectionMaxHitPoints = coreDamageSectionMaxHitPoints;
		this.explosionEffect = explosionEffect;
		this.engineType = engineType;
		this.engineSoundNameEmergency = engineSoundNameEmergency;
		this.engineSoundNameFull = engineSoundNameFull;
		this.engineSoundNameHalf = engineSoundNameHalf;
		this.victoryPointCost = victoryPointCost;
		this.availableInMultiplayer = availableInMultiplayer;
		this.availableNameID00 = availableNameID00;
		this.availableNameID01 = availableNameID01;
		this.availableNameID02 = availableNameID02;
		this.availableNameID03 = availableNameID03;
		this.availableNameID04 = availableNameID04;
		this.availableNameID05 = availableNameID05;
		this.availableNameID06 = availableNameID06;
		this.availableNameID07 = availableNameID07;
		this.availableNameID08 = availableNameID08;
		this.availableNameID09 = availableNameID09;
		this.availableNameID10 = availableNameID10;
		this.availableNameID11 = availableNameID11;
		this.maxNumberOfGunners = maxNumberOfGunners;
		this.maxNumberOfFirstMates = maxNumberOfFirstMates;
		this.maxNumberOfNavigators = maxNumberOfNavigators;
		this.maxNumberOfEngineers = maxNumberOfEngineers;
		this.maxNumberOfRiggers = maxNumberOfRiggers;
		this.maxNumberOfFighters = maxNumberOfFighters;
		this.maxNumberOfLookouts = maxNumberOfLookouts;
		this.repairEffect = repairEffect;
	}


	@Override
	public int getNumInternalLines () {
		return 36 + guiInfoChunk.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 42); // <number of lines> ConstData

		scanner.nextLine (); // {
		guiInfoChunk = new GUIInfoChunk (scanner);
		nameID = readString (scanner);
		race = Race.valueOf (readString (scanner));
		isTender = readBoolean (scanner);
		numberOfLifeboats = readInt (scanner);
		isLifeboat = readBoolean (scanner);
		isCloakable = readBoolean (scanner);
		shipSize = readInt (scanner);
		coreDamageSectionMaxHitPoints = readInt (scanner);
		explosionEffect = Effect.effects.get (readString (scanner));
		engineType = EngineType.valueOf (readString (scanner));
		engineSoundNameEmergency = readString (scanner);
		engineSoundNameFull = readString (scanner);
		engineSoundNameHalf = readString (scanner);
		victoryPointCost = readInt (scanner);
		availableInMultiplayer = readBoolean (scanner);
		availableNameID00 = readString (scanner);
		availableNameID01 = readString (scanner);
		availableNameID02 = readString (scanner);
		availableNameID03 = readString (scanner);
		availableNameID04 = readString (scanner);
		availableNameID05 = readString (scanner);
		availableNameID06 = readString (scanner);
		availableNameID07 = readString (scanner);
		availableNameID08 = readString (scanner);
		availableNameID09 = readString (scanner);
		availableNameID10 = readString (scanner);
		availableNameID11 = readString (scanner);
		maxNumberOfGunners = readInt (scanner);
		maxNumberOfCaptains = readInt (scanner);
		maxNumberOfFirstMates = readInt (scanner);
		maxNumberOfNavigators = readInt (scanner);
		maxNumberOfEngineers = readInt (scanner);
		maxNumberOfRiggers = readInt (scanner);
		maxNumberOfFighters = readInt (scanner);
		maxNumberOfLookouts = readInt (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (2400)
		        .append ("\n00000042 ConstData")
		        .append ("\n{")
		        .append (guiInfoChunk.toCode (0))
		        .append ("\n\tDisplayable Shipname String ID String '").append (nameID).append ('\'')
		        .append ("\n\tShip Race String '").append (race).append ('\'')
		        .append ("\n\tIs Tender Bool ").append (toBool (isTender))
		        .append ("\n\tNumber of lifeboats Int ").append (numberOfLifeboats)
		        .append ("\n\tIs Lifeboat Bool ").append (toBool (isLifeboat))
		        .append ("\n\tIs Cloakable Bool ").append (toBool (isCloakable))
		        .append ("\n\tShip Size Int ").append (shipSize)
		        .append ("\n\tCore Damage Section Max HitPoints Int ").append (coreDamageSectionMaxHitPoints)
		        .append ("\n\tExplosion Effect Name String '").append (explosionEffect).append ('\'')
		        .append ("\n\tEngineType String '").append (engineType).append ('\'')
		        .append ("\n\tEngineSoundName Emergency String '").append (engineSoundNameEmergency).append ('\'')
		        .append ("\n\tEngineSoundName Full String '").append (engineSoundNameFull).append ('\'')
		        .append ("\n\tEngineSoundName Half String '").append (engineSoundNameHalf).append ('\'')
		        .append ("\n\tVictory Point Cost Int ").append (victoryPointCost)
		        .append ("\n\tAvailable in Multiplayer? Bool ").append (toBool (availableInMultiplayer))
		        .append ("\n\tAvailable Unique Ship Name ID 00 String '").append (availableNameID00).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 01 String '").append (availableNameID01).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 02 String '").append (availableNameID02).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 03 String '").append (availableNameID03).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 04 String '").append (availableNameID04).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 05 String '").append (availableNameID05).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 06 String '").append (availableNameID06).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 07 String '").append (availableNameID07).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 08 String '").append (availableNameID08).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 09 String '").append (availableNameID09).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 10 String '").append (availableNameID10).append ('\'')
		        .append ("\n\tAvailable Unique Ship Name ID 11 String '").append (availableNameID11).append ('\'')
		        .append ("\n\tMax number of Gunners Int ").append (maxNumberOfGunners)
		        .append ("\n\tMax number of Captains Int ").append (maxNumberOfCaptains)
		        .append ("\n\tMax number of First Mates Int ").append (maxNumberOfFirstMates)
		        .append ("\n\tMax number of Navigators Int ").append (maxNumberOfNavigators)
		        .append ("\n\tMax number of Engineers Int ").append (maxNumberOfEngineers)
		        .append ("\n\tMax number of Riggers Int ").append (maxNumberOfRiggers)
		        .append ("\n\tMax number of Fighters Int ").append (maxNumberOfFighters)
		        .append ("\n\tMax number of Lookouts Int ").append (maxNumberOfLookouts)
		        .append ("\n\tRepair Effect Name String '").append (repairEffect).append ('\'')
		        .append ("\n}");
	}

}
