package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.GUIInfoChunk;
import data.worldObjects.elements.ReportType;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_DragonCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class DragonCustomInfoFactory implements ConstDataCustomInfo {

	/** The {@link GUIInfoChunk} */
	public GUIInfoChunk	guiInfoChunk;

	/** The shortest time between sounds */
	public float		shortestTimeBetweenSounds;

	/** The longest time between sounds */
	public float		longestTimeBetweenSounds;

	/** Sound 0 */
	public String		sound_0;
	/** Sound 1 */
	public String		sound_1;
	/** Sound 2 */
	public String		sound_2;
	/** Sound 3 */
	public String		sound_3;
	/** Sound 4 */
	public String		sound_4;
	/** Sound 5 */
	public String		sound_5;
	/** Sound 6 */
	public String		sound_6;
	/** Sound 7 */
	public String		sound_7;
	/** Sound 8 */
	public String		sound_8;
	/** Sound 9 */
	public String		sound_9;

	/** Do spotters report this object? */
	public boolean		reportSpottingBool;
	/** Type of report */
	public ReportType	reportType;
	/** Max sound distance */
	public float		soundDistance;



	/**
	 * Creates a new {@link DragonCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public DragonCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link DragonCustomInfoFactory}
	 *
	 * @param guiInfoChunk The {@link GUIInfoChunk}
	 * @param shortestTimeBetweenSounds The shortest time between sounds
	 * @param longestTimeBetweenSounds The longest time between sounds
	 * @param sound_0 Sound 0
	 * @param sound_1 Sound 1
	 * @param sound_2 Sound 2
	 * @param sound_3 Sound 3
	 * @param sound_4 Sound 4
	 * @param sound_5 Sound 5
	 * @param sound_6 Sound 6
	 * @param sound_7 Sound 7
	 * @param sound_8 Sound 8
	 * @param sound_9 Sound 9
	 * @param reportSpottingBool Do spotters report this object?
	 * @param reportType Type of report
	 * @param soundDistance Max sound distance
	 */
	public DragonCustomInfoFactory (GUIInfoChunk guiInfoChunk, float shortestTimeBetweenSounds, float longestTimeBetweenSounds,
	                                String sound_0, String sound_1, String sound_2, String sound_3, String sound_4, String sound_5, String sound_6, String sound_7, String sound_8, String sound_9,
	                                boolean reportSpottingBool, ReportType reportType, float soundDistance) {
		super ();
		this.guiInfoChunk = guiInfoChunk;
		this.shortestTimeBetweenSounds = shortestTimeBetweenSounds;
		this.longestTimeBetweenSounds = longestTimeBetweenSounds;
		this.sound_0 = sound_0;
		this.sound_1 = sound_1;
		this.sound_2 = sound_2;
		this.sound_3 = sound_3;
		this.sound_4 = sound_4;
		this.sound_5 = sound_5;
		this.sound_6 = sound_6;
		this.sound_7 = sound_7;
		this.sound_8 = sound_8;
		this.sound_9 = sound_9;
		this.reportSpottingBool = reportSpottingBool && reportType != null;
		this.reportType = reportSpottingBool ? reportType : null;
		this.soundDistance = soundDistance;
	}


	@Override
	public final int getNumInternalLines () {
		return (reportSpottingBool && reportType != null) ? 19 : 18;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		int n = readNumLines (scanner); // <number of lines> ConstData

		scanner.nextLine (); // {
		guiInfoChunk = new GUIInfoChunk (scanner);
		shortestTimeBetweenSounds = readFloat (scanner);
		longestTimeBetweenSounds = readFloat (scanner);
		sound_0 = readString (scanner);
		sound_1 = readString (scanner);
		sound_2 = readString (scanner);
		sound_3 = readString (scanner);
		sound_4 = readString (scanner);
		sound_5 = readString (scanner);
		sound_6 = readString (scanner);
		sound_7 = readString (scanner);
		sound_8 = readString (scanner);
		sound_9 = readString (scanner);
		reportSpottingBool = readBoolean (scanner);
		reportType = (n == 19 && reportSpottingBool) ? ReportType.valueOf (readString (scanner)) : null;
		soundDistance = readFloat (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (400)
		        .append ("\n").append (toChars8 (getNumInternalLines ())).append (" ConstData")
		        .append ("\n{")
		        .append (guiInfoChunk.toCode (0))
		        .append ("\n\tShortestTimeBetweenSounds Float ").append (toFloat6 (shortestTimeBetweenSounds))
		        .append ("\n\tLongestTimeBetweenSounds Float ").append (toFloat6 (longestTimeBetweenSounds))
		        .append ("\n\tSound_0 String '").append (sound_0).append ('\'')
		        .append ("\n\tSound_1 String '").append (sound_1).append ('\'')
		        .append ("\n\tSound_2 String '").append (sound_2).append ('\'')
		        .append ("\n\tSound_3 String '").append (sound_3).append ('\'')
		        .append ("\n\tSound_4 String '").append (sound_4).append ('\'')
		        .append ("\n\tSound_5 String '").append (sound_5).append ('\'')
		        .append ("\n\tSound_6 String '").append (sound_6).append ('\'')
		        .append ("\n\tSound_7 String '").append (sound_7).append ('\'')
		        .append ("\n\tSound_8 String '").append (sound_8).append ('\'')
		        .append ("\n\tSound_9 String '").append (sound_9).append ('\'')
		        .append ("\n\tReport spotting Bool ").append (toBool (reportSpottingBool));
		if (reportSpottingBool && reportType != null) {
			sb.append ("\n\tReport type String '").append (reportType).append ('\'');
		}
		return sb.append ("\n\tSound Distance Float ").append (getFloat3 (soundDistance))
		        .append ("\n}");
	}

}
