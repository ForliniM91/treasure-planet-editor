package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypePhysics;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Vector3;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypePhysics#FT_SpaceObjectPhysics}
 *
 * @author MarcoForlini
 */
public class SpaceObjectPhysics implements ConstDataPhysics {

	/** Center of mass */
	public Vector3	centerOfMass;

	/** Mass */
	public float	mass;

	/** Acceleration */
	public float	acceleration;





	/**
	 * Creates a new {@link SpaceObjectPhysics}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public SpaceObjectPhysics (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link SpaceObjectPhysics}
	 *
	 * @param centerOfMass The center of mass
	 * @param mass The mass
	 * @param acceleration The acceleration
	 */
	public SpaceObjectPhysics (Vector3 centerOfMass, float mass, float acceleration) {
		super ();
		this.centerOfMass = centerOfMass;
		this.mass = mass;
		this.acceleration = acceleration;
	}


	@Override
	public final int getNumInternalLines () {
		return 3;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 3); // <number of lines> ConstData

		scanner.nextLine (); // {
		centerOfMass = readVector (scanner);
		mass = readFloat (scanner);
		acceleration = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (137)
		        .append ("\n00000003 ConstData")
		        .append ("\n{")
		        .append ("\n\tCenterOfMass ").append (centerOfMass)
		        .append ("\n\tMass Float ").append (toFloat6 (mass))
		        .append ("\n\tAcceleration Float ").append (toFloat6 (acceleration))
		        .append ("\n}");
	}
}
