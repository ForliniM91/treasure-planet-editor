package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeAI;
import data.worldObjects.elements.UnexpectedNumLines;


/**
 * The ConstData for {@link FactoryTypeAI#FT_SpaceAnimalAI}
 *
 * @author MarcoForlini
 */
public class SpaceAnimalAI implements ConstDataAI {

	/** Singletone instance */
	public static final SpaceAnimalAI instance = new SpaceAnimalAI ();

	/**
	 * Dummy constructor. It doesn't build a new object, and just return the singletone instance
	 *
	 * @param s The scanner
	 * @return The singletone instance
	 * @throws UnexpectedNumLines If the number of lines is wrong
	 */
	public static final SpaceAnimalAI dummyConstructor (Scanner s) throws UnexpectedNumLines {
		instance.readCode (s);
		return instance;
	}


	private SpaceAnimalAI () {}


	@Override
	public final int getNumInternalLines () {
		return 0;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 0); // <number of lines> ConstData
		scanner.nextLine (); // {
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return "\n00000000 ConstData\n{\n}";
	}

}
