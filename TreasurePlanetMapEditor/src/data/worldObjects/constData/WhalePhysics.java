package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypePhysics;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Vector3;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypePhysics#FT_DragonPhysics}
 *
 * @author MarcoForlini
 */
public class WhalePhysics implements ConstDataPhysics {

	/** Center of mass */
	public Vector3	centerOfMass;

	/** Mass */
	public float	mass;

	/** Max thrust */
	public float	maxThrust;

	/** Max speed */
	public float	maxSpeed;

	/** Rotational friction */
	public float	rotationalFriction;

	/** Max angular acceleration */
	public float	maxAngularAcceleration;

	/** Max dive pitch */
	public float	maxDivePitch;

	/** Max climb pitch */
	public float	maxClimbPitch;

	/** Dive time */
	public float	diveTime;





	/**
	 * Creates a new {@link WhalePhysics}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public WhalePhysics (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link WhalePhysics}
	 *
	 * @param centerOfMass The center of mass
	 * @param mass The mass
	 * @param maxThrust The max thrust
	 * @param maxSpeed The max speed
	 * @param rotationalFriction The rotational friction
	 * @param maxAngularAcceleration The max angular acceleration
	 * @param maxDivePitch The max dive pitch
	 * @param maxClimbPitch The max climb pitch
	 * @param diveTime The dive time
	 */
	public WhalePhysics (Vector3 centerOfMass, float mass, float maxThrust, float maxSpeed, float rotationalFriction,
	                     float maxAngularAcceleration, float maxDivePitch, float maxClimbPitch, float diveTime) {
		this.centerOfMass = centerOfMass;
		this.mass = mass;
		this.maxThrust = maxThrust;
		this.maxSpeed = maxSpeed;
		this.rotationalFriction = rotationalFriction;
		this.maxAngularAcceleration = maxAngularAcceleration;
		this.maxDivePitch = maxDivePitch;
		this.maxClimbPitch = maxClimbPitch;
		this.diveTime = diveTime;
	}


	@Override
	public final int getNumInternalLines () {
		return 9;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 9); // <number of lines> ConstData

		scanner.nextLine (); // {
		centerOfMass = readVector (scanner);
		mass = readFloat (scanner);
		maxThrust = readFloat (scanner);
		maxSpeed = readFloat (scanner);
		rotationalFriction = readFloat (scanner);
		maxAngularAcceleration = readFloat (scanner);
		maxDivePitch = readFloat (scanner);
		maxClimbPitch = readFloat (scanner);
		diveTime = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder ("\n00000009 ConstData")
		        .append ("\n{")
		        .append ("\n\tCenterOfMass").append (centerOfMass)
		        .append ("\n\tMass Float ").append (toFloat6 (mass))
		        .append ("\n\tMaxThrust Float) ").append (toFloat6 (maxThrust))
		        .append ("\n\tMaxSpeed Float ").append (toFloat6 (maxSpeed))
		        .append ("\n\tRotationalFriction Float ").append (toFloat6 (rotationalFriction))
		        .append ("\n\tMaxAngularAcceleration Float ").append (toFloat6 (maxAngularAcceleration))
		        .append ("\n\tMax Dive Pitch Float ").append (toFloat6 (maxDivePitch))
		        .append ("\n\tMax Climb Pitch Float ").append (toFloat6 (maxClimbPitch))
		        .append ("\n\tDive Time Float ").append (toFloat6 (diveTime))
		        .append ("\n}");
	}

}
