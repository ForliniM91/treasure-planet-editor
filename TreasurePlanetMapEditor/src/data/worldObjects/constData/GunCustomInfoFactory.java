package data.worldObjects.constData;

import java.util.Scanner;

import data.effects.Effect;
import data.worldObjects.WorldObject;
import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.FiringCrewAlert;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.MountType;
import entities.Race;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_GunCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class GunCustomInfoFactory implements ConstDataCustomInfo {

	/** Primary bullet */
	public WorldObject		primaryBullet;
	/** Secondary bullet */
	public WorldObject		secondaryBullet;
	/** Sound name */
	public String			soundName;
	/** Muzzle flash effect */
	public Effect			muzzleFlashEffect;
	/** Muzzle speed */
	public float			muzzleSpeed;

	/** Pause time between bullets */
	public float			duringBurstReloadTime;
	/** Number of bullets per burst */
	public int				bulletsPerBurst;
	/** Weapon reload time */
	public float			timeBetweenBurst;
	/** Recalculate accuracy for each bullet */
	public boolean			calculateAccuracyDeviationDuringBurst;

	/** Gun texture in the weapon bar */
	public String			weaponBarIconTexture;

	/** Vertical rotation speed */
	public float			verticalRotationSpeed;
	/** Horizontal rotation speed */
	public float			horizontalRotationSpeed;
	/** Min vertical angle */
	public float			verticalMinAngle;
	/** Max vertical angle */
	public float			verticalMaxAngle;
	/** Log angle */
	public float			lobAngle;

	/** Max range (can't shot farther) */
	public float			maximumRange;
	/** Long range (above this distance, weapon have low precision) */
	public float			longRange;
	/** Effective range (below this distance, weapon have high precision) */
	public float			effectiveRange;
	/** Is bullet affected by gravity? */
	public boolean			bulletAffectedByGravity;
	/** Max range accuracy deviation */
	public float			maximumRange_AD;
	/** Long range accuracy deviation */
	public float			longRange_AD;
	/** Effective range accuracy deviation */
	public float			effectiveRange_AD;
	/** Max range ricochet factor */
	public float			maximumRange_RF;
	/** Long range ricochet factor */
	public float			longRange_RF;
	/** Effective range ricochet factor */
	public float			effectiveRange_RF;

	/** Victory point cost */
	public int				victoryPointCost;
	/** ID of name string */
	public String			nameID;
	/** Mount type */
	public MountType		mountType;
	/** Is not available to this race */
	public Race				exclusionRace;
	/** Can be fired while cloaked? */
	public boolean			canBeFiredWhileCloaked;
	/** Firing crew alert */
	public FiringCrewAlert	firingCrewAlert;
	/** Sound max distance */
	public float			soundMaxDistance;
	/** Sound volume */
	public float			soundVolume;





	/**
	 * Creates a new {@link GunCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public GunCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link GunCustomInfoFactory}
	 *
	 * @param primaryBullet Primary bullet
	 * @param secondaryBullet Secondary bullet
	 * @param soundName Sound name
	 * @param muzzleFlashEffect Muzzle flash effect
	 * @param muzzleSpeed Muzzle speed
	 * @param duringBurstReloadTime Pause time between bullets
	 * @param bulletsPerBurst Number of bullets per burst
	 * @param timeBetweenBurst Weapon reload time
	 * @param calculateAccuracyDeviationDuringBurst Recalculate accuracy for each bullet
	 * @param weaponBarIconTexture Gun texture in the weapon bar
	 * @param verticalRotationSpeed Vertical rotation speed
	 * @param horizontalRotationSpeed Horizontal rotation speed
	 * @param verticalMinAngle Min vertical angle
	 * @param verticalMaxAngle Max vertical angle
	 * @param lobAngle Log angle
	 * @param maximumRange Max range (can't shot farther)
	 * @param longRange Long range (above this distance, weapon have low precision)
	 * @param effectiveRange Effective range (below this distance, weapon have high precision)
	 * @param bulletAffectedByGravity Is bullet affected by gravity?
	 * @param maximumRange_AD Max range accuracy deviation
	 * @param longRange_AD Long range accuracy deviation
	 * @param effectiveRange_AD Effective range accuracy deviation
	 * @param maximumRange_RF Max range ricochet factor
	 * @param longRange_RF Long range ricochet factor
	 * @param effectiveRange_RF Effective range ricochet factor
	 * @param victoryPointCost Victory point cost
	 * @param nameID ID of name string
	 * @param mountType Mount type
	 * @param exclusionRace Is not available to this race
	 * @param canBeFiredWhileCloaked Can be fired while cloaked?
	 * @param firingCrewAlert Firing crew alert
	 * @param soundMaxDistance Sound max distance
	 * @param soundVolume Sound volume
	 */
	public GunCustomInfoFactory (WorldObject primaryBullet, WorldObject secondaryBullet, String soundName, Effect muzzleFlashEffect,
	                             float muzzleSpeed, float duringBurstReloadTime, int bulletsPerBurst, float timeBetweenBurst, boolean calculateAccuracyDeviationDuringBurst, String weaponBarIconTexture,
	                             float verticalRotationSpeed, float horizontalRotationSpeed, float verticalMinAngle, float verticalMaxAngle, float lobAngle,
	                             float maximumRange, float longRange, float effectiveRange, boolean bulletAffectedByGravity,
	                             float maximumRange_AD, float longRange_AD, float effectiveRange_AD, float maximumRange_RF, float longRange_RF, float effectiveRange_RF,
	                             int victoryPointCost, String nameID, MountType mountType, Race exclusionRace, boolean canBeFiredWhileCloaked,
	                             FiringCrewAlert firingCrewAlert, float soundMaxDistance, float soundVolume) {
		this.primaryBullet = primaryBullet;
		this.secondaryBullet = secondaryBullet;
		this.soundName = soundName;
		this.muzzleFlashEffect = muzzleFlashEffect;
		this.muzzleSpeed = muzzleSpeed;
		this.duringBurstReloadTime = duringBurstReloadTime;
		this.bulletsPerBurst = bulletsPerBurst;
		this.timeBetweenBurst = timeBetweenBurst;
		this.calculateAccuracyDeviationDuringBurst = calculateAccuracyDeviationDuringBurst;
		this.weaponBarIconTexture = weaponBarIconTexture;
		this.verticalRotationSpeed = verticalRotationSpeed;
		this.horizontalRotationSpeed = horizontalRotationSpeed;
		this.verticalMinAngle = verticalMinAngle;
		this.verticalMaxAngle = verticalMaxAngle;
		this.lobAngle = lobAngle;
		this.maximumRange = maximumRange;
		this.longRange = longRange;
		this.effectiveRange = effectiveRange;
		this.bulletAffectedByGravity = bulletAffectedByGravity;
		this.maximumRange_AD = maximumRange_AD;
		this.longRange_AD = longRange_AD;
		this.effectiveRange_AD = effectiveRange_AD;
		this.maximumRange_RF = maximumRange_RF;
		this.longRange_RF = longRange_RF;
		this.effectiveRange_RF = effectiveRange_RF;
		this.victoryPointCost = victoryPointCost;
		this.nameID = nameID;
		this.mountType = mountType;
		this.exclusionRace = exclusionRace;
		this.canBeFiredWhileCloaked = canBeFiredWhileCloaked;
		this.firingCrewAlert = firingCrewAlert;
		this.soundMaxDistance = soundMaxDistance;
		this.soundVolume = soundVolume;
	}


	@Override
	public final int getNumInternalLines () {
		return 33;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 33); // <number of lines> ConstData
		scanner.nextLine (); // {
		primaryBullet = WorldObject.objects.get (readString (scanner));
		secondaryBullet = WorldObject.objects.get (readString (scanner));
		soundName = readString (scanner);
		muzzleFlashEffect = Effect.effects.get (readString (scanner));
		muzzleSpeed = readFloat (scanner);
		duringBurstReloadTime = readFloat (scanner);
		bulletsPerBurst = readInt (scanner);
		timeBetweenBurst = readFloat (scanner);
		calculateAccuracyDeviationDuringBurst = readBoolean (scanner);
		weaponBarIconTexture = readString (scanner);
		verticalRotationSpeed = readFloat (scanner);
		horizontalRotationSpeed = readFloat (scanner);
		verticalMinAngle = readFloat (scanner);
		verticalMaxAngle = readFloat (scanner);
		lobAngle = readFloat (scanner);
		maximumRange = readFloat (scanner);
		longRange = readFloat (scanner);
		effectiveRange = readFloat (scanner);
		bulletAffectedByGravity = readBoolean (scanner);
		maximumRange_AD = readFloat (scanner);
		longRange_AD = readFloat (scanner);
		effectiveRange_AD = readFloat (scanner);
		maximumRange_RF = readFloat (scanner);
		longRange_RF = readFloat (scanner);
		effectiveRange_RF = readFloat (scanner);
		victoryPointCost = readInt (scanner);
		nameID = readString (scanner);
		mountType = MountType.valueOf (readString (scanner));
		exclusionRace = Race.valueOf (readString (scanner));
		canBeFiredWhileCloaked = readBoolean (scanner);
		firingCrewAlert = FiringCrewAlert.valueOf (readString (scanner));
		soundMaxDistance = readFloat (scanner);
		soundVolume = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (1600)
		        .append ("\n000000033 ConstData")
		        .append ("\n{")
		        .append ("\n\tBulletTypeName_Primary String '").append (primaryBullet).append ('\'')
		        .append ("\n\tBulletTypeName_Secondary String '").append (secondaryBullet).append ('\'')
		        .append ("\n\tSoundName String '").append (soundName).append ('\'')
		        .append ("\n\tMuzzleFlashEffect String '").append (muzzleFlashEffect).append ('\'')
		        .append ("\n\tMuzzleSpeed Float ").append (toFloat6 (muzzleSpeed))
		        .append ("\n\tDuringBurstReloadTime Float ").append (toFloat6 (duringBurstReloadTime))
		        .append ("\n\tBulletsPerBurst Int ").append (bulletsPerBurst)
		        .append ("\n\tTimeBetweenBursts Float ").append (toFloat6 (timeBetweenBurst))
		        .append ("\n\tCalculateAccuracyDeviationDuringBurst Bool ").append (toBool (calculateAccuracyDeviationDuringBurst))
		        .append ("\n\tWeaponBar Icon Texture String '").append (weaponBarIconTexture).append ('\'')
		        .append ("\n\tVerticalRotationSpeed Float ").append (toFloat6 (verticalRotationSpeed))
		        .append ("\n\tHorizontalRotationSpeed Float ").append (toFloat6 (horizontalRotationSpeed))
		        .append ("\n\tVerticalMinAngle Float ").append (toFloat6 (verticalMinAngle))
		        .append ("\n\tVerticalMaxAngle Float ").append (toFloat6 (verticalMaxAngle))
		        .append ("\n\tLobAngle Float ").append (toFloat6 (lobAngle))
		        .append ("\n\tMaximumRange_Range Float ").append (toFloat6 (maximumRange))
		        .append ("\n\tLongRange_Range Float ").append (toFloat6 (longRange))
		        .append ("\n\tEffectiveRange_Range Float ").append (toFloat6 (effectiveRange))
		        .append ("\n\tBulletAffectedByGravity Bool ").append (toBool (bulletAffectedByGravity))
		        .append ("\n\tMaximumRange_AccuracyDeviation Float ").append (toFloat6 (maximumRange_AD))
		        .append ("\n\tLongRange_AccuracyDeviation Float ").append (toFloat6 (longRange_AD))
		        .append ("\n\tEffectiveRange_AccuracyDeviation Float ").append (toFloat6 (effectiveRange_AD))
		        .append ("\n\tMaximumRange_RicochetFactor Float ").append (toFloat6 (maximumRange_RF))
		        .append ("\n\tLongRange_RicochetFactor Float ").append (toFloat6 (longRange_RF))
		        .append ("\n\tEffectiveRange_RicochetFactor Float ").append (toFloat6 (effectiveRange_RF))
		        .append ("\n\tVictory Point Cost Int ").append (victoryPointCost)
		        .append ("\n\tGun Name String ID String '").append (nameID).append ('\'')
		        .append ("\n\tMount Type String '").append (mountType).append ('\'')
		        .append ("\n\tExclusion Race String '").append (exclusionRace).append ('\'')
		        .append ("\n\tCan Be Fired While Cloaked Bool ").append (toBool (canBeFiredWhileCloaked))
		        .append ("\n\tFiring Crew Alert String '").append (firingCrewAlert).append ('\'')
		        .append ("\n\tSoundMaxDistance Float ").append (toFloat6 (soundMaxDistance))
		        .append ("\n\tSoundVolume Float ").append (toFloat6 (soundVolume))
		        .append ("\n}");
	}

}
