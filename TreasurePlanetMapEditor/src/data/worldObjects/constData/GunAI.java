package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeAI;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeAI#FT_GunAI}
 *
 * @author MarcoForlini
 */
public class GunAI implements ConstDataAI {

	/** Attack area */
	public boolean	lobbing;

	/** Places mines */
	public boolean	mineLaying;

	/** Launches torpedos */
	public boolean	torpedoLauncher;

	/** Automatically shots mines and torpedo */
	public boolean	pointDefense;




	/**
	 * Creates a new {@link GunAI}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public GunAI (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link GunAI}
	 *
	 * @param lobbing Attack area
	 * @param mineLaying Places mines
	 * @param torpedoLauncher Launches torpedos
	 * @param pointDefense Automatically shots mines and torpedos
	 */
	public GunAI (boolean lobbing, boolean mineLaying, boolean torpedoLauncher, boolean pointDefense) {
		this.lobbing = lobbing;
		this.mineLaying = mineLaying;
		this.torpedoLauncher = torpedoLauncher;
		this.pointDefense = pointDefense;
	}


	@Override
	public final int getNumInternalLines () {
		return 4;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 4); // <number of lines> ConstData

		scanner.nextLine (); // {
		lobbing = readBoolean (scanner);
		mineLaying = readBoolean (scanner);
		torpedoLauncher = readBoolean (scanner);
		pointDefense = readBoolean (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (156)
		        .append ("\n00000004 ConstData")
		        .append ("\n{")
		        .append ("\n\tIs Lobbing Gun Bool ").append (toBool (lobbing))
		        .append ("\n\tIs MineLaying Gun Bool ").append (toBool (mineLaying))
		        .append ("\n\tIs TorpedoLauncher Gun Bool ").append (toBool (torpedoLauncher))
		        .append ("\n\tIs PointDefense Gun Bool ").append (toBool (pointDefense))
		        .append ("\n}");
	}

}
