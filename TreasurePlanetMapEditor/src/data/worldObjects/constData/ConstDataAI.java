package data.worldObjects.constData;

/**
 * ConstData for AI definition
 *
 * @author MarcoForlini
 */
public interface ConstDataAI extends ConstData {
	// Nothing to add
}
