package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.DetectionType;
import data.worldObjects.elements.FactoryTypeCollision;
import data.worldObjects.elements.ResponseType;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Vector3;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeCollision#FT_BoundingRenderEntity}
 *
 * @author MarcoForlini
 */
public class BoundingRenderEntity implements ConstDataCollision {

	/** Detection type */
	public DetectionType	detectionType;

	/** Response type */
	public ResponseType		responseType;

	/** Do object defines a custom sphere size? */
	public boolean			userDefinedSphereSize;
	/** Custom sphere center */
	public Vector3			localPosition;
	/** Custom sphere radius */
	public float			radius;

	/** Do object defines a custom bounding box? */
	public boolean			userDefinedBoundingBoxExtents;
	/** Custom bounding box min extends */
	public Vector3			minExtents;
	/** Custom bounding box max extends */
	public Vector3			maxExtents;




	/**
	 * Creates a new {@link BoundingRenderEntity}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public BoundingRenderEntity (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link BoundingRenderEntity}
	 *
	 * @param detectionType Detection type
	 * @param responseType Response type
	 * @param userDefinedSphereSize Do object defines a custom sphere size?
	 * @param localPosition Custom sphere center
	 * @param radius Custom sphere radius
	 * @param userDefinedBoundingBoxExtents Do object defines a custom bounding box?
	 * @param minExtents Custom bounding box min extends
	 * @param maxExtents Custom bounding box max extends
	 */
	public BoundingRenderEntity (DetectionType detectionType, ResponseType responseType, boolean userDefinedSphereSize, Vector3 localPosition, float radius, boolean userDefinedBoundingBoxExtents, Vector3 minExtents, Vector3 maxExtents) {
		this.detectionType = detectionType;
		this.responseType = responseType;
		this.userDefinedSphereSize = userDefinedSphereSize;
		this.localPosition = localPosition;
		this.radius = radius;
		this.userDefinedBoundingBoxExtents = userDefinedBoundingBoxExtents;
		this.minExtents = minExtents;
		this.maxExtents = maxExtents;
	}


	@Override
	public final int getNumInternalLines () {
		return userDefinedSphereSize ? (userDefinedBoundingBoxExtents ? 8 : 6) : (userDefinedBoundingBoxExtents ? 6 : 4);
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		int n = readNumLines (scanner); // <number of lines> ConstData

		scanner.nextLine (); // {
		detectionType = DetectionType.valueOf (readString (scanner));
		responseType = ResponseType.valueOf (readString (scanner));
		if (readBoolean (scanner)) { // User defined sphere size Bool
			userDefinedSphereSize = true;
			localPosition = readVector (scanner);
			radius = readFloat (scanner);
		} else {
			userDefinedSphereSize = false;
			localPosition = null;
			radius = 0;
		}
		if (readBoolean (scanner)) { // UserDefinedBoundingBoxExtents Bool
			userDefinedBoundingBoxExtents = true;
			minExtents = readVector (scanner);
			maxExtents = readVector (scanner);
		} else {
			userDefinedBoundingBoxExtents = false;
			minExtents = null;
			maxExtents = null;
		}
		scanner.nextLine (); // }

		checkNumLines (n);
	}



	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (500)
		        .append ('\n').append (toChars8 (getNumInternalLines ())).append (" ConstData")
		        .append ("\n{")
		        .append ("\n\tDetectionType String ").append (detectionType)
		        .append ("\n\tResponseType String ").append (responseType);
		if (userDefinedSphereSize) {
			sb.append ("\n\tUser defined sphere size Bool True")
			        .append ("\n\tLocalPosition ").append (localPosition)
			        .append ("\n\tRadius Float ").append (toFloat6 (radius));
		} else {
			sb.append ("\n\tUser defined sphere size Bool False ");
		}
		if (userDefinedBoundingBoxExtents) {
			sb.append ("\n\tUserDefinedBoundingBoxExtents Bool True")
			        .append ("\n\tMinExtents ").append (minExtents)
			        .append ("\n\tMaxExtents ").append (maxExtents);
		} else {
			sb.append ("\n\tUserDefinedBoundingBoxExtents Bool False ");
		}
		sb.append ("\n}");
		return sb.toString ();
	}

}
