package data.worldObjects.constData;

/**
 * ConstData for Render definition
 *
 * @author MarcoForlini
 */
public interface ConstDataRender extends ConstData {
	// Nothing to add
}
