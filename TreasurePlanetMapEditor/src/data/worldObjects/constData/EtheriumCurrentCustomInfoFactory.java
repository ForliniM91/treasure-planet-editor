package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_EtheriumCurrentCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class EtheriumCurrentCustomInfoFactory implements ConstDataCustomInfo {

	/** Pull strength */
	public float	magnitude;

	/** Size of the black hole */
	public float	radius;




	/**
	 * Creates a new {@link EtheriumCurrentCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public EtheriumCurrentCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link EtheriumCurrentCustomInfoFactory}
	 *
	 * @param magnitude Pull strength
	 * @param radius Size of the black hole
	 */
	public EtheriumCurrentCustomInfoFactory (float magnitude, float radius) {
		this.magnitude = magnitude;
		this.radius = radius;
	}


	@Override
	public final int getNumInternalLines () {
		return 2;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 2); // <number of lines> ConstData

		scanner.nextLine (); // {
		magnitude = readFloat (scanner);
		radius = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (80)
		        .append ("\n00000002 ConstData")
		        .append ("\n{")
		        .append ("\n\tMagnitude Float ")
		        .append ("\n\tRadius Float ")
		        .append ("\n}");
	}

}
