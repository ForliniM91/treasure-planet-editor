package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.DetectionType;
import data.worldObjects.elements.FactoryTypeCollision;
import data.worldObjects.elements.ResponseType;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * The ConstData for {@link FactoryTypeCollision#FT_Point}
 *
 * @author MarcoForlini
 */
public class Point implements ConstDataCollision {

	/** Detection type */
	public DetectionType	detectionType;

	/** Response type */
	public ResponseType		responseType;





	/**
	 * Creates a new {@link Point}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Point (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link Point}
	 *
	 * @param detectionType Detection type
	 * @param responseType Response type
	 */
	public Point (DetectionType detectionType, ResponseType responseType) {
		this.detectionType = detectionType;
		this.responseType = responseType;
	}


	@Override
	public final int getNumInternalLines () {
		return 2;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 2); // <number of lines> ConstData

		scanner.nextLine (); // {
		detectionType = DetectionType.valueOf (readString (scanner));
		responseType = ResponseType.valueOf (readString (scanner));
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (150)
		        .append ("\n00000002 ConstData")
		        .append ("\n{")
		        .append ("\n\tDetectionType String ").append (detectionType)
		        .append ("\n\tResponseType String ").append (responseType)
		        .append ("\n}");
	}

}
