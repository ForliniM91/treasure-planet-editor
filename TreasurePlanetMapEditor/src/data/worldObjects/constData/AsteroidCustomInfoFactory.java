package data.worldObjects.constData;

import java.util.Scanner;

import data.effects.Effect;
import data.worldObjects.WorldObject;
import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_AsteroidCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class AsteroidCustomInfoFactory implements ConstDataCustomInfo {

	/** Max hit points */
	public int			maxHitPoints;

	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath0;
	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath1;
	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath2;
	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath3;
	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath4;
	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath5;
	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath6;
	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath7;
	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath8;
	/** Create object upon death */
	public WorldObject	worldObjectToCreateUponDeath9;

	/** Explosion {@link Effect} */
	public Effect		explosionEffect;

	/** Do spotters report this object? */
	public boolean		reportSpottingBool;




	/**
	 * Creates a new {@link AsteroidCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public AsteroidCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link AsteroidCustomInfoFactory}
	 *
	 * @param maxHitPoints Max hit points
	 * @param worldObjectToCreateUponDeath0 Create object upon death
	 * @param worldObjectToCreateUponDeath1 Create object upon death
	 * @param worldObjectToCreateUponDeath2 Create object upon death
	 * @param worldObjectToCreateUponDeath3 Create object upon death
	 * @param worldObjectToCreateUponDeath4 Create object upon death
	 * @param worldObjectToCreateUponDeath5 Create object upon death
	 * @param worldObjectToCreateUponDeath6 Create object upon death
	 * @param worldObjectToCreateUponDeath7 Create object upon death
	 * @param worldObjectToCreateUponDeath8 Create object upon death
	 * @param worldObjectToCreateUponDeath9 Create object upon death
	 * @param explosionEffect Explosion {@link Effect}
	 * @param reportSpottingBool Do spotters report this object?
	 */
	public AsteroidCustomInfoFactory (int maxHitPoints,
	                                  WorldObject worldObjectToCreateUponDeath0, WorldObject worldObjectToCreateUponDeath1,
	                                  WorldObject worldObjectToCreateUponDeath2, WorldObject worldObjectToCreateUponDeath3,
	                                  WorldObject worldObjectToCreateUponDeath4, WorldObject worldObjectToCreateUponDeath5,
	                                  WorldObject worldObjectToCreateUponDeath6, WorldObject worldObjectToCreateUponDeath7,
	                                  WorldObject worldObjectToCreateUponDeath8, WorldObject worldObjectToCreateUponDeath9,
	                                  Effect explosionEffect, boolean reportSpottingBool) {
		super ();
		this.maxHitPoints = maxHitPoints;
		this.worldObjectToCreateUponDeath0 = worldObjectToCreateUponDeath0;
		this.worldObjectToCreateUponDeath1 = worldObjectToCreateUponDeath1;
		this.worldObjectToCreateUponDeath2 = worldObjectToCreateUponDeath2;
		this.worldObjectToCreateUponDeath3 = worldObjectToCreateUponDeath3;
		this.worldObjectToCreateUponDeath4 = worldObjectToCreateUponDeath4;
		this.worldObjectToCreateUponDeath5 = worldObjectToCreateUponDeath5;
		this.worldObjectToCreateUponDeath6 = worldObjectToCreateUponDeath6;
		this.worldObjectToCreateUponDeath7 = worldObjectToCreateUponDeath7;
		this.worldObjectToCreateUponDeath8 = worldObjectToCreateUponDeath8;
		this.worldObjectToCreateUponDeath9 = worldObjectToCreateUponDeath9;
		this.explosionEffect = explosionEffect;
		this.reportSpottingBool = reportSpottingBool;
	}



	@Override
	public final int getNumInternalLines () {
		return 13;
	}



	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 13); // <number of lines> ConstData

		scanner.nextLine (); // {
		maxHitPoints = Integer.parseInt (scanner.nextLine ().substring (20));
		worldObjectToCreateUponDeath0 = WorldObject.objects.get (readString (scanner));
		worldObjectToCreateUponDeath1 = WorldObject.objects.get (readString (scanner));
		worldObjectToCreateUponDeath2 = WorldObject.objects.get (readString (scanner));
		worldObjectToCreateUponDeath3 = WorldObject.objects.get (readString (scanner));
		worldObjectToCreateUponDeath4 = WorldObject.objects.get (readString (scanner));
		worldObjectToCreateUponDeath5 = WorldObject.objects.get (readString (scanner));
		worldObjectToCreateUponDeath6 = WorldObject.objects.get (readString (scanner));
		worldObjectToCreateUponDeath7 = WorldObject.objects.get (readString (scanner));
		worldObjectToCreateUponDeath8 = WorldObject.objects.get (readString (scanner));
		worldObjectToCreateUponDeath9 = WorldObject.objects.get (readString (scanner));
		explosionEffect = Effect.effects.get (readString (scanner));
		reportSpottingBool = readBoolean (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (1000)
		        .append ("\n00000013 ConstData")
		        .append ("\n{")
		        .append ("\n\tMax Hit Points Int ").append (maxHitPoints)
		        .append ("\n\tWorldObject To Create Upon Death 0 String '").append (worldObjectToCreateUponDeath0).append ('\'')
		        .append ("\n\tWorldObject To Create Upon Death 1 String '").append (worldObjectToCreateUponDeath1).append ('\'')
		        .append ("\n\tWorldObject To Create Upon Death 2 String '").append (worldObjectToCreateUponDeath2).append ('\'')
		        .append ("\n\tWorldObject To Create Upon Death 3 String '").append (worldObjectToCreateUponDeath3).append ('\'')
		        .append ("\n\tWorldObject To Create Upon Death 4 String '").append (worldObjectToCreateUponDeath4).append ('\'')
		        .append ("\n\tWorldObject To Create Upon Death 5 String '").append (worldObjectToCreateUponDeath5).append ('\'')
		        .append ("\n\tWorldObject To Create Upon Death 6 String '").append (worldObjectToCreateUponDeath6).append ('\'')
		        .append ("\n\tWorldObject To Create Upon Death 7 String '").append (worldObjectToCreateUponDeath7).append ('\'')
		        .append ("\n\tWorldObject To Create Upon Death 8 String '").append (worldObjectToCreateUponDeath8).append ('\'')
		        .append ("\n\tWorldObject To Create Upon Death 9 String '").append (worldObjectToCreateUponDeath9).append ('\'')
		        .append ("\n\tExplosionEffect String '").append (explosionEffect).append ('\'')
		        .append ("\n\tReport Spotting Bool ").append (toBool (reportSpottingBool))
		        .append ("\n}");
	}

}
