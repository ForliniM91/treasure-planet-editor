package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.DetectionType;
import data.worldObjects.elements.FactoryTypeCollision;
import data.worldObjects.elements.ResponseType;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Vector3;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeCollision#FT_BoundingSphere}
 *
 * @author MarcoForlini
 */
public class BoundingSphere implements ConstDataCollision {

	/** Detection type */
	public DetectionType	detectionType;

	/** Response type */
	public ResponseType		responseType;

	/** Do object defines a custom sphere size? */
	public boolean			userDefinedSphereSize;
	/** Custom sphere center */
	public Vector3			localPosition;
	/** Custom sphere radius */
	public float			radius;




	/**
	 * Creates a new {@link BoundingSphere}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public BoundingSphere (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link BoundingSphere}
	 *
	 * @param detectionType Detection type
	 * @param responseType Response type
	 * @param userDefinedSphereSize Do object defines a custom sphere size?
	 * @param localPosition Custom sphere center
	 * @param radius Custom sphere radius
	 */
	public BoundingSphere (DetectionType detectionType, ResponseType responseType, boolean userDefinedSphereSize, Vector3 localPosition, float radius) {
		this.detectionType = detectionType;
		this.responseType = responseType;
		this.userDefinedSphereSize = userDefinedSphereSize;
		this.localPosition = localPosition;
		this.radius = radius;
	}


	@Override
	public final int getNumInternalLines () {
		return userDefinedSphereSize ? 5 : 3;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		int n = readNumLines (scanner); // <number of lines> ConstData

		scanner.nextLine (); // {
		detectionType = DetectionType.valueOf (readString (scanner));
		responseType = ResponseType.valueOf (readString (scanner));
		if (readBoolean (scanner)) { // User defined sphere size Bool
			userDefinedSphereSize = true;
			localPosition = readVector (scanner);
			radius = readFloat (scanner);
		} else {
			userDefinedSphereSize = false;
			localPosition = null;
			radius = 0;
		}
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (300)
		        .append ('\n').append (toChars8 (getNumInternalLines ())).append (" ConstData")
		        .append ("\n{")
		        .append ("\n\tDetectionType String ").append (detectionType)
		        .append ("\n\tResponseType String ").append (responseType);
		if (userDefinedSphereSize) {
			sb.append ("\n\tUser defined sphere size Bool True")
			        .append ("\n\tLocalPosition ").append (localPosition)
			        .append ("\n\tRadius Float ").append (toFloat6 (radius));
		} else {
			sb.append ("\n\tUser defined sphere size Bool False ");
		}
		return sb.append ("\n}");
	}

}
