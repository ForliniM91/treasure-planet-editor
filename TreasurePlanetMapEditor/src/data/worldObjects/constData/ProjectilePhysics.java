package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypePhysics;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Vector3;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypePhysics#FT_ProjectilePhysics}
 *
 * @author MarcoForlini
 */
public class ProjectilePhysics implements ConstDataPhysics {

	/** Center of mass */
	public Vector3	centerOfMass;

	/** Mass */
	public float	mass;





	/**
	 * Creates a new {@link ProjectilePhysics}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public ProjectilePhysics (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link ProjectilePhysics}
	 *
	 * @param centerOfMass the center of mass
	 * @param mass the mass
	 */
	public ProjectilePhysics (Vector3 centerOfMass, float mass) {
		this.centerOfMass = centerOfMass;
		this.mass = mass;
	}


	@Override
	public final int getNumInternalLines () {
		return 2;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 2); // <number of lines> ConstData

		scanner.nextLine (); // {
		centerOfMass = readVector (scanner);
		mass = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (101)
		        .append ("\n00000002 ConstData")
		        .append ("\n{")
		        .append ("\n\tCenterOfMass").append (centerOfMass)
		        .append ("\n\tMass Float ").append (toFloat6 (mass))
		        .append ("\n}");
	}

}
