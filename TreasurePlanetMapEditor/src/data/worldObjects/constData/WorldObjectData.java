package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.Definition;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * @author MarcoForlini
 */
public class WorldObjectData implements ConstData {

	/** Name of Physics */
	public String	physics;

	/** Name of Collision */
	public String	collision;

	/** Name of Render */
	public String	render;

	/** Name of AI */
	public String	intelligence;

	/** Name of CustomInfo */
	public String	customInfo;





	/**
	 * Creates a new {@link WorldObjectData}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public WorldObjectData (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link WorldObjectData}
	 *
	 * @param ID ID of the object
	 * @param physics the name of Physics
	 * @param collision the name of Collision
	 * @param render the name of Render
	 * @param intelligence the name of AI
	 * @param customInfo the name of CustomInfo
	 */
	public WorldObjectData (String ID,
	                        Definition <? extends ConstDataPhysics> physics,
	                        Definition <? extends ConstDataCollision> collision,
	                        Definition <? extends ConstDataRender> render,
	                        Definition <? extends ConstDataAI> intelligence,
	                        Definition <? extends ConstDataCustomInfo> customInfo) {
		this.physics = physics != null ? ID : "";
		this.collision = collision != null ? ID : "";
		this.render = render != null ? ID : "";
		this.intelligence = intelligence != null ? ID : "";
		this.customInfo = customInfo != null ? ID : "";
	}

	/**
	 * Creates a new {@link WorldObjectData}
	 *
	 * @param ID ID of the object
	 * @param physics the name of Physics
	 * @param collision the name of Collision
	 * @param render the name of Render
	 * @param AI the name of AI
	 * @param customInfo the name of CustomInfo
	 */
	public WorldObjectData (String ID, boolean physics, boolean collision, boolean render, boolean AI, boolean customInfo) {
		this.physics = physics ? ID : "";
		this.collision = collision ? ID : "";
		this.render = render ? ID : "";
		intelligence = AI ? ID : "";
		this.customInfo = customInfo ? ID : "";
	}


	@Override
	public int getNumInternalLines () {
		return 5;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 5); // <number of lines> ConstData

		scanner.nextLine (); // {
		physics = readString (scanner);
		collision = readString (scanner);
		render = readString (scanner);
		intelligence = readString (scanner);
		customInfo = readString (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (200)
		        .append ("\n00000005 ConstData")
		        .append ("\n{")
		        .append ("\n\tHasPhysics String '").append (physics).append ('\'')
		        .append ("\n\tHasCollision String '").append (collision).append ('\'')
		        .append ("\n\tHasRender String '").append (render).append ('\'')
		        .append ("\n\tHasAI String '").append (intelligence).append ('\'')
		        .append ("\n\tHasCustomInfo String '").append (customInfo).append ('\'')
		        .append ("\n}");
	}

}
