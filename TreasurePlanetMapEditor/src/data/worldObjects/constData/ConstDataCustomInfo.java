package data.worldObjects.constData;

/**
 * ConstData for CustomInfo definition
 *
 * @author MarcoForlini
 */
public interface ConstDataCustomInfo extends ConstData {
	// Nothing to add
}
