package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeAI;
import data.worldObjects.elements.UnexpectedNumLines;


/**
 * The ConstData for {@link FactoryTypeAI#FT_SpaceObjectAI}
 *
 * @author MarcoForlini
 */
public class SpaceObjectAI implements ConstDataAI {

	/** Singletone instance */
	public static final SpaceObjectAI instance = new SpaceObjectAI ();

	/**
	 * Dummy constructor. It doesn't build a new object, and just return the singletone instance
	 *
	 * @param scanner The scanner to read
	 * @return The singletone instance
	 * @throws UnexpectedNumLines If the number of lines is wrong
	 */
	public static final SpaceObjectAI dummyConstructor (Scanner scanner) throws UnexpectedNumLines {
		instance.readCode (scanner);
		return instance;
	}


	private SpaceObjectAI () {}


	@Override
	public final int getNumInternalLines () {
		return 0;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 0); // <number of lines> ConstData
		scanner.nextLine (); // {
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return "\n00000000 ConstData\n{\n}";
	}

}
