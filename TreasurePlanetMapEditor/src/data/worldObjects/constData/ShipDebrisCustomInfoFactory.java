package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_ShipDebrisCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class ShipDebrisCustomInfoFactory implements ConstDataCustomInfo {

	/** Lifetime min */
	public float	lifeTimeMin;

	/** Lifetime max */
	public float	lifeTimeMax;





	/**
	 * Creates a new {@link ShipDebrisCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public ShipDebrisCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link ShipDebrisCustomInfoFactory}
	 *
	 * @param lifeTimeMin Lifetime min
	 * @param lifeTimeMax Lifetime max
	 */
	public ShipDebrisCustomInfoFactory (float lifeTimeMin, float lifeTimeMax) {
		this.lifeTimeMin = lifeTimeMin;
		this.lifeTimeMax = lifeTimeMax;
	}


	@Override
	public final int getNumInternalLines () {
		return 2;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 2); // <number of lines> ConstData

		scanner.nextLine (); // {
		lifeTimeMin = readFloat (scanner);
		lifeTimeMax = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (84)
		        .append ("\n00000002 ConstData")
		        .append ("\n{")
		        .append ("\n\tLifeTimeMin Float ").append (toFloat6 (lifeTimeMin))
		        .append ("\n\tLifeTimeMax Float ").append (toFloat6 (lifeTimeMax))
		        .append ("\n}");
	}

}
