package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Race;
import entities.Skill;
import entities.Species;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_CrewCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class CrewCustomInfoFactory implements ConstDataCustomInfo {

	/** Race */
	public Race		race;

	/** Recruitment cost */
	public int		points;

	/** Name ID */
	public String	nameID;

	/** Leadership skill */
	public Skill	leadership;
	/** Navigation skill */
	public Skill	navigation;
	/** Spotting skill */
	public Skill	spotting;
	/** Engineering skill */
	public Skill	engineering;
	/** Rigging skill */
	public Skill	rigging;
	/** Combat skill */
	public Skill	combat;
	/** Gunnery skill */
	public Skill	gunnery;

	/** Head texture */
	public String	headTexture;

	/** Species */
	public Species	species;





	/**
	 * Creates a new {@link CrewCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public CrewCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link CrewCustomInfoFactory}
	 *
	 * @param race Race
	 * @param points Recruitment cost
	 * @param nameID Name ID
	 * @param leadership Leadership skill
	 * @param navigation Navigation skill
	 * @param spotting Spotting skill
	 * @param engineering Engineering skill
	 * @param rigging Riggin skill
	 * @param combat Combat skill
	 * @param gunnery Gunnery skill
	 * @param headTexture Head texture
	 * @param species Species
	 */
	public CrewCustomInfoFactory (Race race, int points, String nameID, Skill leadership, Skill navigation, Skill spotting, Skill engineering, Skill rigging, Skill combat, Skill gunnery, String headTexture, Species species) {
		super ();
		this.race = race;
		this.points = points;
		this.nameID = nameID;
		this.leadership = leadership;
		this.navigation = navigation;
		this.spotting = spotting;
		this.engineering = engineering;
		this.rigging = rigging;
		this.combat = combat;
		this.gunnery = gunnery;
		this.headTexture = headTexture;
		this.species = species;
	}


	@Override
	public final int getNumInternalLines () {
		return 19;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		readNumLines (scanner, 19); // <number of lines> ConstData

		scanner.nextLine (); // {
		race = Race.valueOf (readString (scanner));
		points = Integer.parseInt (scanner.nextLine ().substring (17));
		nameID = readString (scanner);
		leadership = new Skill (scanner);
		navigation = new Skill (scanner);
		spotting = new Skill (scanner);
		engineering = new Skill (scanner);
		rigging = new Skill (scanner);
		combat = new Skill (scanner);
		gunnery = new Skill (scanner);
		headTexture = readString (scanner);
		species = Species.valueOf (readString (scanner));
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (900)
		        .append ("\n00000019 ConstData")
		        .append ("\n{")
		        .append ("\n\tCrew Race String '").append (race).append ('\'')
		        .append ("\n\tPoint Value Int ").append (points)
		        .append ("\n\tCrew Name String ID String '").append (nameID).append ('\'')
		        .append (leadership.toCode (0))
		        .append (navigation.toCode (0))
		        .append (spotting.toCode (0))
		        .append (engineering.toCode (0))
		        .append (rigging.toCode (0))
		        .append (combat.toCode (0))
		        .append (gunnery.toCode (0))
		        .append ("\n\tTalking Head Texture String '").append (headTexture).append ('\'')
		        .append ("\n\tSpecies String '").append (species).append ('\'')
		        .append ("\n}");
	}

}
