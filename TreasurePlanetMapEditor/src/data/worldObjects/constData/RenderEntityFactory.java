package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeRender;
import data.worldObjects.elements.MeshAttributeManager;
import data.worldObjects.elements.RenderEffect;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeRender#FT_RenderEntityFactory}
 *
 * @author MarcoForlini
 */
public class RenderEntityFactory implements ConstDataRender {

	/** Is a distant object? */
	public boolean				distant;
	/** Name of the mesh */
	public String				meshSceneName;
	/** The render effect */
	public RenderEffect			renderEffect;
	/** Is it visible? " */
	public boolean				visible;
	/** The mesh attribute manager */
	public MeshAttributeManager	meshAttributeManager;
	/** Rotation speed */
	public float				rotationSpeed;



	/**
	 * Creates a new {@link RenderEntityFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public RenderEntityFactory (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link RenderEntityFactory}
	 *
	 * @param distant Is a distant object?
	 * @param meshSceneName Name of the mesh
	 * @param renderEffect The render effect
	 * @param visible Is it visible?
	 * @param meshAttributeManager The mesh attribute manager
	 * @param rotationSpeed Rotation speed
	 */
	public RenderEntityFactory (boolean distant, String meshSceneName, RenderEffect renderEffect, boolean visible, MeshAttributeManager meshAttributeManager, float rotationSpeed) {
		this.distant = distant;
		this.meshSceneName = meshSceneName;
		this.renderEffect = renderEffect;
		this.visible = visible;
		this.meshAttributeManager = meshAttributeManager;
		this.rotationSpeed = rotationSpeed;
	}


	@Override
	public final int getNumInternalLines () {
		return 5 + meshAttributeManager.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner); // <number of lines> ConstData

		scanner.nextLine (); // {
		distant = readBoolean (scanner);
		meshSceneName = readString (scanner);
		renderEffect = RenderEffect.valueOf (readString (scanner));
		visible = readBoolean (scanner);
		meshAttributeManager = new MeshAttributeManager (scanner);
		rotationSpeed = readFloat (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (3000)
		        .append ('\n').append (toChars8 (getNumInternalLines ())).append (" ConstData")
		        .append ("\n{")
		        .append ("\n\tDistant Bool ").append (toBool (distant))
		        .append ("\n\tMesh Scene Name String '").append (meshSceneName).append ('\'')
		        .append ("\n\tRender Effect String '").append (renderEffect).append ('\'')
		        .append ("\n\tVisible Bool ").append (toBool (visible))
		        .append ("\n").append (meshAttributeManager.toCode (1))
		        .append ("\n\tROTATIONSPEED Float ").append (toFloat6 (rotationSpeed))
		        .append ("\n}");
	}

}
