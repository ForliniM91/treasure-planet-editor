package data.worldObjects.constData;

/**
 * ConstData for Collision definition
 *
 * @author MarcoForlini
 */
public interface ConstDataCollision extends ConstData {
	// Nothing to add
}
