package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.NebulaDamagePotential;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.NebulaDamageType;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_NebulaCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class NebulaCustomInfoFactory implements ConstDataCustomInfo {

	private static final NebulaDamageType[] nebulaDamageTypes = NebulaDamageType.values ();



	/** Minimum lightning spawn distance from ships */
	public float					minimumLightningSpawnDistanceFromShips;

	/** Minimum lightning bolt length */
	public float					minimumLightningBoltLength;

	/** Maximum lightning spawn point Z amplitude */
	public float					maximumLightningSpawnPointZAmplitude;

	/** Meteor hit chance modifier */
	public float					meteorHitChanceModifier;

	/** Nebula damage potentials */
	public NebulaDamagePotential[]	nebulaDamagePotentials;

	/** Sound for nebula */
	public String					nebulaSoundName;

	/** Sound for solar storm */
	public String					solarStormSoundName;

	/** Sound for meteor shower */
	public String					meteorShowerSoundName;





	/**
	 * Creates a new {@link NebulaCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public NebulaCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		nebulaDamagePotentials = new NebulaDamagePotential[3];
		readCode (scanner);
	}


	/**
	 * Creates a new {@link NebulaCustomInfoFactory}
	 *
	 * @param minimumLightningSpawnDistanceFromShips Minimum lightning spawn distance from ships
	 * @param minimumLightningBoltLength Minimum lightning bolt length
	 * @param maximumLightningSpawnPointZAmplitude Maximum lightning spawn point Z amplitude
	 * @param meteorHitChanceModifier Meteor hit chance modifier
	 * @param nebulaDamagePotentials Nebula damage potentials
	 * @param nebulaSoundName Sound for nebula
	 * @param solarStormSoundName Sound for solar storm
	 * @param meteorShowerSoundName Sound for meteor shower
	 */
	public NebulaCustomInfoFactory (float minimumLightningSpawnDistanceFromShips, float minimumLightningBoltLength, float maximumLightningSpawnPointZAmplitude, float meteorHitChanceModifier,
	                                NebulaDamagePotential[] nebulaDamagePotentials, String nebulaSoundName, String solarStormSoundName, String meteorShowerSoundName) {
		this.minimumLightningSpawnDistanceFromShips = minimumLightningSpawnDistanceFromShips;
		this.minimumLightningBoltLength = minimumLightningBoltLength;
		this.maximumLightningSpawnPointZAmplitude = maximumLightningSpawnPointZAmplitude;
		this.meteorHitChanceModifier = meteorHitChanceModifier;
		this.nebulaDamagePotentials = nebulaDamagePotentials;
		this.nebulaSoundName = nebulaSoundName;
		this.solarStormSoundName = solarStormSoundName;
		this.meteorShowerSoundName = meteorShowerSoundName;
	}


	@Override
	public final int getNumInternalLines () {
		return 160;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		readNumLines (scanner, 160); // <number of lines> ConstData
		scanner.nextLine (); // {
		minimumLightningSpawnDistanceFromShips = readFloat (scanner);
		minimumLightningBoltLength = readFloat (scanner);
		maximumLightningSpawnPointZAmplitude = readFloat (scanner);
		meteorHitChanceModifier = readFloat (scanner);
		for (int i = 0; i < 3; i++) {
			nebulaDamagePotentials[i] = new NebulaDamagePotential (nebulaDamageTypes[i], scanner);
		}
		nebulaSoundName = readString (scanner);
		solarStormSoundName = readString (scanner);
		meteorShowerSoundName = readString (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (6000)
		        .append ("\n00000160 ConstData")
		        .append ("\n{")
		        .append ("\n\tMinimum Lightning Spawn Distance From Ships Float ").append (minimumLightningSpawnDistanceFromShips)
		        .append ("\n\tMinimum Lightning Bolt Length Float ").append (minimumLightningBoltLength)
		        .append ("\n\tMaximum Lightning Spawn Point Z Amplitude Float ").append (maximumLightningSpawnPointZAmplitude)
		        .append ("\n\tMeteor Hit Chance Modifier Float ").append (meteorHitChanceModifier);
		for (NebulaDamagePotential nebulaDamagePotential : nebulaDamagePotentials) {
			sb.append (nebulaDamagePotential.toCode (1));
		}
		return sb
		        .append ("\n\tNebulaSoundName String '").append (nebulaSoundName).append ('\'')
		        .append ("\n\tSolarStormSoundName String '").append (solarStormSoundName).append ('\'')
		        .append ("\n\tMeteorShowerSoundName String '").append (meteorShowerSoundName).append ('\'')
		        .append ("\n}").toString ();
	}

}
