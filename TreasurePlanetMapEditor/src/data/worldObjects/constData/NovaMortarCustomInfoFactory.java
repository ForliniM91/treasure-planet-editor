package data.worldObjects.constData;

import java.util.Scanner;

import data.effects.Effect;
import data.worldObjects.elements.DamagePotential;
import data.worldObjects.elements.DecalDamageSize;
import data.worldObjects.elements.FactoryTypeCustomInfo;
import data.worldObjects.elements.ImpactSound;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Material;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypeCustomInfo#FT_NovaMortarCustomInfoFactory}
 *
 * @author MarcoForlini
 */
public class NovaMortarCustomInfoFactory implements ConstDataCustomInfoBullet {

	private static final Material[] materials = Material.values ();


	/** Hit effect */
	public Effect				hitEffect;
	/** Bullet effect */
	public Effect				bulletEffect;
	/** Damage to core section */
	public int					damageHitPoints;
	/** Size of decal */
	public DecalDamageSize		decalDamageSize;
	/** Damage potentials */
	public DamagePotential[]	damagePotentials;

	/** Impact sound max distance */
	public float				impactSoundMaxDistance;
	/** Impact sound volume */
	public float				impactSoundVolume;
	/** Travel sound max distance */
	public float				travelSoundMaxDistance;
	/** Travel sound volume */
	public float				travelSoundVolume;
	/** Impact sounds */
	public ImpactSound[]		impactSounds;
	/** Travel sound */
	public String				travelSound;





	/**
	 * Creates a new {@link NovaMortarCustomInfoFactory}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public NovaMortarCustomInfoFactory (Scanner scanner) throws UnexpectedTokenException {
		damagePotentials = new DamagePotential[6];
		impactSounds = new ImpactSound[6];
		readCode (scanner);
	}


	/**
	 * Creates a new {@link NovaMortarCustomInfoFactory}
	 *
	 * @param hitEffect Hit effect
	 * @param bulletEffect Bullet effect
	 * @param damageHitPoints Damage to core section
	 * @param decalDamageSize Size of decal
	 * @param damagePotentials Damage potentials
	 * @param impactSoundMaxDistance Impact sound max distance
	 * @param impactSoundVolume Impact sound volume
	 * @param travelSoundMaxDistance Travel sound max distance
	 * @param travelSoundVolume Travel sound volume
	 * @param impactSounds Impact sounds
	 * @param travelSound Travel sound
	 */
	public NovaMortarCustomInfoFactory (Effect hitEffect, Effect bulletEffect, int damageHitPoints, DecalDamageSize decalDamageSize, DamagePotential[] damagePotentials,
	                                    float impactSoundMaxDistance, float impactSoundVolume, float travelSoundMaxDistance, float travelSoundVolume, ImpactSound[] impactSounds, String travelSound) {
		this.hitEffect = hitEffect;
		this.bulletEffect = bulletEffect;
		this.damageHitPoints = damageHitPoints;
		this.decalDamageSize = decalDamageSize;
		this.damagePotentials = damagePotentials;
		this.impactSoundMaxDistance = impactSoundMaxDistance;
		this.impactSoundVolume = impactSoundVolume;
		this.travelSoundMaxDistance = travelSoundMaxDistance;
		this.travelSoundVolume = travelSoundVolume;
		this.impactSounds = impactSounds;
		this.travelSound = travelSound;
	}


	@Override
	public int getNumInternalLines () {
		return 111;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 111); // <number of lines> ConstData

		scanner.nextLine (); // {
		hitEffect = Effect.effects.get (readString (scanner));
		bulletEffect = Effect.effects.get (readString (scanner));
		damageHitPoints = readInt (scanner);
		decalDamageSize = DecalDamageSize.valueOf (readString (scanner));
		for (int i = 0; i < 6; i++) {
			damagePotentials[i] = new DamagePotential (materials[i], scanner);
		}
		impactSoundMaxDistance = readFloat (scanner);
		impactSoundVolume = readFloat (scanner);
		travelSoundMaxDistance = readFloat (scanner);
		travelSoundVolume = readFloat (scanner);
		for (int i = 0; i < 6; i++) {
			impactSounds[i] = new ImpactSound (materials[i], scanner);
		}
		travelSound = readString (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (4500)
		        .append ("\n00000111 ConstData")
		        .append ("\n{")
		        .append ("\n\tHitEffect String '").append (hitEffect).append ('\'')
		        .append ("\n\tBulletEffect String '").append (bulletEffect).append ('\'')
		        .append ("\n\tDamage HitPoints Int ").append (damageHitPoints)
		        .append ("\n\tDecal Damage Size String '").append (decalDamageSize).append ('\'');
		for (DamagePotential damagePotential : damagePotentials) {
			sb.append (damagePotential.toCode (1));
		}
		sb.append ("\n\tImpact Sound Max Distance Float ").append (toFloat6 (impactSoundMaxDistance))
		        .append ("\n\tImpact Sound Volume Float ").append (toFloat6 (impactSoundVolume))
		        .append ("\n\tTravel Sound Max Distance Float ").append (toFloat6 (travelSoundMaxDistance))
		        .append ("\n\tTravel Sound Volume Float ").append (toFloat6 (travelSoundVolume));
		for (ImpactSound impactSound : impactSounds) {
			sb.append (impactSound.toCode (0));
		}
		return sb
		        .append ("\n\tTravel Sound String '").append (travelSound).append ('\'')
		        .append ("\n}").toString ();
	}

}
