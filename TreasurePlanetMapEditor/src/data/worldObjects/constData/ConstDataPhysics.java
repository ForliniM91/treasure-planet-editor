package data.worldObjects.constData;

/**
 * ConstData for Physics definition
 *
 * @author MarcoForlini
 */
public interface ConstDataPhysics extends ConstData {
	// Nothing to add
}
