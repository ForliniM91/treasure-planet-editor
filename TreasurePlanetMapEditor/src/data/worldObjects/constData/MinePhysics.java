package data.worldObjects.constData;

import java.util.Scanner;

import data.worldObjects.elements.FactoryTypePhysics;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Vector3;
import interfaces.WriteCode;


/**
 * The ConstData for {@link FactoryTypePhysics#FT_MinePhysics}
 *
 * @author MarcoForlini
 */
public class MinePhysics implements ConstDataPhysics {

	/** Center of mass */
	public Vector3	centerOfMass;

	/** Mass */
	public float	mass;

	/** Maximum speed */
	public float	maximumSpeed;





	/**
	 * Creates a new {@link MinePhysics}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public MinePhysics (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link MinePhysics}
	 *
	 * @param centerOfMass The center of mass
	 * @param mass The mass
	 * @param maximumSpeed The maximum speed
	 */
	public MinePhysics (Vector3 centerOfMass, float mass, float maximumSpeed) {
		this.centerOfMass = centerOfMass;
		this.mass = mass;
		this.maximumSpeed = maximumSpeed;
	}


	@Override
	public final int getNumInternalLines () {
		return 3;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 3); // <number of lines> ConstData
		scanner.nextLine (); // {
		centerOfMass = readVector (scanner);
		mass = readFloat (scanner);
		maximumSpeed = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (133)
		        .append ("\n00000003 ConstData")
		        .append ("\n{")
		        .append ("\n\tCenterOfMass ").append (centerOfMass)
		        .append ("\n\tMass Float ").append (toFloat6 (mass))
		        .append ("\n\tMaximum speed Float ").append (toFloat6 (maximumSpeed))
		        .append ("\n}");
	}
}
