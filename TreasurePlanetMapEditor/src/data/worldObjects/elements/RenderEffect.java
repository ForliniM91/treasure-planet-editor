package data.worldObjects.elements;

/**
 * Render effect
 * 
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum RenderEffect {

	mesh,
	;

	@Override
	public String toString () {
		return name ();
	}

}
