package data.worldObjects.elements;

import java.util.Scanner;

import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * Define all attributes of the mesh
 *
 * @author MarcoForlini
 */
public class MeshAttributeManager implements ReadCode, WriteCode {

	/** The mesh attributes */
	public MeshAttributes[] meshAttributes;




	/**
	 * Creates a new {@link MeshAttributeManager}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public MeshAttributeManager (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link MeshAttributeManager}
	 *
	 * @param meshAttributes
	 */
	public MeshAttributeManager (MeshAttributes[] meshAttributes) {
		this.meshAttributes = meshAttributes;
	}


	@Override
	public int getNumInternalLines () {
		return 1 + 11 * meshAttributes.length;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner); // <number of lines> ConstData

		scanner.nextLine (); // {
		int size = readInt (scanner);
		meshAttributes = new MeshAttributes[size];
		for (int i = 0; i < size; i++) {
			meshAttributes[i] = new MeshAttributes (scanner);
		}
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation];
		int nextInd = indentation + 1;
		StringBuilder sb = new StringBuilder (2000)
		        .append (ind).append (toChars8 (getNumInternalLines ())).append ("Mesh Attribute Manager")
		        .append (ind).append ('{')
		        .append (ind__).append ("MeshAttributes - Size Int ").append (meshAttributes.length);
		for (MeshAttributes meshAttribute : meshAttributes) {
			sb.append (meshAttribute.toCode (nextInd));
		}
		return sb.append (ind).append ('}');
	}

}
