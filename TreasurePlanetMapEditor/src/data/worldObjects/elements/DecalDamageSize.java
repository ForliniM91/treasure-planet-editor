package data.worldObjects.elements;

/**
 * Size of decal damage
 * 
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum DecalDamageSize {

	Small,
	Medium,
	Large

}
