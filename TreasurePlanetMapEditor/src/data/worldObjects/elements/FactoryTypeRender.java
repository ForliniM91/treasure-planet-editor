package data.worldObjects.elements;

import data.worldObjects.constData.ConstDataRender;
import data.worldObjects.constData.RenderEntityFactory;
import interfaces.Constructor;


/**
 * The FactoryType field of a WorldObject, which define what attributes will be defined next
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum FactoryTypeRender implements FactoryType <ConstDataRender> {

	FT_RenderEntityFactory ("RenderEntityFactory", RenderEntityFactory::new);



	public String							name;

	public Constructor <ConstDataRender>	constructor;


	private FactoryTypeRender (String name, Constructor <ConstDataRender> constructor) {
		this.name = name;
		this.constructor = constructor;
	}

	public static FactoryTypeRender valueOfEnum (String name) {
		switch (name) {
			case "RenderEntityFactory":
				return FT_RenderEntityFactory;
			default:
				return null;
		}
	}

	@Override
	public Constructor <ConstDataRender> getConstructor () {
		return constructor;
	}

	@Override
	public String toString () {
		return name;
	}

}
