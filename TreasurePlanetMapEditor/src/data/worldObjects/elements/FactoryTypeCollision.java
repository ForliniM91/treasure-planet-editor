package data.worldObjects.elements;

import data.worldObjects.constData.BoundingRenderEntity;
import data.worldObjects.constData.BoundingSphere;
import data.worldObjects.constData.ConstDataCollision;
import data.worldObjects.constData.Point;
import interfaces.Constructor;


/**
 * The FactoryType field of a WorldObject, which define what attributes will be defined next
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum FactoryTypeCollision implements FactoryType <ConstDataCollision> {

	FT_BoundingRenderEntity ("BoundingRenderEntity", BoundingRenderEntity::new),
	FT_Point ("Point", Point::new),
	FT_BoundingSphere ("BoundingSphere", BoundingSphere::new);



	public String							name;

	public Constructor <ConstDataCollision>	constructor;


	private FactoryTypeCollision (String name, Constructor <ConstDataCollision> constructor) {
		this.name = name;
		this.constructor = constructor;
	}

	public static FactoryTypeCollision valueOfEnum (String name) {
		switch (name) {
			case "BoundingRenderEntity":
				return FT_BoundingRenderEntity;
			case "Point":
				return FT_Point;
			case "BoundingSphere":
				return FT_BoundingSphere;
			default:
				return null;
		}
	}

	@Override
	public Constructor <ConstDataCollision> getConstructor () {
		return constructor;
	}

	@Override
	public String toString () {
		return name;
	}

}
