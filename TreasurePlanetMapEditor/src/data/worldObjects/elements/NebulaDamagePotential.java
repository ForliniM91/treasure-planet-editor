package data.worldObjects.elements;

import java.util.Scanner;

import entities.Material;
import entities.NebulaDamageType;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * The Nebula Damage Potential
 *
 * @author MarcoForlini
 */
public class NebulaDamagePotential implements ReadCode, WriteCode {

	private static final Material[] materials = Material.values ();


	/** Name of damage */
	public NebulaDamageType		damageType;

	/** */
	public DamagePotential[]	damagePotentials;



	/**
	 * Creates a new {@link NebulaDamagePotential}
	 *
	 * @param damageType The type of damage
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public NebulaDamagePotential (NebulaDamageType damageType, Scanner scanner) throws UnexpectedTokenException {
		this.damageType = damageType;
		damagePotentials = new DamagePotential[3];
		readCode (scanner);
	}


	/**
	 * Creates a new {@link NebulaDamagePotential}
	 *
	 * @param damageType The type of damage
	 * @param damagePotentials The damage potential
	 */
	public NebulaDamagePotential (NebulaDamageType damageType, DamagePotential[] damagePotentials) {
		this.damageType = damageType;
		this.damagePotentials = damagePotentials;
	}


	@Override
	public int getNumInternalLines () {
		return 48;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 48); // <number of lines> ConstData

		scanner.nextLine (); // {
		for (int i = 0; i < 6; i++) {
			damagePotentials[i] = new DamagePotential (materials[i], scanner);
		}
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (500)
		        .append ("\n\t00000048 ").append (damageType).append (" Damage Potential\n\t{");
		for (DamagePotential damagePotential : damagePotentials) {
			sb.append (damagePotential.toCode (indentation + 1));
		}
		return sb.append ("\n\t}");
	}

}
