package data.worldObjects.elements;

import java.util.Scanner;

import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * The ConstData of {@link GUIInfoChunk}
 *
 * @author MarcoForlini
 */
public class GUIInfoChunk implements ReadCode, WriteCode {

	/** ShipBar icon texture */
	public String	shipBarIconTexture;

	/** The percentage of radius */
	public float	selectedIndicatorPercentageOfRadius;

	/** The distance */
	public float	distanceToStartSpherePicking;


	/**
	 * Creates a new {@link GUIInfoChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedNumLines If the scanner contains an unexpected token
	 */
	public GUIInfoChunk (Scanner scanner) throws UnexpectedNumLines {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link GUIInfoChunk}
	 *
	 * @param shipBarIconTexture ShipBar icon texture
	 * @param selectedIndicatorPercentageOfRadius The percentage of radius
	 * @param distanceToStartSpherePicking The distance
	 */
	public GUIInfoChunk (String shipBarIconTexture, float selectedIndicatorPercentageOfRadius, float distanceToStartSpherePicking) {
		this.shipBarIconTexture = shipBarIconTexture;
		this.selectedIndicatorPercentageOfRadius = selectedIndicatorPercentageOfRadius;
		this.distanceToStartSpherePicking = distanceToStartSpherePicking;
	}


	@Override
	public int getNumInternalLines () {
		return shipBarIconTexture != null ? 3 : 2;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		int n = readNumLines (scanner); // <number of lines> ConstData
		if (n < 2 || n > 3) {
			throw new UnexpectedNumLines (Integer.toString (n));
		}
		scanner.nextLine (); // {
		shipBarIconTexture = (n == 3) ? readString (scanner) : null;
		selectedIndicatorPercentageOfRadius = readFloat (scanner);
		distanceToStartSpherePicking = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (300)
		        .append ("\n\t").append (toChars8 (getNumInternalLines ())).append (" GUIInfo Chunk")
		        .append ("\n\t{");
		if (shipBarIconTexture != null) {
			sb.append ("\n\t\tShipBar Icon Texture String '" + shipBarIconTexture + '\'');
		}
		return sb
		        .append ("\n\t\tSelected Indicator Percentage Of Radius Float ").append (toFloat6 (selectedIndicatorPercentageOfRadius))
		        .append ("\n\t\tDistance to Start Sphere Picking Float ").append (toFloat6 (distanceToStartSpherePicking))
		        .append ("\n\t}").toString ();
	}

}
