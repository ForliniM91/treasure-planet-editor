/**
 *
 */
package data.worldObjects.elements;

/**
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum CriticalSections {

	CargoHold ("Cargo Hold"),
	Engines ("Engines"),
	MaxVelocity ("MaxVelocity"),
	Munnitions ("Munnitions"),
	PowerTransformerEngines ("Power Transformer Engines"),
	PowerTransformerGuns ("Power Transformer Guns"),
	RudderLinkage ("Rudder Linkage"),
	SpotterEquitment ("Spotter Equitment"),
	;


	private String name;

	/**
	 * Creates a new {@link CriticalSections}
	 *
	 * @param name
	 */
	private CriticalSections (String name) {
		this.name = name;
	}


	@Override
	public String toString () {
		return name;
	}

}
