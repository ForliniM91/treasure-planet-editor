package data.worldObjects.elements;

/**
 * Alert made by Crew members when firing a weapon
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum FiringCrewAlert {

	INVALID,
	GUNNER_FIRE,
	GUNNER_FIRE_TORPEDO

}
