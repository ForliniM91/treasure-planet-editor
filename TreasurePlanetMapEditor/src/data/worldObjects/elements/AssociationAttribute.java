/**
 *
 */
package data.worldObjects.elements;

import interfaces.WriteCode;


/**
 * An association attribute for the field AssociationName
 *
 * @author MarcoForlini
 */
public interface AssociationAttribute extends WriteCode {
	// Nothing to add
}
