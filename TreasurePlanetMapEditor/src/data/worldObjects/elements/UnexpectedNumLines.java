/**
 *
 */
package data.worldObjects.elements;

/**
 * Exception thrown when the number of lines in a file section is unexpected
 *
 * @author MarcoForlini
 */
public class UnexpectedNumLines extends UnexpectedTokenException {

	private static final long serialVersionUID = -2969600007294071444L;

	/**
	 * Creates a new {@link UnexpectedNumLines}
	 */
	public UnexpectedNumLines () {
		super ("Unexpected number of lines");
	}

	/**
	 * Creates a new {@link UnexpectedNumLines}
	 *
	 * @param numLines The number of lines
	 */
	public UnexpectedNumLines (String numLines) {
		super ("Unexpected number of lines: \"" + numLines);
	}

}
