package data.worldObjects.elements;

/**
 * Strings used to define an object detection type
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum DetectionType {

	Null ("Null"),
	BoundingRenderEntity ("BoundingRenderEntity"),
	OBBTree ("OBBTree"),
	Point ("Point"),
	AttackableWeaponFire ("Attackable WeaponFire"),
	DefensiveWeaponFire ("Defensive WeaponFire"),
	;

	private String name;

	private DetectionType (String name) {
		this.name = name;
	}


	@Override
	public String toString () {
		return name;
	}

}
