package data.worldObjects.elements;

/**
 * Attributes of the meshes
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum AttributeName {

	DamageSection,
	GunPlacement,
	GunMuzzlePlacement,
	GunVerticalPivotPlacement,
	EnginePortPlacement,
	WakePlacement,
	DockPoint,
	ToweePoint,
	MeshHitPoints,
	TorpedoHomingPoint,
	BoardingEffectPoint,
	FlagAttachmentPoint,
	PlayEffect,
	VolcanoSmoke

}
