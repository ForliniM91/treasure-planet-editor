package data.worldObjects.elements;

import data.worldObjects.constData.ConstData;
import interfaces.Constructor;


/**
 * The FactoryType field of a WorldObject, which define what attributes will be defined next
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public interface FactoryType <T extends ConstData> {

	Constructor <T> getConstructor ();

}
