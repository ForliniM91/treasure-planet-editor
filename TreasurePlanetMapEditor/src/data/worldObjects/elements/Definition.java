package data.worldObjects.elements;

import java.util.Objects;
import java.util.Scanner;

import data.worldObjects.constData.ConstData;
import data.worldObjects.constData.ConstDataAI;
import data.worldObjects.constData.ConstDataCollision;
import data.worldObjects.constData.ConstDataCustomInfo;
import data.worldObjects.constData.ConstDataPhysics;
import data.worldObjects.constData.ConstDataRender;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * A definition block
 *
 * @author MarcoForlini
 * @param <T> Type of ConstData
 */
public class Definition <T extends ConstData> implements ReadCode, WriteCode {

	/** A null AI */
	public static final Definition <ConstDataAI>			nullAI			= new Definition<> (DefinitionType.AIEntityDefinition);
	/** A null Render */
	public static final Definition <ConstDataRender>		nullRender		= new Definition<> (DefinitionType.RenderEntityDefinition);
	/** A null Physics */
	public static final Definition <ConstDataPhysics>		nullPhysics		= new Definition<> (DefinitionType.PhysicsDefinition);
	/** A null Collision */
	public static final Definition <ConstDataCollision>		nullCollision	= new Definition<> (DefinitionType.CollisionDefinition);
	/** A null CustomInfo */
	public static final Definition <ConstDataCustomInfo>	nullCustomInfo	= new Definition<> (DefinitionType.CustomInfoDefinition);



	/** Definition type */
	public DefinitionType <T>	definitionType;

	/** Entity type */
	public String				entityType;

	/** Factory type */
	public FactoryType <T>		factoryType;

	/** Const data */
	public T					constData;




	private Definition (DefinitionType <T> definitionType) {
		this.definitionType = definitionType;
		this.entityType = null;
		this.factoryType = null;
		this.constData = null;
	}


	/**
	 * Creates a new {@link Definition}
	 *
	 * @param definitionType The definition type
	 * @param entityType The entity type
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Definition (DefinitionType <T> definitionType, String entityType, Scanner scanner) throws UnexpectedTokenException {
		this.definitionType = definitionType;
		this.entityType = entityType;
		readCode (scanner);
	}


	/**
	 * Creates a new {@link Definition}
	 *
	 * @param definitionType The definition type
	 * @param entityType The entity type
	 * @param factoryType The factory type
	 * @param constData The const data
	 */
	public Definition (DefinitionType <T> definitionType, String entityType, FactoryType <T> factoryType, T constData) {
		Objects.requireNonNull (definitionType, "Definition type can't be null");
		Objects.requireNonNull (entityType, "Entity can't be null!"); // This effectively prevent the creation of more null definitions
		Objects.requireNonNull (factoryType, "Factory type can't be null!");
		Objects.requireNonNull (constData, "ConstData can't be null!");
		this.definitionType = definitionType;
		this.entityType = entityType;
		this.factoryType = factoryType;
		this.constData = constData;
	}


	@Override
	public int getNumInternalLines () {
		return (constData == null) ? 2 : (3 + constData.getNumLines ());
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		factoryType = definitionType.constructorGetter.apply (readString (scanner)); // FactoryType String '<FactoryType<T>>'
		constData = factoryType.getConstructor ().create (scanner);
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (500)
		        .append ("\nDefinition String '").append (definitionType).append ('\'');
		if (entityType != null && factoryType != null && constData != null) {
			return sb.append ("\nEntityType String '").append (entityType).append ('\'')
			        .append ("\nFactoryType String '").append (factoryType).append ('\'')
			        .append (constData.toCode (0));
		} else {
			return sb.append ("\nEntityType String ''");
		}
	}

}
