package data.worldObjects.elements;

import data.worldObjects.constData.ConstDataAI;
import data.worldObjects.constData.DragonAI;
import data.worldObjects.constData.GunAI;
import data.worldObjects.constData.IslandAI;
import data.worldObjects.constData.MineAI;
import data.worldObjects.constData.ShipAI;
import data.worldObjects.constData.SpaceAnimalAI;
import data.worldObjects.constData.SpaceObjectAI;
import data.worldObjects.constData.VolcanoAI;
import interfaces.Constructor;


/**
 * The FactoryType field of a WorldObject, which define what attributes will be defined next
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum FactoryTypeAI implements FactoryType <ConstDataAI> {

	FT_IslandAI ("IslandAI", IslandAI::new),
	FT_VolcanoAI ("VolcanoAI", VolcanoAI::dummyConstructor),
	FT_SpaceAnimalAI ("SpaceAnimalAI", SpaceAnimalAI::dummyConstructor),
	FT_DragonAI ("DragonAI", DragonAI::new),
	FT_SpaceObjectAI ("SpaceObjectAI", SpaceObjectAI::dummyConstructor),
	FT_GunAI ("GunAI", GunAI::new),
	FT_MineAI ("MineAI", MineAI::new),
	FT_ShipAI ("ShipAI", ShipAI::new);



	public String						name;

	public Constructor <ConstDataAI>	constructor;


	private FactoryTypeAI (String name, Constructor <ConstDataAI> constructor) {
		this.name = name;
		this.constructor = constructor;
	}

	public static FactoryTypeAI valueOfEnum (String name) {
		switch (name) {
			case "IslandAI":
				return FT_IslandAI;
			case "VolcanoAI":
				return FT_VolcanoAI;
			case "SpaceAnimalAI":
				return FT_SpaceAnimalAI;
			case "DragonAI":
				return FT_DragonAI;
			case "SpaceObjectAI":
				return FT_SpaceObjectAI;
			case "GunAI":
				return FT_GunAI;
			case "MineAI":
				return FT_MineAI;
			case "ShipAI":
				return FT_ShipAI;
			default:
				return null;
		}
	}

	@Override
	public Constructor <ConstDataAI> getConstructor () {
		return constructor;
	}

	@Override
	public String toString () {
		return name;
	}

}
