package data.worldObjects.elements;

import java.util.Scanner;

import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * Definition of a mesh section
 *
 * @author MarcoForlini
 */
public class MeshAttributes implements ReadCode, WriteCode {

	/** Name of the mesh */
	public String			meshName;

	/** Attributes of the mesh */
	public MeshAttribute[]	meshAttributes;





	/**
	 * Creates a new {@link MeshAttributes}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public MeshAttributes (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link MeshAttributes}
	 *
	 * @param meshName Mesh section
	 * @param meshAttributes Mesh attributes
	 */
	public MeshAttributes (String meshName, MeshAttribute[] meshAttributes) {
		this.meshName = meshName;
		this.meshAttributes = meshAttributes;
	}


	@Override
	public int getNumInternalLines () {
		return 2 + 6 * meshAttributes.length;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner); // <number of lines> ConstData

		scanner.nextLine (); // {
		meshName = readString (scanner);
		int size = readInt (scanner);
		meshAttributes = new MeshAttribute[size];
		for (int i = 0; i < size; i++) {
			meshAttributes[i] = new MeshAttribute (scanner);
		}
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[nextInd];
		StringBuilder sb = new StringBuilder (200)
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" MeshAttributes - Element")
		        .append (ind).append ('{')
		        .append (ind__).append ("MeshName String '").append (meshName).append ('\'')
		        .append (ind__).append ("MeshAttribute - Size Int ").append (meshAttributes.length);
		for (MeshAttribute meshAttribute : meshAttributes) {
			sb.append (meshAttribute.toCode (nextInd));
		}
		return sb.append (ind).append ('}');
	}

}
