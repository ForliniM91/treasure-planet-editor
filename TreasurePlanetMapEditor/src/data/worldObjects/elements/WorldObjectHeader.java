/**
 *
 */
package data.worldObjects.elements;

/**
 * WorldObject header
 * 
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class WorldObjectHeader {

	public String	ID;
	public boolean	hasPhysics;
	public boolean	hasCollision;
	public boolean	hasRender;
	public boolean	hasAI;
	public boolean	hasCustomInfo;

	/**
	 * Creates a new {@link WorldObjectHeader}
	 *
	 * @param ID
	 * @param hasPhysics
	 * @param hasCollision
	 * @param hasRender
	 * @param hasAI
	 * @param hasCustomInfo
	 */
	public WorldObjectHeader (String ID, boolean hasPhysics, boolean hasCollision, boolean hasRender, boolean hasAI, boolean hasCustomInfo) {
		this.ID = ID;
		this.hasPhysics = hasPhysics;
		this.hasCollision = hasCollision;
		this.hasRender = hasRender;
		this.hasAI = hasAI;
		this.hasCustomInfo = hasCustomInfo;
	}

}
