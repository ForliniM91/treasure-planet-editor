/**
 *
 */
package data.worldObjects.elements;

/**
 * @author MarcoForlini
 */
public enum EngineType {

	/** Absorb sun energy with solar sails. Affected by energy net weapons and nebula. */
	SolarSail ("Solar Sail"),

	/** Burn the dark matter. Unaffected by energy net weapons and nebula. */
	DarkMatter ("Dark matter");



	private String name;

	private EngineType (String name) {
		this.name = name;
	}

	@Override
	public String toString () {
		return name;
	}

}
