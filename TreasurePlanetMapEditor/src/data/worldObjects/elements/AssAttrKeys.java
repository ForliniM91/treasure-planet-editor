/**
 *
 */
package data.worldObjects.elements;

/**
 * An association attribute with key-value pair
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum AssAttrKeys {

	vitalToShip,
	vitalToMaxVelocity,
	vitalToManeuverability,
	vitalToMission,
	swappable,
	material,
	hitpoints,
	vitalSectionCoreDamagePercent,
	criticals,
	adjacent,
	debrisMeshScene,
	destructFx,
	debrisFx

}
