package data.worldObjects.elements;

import data.worldObjects.constData.AsteroidCustomInfoFactory;
import data.worldObjects.constData.BlackHoleCustomInfoFactory;
import data.worldObjects.constData.BulletCustomInfoFactory;
import data.worldObjects.constData.ConstDataCustomInfo;
import data.worldObjects.constData.CrewCustomInfoFactory;
import data.worldObjects.constData.DragonCustomInfoFactory;
import data.worldObjects.constData.EnergyNetCustomInfoFactory;
import data.worldObjects.constData.EtheriumCurrentCustomInfoFactory;
import data.worldObjects.constData.GrapplingHarpoonCustomInfoFactory;
import data.worldObjects.constData.GravChargeCustomInfoFactory;
import data.worldObjects.constData.GunCustomInfoFactory;
import data.worldObjects.constData.IslandCustomInfoFactory;
import data.worldObjects.constData.MineCustomInfoFactory;
import data.worldObjects.constData.NebulaCustomInfoFactory;
import data.worldObjects.constData.NovaMortarCustomInfoFactory;
import data.worldObjects.constData.ShipCustomInfoFactory;
import data.worldObjects.constData.ShipDebrisCustomInfoFactory;
import data.worldObjects.constData.SpaceAnimalCustomInfoFactory;
import data.worldObjects.constData.StarMortarCustomInfoFactory;
import data.worldObjects.constData.TorpedoCustomInfoFactory;
import interfaces.Constructor;


/**
 * The FactoryType field of a WorldObject, which define what attributes will be defined next
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum FactoryTypeCustomInfo implements FactoryType <ConstDataCustomInfo> {

	FT_DragonCustomInfoFactory ("DragonCustomInfoFactory", DragonCustomInfoFactory::new),
	FT_SpaceAnimalCustomInfoFactory ("SpaceAnimalCustomInfoFactory", SpaceAnimalCustomInfoFactory::new),
	FT_AsteroidCustomInfoFactory ("AsteroidCustomInfoFactory", AsteroidCustomInfoFactory::new),
	FT_IslandCustomInfoFactory ("IslandCustomInfoFactory", IslandCustomInfoFactory::new),
	FT_BlackHoleCustomInfoFactory ("BlackHoleCustomInfoFactory", BlackHoleCustomInfoFactory::new),
	FT_EtheriumCurrentCustomInfoFactory ("EtheriumCurrentCustomInfoFactory", EtheriumCurrentCustomInfoFactory::new),
	FT_NebulaCustomInfoFactory ("NebulaCustomInfoFactory", NebulaCustomInfoFactory::new),
	FT_CrewCustomInfoFactory ("CrewCustomInfoFactory", CrewCustomInfoFactory::new),
	FT_GunCustomInfoFactory ("GunCustomInfoFactory", GunCustomInfoFactory::new),
	FT_ShipCustomInfoFactory ("ShipCustomInfoFactory", ShipCustomInfoFactory::new),
	FT_ShipDebrisCustomInfoFactory ("ShipDebrisCustomInfoFactory", ShipDebrisCustomInfoFactory::new),

    // ConstDataCustomInfoBullet
	FT_BulletCustomInfoFactory ("BulletCustomInfoFactory", BulletCustomInfoFactory::new),
	FT_GrapplingHarpoonCustomInfoFactory ("GrapplingHarpoonCustomInfoFactory", GrapplingHarpoonCustomInfoFactory::new),
	FT_EnergyNetCustomInfoFactory ("EnergyNetCustomInfoFactory", EnergyNetCustomInfoFactory::new),
	FT_GravChargeCustomInfoFactory ("GravChargeCustomInfoFactory", GravChargeCustomInfoFactory::new),
	FT_StarMortarCustomInfoFactory ("StarMortarCustomInfoFactory", StarMortarCustomInfoFactory::new),
	FT_NovaMortarCustomInfoFactory ("NovaMortarCustomInfoFactory", NovaMortarCustomInfoFactory::new),
	FT_TorpedoCustomInfoFactory ("TorpedoCustomInfoFactory", TorpedoCustomInfoFactory::new),
	FT_MineCustomInfoFactory ("MineCustomInfoFactory", MineCustomInfoFactory::new);



	public String								name;

	public Constructor <ConstDataCustomInfo>	constructor;


	private FactoryTypeCustomInfo (String name, Constructor <ConstDataCustomInfo> constructor) {
		this.name = name;
		this.constructor = constructor;
	}

	public static FactoryTypeCustomInfo valueOfEnum (String name) {
		switch (name) {
			case "DragonCustomInfoFactory":
				return FT_DragonCustomInfoFactory;
			case "SpaceAnimalCustomInfoFactory":
				return FT_SpaceAnimalCustomInfoFactory;
			case "AsteroidCustomInfoFactory":
				return FT_AsteroidCustomInfoFactory;
			case "IslandCustomInfoFactory":
				return FT_IslandCustomInfoFactory;
			case "BlackHoleCustomInfoFactory":
				return FT_BlackHoleCustomInfoFactory;
			case "EtheriumCurrentCustomInfoFactory":
				return FT_EtheriumCurrentCustomInfoFactory;
			case "NebulaCustomInfoFactory":
				return FT_NebulaCustomInfoFactory;
			case "CrewCustomInfoFactory":
				return FT_CrewCustomInfoFactory;
			case "GunCustomInfoFactory":
				return FT_GunCustomInfoFactory;
			case "ShipCustomInfoFactory":
				return FT_ShipCustomInfoFactory;
			case "ShipDebrisCustomInfoFactory":
				return FT_ShipDebrisCustomInfoFactory;
			case "BulletCustomInfoFactory":
				return FT_BulletCustomInfoFactory;
			case "GrapplingHarpoonCustomInfoFactory":
				return FT_GrapplingHarpoonCustomInfoFactory;
			case "EnergyNetCustomInfoFactory":
				return FT_EnergyNetCustomInfoFactory;
			case "GravChargeCustomInfoFactory":
				return FT_GravChargeCustomInfoFactory;
			case "StarMortarCustomInfoFactory":
				return FT_StarMortarCustomInfoFactory;
			case "NovaMortarCustomInfoFactory":
				return FT_NovaMortarCustomInfoFactory;
			case "TorpedoCustomInfoFactory":
				return FT_TorpedoCustomInfoFactory;
			case "MineCustomInfoFactory":
				return FT_MineCustomInfoFactory;
			default:
				return null;
		}
	}

	@Override
	public Constructor <ConstDataCustomInfo> getConstructor () {
		return constructor;
	}

	@Override
	public String toString () {
		return name;
	}

}
