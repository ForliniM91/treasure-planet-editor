package data.worldObjects.elements;

import java.util.Scanner;

import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * Definition of a part of a mesh section
 *
 * @author MarcoForlini
 */
public class MeshAttribute implements ReadCode, WriteCode {

	/** Name of attribute */
	public AttributeName	attributeName;
	/** Descriptor */
	public String			descriptorName;
	/** Sequence of key-value to associate */
	public AssociationName	associationName;




	/**
	 * Creates a new {@link MeshAttribute}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public MeshAttribute (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link MeshAttribute}
	 *
	 * @param attributeName Name of attribute
	 * @param descriptorName Descriptor
	 * @param associationName Sequence of key-value to associate
	 */
	public MeshAttribute (AttributeName attributeName, String descriptorName, AssociationName associationName) {
		this.attributeName = attributeName;
		this.descriptorName = descriptorName;
		this.associationName = associationName;
	}


	@Override
	public int getNumInternalLines () {
		return 6;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		readNumLines (scanner, 3); // <number of lines> ConstData
		scanner.nextLine (); // {
		attributeName = AttributeName.valueOf (readString (scanner));
		descriptorName = readString (scanner);
		associationName = new AssociationName (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation + 1];
		return new StringBuilder (500)
		        .append (ind).append ("00000003 MeshAttribute - Element")
		        .append (ind).append ('{')
		        .append (ind__).append ("AttributeName String '").append (attributeName).append ('\'')
		        .append (ind__).append ("DescriptorName String '").append (descriptorName).append ('\'')
		        .append (ind__).append ("AssociationName String '").append (associationName.toCode (0)).append ('\'')
		        .append (ind).append ('}');
	}

}
