package data.worldObjects.elements;

import data.worldObjects.constData.ConstDataPhysics;
import data.worldObjects.constData.DragonPhysics;
import data.worldObjects.constData.MinePhysics;
import data.worldObjects.constData.ProjectilePhysics;
import data.worldObjects.constData.ShipDemo;
import data.worldObjects.constData.SpaceObjectPhysics;
import data.worldObjects.constData.TorpedoPhysics;
import data.worldObjects.constData.WhalePhysics;
import interfaces.Constructor;


/**
 * The FactoryType field of a WorldObject, which define what attributes will be defined next
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum FactoryTypePhysics implements FactoryType <ConstDataPhysics> {

	FT_ProjectilePhysics ("ProjectilePhysics", ProjectilePhysics::new),
	FT_DragonPhysics ("DragonPhysics", DragonPhysics::new),
	FT_WhalePhysics ("Whale Physics", WhalePhysics::new),
	FT_SpaceObjectPhysics ("SpaceObjectPhysics", SpaceObjectPhysics::new),
	FT_TorpedoPhysics ("TorpedoPhysics", TorpedoPhysics::new),
	FT_MinePhysics ("MinePhysics", MinePhysics::new),
	FT_ShipDemo ("ShipDemo", ShipDemo::new);



	public String							name;

	public Constructor <ConstDataPhysics>	constructor;


	private FactoryTypePhysics (String name, Constructor <ConstDataPhysics> constructor) {
		this.name = name;
		this.constructor = constructor;
	}

	public static FactoryTypePhysics valueOfEnum (String name) {
		switch (name) {
			case "ProjectilePhysics":
				return FT_ProjectilePhysics;
			case "DragonPhysics":
				return FT_DragonPhysics;
			case "Whale Physics":
				return FT_WhalePhysics;
			case "SpaceObjectPhysics":
				return FT_SpaceObjectPhysics;
			case "TorpedoPhysics":
				return FT_TorpedoPhysics;
			case "MinePhysics":
				return FT_MinePhysics;
			case "ShipDemo":
				return FT_ShipDemo;
			default:
				return null;
		}
	}

	@Override
	public Constructor <ConstDataPhysics> getConstructor () {
		return constructor;
	}

	@Override
	public String toString () {
		if (this == FT_WhalePhysics) {
			return "Whale Physics";
		}
		return name;
	}

}
