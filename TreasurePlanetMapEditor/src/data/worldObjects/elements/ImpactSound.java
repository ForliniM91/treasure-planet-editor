package data.worldObjects.elements;

import java.util.Scanner;

import entities.Material;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * Impact sound of a Bullet
 *
 * @author MarcoForlini
 */
public class ImpactSound implements ReadCode, WriteCode {

	/** Material of target */
	public Material	material;

	/** Ricochet sound */
	public String	ricochetSound;

	/** Impact sound */
	public String	sound_0;
	/** Impact sound */
	public String	sound_1;
	/** Impact sound */
	public String	sound_2;
	/** Impact sound */
	public String	sound_3;
	/** Impact sound */
	public String	sound_4;



	/**
	 * Creates a new {@link ImpactSound}
	 *
	 * @param material Material of target
	 * @param scanner The scanner to read
	 * @throws UnexpectedNumLines If the scanner contains an unexpected token
	 */
	public ImpactSound (Material material, Scanner scanner) throws UnexpectedNumLines {
		this.material = material;
		readCode (scanner);
	}

	/**
	 * Creates a new {@link ImpactSound}
	 *
	 * @param material Material of target
	 * @param ricochetSound Ricochet sound
	 * @param sound_0 Impact sound
	 * @param sound_1 Impact sound
	 * @param sound_2 Impact sound
	 * @param sound_3 Impact sound
	 * @param sound_4 Impact sound
	 */
	public ImpactSound (Material material, String ricochetSound, String sound_0, String sound_1, String sound_2, String sound_3, String sound_4) {
		this.material = material;
		this.ricochetSound = ricochetSound;
		this.sound_0 = sound_0;
		this.sound_1 = sound_1;
		this.sound_2 = sound_2;
		this.sound_3 = sound_3;
		this.sound_4 = sound_4;
	}



	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		ricochetSound = readString (scanner);
		readNumLines (scanner, 5); // <number of lines> ConstData
		scanner.nextLine (); // {
		sound_0 = readString (scanner);
		sound_1 = readString (scanner);
		sound_2 = readString (scanner);
		sound_3 = readString (scanner);
		sound_4 = readString (scanner);
		scanner.nextLine (); // }
	}

	@Override
	public int getNumInternalLines () {
		return 9;
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (400)
		        .append ("\n\tRicochetSound: ").append (material).append (" String '").append (ricochetSound).append ('\'')
		        .append ("\n\t00000005 ImpactSounds").append (material)
		        .append ("\n\t{")
		        .append ("\n\t\tSound_0 String '").append (sound_0).append ('\'')
		        .append ("\n\t\tSound_0 String '").append (sound_1).append ('\'')
		        .append ("\n\t\tSound_0 String '").append (sound_2).append ('\'')
		        .append ("\n\t\tSound_0 String '").append (sound_3).append ('\'')
		        .append ("\n\t\tSound_0 String '").append (sound_4).append ('\'')
		        .append ("\n\t}");
	}


}
