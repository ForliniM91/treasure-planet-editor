package data.worldObjects.elements;

import java.util.Scanner;

import entities.Material;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * The damage descriptor against a specific material
 *
 * @author MarcoForlini
 */
public class DamagePotential implements WriteCode, ReadCode {

	/** Target material */
	public Material	material;

	/** Chance of critical damage */
	public float	chanceOfCriticalDamage;

	/** Chance of fire */
	public float	chanceOfFire;

	/** Fire strength */
	public float	initialFireStrength;

	/** Min damage */
	public float	damageLowerBound;

	/** Max damage */
	public float	damageUpperBound;


	/**
	 * Creates a new {@link DamagePotential}
	 *
	 * @param material Target material
	 * @param scanner The scanner to read
	 * @throws UnexpectedNumLines If the scanner contains an unexpected token
	 */
	public DamagePotential (Material material, Scanner scanner) throws UnexpectedNumLines {
		this.material = material;
		readCode (scanner);
	}

	/**
	 * Creates a new {@link DamagePotential}
	 *
	 * @param material Target material
	 * @param chanceOfCriticalDamage Chance of critical damage
	 * @param chanceOfFire Chacne of fire
	 * @param initialFireStrength Fire strength
	 * @param damageLowerBound Min damage
	 * @param damageUpperBound Max damage
	 */
	public DamagePotential (Material material, float chanceOfCriticalDamage, float chanceOfFire, float initialFireStrength, float damageLowerBound, float damageUpperBound) {
		this.material = material;
		this.chanceOfCriticalDamage = chanceOfCriticalDamage;
		this.chanceOfFire = chanceOfFire;
		this.initialFireStrength = initialFireStrength;
		this.damageLowerBound = damageLowerBound;
		this.damageUpperBound = damageUpperBound;
	}



	@Override
	public int getNumInternalLines () {
		return 8;
	}

	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		scanner.nextLine (); // #
		scanner.nextLine (); // # <material> DamagePotential ; we already know that
		scanner.nextLine (); // #
		chanceOfCriticalDamage = readFloat (scanner);
		chanceOfFire = readFloat (scanner);
		initialFireStrength = readFloat (scanner);
		damageLowerBound = readFloat (scanner);
		damageUpperBound = readFloat (scanner);
	}

	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		return new StringBuilder ()
		        .append (ind).append ("# ")
		        .append (ind).append ("# ").append (material).append (" DamagePotential")
		        .append (ind).append ("# ")
		        .append (ind).append ("Chance of Critical Damage: ").append (material.shortName).append (" Float ").append (toFloat6 (chanceOfCriticalDamage))
		        .append (ind).append ("Chance of Fire: ").append (material.shortName).append (" Float ").append (toFloat6 (chanceOfFire))
		        .append (ind).append ("Initial Fire Strength: ").append (material.shortName).append (" Float ").append (toFloat6 (initialFireStrength))
		        .append (ind).append ("Damage Lower Bound: ").append (material.shortName).append (" Float ").append (toFloat6 (damageLowerBound))
		        .append (ind).append ("Damage Upper Bound: ").append (material.shortName).append (" Float ").append (toFloat6 (damageUpperBound));
	}

}
