package data.worldObjects.elements;

import java.util.function.Function;

import data.worldObjects.constData.ConstData;
import data.worldObjects.constData.ConstDataAI;
import data.worldObjects.constData.ConstDataCollision;
import data.worldObjects.constData.ConstDataCustomInfo;
import data.worldObjects.constData.ConstDataPhysics;
import data.worldObjects.constData.ConstDataRender;


/**
 * Strings used to declare and define a set of attributes of an object
 *
 * @author MarcoForlini
 * @param <T> Type of ConstData
 */
public class DefinitionType <T extends ConstData> { // Can't be an enum because of this: <T extends ConstData>

	/** Defines the AI */
	public static final DefinitionType <ConstDataAI> AIEntityDefinition = new DefinitionType<> ("AIENTITYDEFINITION", (String name) -> {
		return FactoryTypeAI.valueOfEnum (name);
	});


	/** Define the aspect */
	public static final DefinitionType <ConstDataRender> RenderEntityDefinition = new DefinitionType<> ("RENDERENTITYDEFINITION", (String name) -> {
		return FactoryTypeRender.valueOfEnum (name);
	});


	/** Define the physical attributes */
	public static final DefinitionType <ConstDataPhysics> PhysicsDefinition = new DefinitionType<> ("PHYSICSDEFINITION", (String name) -> {
		return FactoryTypePhysics.valueOfEnum (name);
	});


	/** Defines the collision and detection mechanics */
	public static final DefinitionType <ConstDataCollision> CollisionDefinition = new DefinitionType<> ("COLLISIONDEFINITION", (String name) -> {
		return FactoryTypeCollision.valueOfEnum (name);
	});


	/** Define custom attributes for the object */
	public static final DefinitionType <ConstDataCustomInfo> CustomInfoDefinition = new DefinitionType<> ("CUSTOMINFODEFINITION", (String name) -> {
		return FactoryTypeCustomInfo.valueOfEnum (name);
	});



	/** Name of the entity */
	public String								name;

	/** An appropriate constructor for the given FactoryType */
	public Function <String, FactoryType <T>>	constructorGetter;


	private DefinitionType (String name, Function <String, FactoryType <T>> constructorGetter) {
		this.name = name;
		this.constructorGetter = constructorGetter;
	}

	@Override
	public String toString () {
		return name;
	}

}
