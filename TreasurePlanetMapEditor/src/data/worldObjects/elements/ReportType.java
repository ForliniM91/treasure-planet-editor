package data.worldObjects.elements;

/**
 * Type of report for spotted objects
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum ReportType {

	SPOTTER_DRAGONS,
	SPOTTER_WHALES;

	@Override
	public String toString () {
		return name ();
	}

}
