package data.worldObjects.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import data.worldObjects.WorldObject;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * @author MarcoForlini
 */
public class AssociationName implements ReadCode, WriteCode {

	/** The list of attributes */
	public List <AssociationAttribute> attributes = new ArrayList<> ();


	/**
	 * Creates a new {@link AssociationName}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public AssociationName (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link AssociationName}
	 *
	 * @param attributes List of attributes
	 */
	public AssociationName (List <AssociationAttribute> attributes) {
		this.attributes = attributes;
	}


	@Override
	public int getNumLines () {
		return 1;
	}


	@Override
	public int getNumInternalLines () {
		return 0;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		String line = scanner.nextLine ().trim ();

		int n = line.length ();
		int min = 24;
		if (min + 1 < line.indexOf ('\'', min + 1)) { // There's something between ''
			int max, eq;
			AssociationAttribute aa;
			do {
				max = line.indexOf (',', min + 1);
				eq = line.indexOf ('=', min + 1);
				if (max < 0) {
					max = n - 1;
				}
				if (eq >= 0 && eq < max) {
					String key = line.substring (min, eq);
					String value = line.substring (eq + 1, max);
					try {
						aa = new AssAttrKeyValue<> (key, Integer.parseInt (value));
					} catch (NumberFormatException e) {
						try {
							aa = new AssAttrKeyValue<> (AssAttrKeys.valueOf (key), value);
						} catch (IllegalArgumentException e2) {
							aa = new AssAttrKeyValue<> (key, value);
						}
					}
				} else {
					String value = line.substring (min, max);
					try {
						aa = new AssAttrValue<> (Integer.parseInt (value));
					} catch (NumberFormatException e) {
						WorldObject wo = WorldObject.objects.get (value);
						if (wo != null) {
							aa = new AssAttrValue<> (wo);
						} else {
							aa = new AssAttrValue<> (value);
						}
					}
				}
				attributes.add (aa);
				min = max + 1;
			} while (min < n);
		}
	}


	@Override
	public String toString () {
		StringBuilder sb = new StringBuilder (200);
		if (attributes.size () > 0) {
			sb.append (attributes.get (0));
			for (int i = 1, n = attributes.size (); i < n; i++) {
				sb.append (',').append (attributes.get (i));
			}
		}
		return sb.toString ();
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (200);
		if (attributes.size () > 0) {
			sb.append (attributes.get (0));
			for (int i = 1, n = attributes.size (); i < n; i++) {
				sb.append (',').append (attributes.get (i));
			}
		}
		return sb;
	}

}
