/**
 *
 */
package data.worldObjects.elements;

/**
 * An association attribute with key-value pair
 *
 * @author MarcoForlini
 * @param <K> Type of key
 * @param <V> Type of value
 */
public class AssAttrKeyValue <K, V> implements AssociationAttribute {

	/** The key */
	public K	key;
	/** The value */
	public V	value;

	/**
	 * Creates a new {@link AssAttrValue}
	 * 
	 * @param key The key
	 * @param value The value
	 */
	public AssAttrKeyValue (K key, V value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String toString () {
		return key.toString () + '=' + value.toString ();
	}

	@Override
	public String toCode (int indentation) {
		return key.toString () + '=' + value.toString ();
	}

}
