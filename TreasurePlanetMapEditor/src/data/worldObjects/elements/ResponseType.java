package data.worldObjects.elements;

/**
 * Strings used to define the reaction type
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum ResponseType {

	Null ("Null"),
	Ship ("Ship"),
	Lifeboat ("Lifeboat"),
	Asteroid ("Asteroid"),
	Bullet ("Bullet"),
	AttackableWeaponFire ("Attackable WeaponFire"),
	DefensiveWeaponFire ("Defensive WeaponFire"),
	SpaceObject ("Space Object"),
	StaticObject ("Static Object"),
	;

	public String name;

	private ResponseType (String name) {
		this.name = name;
	}

	@Override
	public String toString () {
		return name;
	}

}
