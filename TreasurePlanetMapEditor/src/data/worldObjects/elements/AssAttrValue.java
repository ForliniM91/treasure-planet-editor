/**
 *
 */
package data.worldObjects.elements;

/**
 * An associative attribute with a value
 * 
 * @author MarcoForlini
 * @param <V> Type of value
 */
public class AssAttrValue <V> implements AssociationAttribute {

	/** The value */
	public V value;

	/**
	 * Creates a new {@link AssAttrValue}
	 *
	 * @param value The value
	 */
	public AssAttrValue (V value) {
		this.value = value;
	}

	@Override
	public String toString () {
		return value.toString ();
	}

	@Override
	public String toCode (int indentation) {
		return value.toString ();
	}

}
