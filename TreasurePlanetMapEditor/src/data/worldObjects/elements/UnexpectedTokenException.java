/**
 *
 */
package data.worldObjects.elements;

import java.io.IOException;


/**
 * An exception thrown if encountering an unexpected token while reading a file
 *
 * @author MarcoForlini
 */
public class UnexpectedTokenException extends IOException {

	private static final long serialVersionUID = 1845529888902350490L;

	/**
	 * Creates a new {@link UnexpectedTokenException}
	 */
	public UnexpectedTokenException () {
		super ("Unexpected token");
	}

	/**
	 * Creates a new {@link UnexpectedTokenException}
	 * 
	 * @param message A custom message
	 */
	public UnexpectedTokenException (String message) {
		super (message);
	}

	/**
	 * Creates a new {@link UnexpectedTokenException}
	 *
	 * @param line The line
	 * @param token The token
	 */
	public UnexpectedTokenException (String line, String token) {
		super ("Unexpected token at: \"" + line + "\" |> " + token);
	}

}
