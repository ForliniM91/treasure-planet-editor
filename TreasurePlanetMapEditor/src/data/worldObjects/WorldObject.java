package data.worldObjects;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import data.worldObjects.constData.AsteroidCustomInfoFactory;
import data.worldObjects.constData.BlackHoleCustomInfoFactory;
import data.worldObjects.constData.BoundingRenderEntity;
import data.worldObjects.constData.BoundingSphere;
import data.worldObjects.constData.ConstDataAI;
import data.worldObjects.constData.ConstDataCollision;
import data.worldObjects.constData.ConstDataCustomInfo;
import data.worldObjects.constData.ConstDataCustomInfoBullet;
import data.worldObjects.constData.ConstDataPhysics;
import data.worldObjects.constData.ConstDataRender;
import data.worldObjects.constData.CrewCustomInfoFactory;
import data.worldObjects.constData.DragonAI;
import data.worldObjects.constData.DragonCustomInfoFactory;
import data.worldObjects.constData.DragonPhysics;
import data.worldObjects.constData.EtheriumCurrentCustomInfoFactory;
import data.worldObjects.constData.GunAI;
import data.worldObjects.constData.GunCustomInfoFactory;
import data.worldObjects.constData.IslandAI;
import data.worldObjects.constData.IslandCustomInfoFactory;
import data.worldObjects.constData.NebulaCustomInfoFactory;
import data.worldObjects.constData.Point;
import data.worldObjects.constData.ProjectilePhysics;
import data.worldObjects.constData.RenderEntityFactory;
import data.worldObjects.constData.ShipAI;
import data.worldObjects.constData.ShipCustomInfoFactory;
import data.worldObjects.constData.ShipDebrisCustomInfoFactory;
import data.worldObjects.constData.ShipDemo;
import data.worldObjects.constData.SpaceAnimalAI;
import data.worldObjects.constData.SpaceAnimalCustomInfoFactory;
import data.worldObjects.constData.SpaceObjectAI;
import data.worldObjects.constData.SpaceObjectPhysics;
import data.worldObjects.constData.VolcanoAI;
import data.worldObjects.constData.WhalePhysics;
import data.worldObjects.constData.WorldObjectData;
import data.worldObjects.elements.Definition;
import data.worldObjects.elements.DefinitionType;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * The ConstData of a WorldObject
 *
 * @author MarcoForlini
 */
public class WorldObject implements ReadCode, WriteCode {



	/** All objects */
	public static final Map <String, WorldObject> objects = new HashMap<> ();




	/** Unique ID */
	public String			ID;

	/** Object's base data */
	public WorldObjectData	worldObjectData;


	/** The physics */
	public Definition <? extends ConstDataPhysics>		physics;

	/** The collision */
	public Definition <? extends ConstDataCollision>	collision;

	/** The render */
	public Definition <? extends ConstDataRender>		render;

	/** The render */
	public Definition <? extends ConstDataAI>			intelligence;

	/** The customInfo */
	public Definition <? extends ConstDataCustomInfo>	customInfo;

	/** No bullet type */
	public WorldObject									bullet	= WorldObject.buildBullet ("NoBulletType", null, null, null, null, null);




	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID Unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The intelligence
	 * @param customInfo The custom info
	 */
	public WorldObject (String ID,
	                    Definition <? extends ConstDataPhysics> physics,
	                    Definition <? extends ConstDataCollision> collision,
	                    Definition <? extends ConstDataRender> render,
	                    Definition <? extends ConstDataAI> intelligence,
	                    Definition <? extends ConstDataCustomInfo> customInfo) {
		this.ID = ID;
		worldObjectData = new WorldObjectData (ID, physics, collision, render, intelligence, customInfo);
		this.physics = physics;
		this.collision = collision;
		this.render = render;
		this.intelligence = intelligence;
		this.customInfo = customInfo;
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param worldObject A {@link WorldObject}
	 */
	public WorldObject (WorldObject worldObject) {
		this (worldObject.ID, worldObject.physics, worldObject.collision, worldObject.render, worldObject.intelligence, worldObject.customInfo);
	}





	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new animal {@link WorldObject}
	 */
	public static WorldObject
	        buildAnimal (String ID, Definition <WhalePhysics> physics,
	                     Definition <BoundingRenderEntity> collision,
	                     Definition <RenderEntityFactory> render,
	                     Definition <SpaceAnimalAI> intelligence,
	                     Definition <SpaceAnimalCustomInfoFactory> customInfo) {
		return new WorldObject (ID, physics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new dragon {@link WorldObject}
	 */
	public static WorldObject
	        buildAnimalDragon (String ID,
	                           Definition <DragonPhysics> physics,
	                           Definition <BoundingRenderEntity> collision,
	                           Definition <RenderEntityFactory> render,
	                           Definition <DragonAI> intelligence,
	                           Definition <DragonCustomInfoFactory> customInfo) {
		return new WorldObject (ID, physics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new asteroid {@link WorldObject}
	 */
	public static WorldObject
	        buildAsteroid (String ID,
	                       Definition <BoundingRenderEntity> collision,
	                       Definition <RenderEntityFactory> render,
	                       Definition <IslandAI> intelligence,
	                       Definition <IslandCustomInfoFactory> customInfo) {
		return new WorldObject (ID, Definition.nullPhysics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new armed asteroid {@link WorldObject}
	 */
	public static WorldObject
	        buildAsteroidArmed (String ID,
	                            Definition <SpaceObjectPhysics> physics,
	                            Definition <BoundingRenderEntity> collision,
	                            Definition <RenderEntityFactory> render,
	                            Definition <SpaceObjectAI> intelligence,
	                            Definition <AsteroidCustomInfoFactory> customInfo) {
		return new WorldObject (ID, physics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new base {@link WorldObject}
	 */
	public static WorldObject
	        buildBase (String ID,
	                   Definition <BoundingRenderEntity> collision,
	                   Definition <RenderEntityFactory> render,
	                   Definition <IslandAI> intelligence,
	                   Definition <IslandCustomInfoFactory> customInfo) {
		return new WorldObject (ID, Definition.nullPhysics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new bullet {@link WorldObject}
	 */
	public static WorldObject
	        buildBullet (String ID,
	                     Definition <ProjectilePhysics> physics,
	                     Definition <Point> collision,
	                     Definition <RenderEntityFactory> render,
	                     Definition <SpaceObjectAI> intelligence,
	                     Definition <ConstDataCustomInfoBullet> customInfo) {
		return new WorldObject (ID, physics, collision, render, intelligence, customInfo); // TODO
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param customInfo The custom info
	 * @return A new crew {@link WorldObject}
	 */
	public static WorldObject
	        buildCrew (String ID, Definition <CrewCustomInfoFactory> customInfo) {
		return new WorldObject (ID, Definition.nullPhysics, Definition.nullCollision, Definition.nullRender, Definition.nullAI, customInfo); // TODO: Category
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new gun {@link WorldObject}
	 */
	public static WorldObject
	        buildGun (String ID,
	                  Definition <RenderEntityFactory> render,
	                  Definition <GunAI> intelligence,
	                  Definition <GunCustomInfoFactory> customInfo) {
		return new WorldObject (ID, Definition.nullPhysics, Definition.nullCollision, render, intelligence, customInfo); // TODO
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new island {@link WorldObject}
	 */
	public static WorldObject
	        buildIsland (String ID,
	                     Definition <BoundingRenderEntity> collision,
	                     Definition <RenderEntityFactory> render,
	                     Definition <IslandAI> intelligence,
	                     Definition <IslandCustomInfoFactory> customInfo) {
		return new WorldObject (ID, Definition.nullPhysics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new volcano island {@link WorldObject}
	 */
	public static WorldObject
	        buildIslandVolcano (String ID,
	                            Definition <BoundingRenderEntity> collision,
	                            Definition <RenderEntityFactory> render,
	                            Definition <VolcanoAI> intelligence,
	                            Definition <IslandCustomInfoFactory> customInfo) {
		return new WorldObject (ID, Definition.nullPhysics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new ship {@link WorldObject}
	 */
	public static WorldObject
	        buildShip (String ID,
	                   Definition <ShipDemo> physics,
	                   Definition <BoundingRenderEntity> collision,
	                   Definition <RenderEntityFactory> render,
	                   Definition <ShipAI> intelligence,
	                   Definition <ShipCustomInfoFactory> customInfo) {
		return new WorldObject (ID, physics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param render The render
	 * @param customInfo The custom info
	 * @return A new ship debris {@link WorldObject}
	 */
	public static WorldObject
	        buildShipDebris (String ID,
	                         Definition <SpaceObjectPhysics> physics,
	                         Definition <BoundingSphere> collision,
	                         Definition <RenderEntityFactory> render,
	                         Definition <ShipDebrisCustomInfoFactory> customInfo) {
		return new WorldObject (ID, physics, collision, render, Definition.nullAI, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new ship wrecked {@link WorldObject}
	 */
	public static WorldObject
	        buildShipWrecked (String ID,
	                          Definition <SpaceObjectPhysics> physics,
	                          Definition <BoundingRenderEntity> collision,
	                          Definition <RenderEntityFactory> render,
	                          Definition <SpaceObjectAI> intelligence,
	                          Definition <AsteroidCustomInfoFactory> customInfo) {
		return new WorldObject (ID, physics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param render The render
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new black hole terrain {@link WorldObject}
	 */
	public static WorldObject
	        buildTerrainBlackHole (String ID,
	                               Definition <SpaceObjectPhysics> physics,
	                               Definition <BoundingSphere> collision,
	                               Definition <RenderEntityFactory> render,
	                               Definition <SpaceObjectAI> intelligence,
	                               Definition <BlackHoleCustomInfoFactory> customInfo) {
		return new WorldObject (ID, physics, collision, render, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param render The render
	 * @return A new distant terrain {@link WorldObject}
	 */
	public static WorldObject
	        buildTerrainDistant (String ID,
	                             Definition <RenderEntityFactory> render) {
		return new WorldObject (ID, Definition.nullPhysics, Definition.nullCollision, render, Definition.nullAI, Definition.nullCustomInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param customInfo The custom info
	 * @return A new etherium current terrain {@link WorldObject}
	 */
	public static WorldObject
	        buildTerrainEtheriumCurrent (String ID,
	                                     Definition <EtheriumCurrentCustomInfoFactory> customInfo) {
		return new WorldObject (ID, Definition.nullPhysics, Definition.nullCollision, Definition.nullRender, Definition.nullAI, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param physics The physics
	 * @param collision The collision
	 * @param intelligence The AI
	 * @param customInfo The custom info
	 * @return A new nebula terrain {@link WorldObject}
	 */
	public static WorldObject
	        buildTerrainNebula (String ID,
	                            Definition <SpaceObjectPhysics> physics,
	                            Definition <BoundingSphere> collision,
	                            Definition <SpaceObjectAI> intelligence,
	                            Definition <NebulaCustomInfoFactory> customInfo) {
		return new WorldObject (ID, physics, collision, Definition.nullRender, intelligence, customInfo);
	}


	/**
	 * Creates a new {@link WorldObject}
	 *
	 * @param ID A unique ID
	 * @param collision The collision
	 * @param render The render
	 * @return A new parliament terrain {@link WorldObject}
	 */
	public static WorldObject
	        TerrainParliament (String ID,
	                           Definition <BoundingRenderEntity> collision,
	                           Definition <RenderEntityFactory> render) {
		return new WorldObject (ID, Definition.nullPhysics, collision, render, Definition.nullAI, Definition.nullCustomInfo);
	}



	/**
	 * Read the given {@link Scanner} and a constructor, and build a new WorldObject
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner returns an unexpected token
	 */
	public WorldObject (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}



	@Override
	public int getNumInternalLines () {
		return 9 + intelligence.getNumLines () + render.getNumLines () + physics.getNumLines () + collision.getNumLines () + customInfo.getNumLines ();
	}



	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		String line = scanner.nextLine (); // Type String '<ID>'
		String tLine = line.trim ();
		ID = tLine.substring (13, line.length () - 1);

		scanner.nextLine (); // 00000005 ConstData
		scanner.nextLine (); // {
		boolean hasPhysics = scanner.nextLine ().substring (20).length () > 1; // HasPhysics String '<ID>'
		boolean hasCollision = scanner.nextLine ().substring (22).length () > 1; // HasCollision String '<ID>'
		boolean hasRender = scanner.nextLine ().substring (18).length () > 1; // HasRender String '<ID>'
		boolean hasAI = scanner.nextLine ().substring (15).length () > 1; // HasAI String '<ID>'
		boolean hasCustomInfo = scanner.nextLine ().substring (23).length () > 1; // HasCustomInfo String '<ID>'
		scanner.nextLine (); // }

		while (scanner.hasNextLine ()) {
			line = scanner.nextLine (); // Definition String '<DefinitionType>'
			String strType = tLine.substring (19, line.length () - 1);

			boolean undef = false;
			line = readString (scanner); // EntityType String '<ID>'
			if (line.isEmpty ()) {
				undef = true;
			}

			switch (strType) {
				case "AIENTITYDEFINITION":
					intelligence = (!hasAI || undef) ? Definition.nullAI : new Definition<> (DefinitionType.AIEntityDefinition, line, scanner);
					break;
				case "RENDERENTITYDEFINITION":
					render = (!hasRender || undef) ? Definition.nullRender : new Definition<> (DefinitionType.RenderEntityDefinition, line, scanner);
					break;
				case "PHYSICSDEFINITION":
					physics = (!hasPhysics || undef) ? Definition.nullPhysics : new Definition<> (DefinitionType.PhysicsDefinition, line, scanner);
					break;
				case "COLLISIONDEFINITION":
					collision = (!hasCollision || undef) ? Definition.nullCollision : new Definition<> (DefinitionType.CollisionDefinition, line, scanner);
					break;
				case "CUSTOMINFODEFINITION":
					customInfo = (!hasCustomInfo || undef) ? Definition.nullCustomInfo : new Definition<> (DefinitionType.CustomInfoDefinition, line, scanner);
					break;
				default:
					throw new UnexpectedTokenException (line, strType);
			}
		}
		worldObjectData = new WorldObjectData (ID, physics, collision, render, intelligence, customInfo);
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (10000)
		        .append ("Type String '").append (ID).append ('\'')
		        .append (worldObjectData.toCode (0))
		        .append (intelligence.toCode (0))
		        .append (render.toCode (0))
		        .append (physics.toCode (0))
		        .append (collision.toCode (0))
		        .append (customInfo.toCode (0));
	}


	@Override
	public String toString () {
		return ID;
	}

}
