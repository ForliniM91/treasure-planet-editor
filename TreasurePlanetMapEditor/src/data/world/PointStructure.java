package data.world;

import java.util.List;

import interfaces.WriteCode;
import logic.Core;


public abstract class PointStructure <T extends Point2D> extends Entity implements WriteCode {

	/** Points list */
	private List <T> points;

	/**
	 * Create a new points structure
	 *
	 * @param ID Structure ID
	 * @param points Points list
	 */
	public PointStructure (String ID, List <T> points) {
		super (ID);
		setPoints (points);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Polygon ID
	 * @param points Points list
	 */
	public void setValues (String ID, List <T> points) {
		setID (ID);
		setPoints (points);
	}

	public List <T> getPoints () {
		return points;
	}

	public void setPoints (List <T> points) {
		this.points = points;
	}

	@Override
	public String toString () {
		return getID () + " " + points.size ();
	}

	@SuppressWarnings ("unchecked")
	@Override
	public PointStructure <T> clone () {
		PointStructure <T> copy = (PointStructure <T>) super.clone ();
		copy.points = Core.cloneListP (points);
		return copy;
	}

}
