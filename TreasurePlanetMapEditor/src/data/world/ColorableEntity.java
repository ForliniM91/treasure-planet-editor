package data.world;

import interfaces.Colorable;

/**
 * A colorable {@link Entity}
 *
 * @author MarcoForlini
 */
public abstract class ColorableEntity extends Entity implements Colorable {

	/**
	 * Creates a new {@link ColorableEntity}
	 *
	 * @param ID the ID of the {@link Entity}
	 */
	public ColorableEntity (String ID) {
		super (ID);
	}

}
