package data.world;

import java.util.List;
import java.util.Vector;

import interfaces.WriteCode;


public class Polygon extends PointStructure <Point2D> implements WriteCode {

	/** Ships objects defined in the map. */
	public static final Vector <Polygon> polygons = new Vector<> ();

	/**
	 * Create a new polygon
	 *
	 * @param ID Polygon ID
	 * @param points Points list
	 */
	public Polygon (String ID, List <Point2D> points) {
		super (ID, points);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Polygon ID
	 * @param points Points list
	 */
	@Override
	public void setValues (String ID, List <Point2D> points) {
		setID (ID);
		setPoints (points);
	}

	@Override
	public Polygon clone () {
		return (Polygon) super.clone ();
	}

	@Override
	public String toString () {
		return getID ();
	}

	@Override
	public CharSequence toCode (int indentation) {
		int nLines = 2 + getPoints ().size ();
		StringBuilder sb = new StringBuilder (500).append ("\n\t\t").append (toChars8 (nLines))
		        .append (" World Polygons Vectors - Element\n\t\t{\n\t\t\tName String '").append (getID ())
		        .append ("'\n\t\t\tPoints Int ").append (getPoints ().size ());
		for (Point2D point : getPoints ()) {
			sb.append ("Points Coord").append (point.toCode (indentation));
		}
		return sb.append ("\n\t\t}");
	}

}
