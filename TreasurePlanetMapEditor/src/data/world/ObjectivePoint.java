package data.world;

import java.util.Arrays;
import java.util.Vector;


public class ObjectivePoint extends PointStructure <Point3D> {

	public static final ObjectivePoint			NO_OBJECTVE_POINT	= new ObjectivePoint ("NO OBJECTIVE POINT", null);
	/** Objective points defined in the map. */
	public static final Vector <ObjectivePoint>	objectivePoints		= new Vector<> ();

	/**
	 * Create a new ObjectivePoint
	 *
	 * @param ID Point ID
	 * @param point Point
	 */
	public ObjectivePoint (String ID, Point3D point) {
		super (ID, Arrays.asList (new Point3D[] { point }));
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID ObjectivePoint ID
	 * @param point ObjectivePoint point
	 */
	public void setValues (String ID, Point3D point) {
		setID (ID);
		setPoints (Arrays.asList (new Point3D[] { point }));
	}

	@Override
	public ObjectivePoint clone () {
		return (ObjectivePoint) super.clone ();
	}

	@Override
	public String toString () {
		return getID ();
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (100).append ("\n\t\t\t00000002 Objective Point Info - Element\n\t\t\t{\n\t\t\t\tName String '").append (getID ())
		        .append ("'\n\t\t\t\tPosition Vector3").append (getPoints ().get (0).toCode (indentation))
		        .append ("\n\t\t\t}");
	}

}
