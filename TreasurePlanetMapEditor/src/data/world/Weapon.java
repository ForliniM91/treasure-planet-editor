package data.world;

import java.util.Vector;

import interfaces.WriteCode;


/**
 * A weapon for the ship
 *
 * @author MarcoForlini
 */
public class Weapon extends Entity implements WriteCode {

	/** All available weapons */
	public static final Vector <Weapon>		weapons		= new Vector<> ();

	/** Chosen weapons for this map */
	public static final Vector <Boolean>	selected	= new Vector<> ();

	static {
		String[] weaponNames = new String[] { "Cannone laser", "Carronata", "Cannone al plasma", "Cannone gattling", "Laser", "Impulso elettrico", "Carica di gravit�", "Arpione" };
		for (String weaponName : weaponNames) {
			weapons.add (new Weapon (weaponName, weaponName));
			selected.add (true);
		}
	}


	/** Weapon ID */
	private String name;

	/**
	 * Create an object Weapon
	 *
	 * @param ID Weapon ID
	 * @param name Weapon name
	 */
	public Weapon (String ID, String name) {
		super (ID);
		setName (name);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Weapon ID
	 * @param name Weapon name
	 */
	public void setValues (String ID, String name) {
		setID (ID);
		setName (name);
	}

	/**
	 * Returns the name of the weapon
	 *
	 * @return the name of the weapon
	 */
	public String getName () {
		return name;
	}

	/**
	 * Sets the name of the weapon
	 *
	 * @param name the new name of the weapon
	 */
	public void setName (String name) {
		this.name = name;
	}

	@Override
	public String toString () {
		return name;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder ("\n\t\tWorld Arms List - Element String '").append (getID ()).append ('\'');
	}

}
