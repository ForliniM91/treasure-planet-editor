package data.world;

import interfaces.WriteCode;


public class Orientation implements Cloneable, WriteCode {

	/** Orientation matrix */
	private float[][] matrix;

	/**
	 * Create a new Orientation
	 *
	 * @param matrix Orienattion matrix
	 */
	public Orientation (float[][] matrix) {
		setMatrix (matrix);
	}

	public float[][] getMatrix () {
		return matrix;
	}

	public void setMatrix (float[][] matrix) {
		this.matrix = matrix;
	}

	@Override
	public String toString () {
		return "|" + toFloat6 (matrix[0][0]) + ' ' + toFloat6 (matrix[0][1]) + ' ' + toFloat6 (matrix[0][2]) + "|\n" +
		       "|" + toFloat6 (matrix[1][0]) + ' ' + toFloat6 (matrix[1][1]) + ' ' + toFloat6 (matrix[1][2]) + "|\n" +
		       "|" + toFloat6 (matrix[2][0]) + ' ' + toFloat6 (matrix[2][1]) + ' ' + toFloat6 (matrix[2][2]) + "|";
	}

	@Override
	public Orientation clone () {
		try {
			return (Orientation) super.clone ();
		} catch (CloneNotSupportedException e) {
			throw new InternalError ();
		}
	}


	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (250).append ("( ")
		        .append (toFloat6 (matrix[0][0])).append (", ")
		        .append (toFloat6 (matrix[0][1])).append (", ")
		        .append (toFloat6 (matrix[0][2])).append (", ")
		        .append (toFloat6 (matrix[1][0])).append (", ")
		        .append (toFloat6 (matrix[1][1])).append (", ")
		        .append (toFloat6 (matrix[1][2])).append (", ")
		        .append (toFloat6 (matrix[2][0])).append (", ")
		        .append (toFloat6 (matrix[2][1])).append (", ")
		        .append (toFloat6 (matrix[2][2]))
		        .append (" )");
	}

	/**
	 * Convert the element to game code
	 *
	 * @param i Index for the line in the matrix
	 * @return The string to be printed on the file
	 */
	public String toCodeLine (int i) {
		return "( " + toFloat6 (matrix[i][0]) + ", " + toFloat6 (matrix[i][1]) + ", " + toFloat6 (matrix[i][2]) + " )";
	}

}
