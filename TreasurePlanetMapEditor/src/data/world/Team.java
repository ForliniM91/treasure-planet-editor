package data.world;

import java.util.Vector;

import entities.Race;
import interfaces.WriteCode;


public class Team extends Entity implements WriteCode {

	/** DUMMY team */
	public static final Team			NONE	= new Team (null, null, Race.NONE, false);

	public static final Vector <Team>	teams	= new Vector<> ();
	static {
		Team.teams.add (Team.NONE);
	}


	/** Team name */
	private String	name;
	/** Race ID */
	private Race	race;
	/** Is the team members race locked to the team race? */
	private boolean	lockRace;

	/**
	 * Create a new team
	 *
	 * @param ID Team ID
	 * @param race Race ID
	 * @param lockRace Is the team members race locked to the team race?
	 */
	public Team (String ID, String name, Race race, boolean lockRace) {
		super (ID);
		setName (name);
		setRace (race);
		setLockRace (lockRace);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Team ID
	 * @param name Team name
	 * @param race Race ID
	 * @param lockRace Is the team members race locked to the team race?
	 */
	public void setValues (String ID, String name, Race race, boolean lockRace) {
		setID (ID);
		setName (name);
		setRace (race);
		setLockRace (lockRace);
	}

	public String getName () {
		return name;
	}

	public void setName (String name) {
		this.name = name;
	}

	public Race getRace () {
		return race;
	}

	public void setRace (Race race) {
		this.race = race;
	}

	public boolean isLockRace () {
		return lockRace;
	}

	public void setLockRace (boolean lockRace) {
		this.lockRace = lockRace;
	}

	@Override
	public Team clone () {
		if (getID () == null) {
			return null; // NONE TEAM. CAN'T CLONE IT
		}
		Team copy = (Team) super.clone ();
		copy.setID (Integer.toHexString (hashCode ()));
		return copy;
	}

	@Override
	public String toString () {
		if (getID () == null) {
			return "None";
		}
		return (lockRace ? "[X] " : "") + "(" + race + ") " + getID ();
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (200)
		        .append ("\n\t00000003 Team List - Element\n\t{\n\t\tTeam Name ID String '").append (getID ())
		        .append ("'\n\t\tRace Int ").append (race.ordinal ())
		        .append ("\n\t\tRace Lock Bool ").append (toBool (lockRace));
	}

}
