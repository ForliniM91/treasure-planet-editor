package data.world;

import interfaces.WriteCode;


public class JournalEntry implements Comparable <JournalEntry>, WriteCode {

	public static String	journalTitle	= null, journalMusic = null;

	/** Text displayed */
	private String			text;
	/** Voice file */
	private String			voice;
	/** Picture texture */
	private String			texture;

	/**
	 * Create a new JournalEntry
	 *
	 * @param text Text displayed
	 * @param voice Voice file
	 * @param texture Picture texture
	 */
	public JournalEntry (String text, String voice, String texture) {
		setValues (text, voice, texture);
	}

	/**
	 * Set all values for this object
	 *
	 * @param text Text displayed
	 * @param voice Voice file
	 * @param texture Picture texture
	 */
	public void setValues (String text, String voice, String texture) {
		setText (text);
		setVoice (voice);
		setTexture (texture);
	}

	public String getText () {
		return text;
	}

	public void setText (String text) {
		this.text = text;
	}

	public String getVoice () {
		return voice;
	}

	public void setVoice (String voice) {
		this.voice = voice;
	}

	public String getTexture () {
		return texture;
	}

	public void setTexture (String texture) {
		this.texture = texture;
	}

	@Override
	public String toString () {
		return text + " > " + Integer.toHexString (hashCode ());
	}

	@Override
	public int compareTo (JournalEntry o) {
		return text.compareTo (o.text);
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (300).append ("\n\t\t\t00000003 Page Info - Element\n\t\t\t{\n\t\t\t\tTextStringID String '").append (text)
		        .append ("'\n\t\t\t\tSpeechEventFileName String '").append (voice)
		        .append ("'\n\t\t\t\tPictureTexture String '").append (texture)
		        .append ("'\n\t\t\t}");
	}

	public static CharSequence journalTitle () {
		return new StringBuilder ("\n\t\t\tTitle StringID String '").append (journalTitle).append ('\'');
	}

	public static CharSequence joournalMusic () {
		return new StringBuilder ("\n\t\tJournal Music Name String '").append (journalMusic).append ('\'');
	}

}
