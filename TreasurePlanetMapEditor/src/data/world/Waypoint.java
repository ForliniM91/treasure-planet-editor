package data.world;

import java.util.List;
import java.util.Vector;

import interfaces.WriteCode;


public class Waypoint extends PointStructure <Point3D> implements WriteCode {

	/** Ships objects defined in the map. */
	public static final Vector <Waypoint> waypoints = new Vector<> ();

	/**
	 * Create a new Waypoint
	 *
	 * @param ID Waypoint ID
	 * @param points Points list
	 */
	public Waypoint (String ID, List <Point3D> points) {
		super (ID, points);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Waypoint ID
	 * @param points Points list
	 */
	@Override
	public void setValues (String ID, List <Point3D> points) {
		setID (ID);
		setPoints (points);
	}

	@Override
	public Waypoint clone () {
		return (Waypoint) super.clone ();
	}

	@Override
	public String toString () {
		return getID ();
	}

	@Override
	public CharSequence toCode (int indentation) {
		int nLines = 2 + getPoints ().size ();
		StringBuilder sb = new StringBuilder (300)
		        .append ("\n\t\t").append (toChars8 (nLines))
		        .append (" Waypoint Path Info Vector - Element\n\t\t{\n\t\t\tWaypoint Path Name String '").append (getID ())
		        .append ("'\n\t\t\tWaypoint Path Points - Size Int ").append (getPoints ().size ());
		for (Point3D point : getPoints ()) {
			sb.append ("Waypoint Path Points - Element Vector3").append (point.toCode (indentation));
		}
		return sb.append ("\n\t\t}");
	}

}
