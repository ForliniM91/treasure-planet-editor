package data.world;

import java.util.Vector;

import interfaces.WriteCode;


/**
 * A sailor for the ship
 *
 * @author MarcoForlini
 */
public class Sailor extends Entity implements WriteCode {

	/** All available sailors */
	public static final Vector <Sailor>		sailors		= new Vector<> ();

	/** Chosen sailors for this map */
	public static final Vector <Boolean>	selected	= new Vector<> ();

	static {
		String[] sailorNames = new String[] { "Mario", "Luigi", "Bowser", "Sonic", "Tails", "Rosso", "Blu", "Topo gigio" };
		for (String sailorName : sailorNames) {
			sailors.add (new Sailor (sailorName, sailorName));
			selected.add (true);
		}
	}


	/** Sailor Name */
	private String name;

	/**
	 * Create a new object Sailor
	 *
	 * @param ID Sailor ID
	 * @param name Sailor name
	 */
	public Sailor (String ID, String name) {
		super (ID);
		setName (name);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Sailor ID
	 * @param name Sailor name
	 */
	public void setValues (String ID, String name) {
		setID (ID);
		setName (name);
	}

	/**
	 * Returns the name of the sailor
	 *
	 * @return the name of the sailor
	 */
	public String getName () {
		return name;
	}

	/**
	 * Sets the name of the sailor
	 *
	 * @param name the new name of the sailor
	 */
	public void setName (String name) {
		this.name = name;
	}

	@Override
	public String toString () {
		return name;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (50).append ("\n\t\tWorld Crew List - Element String '").append (getID ()).append ('\'');
	}

}
