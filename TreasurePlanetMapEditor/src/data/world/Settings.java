package data.world;

import java.awt.Color;

import entities.GameColor;
import logic.Core;

/**
 * Class with all map settings
 *
 * @author MarcoForlini
 */
public class Settings {


	public static String	mapID					= "";

	public static String	mapName					= "";

	public static String	mapDesc					= "";

	public static String	seed					= "";




	/** Map width */
	public static int		width					= 4000;

	/** Map height */
	public static int		height					= 3000;

	/** Map depth */
	public static int		depth					= 4000;

	/** World buffer size */
	public static int		worldBufferSize			= 500;

	/** Light X direction */
	public static float		x						= 0.5f;

	/** Light Y direction */
	public static float		y						= 0.5f;

	/** Light Z direction */
	public static float		z						= -0.5f;

	/** Background texture */
	public static String	background				= Core.backgrounds[0];

	/** Star map texture */
	public static String	starMap					= Core.starmaps[0];

	/** Ambient color */
	public static Color		ambientColor			= new GameColor ();

	/** Roof color */
	public static Color		roofColor				= new GameColor ();

	/** Floor color */
	public static Color		floorColor				= new GameColor ();

	/** Is it a campaing map? */
	public static boolean	isCampaign				= false;

	/** Is it a scenario map? */
	public static boolean	isSkirmish				= true;

	/** Is it an hystorical map? */
	public static boolean	isHystorical			= false;

	/** Is it the last mission of the campaing? */
	public static boolean	isLastMission			= false;

	/** Is alliance change allowed? */
	public static boolean	isAllianceChangeAllowed	= true;

	/** Do islands make sounds? */
	public static boolean	isIslandsMakeSounds		= true;

}
