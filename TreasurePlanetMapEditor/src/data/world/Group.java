package data.world;

import java.util.Vector;

import interfaces.WriteCode;
import logic.Core;
import logic.MaxIndex;


public class Group extends Entity implements WriteCode {

	/** Entities list */
	private Vector <WorldObjectInstance>	objects;
	/** Groups defined in the map. */
	public static final Vector <Group>		groups	= new Vector<> ();

	/**
	 * Create a new list
	 *
	 * @param list Objects list
	 */
	public Group (String ID, Vector <WorldObjectInstance> objects) {
		super (ID);
		setObjects (objects);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Group ID
	 * @param list Objects list
	 */
	public void setValue (String ID, Vector <WorldObjectInstance> objects) {
		setID (ID);
		setObjects (objects);
	}

	public Vector <WorldObjectInstance> getObjects () {
		return objects;
	}

	public void setObjects (Vector <WorldObjectInstance> objects) {
		this.objects = objects;
	}

	@Override
	public Group clone () {
		Group copy = (Group) super.clone ();
		try {
			copy.setID (Core.getCloneID (getID (), Group.groups));
		} catch (MaxIndex e) {
			return null;
		}
		copy.setObjects ((Vector <WorldObjectInstance>) Core.cloneListE (objects));
		return copy;
	}

	@Override
	public String toString () {
		return getID ();
	}

	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (500).append ("\n\t\t").append (toChars8 (objects.size () + 2))
		        .append (" Group\n\t\t{\n\t\t\tName String '").append (getID ())
		        .append ("'\n\t\t\tWorld Object IDs - Size Int ").append (objects.size ());
		for (WorldObjectInstance wo : objects) {
			sb.append ("\n\t\t\tWorld Object IDs - Element Int ").append (wo.getNumID ());
		}
		return sb.append ("\n\t\t}");
	}

}
