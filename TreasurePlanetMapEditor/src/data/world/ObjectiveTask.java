package data.world;

import java.util.Vector;

import entities.TaskState;
import interfaces.WriteCode;


public class ObjectiveTask extends Entity implements WriteCode {

	private static short						currentObjectivePoint	= -1;
	private static boolean						visibleOnStarMap		= true;

	/** String containing the task text */
	private String								text;
	/** Task state */
	private TaskState							state;
	/** Objective task defined in the map. */
	public static final Vector <ObjectiveTask>	objectiveTasks			= new Vector<> ();

	/**
	 * Create a new Task
	 *
	 * @param ID Task ID
	 * @param text Task text
	 * @param active Is active
	 * @param completed Is completed
	 * @param failed Is failed
	 */
	public ObjectiveTask (String ID, String text, TaskState state) {
		super (ID);
		setText (text);
		setState (state);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Task ID
	 * @param text Task text
	 * @param active IsActive
	 * @param completed IsCompleted
	 * @param failed IsFailed
	 */
	public void setValues (String ID, String text, TaskState state) {
		setID (ID);
		setText (text);
		setState (state);
	}

	public String getText () {
		return text;
	}

	public void setText (String text) {
		this.text = text;
	}

	public TaskState getState () {
		return state;
	}

	public void setState (TaskState state) {
		this.state = state;
	}

	@Override
	public String toString () {
		switch (state) {
			case ACTIVE:
				return "[A]   " + getID ();
			case COMPLETED:
				return "[C]   " + getID ();
			case FAILED:
				return "[F]   " + getID ();
			default:
				return getID ();
		}
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (500).append ("\n\t\t\t00000005 Objective Task Array - Element\n\t\t\t{\n\t\t\t\tName String '").append (getID ())
		        .append ("'\n\t\t\t\tTextStringID String '").append (text)
		        .append ("'n\t\t\t\tActive Bool ").append (toBool (state == TaskState.ACTIVE))
		        .append ("n\t\t\t\tCompleted Bool").append (toBool (state == TaskState.COMPLETED))
		        .append ("n\t\t\t\tFailed Bool False ").append (toBool (state == TaskState.FAILED))
		        .append ("\n\t\t\t}");
	}

	public CharSequence objective () {
		return new StringBuilder ("\n\t\t\tCurrent Objective Point Int ").append (currentObjectivePoint)
		        .append ("\n\t\t\tCurrent Point Visible On StarMap Bool ").append (toBool (visibleOnStarMap));
	}

}
