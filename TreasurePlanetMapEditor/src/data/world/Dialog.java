package data.world;

import java.util.Vector;

import entities.GameColor;
import interfaces.WriteCode;
import logic.Core;
import logic.MaxIndex;


public class Dialog extends ColorableEntity implements WriteCode {

	/** Dialog ID */
	private String						ID;
	/** Speaker ID */
	private String						speaker;
	/** Show head */
	private boolean						showHead;
	/** Name of the file for the face */
	private String						faceTexture;
	/** Name of the file for the voice */
	private String						voice;
	/** Text ID */
	private String						text;
	/** Color for the text */
	private GameColor					textColor;
	/** Has been player once */
	private boolean						playedOnce;
	/** Dialogs defined in the map. */
	public static final Vector <Dialog>	dialogs	= new Vector<> ();

	/**
	 * Create a new dialog
	 *
	 * @param ID Dialog ID
	 * @param speaker Speaker ID
	 * @param faceTexture Name of the file for the face
	 * @param voice Name of the file for the voice
	 * @param text Text ID
	 * @param textColor Color for the text
	 */
	public Dialog (String ID, String speaker, boolean showHead, String faceTexture, String voice, String text, GameColor textColor) {
		this (ID, speaker, showHead, faceTexture, voice, text, textColor, false);
	}

	/**
	 * Create a new dialog
	 *
	 * @param ID Dialog ID
	 * @param speaker Speaker ID
	 * @param faceTexture Name of the file for the face
	 * @param voice Name of the file for the voice
	 * @param text Text ID
	 * @param textColor Color for the text
	 * @param playedOnce Has been played once
	 */
	public Dialog (String ID, String speaker, boolean showHead, String faceTexture, String voice, String text, GameColor textColor, boolean playedOnce) {
		super (ID);
		setSpeaker (speaker);
		setShowHead (showHead);
		setFaceTexture (faceTexture);
		setVoice (voice);
		setText (text);
		setColor (textColor);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Dialog ID
	 * @param speaker Speaker ID
	 * @param showHead Show the speaker head
	 * @param faceTexture Name of the file for the face
	 * @param voice Name of the file for the voice
	 * @param text Text ID
	 * @param textColor Color for the text
	 */
	public void setValues (String ID, String speaker, boolean showHead, String faceTexture, String voice, String text, GameColor textColor) {
		setValues (ID, speaker, showHead, faceTexture, voice, text, textColor, false);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Dialog ID
	 * @param speaker Speaker ID
	 * @param showHead Show the speaker head
	 * @param faceTexture Name of the file for the face
	 * @param voice Name of the file for the voice
	 * @param text Text ID
	 * @param textColor Color for the text
	 * @param playedOnce Has been played once
	 */
	public void setValues (String ID, String speaker, boolean showHead, String faceTexture, String voice, String text, GameColor textColor, boolean playedOnce) {
		setID (ID);
		setSpeaker (speaker);
		setShowHead (showHead);
		setFaceTexture (faceTexture);
		setVoice (voice);
		setText (text);
		setColor (textColor);
		setPlayedOnce (playedOnce);
	}

	@Override
	public String getID () {
		return ID;
	}

	@Override
	public void setID (String iD) {
		ID = iD;
	}

	public String getSpeaker () {
		return speaker;
	}

	public void setSpeaker (String speaker) {
		this.speaker = speaker;
	}

	public boolean isShowHead () {
		return showHead;
	}

	public void setShowHead (boolean showHead) {
		this.showHead = showHead;
	}

	public String getFaceTexture () {
		return faceTexture;
	}

	public void setFaceTexture (String faceTexture) {
		this.faceTexture = faceTexture;
	}

	public String getVoice () {
		return voice;
	}

	public void setVoice (String voice) {
		this.voice = voice;
	}

	public String getText () {
		return text;
	}

	public void setText (String text) {
		this.text = text;
	}

	@Override
	public GameColor getColor () {
		return textColor;
	}

	@Override
	public void setColor (GameColor textColor) {
		this.textColor = textColor;
	}

	public boolean isPlayedOnce () {
		return playedOnce;
	}

	public void setPlayedOnce (boolean playedOnce) {
		this.playedOnce = playedOnce;
	}

	@Override
	public Dialog clone () {
		Dialog copy = (Dialog) super.clone ();
		try {
			copy.setID (Core.getCloneID (getID (), dialogs));
		} catch (MaxIndex e) {
			return null;
		}
		copy.voice = null;
		copy.text = null;
		return copy;
	}

	@Override
	public String toString () {
		return ID;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (1000).append ("\n\t\t00000017 Speech Event List - Element\n\t\t{\n\t\t\tName String '").append (getID ())
		        .append ("\n\t\t\tSound FileName String '").append (voice)
		        .append ("\n\t\t\tText Color Colour").append (textColor.toCode (0))
		        .append ("\n\t\t\tFaceTexture String '").append (faceTexture)
		        .append ("\n\t\t\tTalkingHeadLocation Int 1\n\t\t\tHas Been Played Once Bool ").append (toBool (playedOnce))
		        .append ("\n\t\t\tIs Secondary Speech Bool False\n\t\t\tDisplay Time Float 0.000000\n\t\t\tOpen Chat Bar Bool True\n\t\t\tOpen Talking Head Bool ").append (toBool (showHead))
		        .append ("\n\t\t\tHas Text Bool True\n\t\t\tUse Sound File Length Bool True\n\t\t\tAlways Open Speech Event Bar Bool True\n\t\t\tValid Text StringID Bool True\n\t\t\tTextStringID String '").append (text)
		        .append ("\n\t\t\tValid Speaker ID Bool True\n\t\t\tSpeakerID String '").append (speaker)
		        .append ("'\n\t\t}");
	}

}
