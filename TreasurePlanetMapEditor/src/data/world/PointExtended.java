package data.world;

public class PointExtended extends Point3D {

	/** Point magnitude */
	private float		magnitude;
	/** LookAt Vector length */
	private float		length;
	/** Point orientation */
	private Orientation	orientation;

	/**
	 * Create a new PointExtended
	 *
	 * @param x X position
	 * @param y Y position
	 * @param z Z position
	 * @param magnitude Point magnitude
	 * @param length LookAt Vector length
	 * @param orientation Point orientation
	 */
	public PointExtended (float x, float y, float z, float magnitude, float length, Orientation orientation) {
		super (x, y, z);
		setMagnitude (magnitude);
		setLength (length);
		setOrientation (orientation);
	}

	/**
	 * Set all values for this object
	 *
	 * @param x X position
	 * @param y Y position
	 * @param z Z position
	 * @param magnitude Point magnitude
	 * @param length LookAt Vector length
	 * @param orientation Point orientation
	 */
	public void setValues (float x, float y, float z, float magnitude, float length, Orientation orientation) {
		setValues (x, y, z);
		setMagnitude (magnitude);
		setLength (length);
		setOrientation (orientation);
	}

	public float getMagnitude () {
		return magnitude;
	}

	public void setMagnitude (float magnitude) {
		this.magnitude = magnitude;
	}

	public float getLength () {
		return length;
	}

	public void setLength (float length) {
		this.length = length;
	}

	public Orientation getOrientation () {
		return orientation;
	}

	public void setOrientation (Orientation orientation) {
		this.orientation = orientation;
	}

	@Override
	public String toString () {
		return super.toString () + magnitude;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (500).append ("\n\t\t\t00000009 World Points - Element\n\t\t\t{\n\t\t\t\tWorld Point Magnitude Float ").append (magnitude)
		        .append ("\n\t\t\t\t00000005 World Point Basis\n\t\t\t\t{\n\t\t\t\t\tPosition Vector3").append (super.toCode (indentation))
		        .append ("\n\t\t\t\t\tLookAt Vector Length Float ").append (length)
		        .append ("\n\t\t\t\t\tOrientation - Cross Vector3").append (orientation.toCodeLine (0))
		        .append ("\n\t\t\t\t\tOrientation - Forward Vector3").append (orientation.toCodeLine (1))
		        .append ("\n\t\t\t\t\tOrientation - Up Vector3").append (orientation.toCodeLine (2))
		        .append ("\n\t\t\t\t}\n\t\t\t}");
	}

	@Override
	public PointExtended clone () {
		PointExtended copy = (PointExtended) super.clone ();
		copy.orientation = orientation.clone ();
		return copy;
	}

	@Override
	public int compareTo (Point2D o) {
		if (o instanceof Point3D == false) {
			return -1; // A 3D point is smaller than an extended one
		}

		int c = super.compareTo (o);
		if (c != 0) {
			return c;
		}

		PointExtended o2 = (PointExtended) o;
		if (magnitude != o2.magnitude) {
			return (magnitude < o2.magnitude ? -1 : 1);
		}
		if (length != o2.length) {
			return (length < o2.length ? -1 : 1);
		}
		return 0;
	}

}
