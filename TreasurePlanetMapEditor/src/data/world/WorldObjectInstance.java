package data.world;

import java.util.Vector;

import data.worldObjects.WorldObject;
import interfaces.Colorable;
import interfaces.WriteCode;


public class WorldObjectInstance extends Entity implements WriteCode {

	/** DUMMY WorldObject */
	public static final WorldObjectInstance	NONE				= new WorldObjectInstance (null, -1, null, null, null, null);

	/** Fixed String 1 */
	private static final String				defaultPosition		= "( 0.000000 0.000000 0.000000 )";
	/** Fixed String 2 */
	private static final String				defaultOrientation	= "( 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000 )";
	/** Fixed String 3 */
	private static final String				lastPart			= "\n\t\t# AIEntity\n\t\tType String ''"
	                                                              + "\n\t\t# RenderEntity\n\t\tType String ''\n\t\t# PhysicsEntity\n\t\tType String ''"
	                                                              + "\n\t\t# CollisionEntity\n\t\tType String ''\n\t\t# CustomInfoEntity\n\t\tType String ''\n\t}";


	/** ID number */
	private int											numID;
	/** Object type */
	private WorldObject									worldObject;
	/** Position */
	private Point3D										position;
	/** Orientation */
	private Orientation									orientation;
	/** Owner */
	private Colorable									owner;

	/** Ships objects defined in the map. */
	public static final Vector <WorldObjectInstance>	ships		= new Vector<> ();

	/** Asteroid objects defined in the map. */
	public static final Vector <WorldObjectInstance>	asteroids	= new Vector<> ();

	/** Nebula objects defined in the map. */
	public static final Vector <WorldObjectInstance>	nebulas		= new Vector<> ();

	/** Etherium objects defined in the map. */
	public static final Vector <WorldObjectInstance>	etherium	= new Vector<> ();

	/** World objects defined in the map. */
	public static final Vector <WorldObjectInstance>	objects		= new Vector<> ();

	/**
	 * Create a new WorldObject
	 *
	 * @param ID Object ID.
	 * @param numID ID Number
	 * @param type Type
	 * @param position Position
	 * @param orientation Orienation
	 * @param owner Owner
	 */
	public WorldObjectInstance (String ID, int numID, WorldObject worldObject, Point3D position, Orientation orientation, Colorable owner) {
		super (ID);
		setNumID (numID);
		setWorldObject (worldObject);
		setPosition (position);
		setOrientation (orientation);
		setOwner (owner);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Object ID
	 * @param numID ID Number
	 * @param type Type
	 * @param position Position
	 * @param orientation Orienation
	 * @param owner Owner
	 */
	public void setValues (String ID, int numID, WorldObject worldObject, Point3D position, Orientation orientation, Colorable owner) {
		setID (ID);
		setNumID (numID);
		setWorldObject (worldObject);
		setPosition (position);
		setOrientation (orientation);
		setOwner (owner);
	}

	public int getNumID () {
		return numID;
	}

	public void setNumID (int numID) {
		this.numID = numID;
	}

	public WorldObject getWorldObject () {
		return worldObject;
	}

	public void setWorldObject (WorldObject worldObject) {
		this.worldObject = worldObject;
	}

	public Point3D getPosition () {
		return position;
	}

	public void setPosition (Point3D position) {
		this.position = position;
	}

	public Orientation getOrientation () {
		return orientation;
	}

	public void setOrientation (Orientation orientation) {
		this.orientation = orientation;
	}

	public Colorable getOwner () {
		return owner;
	}

	public void setOwner (Colorable owner) {
		this.owner = owner;
	}

	@Override
	public WorldObjectInstance clone () {
		WorldObjectInstance copy = (WorldObjectInstance) super.clone ();
		copy.position = position.clone ();
		copy.orientation = orientation.clone ();
		return copy;
	}

	@Override
	public String toString () {
		return "" + getID ();
	}

	@Override
	public int compareTo (Entity o) {
		if (equals (o)) {
			return 0;
		}
		if (o instanceof WorldObjectInstance == false) {
			return 1;
		}
		return Integer.valueOf (numID).compareTo (Integer.valueOf (((WorldObjectInstance) o).numID));
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (400).append ("\n\tID Int ").append (numID)
		        .append ("\n\tType String '").append (worldObject.ID)
		        .append ("'\n\t00000014 State\n\t{\n\t\tHasState Bool False").append ("\n\t\tPosition Vector3").append ((position != null ? position : defaultPosition))
		        .append ("\n\t\tOrientation Matrix33").append ((orientation != null ? orientation : defaultOrientation))
		        .append ("\n\t\tPlayerIndex Int ").append ((owner != null ? Player.players.indexOf (owner) : "-1"))
		        .append (lastPart);
	}

}
