package data.world;

import java.util.Vector;

import interfaces.WriteCode;


public class Timer extends Entity implements WriteCode {

	/** Timer value */
	private double						time;
	/** Timer state */
	private boolean						state;
	/** Timers defined in the map. */
	public static final Vector <Timer>	timers	= new Vector<> ();

	/**
	 * Create a new timer
	 *
	 * @param ID Timer ID
	 * @param value Timer value
	 */
	public Timer (String ID, float time) {
		this (ID, time, false);
	}

	/**
	 * Create a new timer
	 *
	 * @param ID Timer ID
	 * @param value Timer value
	 * @param state Timer state
	 */
	public Timer (String ID, double time, boolean state) {
		super (ID);
		setTime (time);
		setState (state);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Timer ID
	 * @param value Timer value
	 */
	public void setValues (String ID, double time, boolean state) {
		setID (ID);
		setTime (time);
		setState (state);
	}

	public double getTime () {
		return time;
	}

	public void setTime (double time) {
		this.time = Math.rint (time * 1000000) / 1000000;
	}

	public boolean getState () {
		return state;
	}

	public void setState (boolean state) {
		this.state = state;
	}

	@Override
	public String toString () {
		return getID () + "   (" + time + ")" + (state ? " [Run]" : "");
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (200)
		        .append ("\n\t\t00000006 Timer List - Element\n\t\t{\n\t\t\tTimer Name String '").append (getID ())
		        .append ("'\n\t\t\tTimer Status Bool ").append (toBool (state))
		        .append ("\n\t\t\t00000001 Timer Value Chunk\n\t\t\t{\n\t\t\t\tStartTime Double ").append (0)
		        .append ("\n\t\t\t}\n\t\t}");
	}

}
