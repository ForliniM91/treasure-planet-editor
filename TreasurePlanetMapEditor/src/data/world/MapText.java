package data.world;

import interfaces.WriteCode;


public class MapText extends Entity implements WriteCode {

	/** Text ID */
	private String	text;
	/** Position on the starmap */
	private Point3D	position;
	/** Visible */
	private boolean	visible;

	/**
	 * Create a new MapText
	 *
	 * @param ID MapText ID
	 * @param text Text ID
	 * @param position Position on the starmap
	 * @param visible Visible
	 */
	public MapText (String ID, String text, Point3D position, boolean visible) {
		super (ID);
		setText (text);
		setPosition (position);
		setVisible (visible);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID MapText ID
	 * @param text Text ID
	 * @param position Position on the starmap
	 * @param visible Visible
	 */
	public void setValues (String ID, String text, Point3D position, boolean visible) {
		setID (ID);
		setText (text);
		setPosition (position);
		setVisible (visible);
	}

	public String getText () {
		return text;
	}

	public void setText (String text) {
		this.text = text;
	}

	public Point3D getPosition () {
		return position;
	}

	public void setPosition (Point3D position) {
		this.position = position;
	}

	public boolean getVisible () {
		return visible;
	}

	public void setVisible (boolean visible) {
		this.visible = visible;
	}

	@Override
	public MapText clone () {
		MapText copy = (MapText) super.clone ();
		copy.position = position.clone ();
		return copy;
	}

	@Override
	public String toString () {
		return text;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (300).append ("\n\t\t\t00000004 MapText Point Info - Element\n\t\t\t{\n\t\t\t\tName String '").append (getID ())
		        .append ("'\n\t\t\t\tDisplayedText String '").append (text)
		        .append ("'\n\t\t\t\tPosition Vector3").append (position.toCode (indentation))
		        .append ("\n\t\t\t\tVisible Bool ").append (toBool (visible))
		        .append ("\n\t\t\t}");
	}

}
