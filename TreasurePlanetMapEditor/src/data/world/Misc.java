package data.world;

public class Misc {


	/* FIXED */ private static final String	worldNameDescr		= "\n\tUse Custom World Name Bool False\n\tCustom World Name String ''"
	                                                              + "\n\tUse Custom World Description Bool False\n\tCustom World Description String ''";
	/* FIXED */ private static final String	dataNebula			= "\n\t\tDATA_NEBULA_CAMERA_EFFECT Int 0\n\t\tDATA_NEXT_NEBULA_CAMERA_EFFECT Int 0"
	                                                              + "\n\t\t00000001 DATA_NEBULA_CAMERA_EFFECT_FADE_IN_TIMER\n\t\t{\n\t\t\tStartTime Double 0\n\t\t}"
	                                                              + "\n\t\t00000001 DATA_NEBULA_CAMERA_EFFECT_SPIN_TIMER\n\t\t{\n\t\t\tStartTime Double 0\n\t\t}";
	/* FIXED */ private static final String	gameTime			= "\n00000008 Game\n{\n\t00000002 Time\n\t{";

	/* SAVE */ public static byte			tick				= 0;
	/* SAVE */ public static float			time				= 0;
	/* SAVE */ public static byte			frame				= -1;
	/* SAVE */ public static boolean		paused				= false;
	/* SAVE */ public static byte			activePlayerIndex	= -1;
	/* SAVE */ public static int			numEffectEventInfoChunks;
	/* SAVE */ public static boolean		initialized			= false;
	/* SAVE */ public static short			winningTeam			= -1;
	/* SAVE */ public static boolean		readAI				= false;

	/* FIXED */ private static final String	LASTPART			=
	                                                 "00000304 GameImpl"
	                                                              + "\n{"
	                                                              + "\tSelectedWorldObjectID Int -1"
	                                                              + "\n\t00000006 GameStats"
	                                                              + "\n\t{"
	                                                              + "\n\t\tPlayerKillInfo - Size Int 0"
	                                                              + "\n\t\tPlayerDockInfo - Size Int 0"
	                                                              + "\n\t\tPlayerLifeBoatSavedInfo - Size Int 0"
	                                                              + "\n\t\tFleetSizeInfo - Size Int 0"
	                                                              + "\n\t\tPlayerDamageInfo - Size Int 0"
	                                                              + "\n\t\tSinglePlayer Points Int 0"
	                                                              + "\n\t}"
	                                                              + "\n\tSinglePlayerVictoryPoints Int 0"
	                                                              + "\n\tSinglePlayerCrewList - Size Int 0"
	                                                              + "\n\tSinglePlayerArmsList - Size Int 0"
	                                                              + "\n\t00000014 HUD"
	                                                              + "\n\t{"
	                                                              + "\n\t\tAre NIS Bars Open Bool False "
	                                                              + "\n\t\tSkip Current Speech Event Bool False "
	                                                              + "\n\t\t00000001 Active Speech Event Timer"
	                                                              + "\n\t\t{"
	                                                              + "\n\t\t\tStartTime Double 0"
	                                                              + "\n\t\t}"
	                                                              + "\n\t\tSpeech Event Active Bool False "
	                                                              + "\n\t\tSpeech Event Queue - Size Int 0"
	                                                              + "\n\t\tArtificial Pause Between Speech Events Active Bool False "
	                                                              + "\n\t\t00000001 Artificial Pause Between Speech Events Timer"
	                                                              + "\n\t\t{"
	                                                              + "\n\t\t\tStartTime Double 0"
	                                                              + "\n\t\t}"
	                                                              + "\n\t\tRequest To Close NIS Bars Bool False "
	                                                              + "\n\t}"
	                                                              + "\n\tGame Complete Bool False "
	                                                              + "\n\tCustom Game Complete Bool False "
	                                                              + "\n\t00000001 Game Complete Timer"
	                                                              + "\n\t{"
	                                                              + "\n\t\tStartTime Double 0"
	                                                              + "\n\t}"
	                                                              + "\n\tCustom Game Complete String String ''"
	                                                              + "\n\tGame Winner Players - Size Int 0"
	                                                              + "\n\tUser Interface Disabled Bool False "
	                                                              + "\n\tIs Campaign Game Bool False "
	                                                              + "\n\tShow Stats Screen Bool True"
	                                                              + "\n\tGoto Next Level Enabled Bool False "
	                                                              + "\n\tNext Level World Name String ''"
	                                                              + "\n\t00000001 Next Level Display Timer"
	                                                              + "\n\t{"
	                                                              + "\n\t\tStartTime Double 0"
	                                                              + "\n\t}"
	                                                              + "\n\tAlready Created Game Bool False "
	                                                              + "\n\t00000218 Game Camera"
	                                                              + "\n\t{"
	                                                              + "\n\t\t00000028 FlyCameraController"
	                                                              + "\n\t\t{"
	                                                              + "\n\t\t\t00000015 Camera"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tAspectRatio Float 1.000000"
	                                                              + "\n\t\t\t\tFieldOfView Float 0.785398"
	                                                              + "\n\t\t\t\tNearPlane Float 0.500000"
	                                                              + "\n\t\t\t\tFarPlane Float 3000.000000"
	                                                              + "\n\t\t\t\tUsingResolutionBasedAspectRatio Bool True"
	                                                              + "\n\t\t\t\tOrthographic Bool False "
	                                                              + "\n\t\t\t\tOrthographicZoom Float 1.000000"
	                                                              + "\n\t\t\t\t00000005 LocalBasis"
	                                                              + "\n\t\t\t\t{"
	                                                              + "\n\t\t\t\t\tPosition Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tLookAt Vector Length Float 1.000000"
	                                                              + "\n\t\t\t\t\tOrientation - Cross Vector3( 1.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\\tt\tOrientation - Forward Vector3( 0.000000, 1.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tOrientation - Up Vector3( 0.000000, 0.000000, 1.000000 )"
	                                                              + "\n\t\t\t\t}"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000002 TargetInfo"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tTargetID Int -1"
	                                                              + "\n\t\t\t\tTargetPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000001 GameTimeUpdateTimer"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tStartTime Double 0"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\tNewFocusData Bool True"
	                                                              + "\n\t\t}"
	                                                              + "\n\t\t00000046 SplineCameraController"
	                                                              + "\n\t\t{"
	                                                              + "\n\t\t\t00000015 Camera"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tAspectRatio Float 1.000000"
	                                                              + "\n\t\t\t\tFieldOfView Float 0.785398"
	                                                              + "\n\t\t\t\tNearPlane Float 0.500000"
	                                                              + "\n\t\t\t\tFarPlane Float 3000.000000"
	                                                              + "\n\t\t\t\tUsingResolutionBasedAspectRatio Bool True"
	                                                              + "\n\t\t\t\tOrthographic Bool False "
	                                                              + "\n\t\t\t\tOrthographicZoom Float 1.000000"
	                                                              + "\n\t\t\t\t00000005 LocalBasis"
	                                                              + "\n\t\t\t\t{"
	                                                              + "\n\t\t\t\t\tPosition Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tLookAt Vector Length Float 10.000000"
	                                                              + "\n\t\t\t\t\tOrientation - Cross Vector3( -1.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tOrientation - Forward Vector3( 0.000000, -1.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tOrientation - Up Vector3( 0.000000, 0.000000, 1.000000 )"
	                                                              + "\n\t\t\t\t}"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000002 TargetInfo"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tTargetID Int -1"
	                                                              + "\n\t\t\t\tTargetPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000001 GameTimeUpdateTimer"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tStartTime Double 0"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\tNewFocusData Bool True"
	                                                              + "\n\t\t\t00000010 PositionInterpolater"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tPosition Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\tStartPointControlPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\tEndPointControlPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\tControlIndex Int 0"
	                                                              + "\n\t\t\t\tAcceleration Float 10.000000"
	                                                              + "\n\t\t\t\tMaxSpeed Float 30.000000"
	                                                              + "\n\t\t\t\tSpeed Float 0.000000"
	                                                              + "\n\t\t\t\tDistance Float 0.000000"
	                                                              + "\n\t\t\t\tPercentage Float -431602080.000000"
	                                                              + "\n\t\t\t\tControlPointsInfo - Size Int 0"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\tPrevTargetPoint Vector3( 0.000000, -10.000000, 0.000000 )"
	                                                              + "\n\t\t\tLockPosition Bool False "
	                                                              + "\n\t\t\tLockOrientation Bool False "
	                                                              + "\n\t\t\tForceOrientation Bool False "
	                                                              + "\n\t\t\tPercentIncrement Float -431602080.000000"
	                                                              + "\n\t\t}"
	                                                              + "\n\t\t00000042 SwoopCameraController"
	                                                              + "\n\t\t{"
	                                                              + "\n\t\t\t00000015 Camera"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tAspectRatio Float 1.000000"
	                                                              + "\n\t\t\t\tFieldOfView Float 0.785398"
	                                                              + "\n\t\t\t\tNearPlane Float 0.500000"
	                                                              + "\n\t\t\t\tFarPlane Float 3000.000000"
	                                                              + "\n\t\t\t\tUsingResolutionBasedAspectRatio Bool True"
	                                                              + "\n\t\t\t\tOrthographic Bool False "
	                                                              + "\n\t\t\t\tOrthographicZoom Float 1.000000"
	                                                              + "\n\t\t\t\t00000005 LocalBasis"
	                                                              + "\n\t\t\t\t{"
	                                                              + "\n\t\t\t\t\tPosition Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tLookAt Vector Length Float 10.000000"
	                                                              + "\n\t\t\t\t\tOrientation - Cross Vector3( -1.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tOrientation - Forward Vector3( 0.000000, -1.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tOrientation - Up Vector3( 0.000000, 0.000000, 1.000000 )"
	                                                              + "\n\t\t\t\t}"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000002 TargetInfo"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tTargetID Int -1"
	                                                              + "\n\t\t\t\tTargetPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000001 GameTimeUpdateTimer"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tStartTime Double 0"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\tNewFocusData Bool True"
	                                                              + "\n\t\t\tDistanceToTarget Float 20.000000"
	                                                              + "\n\t\t\tDesiredToTarget Float 20.000000"
	                                                              + "\n\t\t\tHorizontalRotation Float 0.000000"
	                                                              + "\n\t\t\tMaxHorizontalRotation Float 0.000000"
	                                                              + "\n\t\t\tMaxDistanceToTarget Float 300.000000"
	                                                              + "\n\t\t\tMinHorizontalRotation Float 0.000000"
	                                                              + "\n\t\t\tMinDistanceToTarget Float 2.000000"
	                                                              + "\n\t\t\tRestrictMinHorizontal Bool False "
	                                                              + "\n\t\t\tRestrictMinDistance Bool True"
	                                                              + "\n\t\t\tRestrictMaxHorizontal Bool False "
	                                                              + "\n\t\t\tRestrictMaxDistance Bool True"
	                                                              + "\n\t\t\tPercentIncrement Float -431602080.000000"
	                                                              + "\n\t\t\tTargetPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\tJumpToFocusPoint Bool False "
	                                                              + "\n\t\t}"
	                                                              + "\n\t\t00000046 TransitionCameraController"
	                                                              + "\n\t\t{"
	                                                              + "\n\t\t\t00000015 Camera"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tAspectRatio Float 1.000000"
	                                                              + "\n\t\t\t\tFieldOfView Float 0.785398"
	                                                              + "\n\t\t\t\tNearPlane Float 0.500000"
	                                                              + "\n\t\t\t\tFarPlane Float 3000.000000"
	                                                              + "\n\t\t\t\tUsingResolutionBasedAspectRatio Bool True"
	                                                              + "\n\t\t\t\tOrthographic Bool False "
	                                                              + "\n\t\t\t\tOrthographicZoom Float 1.000000"
	                                                              + "\n\t\t\t\t00000005 LocalBasis"
	                                                              + "\n\t\t\t\t{"
	                                                              + "\n\t\t\t\t\tPosition Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tLookAt Vector Length Float 10.000000"
	                                                              + "\n\t\t\t\t\tOrientation - Cross Vector3( -1.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tOrientation - Forward Vector3( 0.000000, -1.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tOrientation - Up Vector3( 0.000000, 0.000000, 1.000000 )"
	                                                              + "\n\t\t\t\t}"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000002 TargetInfo"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tTargetID Int -1"
	                                                              + "\n\t\t\t\tTargetPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000001 GameTimeUpdateTimer"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\rStartTime Double 0"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\tNewFocusData Bool True"
	                                                              + "\n\t\t\t00000010 PositionInterpolater"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tPosition Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\tStartPointControlPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\tEndPointControlPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\tControlIndex Int 0"
	                                                              + "\n\t\t\t\tAcceleration Float 10.000000"
	                                                              + "\n\t\t\t\tMaxSpeed Float 30.000000"
	                                                              + "\n\t\t\t\tSpeed Float 0.000000"
	                                                              + "\n\t\t\t\tDistance Float 0.000000"
	                                                              + "\n\t\t\t\tPercentage Float -431602080.000000"
	                                                              + "\n\t\t\t\tControlPointsInfo - Size Int 0"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\tPrevTargetPoint Vector3( 0.000000, -10.000000, 0.000000 )"
	                                                              + "\n\t\t\tLockPosition Bool False "
	                                                              + "\n\t\t\tLockOrientation Bool False "
	                                                              + "\n\t\t\tForceOrientation Bool False "
	                                                              + "\n\t\t\tPercentIncrement Float -431602080.000000"
	                                                              + "\n\t\t}"
	                                                              + "\n\t\t00000034 AttachCameraController"
	                                                              + "\n\t\t{"
	                                                              + "\n\t\t\t00000015 Camera"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tAspectRatio Float 1.000000"
	                                                              + "\n\t\t\t\tFieldOfView Float 0.785398"
	                                                              + "\n\t\t\t\tNearPlane Float 0.500000"
	                                                              + "\n\t\t\t\tFarPlane Float 3000.000000"
	                                                              + "\n\t\t\t\tUsingResolutionBasedAspectRatio Bool True"
	                                                              + "\n\t\t\t\tOrthographic Bool False "
	                                                              + "\n\t\t\t\tOrthographicZoom Float 1.000000"
	                                                              + "\n\t\t\t\t00000005 LocalBasis"
	                                                              + "\n\t\t\t\t{"
	                                                              + "\n\t\t\t\t\tPosition Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tLookAt Vector Length Float 10.000000"
	                                                              + "\n\t\t\t\t\tOrientation - Cross Vector3( -1.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tOrientation - Forward Vector3( 0.000000, -1.000000, 0.000000 )"
	                                                              + "\n\t\t\t\t\tOrientation - Up Vector3( 0.000000, 0.000000, 1.000000 )"
	                                                              + "\n\t\t\t\t}"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000002 TargetInfo"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tTargetID Int -1"
	                                                              + "\n\t\t\t\tTargetPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\t00000001 GameTimeUpdateTimer"
	                                                              + "\n\t\t\t{"
	                                                              + "\n\t\t\t\tStartTime Double 0"
	                                                              + "\n\t\t\t}"
	                                                              + "\n\t\t\tNewFocusData Bool True"
	                                                              + "\n\t\t\tAttachedObjectID Int -1"
	                                                              + "\n\t\t\tDistance Float 1.000000"
	                                                              + "\n\t\t\tAngleYZ Float 0.000000"
	                                                              + "\n\t\t\tAngleXY Float 0.000000"
	                                                              + "\n\t\t\tLockOrientation Bool False "
	                                                              + "\n\t\t\tPercentIncrement Float -431602080.000000"
	                                                              + "\n\t\t}"
	                                                              + "\n\t\tCameraMode Int 0"
	                                                              + "\n\t\tTargetModeForTransition Int -842150451"
	                                                              + "\n\t\tInTransitionMode Bool False "
	                                                              + "\n\t\tTurnOffNISWhenDoneTransition Bool True"
	                                                              + "\n\t\tTransitionStartPoint Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\tLastTargetPosition Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t\tLastCameraPosition Vector3( 0.000000, 0.000000, 0.000000 )"
	                                                              + "\n\t}"
	                                                              + "\n\t00000018 SoundPlayer"
	                                                              + "\n\t{"
	                                                              + "\n\t\t00000006 CurrentMix"
	                                                              + "\n\t\t{"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t}"
	                                                              + "\n\t\t00000006 UserMix"
	                                                              + "\n\t\t{"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t\tPercentageVolume Float 1.000000"
	                                                              + "\n\t\t}"
	                                                              + "\n\t}"
	                                                              + "\n\t00000001 MusicPlayer"
	                                                              + "\n\t{"
	                                                              + "\n\t\tIs Playing Bool False "
	                                                              + "\n\t}"
	                                                              + "\n\t00000005 TPSound"
	                                                              + "\n\t{"
	                                                              + "\n\t\tMusicVolume Float 1.000000"
	                                                              + "\n\t\tSpeechOcurring Bool False "
	                                                              + "\n\t\tCreatedSoundData - Size Int 0"
	                                                              + "\n\t\tAbyss Volume Float 0.000000"
	                                                              + "\n\t\tInside Abyss Bool False "
	                                                              + "\n\t}"
	                                                              + "\n\tRadar Active Bool True"
	                                                              + "\n\tCrew Speech Silent State Bool False "
	                                                              + "\n}";



	private Misc () {}

	// public static String multiplayer () {
	// return "\n\tIsMultiplayerMap Bool " + toBool (!Settings.isCampaign);
	// }
	//
	// public static String assempleFleed () {
	// return "\n\tMustAssembleFleet Bool " + toBool (Settings.isSkirmish);
	// }
	//
	// public static String description () {
	// return "\n\tWorld Description String '" + Settings.mapDesc + '\'';
	// }
	//
	// public static String nameID () {
	// return "\n\tWorldNameID String '" + Settings.mapName + '\'';
	// }
	//
	// public static String multiplayer2 () {
	// return "\n\tIsCampaign Bool " + toBool (Settings.isCampaign);
	// }
	//
	// public static String worldNameDescr () {
	// return worldNameDescr;
	// }
	//
	// public static String gameTime () {
	// return gameTime + "\n\t\t" + "Game Tick Int " + tick + "\n\t\tGame Time Double " + time
	// + "\n\t}\n\tFrame Int " + frame + "\n\tPaused Bool " + toBool (paused) + "\n\tActivePlayerIndex Int " + activePlayerIndex;
	// }
	//
	// public static String worldName () {
	// return "\n\t\tWorldName String '" + Settings.mapID
	// + "'\n\tWorld Size - Min Vector3" + new Point3D (-Settings.width / 2, -Settings.depth / 2, -Settings.height / 2).toCode (int indentation)
	// + "\n\tWorld Size - Max Vector3" + new Point3D (Settings.width / 2, Settings.depth / 2, Settings.height / 2).toCode (int indentation);
	// }
	//
	// public static String description2 () {
	// return "\n\tWorld Description Sting ID String '" + Settings.mapDesc + '\'';
	// }
	//
	// public static String nameID2 () {
	// return "\n\tWorld Name Sting ID String '" + Settings.mapName + '\'';
	// }
	//
	// public static String numEffectEventInfoChunks () {
	// return "\n\t\t00000001 Effect Event Keeper\n\t\t{\n\t\t\tNumEffectEventInfoChunks Int " + numEffectEventInfoChunks + "\n\t\t}";
	// }
	//
	// public static String skybox () {
	// return "\n\t\tSkybox mesh name String '" + Settings.background
	// + "'\n\t\tAmbient Light Colour" + new GameColor (Settings.ambientColor)
	// + "\n\t\tVector for roof light orientation Vector3" + new Point3D (Settings.x, Settings.y, Settings.z).toCode (int indentation)
	// + "\nHemispherical floor light color Colour" + new GameColor (Settings.roofColor)
	// + "\nHemispherical roof light color Colour" + new GameColor (Settings.floorColor)
	// + "\n\t\tWorld Initialized State Bool " + toBool (initialized)
	// + "\n\t\tWorld Buffer Size Float " + Settings.worldBufferSize;
	// }
	//
	// public static String winningTeam () {
	// return "\n\t\tWinning Team Int " + winningTeam;
	// }
	//
	// public static String worldMap () {
	// return "\n\t\t00000001 World Map\n\t\t{\n\t\t\tBackdrop Texture Name String '" + Settings.starMap + "'\n\t\t}";
	// }
	//
	// public static String assembleFleet2 () {
	// return "\n\t\tCan Assemble Fleets Bool " + toBool (Settings.isSkirmish);
	// }
	//
	// public static String readAI () {
	// return "\n\t\tREADAIENTITYCOUNTS Bool " + toBool (readAI);
	// }
	//
	// public static String endMovie () {
	// return "\n\t\tPlayEndMovie Bool " + toBool (Settings.isLastMission);
	// }
	//
	// public static String allianceChangeAllowed () {
	// return "\n\t\tIs Alliance Change Allowed Bool " + toBool (Settings.isAllianceChangeAllowed);
	// }
	//
	// public static String islandsMakeSounds () {
	// return "\n\t\tIslands Make Sounds Bool " + toBool (Settings.isIslandsMakeSounds);
	// }
	//
	// public static String dataNebula () {
	// return dataNebula;
	// }
	//
	// public static String lastPart () {
	// return LASTPART;
	// }

}
