package data.world;

import java.util.Vector;

import interfaces.WriteCode;
import logic.Core;
import logic.MaxIndex;


public class Trigger extends Entity implements WriteCode {

	private boolean							isActive;
	private boolean							runOnce;
	private Vector <Condition>				conditions;
	private Vector <Action>					actions;
	/** Triggers defined in the map. */
	public static final Vector <Trigger>	triggers	= new Vector<> ();

	/*
	 * public Trigger() {
	 * this(MainClass.NONE, false, true);
	 * }
	 */

	public Trigger (String ID, boolean isActive, boolean runOnce) {
		this (ID, isActive, runOnce, null, null);
	}

	public Trigger (String ID, boolean isActive, boolean runOnce, Vector <Condition> conditions, Vector <Action> actions) {
		super (ID);
		setActive (isActive);
		setRunOnce (runOnce);
		setConditions (conditions);
		setActions (actions);
	}


	public void setValues (String ID, boolean isActive, boolean runOnce, Vector <Condition> conditions, Vector <Action> actions) {
		setID (ID);
		setActive (isActive);
		setRunOnce (runOnce);
		setConditions (conditions);
		setActions (actions);
	}

	public boolean isActive () {
		return isActive;
	}

	public void setActive (boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isRunOnce () {
		return runOnce;
	}

	public void setRunOnce (boolean runOnce) {
		this.runOnce = runOnce;
	}

	public Vector <Condition> getConditions () {
		return conditions;
	}

	public void setConditions (Vector <Condition> conditions) {
		this.conditions = conditions;
	}

	public Vector <Action> getActions () {
		return actions;
	}

	public void setActions (Vector <Action> actions) {
		this.actions = actions;
	}

	@Override
	public Trigger clone () {
		Vector <Condition> conditions2 = new Vector<> ();
		Vector <Action> actions2 = new Vector<> ();
		for (int i = 0; i < conditions.size (); i++) {
			conditions2.add (conditions.get (i).clone ());
		}
		for (int i = 0; i < actions.size (); i++) {
			actions2.add (actions.get (i).clone ());
		}

		Trigger copy = (Trigger) super.clone ();
		try {
			copy.setID (Core.getCloneID (getID (), triggers));
		} catch (MaxIndex e) {
			return null;
		}
		copy.conditions = conditions2;
		copy.actions = actions2;
		return copy;
	}

	@Override
	public String toString () {
		StringBuilder sb = new StringBuilder (100);
		if (isActive) {
			sb.append ("[A] ");
		}
		if (!runOnce) {
			sb.append ("[R] ");
		}
		return sb.append (getID ())
		        .append ("     (Conditions: ").append (conditions.size ())
		        .append ("   ==>   Actions: ").append (actions.size ())
		        .append (')').toString ();
	}

	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (500).append ("\n\t\t\tRule Name String '").append (getID ())
		        .append ("'\n\t\t\tRun Once Bool ").append (toBool (runOnce))
		        .append ("\n\t\t\tIs Active Bool ").append (toBool (isActive))
		        .append ("\n\t\t\tNumConditions Int ").append (conditions.size ());
		for (Condition condition : conditions) {
			sb.append (condition.toCode (indentation));
		}
		sb.append ("\n\t\t\tNumActions Int " + actions.size ());
		for (Action action : actions) {
			sb.append (action.toCode (indentation));
		}
		return sb;
	}

}
