package data.world;

import java.util.Vector;

import interfaces.WriteCode;


public class Flag extends Entity implements WriteCode {

	/** Flag value */
	private boolean						value;
	/** Flags defined in the map. */
	public static final Vector <Flag>	flags	= new Vector<> ();

	/**
	 * Create a new flag
	 *
	 * @param ID Flag ID
	 * @param value Flag value
	 */
	public Flag (String ID, boolean value) {
		super (ID);
		setValue (value);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Flag ID
	 * @param value Flag value
	 */
	public void setValues (String ID, boolean value) {
		setID (ID);
		setValue (value);
	}

	public boolean isValue () {
		return value;
	}

	public void setValue (boolean value) {
		this.value = value;
	}

	@Override
	public String toString () {
		return getID () + (value ? "   [TRUE]" : "   [FALSE]");
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (500).append ("\n\t\t00000002 Flag List - Element\n\t\t{\n\t\t\tFlag Name String '").append (getID ())
		        .append ("'\n\t\t\tFlag Value Bool ").append (toBool (value)).append ("\n\t\t}");
	}

}
