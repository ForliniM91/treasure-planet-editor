package data.world;

import entities.ConditionType;
import entities.Field;
import entities.VarType;
import interfaces.TriggerElement;
import interfaces.WriteCode;


public class Condition implements Comparable <Condition>, TriggerElement <ConditionType>, Cloneable {

	/** The action type */
	private ConditionType	condition;
	/** The fields values */
	private Object[]		values;

	/**
	 * Create a new condition
	 *
	 * @param conditionType Type of the condition
	 * @param fields Values for the fields
	 */
	public Condition (ConditionType condition, Object[] values) {
		setElementType (condition);
		if (values == null) {
			if (condition.getFields ().length != 0) {
				throw new IllegalArgumentException ("Condition creation: the condition require some parameters! Programmer fault!");
			}
		} else if (values.length != condition.getFields ().length) {
			throw new IllegalArgumentException ("Condition creation: the parameter number is wrong! Programmer fault!");
		}
		setValues (values);
	}

	/**
	 * Set all values for this object
	 *
	 * @param conditionType Type of the condition
	 * @param values Values for the fields
	 */
	public void setValues (ConditionType condition, Object[] values) {
		setElementType (condition);
		setValues (values);
	}

	@Override
	public ConditionType getElementType () {
		return condition;
	}

	@Override
	public void setElementType (ConditionType t) {
		condition = t;
	}

	@Override
	public Object[] getValues () {
		return values;
	}

	@Override
	public void setValues (Object[] values) {
		this.values = values;
	}

	@Override
	public String toString () {
		return condition + "     (" + Integer.toHexString (hashCode ()) + ')';
	}

	@Override
	public Condition clone () {
		try {
			return (Condition) super.clone ();
		} catch (CloneNotSupportedException e) {
			throw new InternalError ();
		}
	}

	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (500).append ("\n\t\t\t").append (toChars8 (values.length + 1)).append (" Condition List\n\t\t\t{\n\t\t\t\tType String '").append (condition.getCode ()).append ('\'');
		if (values != null) {
			Field[] fields = condition.getFields ();
			for (int i = 0; i < values.length; i++) {
				if (fields[i].getUsedDomain () == VarType.STRING) {
					sb.append ("\n").append (fields[i].toCode (0)).append (" '").append (values[i]).append ('\'');
				} else {
					sb.append ("\n").append (fields[i].toCode (0)).append (' ').append (values[i]);
				}
			}
		}
		return sb.append ("\n\t\t\t}");
	}

	@Override
	public int compareTo (Condition o) {
		return Integer.valueOf (condition.getIndex ()).compareTo (Integer.valueOf (o.condition.getIndex ()));
	}

}
