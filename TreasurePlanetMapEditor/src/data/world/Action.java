package data.world;

import entities.ActionType;
import entities.Field;
import entities.VarType;
import interfaces.TriggerElement;


public class Action implements Comparable <Action>, TriggerElement <ActionType>, Cloneable {

	/** The action type */
	private ActionType	action;
	/** The fields values */
	private Object[]	values;

	/**
	 * Create a new action
	 *
	 * @param actionType Type of the action
	 * @param fields Values for the fields
	 */
	public Action (ActionType action, Object[] values) {
		setElementType (action);
		if (values == null) {
			if (action.getFields ().length != 0) {
				throw new IllegalArgumentException ("Action creation: the action require some parameters! Programmer fault!");
			}
		} else if (values.length != action.getFields ().length) {
			throw new IllegalArgumentException ("Action creation: the parameter number is wrong! Programmer fault!");
		}
		setValues (values);
	}

	/**
	 * Set all values for this object
	 *
	 * @param conditionType Type of the condition
	 * @param values Values for the fields
	 */
	public void setValues (ActionType action, Object[] values) {
		setElementType (action);
		setValues (values);
	}

	@Override
	public ActionType getElementType () {
		return action;
	}

	@Override
	public void setElementType (ActionType t) {
		action = t;
	}

	@Override
	public Object[] getValues () {
		return values;
	}

	@Override
	public void setValues (Object[] values) {
		this.values = values;
	}

	@Override
	public String toString () {
		return action + "     (" + Integer.toHexString (hashCode ()) + ')';
	}

	@Override
	public Action clone () {
		try {
			return (Action) super.clone ();
		} catch (CloneNotSupportedException e) {
			throw new InternalError ();
		}
	}

	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (500).append ("\n\t\t\t").append (toChars8 (values.length + 1)).append (" Action List\n\t\t\t{\n\t\t\t\tType String '").append (action.getCode ()).append ('\'');
		Field[] fields = action.getFields ();
		for (int i = 0; i < values.length; i++) {
			if (fields[i].getUsedDomain () == VarType.STRING) {
				sb.append ("\n").append (fields[i].toCode (0)).append (" '").append (values[i]).append ('\'');
			} else {
				sb.append ("\n").append (fields[i].toCode (0)).append (' ').append (values[i]);
			}
		}
		return sb.append ("\n\t\t\t}");
	}

	@Override
	public int compareTo (Action o) {
		return Integer.valueOf (action.getIndex ()).compareTo (Integer.valueOf (o.action.getIndex ()));
	}

}
