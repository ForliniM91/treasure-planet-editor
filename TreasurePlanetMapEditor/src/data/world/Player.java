package data.world;

import java.util.Vector;

import entities.FormationType;
import entities.GameColor;
import entities.Race;
import interfaces.WriteCode;
import logic.Core;
import logic.MaxIndex;


public class Player extends ColorableEntity implements WriteCode {

	/** Fixed String 1 */
	private static final String		s1					= "\n\t\tIs Used In Game Bool False\n\t\tMultiplayer Name String ''";
	/** Fixed String 2 */
	private static final String		defaultPosition		= "( 0.000000 0.000000 0.000000 )";
	/** Fixed String 3 */
	private static final String		defaultOrientation	= "( 0.000000 0.000000 0.000000 )";
	/** Fixed String 4 */
	private static final String		lastPart			= "\n\t\t00000017 FleetAI\n\t\t{"
	                                                      + "\n\t\t\t00000001 UPDATETIMER\n\t\t\t{\n\t\t\t\tStartTime Double 0\n\t\t\t}"
	                                                      + "\n\t\t\t00000001 OFFSETTIMER\n\t\t\t{\n\t\t\t\tStartTime Double 0\n\t\t\t}"
	                                                      + "\n\t\t\tOFFSETTIME Float 0.062500\n\t\t\tUPDATETIME Float 0.500000"
	                                                      + "\n\t\t\t00000001 FORMATION\n\t\t\t{\n\t\t\tFORMATIONTYPE String 'None'\n\t\t\t}"
	                                                      + "\n\t\t\tSHIPINFO - Size Int 0\n\t\t\tHOLDFIREACTIVE Bool False"
	                                                      + "\n\t\t\tAITYPE String 'AIFLEET'\n\t\t\t}\n\t\tFlagIndex Int 0\n\t}";

	/** Colors */
	public static final GameColor[]	colors				= {
	                                                        new GameColor (0, 0, 1, 1),
	                                                        new GameColor (1, 0, 0, 1),
	                                                        new GameColor (0, 1, 1, 1),
	                                                        new GameColor (0.501961F, 0, 0, 1),
	                                                        new GameColor (0, 0, 0.501961F, 1),
	                                                        new GameColor (1, 0, 1, 1),
	                                                        new GameColor (0, 0.501961F, 0.501961F, 1),
	                                                        new GameColor (0.874510F, 0.372549F, 0.372549F, 1)
	};


	/** Team */
	private Team						team;
	/** Race */
	private Race						race;
	/** Color */
	private GameColor					color;
	/** Starting formation */
	private FormationType				formation;
	/** Starting position */
	private Point3D						position;
	/** Starting orientation */
	private Point3D						orientation;
	/** Is this player playable by an human player */
	private boolean						playable;
	/** Payers defined in the map. */
	public static final Vector <Player>	players	= new Vector<> ();

	/**
	 * Create a player knowing the ID, team and race.
	 *
	 * @param team Team ID
	 * @param race Race ID
	 * @param playable Is playable?
	 */
	public Player (String ID, Team team, Race race, GameColor color, FormationType formation, boolean playable) {
		this (ID, team, race, color, formation, null, null, playable);
	}

	/**
	 * Create a player knowing the ID, team and race.
	 *
	 * @param ID Player ID
	 * @param team Team ID
	 * @param race Race ID
	 * @param color Player color
	 * @param position Starting position
	 * @param direction Starting orientation
	 * @param playable Is playable?
	 */
	public Player (String ID, Team team, Race race, GameColor color, FormationType formation, Point3D position, Point3D orientation, boolean playable) {
		super (ID);
		setTeam (team);
		setRace (race);
		setColor (color);
		setFormation (formation);
		setPosition (position);
		setOrientation (orientation);
		setPlayable (playable);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID Player ID
	 * @param team Team ID
	 * @param race Race ID
	 * @param color Player color
	 * @param formation Starting formation
	 * @param position Starting position
	 * @param direction Starting orientation
	 * @param playable Is playable?
	 */
	public void setValues (String ID, Team team, Race race, GameColor color, FormationType formation, Point3D position, Point3D orientation, boolean playable) {
		setID (ID);
		setTeam (team);
		setRace (race);
		setColor (color);
		setFormation (formation);
		setPosition (position);
		setOrientation (orientation);
		setPlayable (playable);
	}

	/**
	 * Set all values for this object, except for position and orientation (used for update purpose)
	 *
	 * @param ID Player ID
	 * @param team Team ID
	 * @param race Race ID
	 * @param color Player color
	 * @param formation Starting formation
	 * @param playable Is playable?
	 */
	public void setValues (String ID, Team team, Race race, GameColor color, FormationType formation, boolean playable) {
		setID (ID);
		setTeam (team);
		setRace (race);
		setColor (color);
		setFormation (formation);
		setPlayable (playable);
	}

	/**
	 * Set position and orientation (used for update purpose)
	 *
	 * @param ID Player ID
	 * @param team Team ID
	 * @param race Race ID
	 * @param playable Is playable?
	 */
	public void setValues (Point3D position, Point3D orientation) {
		setPosition (position);
		setOrientation (orientation);
	}

	public Team getTeam () {
		return team;
	}

	public void setTeam (Team team) {
		this.team = team;
	}

	public Race getRace () {
		return race;
	}

	public void setRace (Race race) {
		this.race = race;
	}

	@Override
	public GameColor getColor () {
		return color;
	}

	@Override
	public void setColor (GameColor color) {
		this.color = color;
	}

	public FormationType getFormation () {
		return formation;
	}

	public void setFormation (FormationType formation) {
		this.formation = formation;
	}

	public Point3D getPosition () {
		return position;
	}

	public void setPosition (Point3D position) {
		this.position = position;
	}

	public Point3D getOrientation () {
		return orientation;
	}

	public void setOrientation (Point3D orientation) {
		this.orientation = orientation;
	}

	public boolean isPlayable () {
		return playable;
	}

	public void setPlayable (boolean playable) {
		this.playable = playable;
	}

	@Override
	public Player clone () {
		Player copy = (Player) super.clone ();
		try {
			copy.setID (Core.getCloneID (getID (), Player.players));
		} catch (MaxIndex e) {
			return null;
		}
		Vector <Player> lsPlayerList = Player.players;
		if (lsPlayerList.size () < colors.length) {
			copy.color = colors[lsPlayerList.size ()];
		} else {
			copy.color = new GameColor ();
		}
		if (position != null) {
			copy.position = position.clone ();
		}
		if (orientation != null) {
			copy.orientation = orientation.clone ();
		}
		return copy;
	}

	@Override
	public String toString () {
		if (team == null || team.getID () == null) {
			return getID ();
		}
		return "(" + team.getID () + ") " + getID ();
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (100).append ("\n\tPlayerInfo - Player Name String '").append (race).append ('\'')
		        .append ("\n\tPlayerInfo - TeamIndex Int ").append (race.index);
	}

	public CharSequence toCode2 (int indentation) {
		return new StringBuilder (400).append ("\n\t00000032 Player\n\t{\n\t\tColor Colour").append (color.toCode (0))
		        .append ("\n\t\tIsPlayable Bool ").append (toBool (playable))
		        .append (s1)
		        .append ("\n\t\tStartPoint Vector3").append ((position != null ? position.toCode (indentation) : defaultPosition))
		        .append ("\n\t\tStartPoint Vector3").append ((orientation != null ? orientation.toCode (indentation) : defaultOrientation))
		        .append ("\n\t\tRace Int ").append (race.ordinal ())
		        .append ("\n\t\tPoints Float 0.000000\n\t\tTeamIndex Int ").append (Team.teams.indexOf (team))
		        .append ("\n\t\tFormationType Int ").append (formation.ordinal ())
		        .append (lastPart);
	}

}
