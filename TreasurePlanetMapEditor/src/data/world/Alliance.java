package data.world;

import java.util.Vector;

import interfaces.EntityInterface;


/**
 * An alliance between a player A and a player B
 *
 * @author MarcoForlini
 */
public class Alliance implements EntityInterface <Alliance> {

	/** Alliances defined in the map. */
	public static final Vector <Alliance>	alliances	= new Vector<> ();

	/** Player A */
	private Player							playerA;
	/** Player B */
	private Player							playerB;

	/**
	 * Create an alliance between PlayerA and PlayerB
	 *
	 * @param playerA Player A
	 * @param playerB Player B
	 */
	public Alliance (Player playerA, Player playerB) {
		setValues (playerA, playerB);
	}

	/**
	 * Set all values for this object
	 *
	 * @param playerA Player A
	 * @param playerB Player B
	 */
	public void setValues (Player playerA, Player playerB) {
		setPlayerA (playerA);
		setPlayerB (playerB);
	}

	/**
	 * Returns the player A of the alliance
	 *
	 * @return the player A of the alliance
	 */
	public Player getPlayerA () {
		return playerA;
	}

	/**
	 * Sets the player A of the alliance
	 *
	 * @param playerA the player A of the alliance
	 */
	public void setPlayerA (Player playerA) {
		this.playerA = playerA;
	}

	/**
	 * Returns the player B of the alliance
	 *
	 * @return the player B of the alliance
	 */
	public Player getPlayerB () {
		return playerB;
	}

	/**
	 * Sets the player B of the alliance
	 *
	 * @param playerB the player B of the alliance
	 */
	public void setPlayerB (Player playerB) {
		this.playerB = playerB;
	}

	@Override
	public String toString () {
		return playerA.getID () + " <> " + playerB.getID ();
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (200).append ("\n\t\t00000002 PlayerAllianceInfoVector - Element\n\t\t{\n\t\t\tPlayer0 Int ").append (Player.players.indexOf (playerA))
		        .append ("\n\t\t\tPlayer1 Int ").append (Player.players.indexOf (playerB)).append ("\n\t\t}");
	}

	@Override
	public int compareTo (Alliance o) {
		if (playerA.getID ().equals (o.playerA.getID ())) {
			return playerB.getID ().compareTo (o.playerB.getID ());
		} else {
			return playerA.getID ().compareTo (o.playerB.getID ());
		}
	}

	@Override
	public boolean isSpecial () {
		return false;
	}

	@Override
	public Alliance clone () {
		if (isSpecial ()) {
			return null; // SPECIAL ENTITY. CAN'T CLONE IT
		}
		try {
			return (Alliance) super.clone ();
		} catch (CloneNotSupportedException e) {
			throw new InternalError ();
		}
	}

}
