package data.world;

import interfaces.WriteCode;


public class Point2D implements Cloneable, Comparable <Point2D>, WriteCode {

	/** X position */
	private float	x;
	/** Y position */
	private float	y;

	/**
	 * Create a new Point2D
	 *
	 * @param x X position
	 * @param y Y position
	 */
	public Point2D (float x, float y) {
		setValues (x, y);
	}

	/**
	 * Set all values for this object
	 *
	 * @param x X position
	 * @param y Y position
	 */
	public void setValues (float x, float y) {
		setX (x);
		setY (y);
	}

	public float getX () {
		return x;
	}

	public void setX (float x) {
		this.x = x;
	}

	public float getY () {
		return y;
	}

	public void setY (float y) {
		this.y = y;
	}

	@Override
	public String toString () {
		return x + ", " + y;
	}

	@Override
	public Point2D clone () {
		try {
			return (Point2D) super.clone ();
		} catch (CloneNotSupportedException e) {
			throw new InternalError ();
		}
	}

	@Override
	public int compareTo (Point2D o) {
		if (x != o.x) {
			return (x < o.x ? -1 : 1);
		}
		if (y != o.y) {
			return (y < o.y ? -1 : 1);
		}
		return 0;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return new StringBuilder (500).append ("( ").append (toFloat6 (x)).append (", ").append (toFloat6 (y)).append (" )");
	}

}
