package data.world;

import interfaces.EntityInterface;


public abstract class Entity implements EntityInterface <Entity>, Cloneable {

	/** Entity ID */
	private String ID;

	/**
	 * Create a new Entity
	 *
	 * @param ID Entity ID
	 */
	public Entity (String ID) {
		setID (ID);
	}

	public String getID () {
		return ID;
	}

	public void setID (String ID) {
		this.ID = ID;
	}

	@Override
	public boolean isSpecial () {
		return ID == null;
	}

	@Override
	public abstract String toString ();

	@Override
	public Entity clone () {
		if (isSpecial ()) {
			return null; // SPECIAL ENTITY. CAN'T CLONE IT
		}
		try {
			Entity copy = (Entity) super.clone ();
			copy.ID += '_' + Integer.toHexString (hashCode ());
			return copy;
		} catch (CloneNotSupportedException e) {
			throw new InternalError ();
		}
	}

	@Override
	public int compareTo (Entity other) {
		if (equals (other)) {
			return 0;
		}
		return ID.compareTo (other.ID);
	}

}
