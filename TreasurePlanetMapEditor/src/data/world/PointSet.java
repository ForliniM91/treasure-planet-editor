package data.world;

import java.util.List;
import java.util.Vector;

import interfaces.WriteCode;


public class PointSet extends PointStructure <PointExtended> implements WriteCode {

	/** Ships objects defined in the map. */
	public static final Vector <PointSet> pointSets = new Vector<> ();

	/**
	 * Create a new PointSet
	 *
	 * @param ID PointSet ID
	 * @param points Points set
	 */
	public PointSet (String ID, List <PointExtended> points) {
		super (ID, points);
	}

	/**
	 * Set all values for this object
	 *
	 * @param ID PointSet ID
	 * @param points Points set
	 */
	@Override
	public void setValues (String ID, List <PointExtended> points) {
		setID (ID);
		setPoints (points);
	}

	@Override
	public PointSet clone () {
		return (PointSet) super.clone ();
	}

	@Override
	public String toString () {
		return getID ();
	}

	@Override
	public CharSequence toCode (int indentation) {
		int nLines = 4 + 11 * getPoints ().size ();
		StringBuilder sb = new StringBuilder (500).append ("\n\t\t").append (toChars8 (nLines))
		        .append (" World Point Sets Vector - Element\n\t\t{\n\t\t\tName String '").append (getID ())
		        .append ("'\n\t\t\tWorld Points - Size Int ").append (getPoints ().size ());
		for (Point2D point : getPoints ()) {
			sb.append (point.toCode (indentation));
		}
		return sb.append ("\n\t\t}");
	}

}
