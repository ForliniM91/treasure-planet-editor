package data.world;

import interfaces.WriteCode;


public class Point3D extends Point2D {

	/** Z position */
	private float z;

	/**
	 * Create a new Point3D
	 * 
	 * @param x X position
	 * @param y Y position
	 * @param z Z position
	 */
	public Point3D (float x, float y, float z) {
		super (x, y);
		setZ (z);
	}

	/**
	 * Set all values for this object
	 * 
	 * @param x X position
	 * @param y Y position
	 * @param z Z position
	 */
	public void setValues (float x, float y, float z) {
		setValues (x, y);
		setZ (z);
	}

	public float getZ () {
		return z;
	}

	public void setZ (float z) {
		this.z = z;
	}

	@Override
	public String toString () {
		return super.toString () + ", " + z;
	}

	@Override
	public CharSequence toCode (int indentation) {
		return "( " + toFloat6 (getX ()) + ", " + toFloat6 (getY ()) + ", " + toFloat6 (z) + " )";
	}

	@Override
	public Point3D clone () {
		return (Point3D) super.clone ();
	}

	@Override
	public int compareTo (Point2D o) {
		if (o instanceof Point3D == false) {
			return -1; // A 2D point is smaller than a 3D one
		}

		int c = super.compareTo (o);
		if (c != 0) {
			return c;
		}

		Point3D o2 = (Point3D) o;
		if (z != o2.z) {
			return (z < o2.z ? -1 : 1);
		}
		return 0;
	}

}
