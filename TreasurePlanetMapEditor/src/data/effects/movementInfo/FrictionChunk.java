/**
 *
 */
package data.effects.movementInfo;

import java.util.Scanner;

import data.effects.elements.Chunk;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * FrictionChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class FrictionChunk implements MovementInfo {

	public Chunk	frictionMultiplierInterpolator;
	public Chunk	frictionInterpolatorChunk;


	/**
	 * Creates a new {@link FrictionChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public FrictionChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link FrictionChunk}
	 *
	 * @param frictionMultiplierInterpolator
	 * @param frictionInterpolatorChunk
	 */
	public FrictionChunk (Chunk frictionMultiplierInterpolator, Chunk frictionInterpolatorChunk) {
		this.frictionMultiplierInterpolator = frictionMultiplierInterpolator;
		this.frictionInterpolatorChunk = frictionInterpolatorChunk;
	}


	@Override
	public int getNumInternalLines () {
		return frictionMultiplierInterpolator.getNumLines () +
		       frictionInterpolatorChunk.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		frictionMultiplierInterpolator = new Chunk (scanner);
		frictionInterpolatorChunk = new Chunk (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		return new StringBuilder (1000)
		        .append (ind).append ("RenderInfoType String 'FrictionChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" RenderInfo")
		        .append (ind).append ('{')
		        .append (frictionMultiplierInterpolator.toCode (indentation + 1))
		        .append (frictionInterpolatorChunk.toCode (indentation + 1))
		        .append (ind).append ('}');
	}


}
