/**
 *
 */
package data.effects.movementInfo;

import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * MovementInfo
 *
 * @author MarcoForlini
 */
public interface MovementInfo extends ReadCode, WriteCode {
	// Nothing
}
