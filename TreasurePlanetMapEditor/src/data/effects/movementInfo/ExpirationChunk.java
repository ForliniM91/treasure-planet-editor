/**
 *
 */
package data.effects.movementInfo;

import java.util.Scanner;

import data.effects.elements.Chunk;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * ExpirationChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class ExpirationChunk implements MovementInfo {

	public Chunk expirationInterpolateChunk;


	/**
	 * Creates a new {@link ExpirationChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public ExpirationChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link ExpirationChunk}
	 *
	 * @param expirationInterpolateChunk
	 */
	public ExpirationChunk (Chunk expirationInterpolateChunk) {
		this.expirationInterpolateChunk = expirationInterpolateChunk;
	}


	@Override
	public int getNumInternalLines () {
		return expirationInterpolateChunk.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		expirationInterpolateChunk = new Chunk (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		return new StringBuilder (1000)
		        .append (ind).append ("RenderInfoType String 'ExpirationChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" RenderInfo")
		        .append (ind).append ('{')
		        .append (expirationInterpolateChunk.toCode (indentation + 1))
		        .append (ind).append ('}');
	}


}
