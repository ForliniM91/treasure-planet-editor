/**
 *
 */
package data.effects.movementInfo;

import java.util.Scanner;

import data.effects.elements.Chunk;
import data.effects.elements.Chunk3;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * AttractionLineChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class AttractionLineChunk implements MovementInfo {

	public Chunk3	firstPositionInterpolator3Chunk;
	public Chunk3	secondPositionInterpolator3Chunk;
	public Chunk	interpolatorComponentAttractionConst;
	public Chunk	interpolatorComponentAttractionWithDist;
	public Chunk	interpolatorComponentAttractionWithDistSquared;
	public boolean	spinAroundLine;




	/**
	 * Creates a new {@link AttractionLineChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public AttractionLineChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link AttractionLineChunk}
	 *
	 * @param firstPositionInterpolator3Chunk
	 * @param secondPositionInterpolator3Chunk
	 * @param interpolatorComponentAttractionConst
	 * @param interpolatorComponentAttractionWithDist
	 * @param interpolatorComponentAttractionWithDistSquared
	 */
	public AttractionLineChunk (Chunk3 firstPositionInterpolator3Chunk, Chunk3 secondPositionInterpolator3Chunk,
	                            Chunk interpolatorComponentAttractionConst, Chunk interpolatorComponentAttractionWithDist, Chunk interpolatorComponentAttractionWithDistSquared,
	                            boolean spinAroundLine) {
		this.firstPositionInterpolator3Chunk = firstPositionInterpolator3Chunk;
		this.secondPositionInterpolator3Chunk = secondPositionInterpolator3Chunk;
		this.interpolatorComponentAttractionConst = interpolatorComponentAttractionConst;
		this.interpolatorComponentAttractionWithDist = interpolatorComponentAttractionWithDist;
		this.interpolatorComponentAttractionWithDistSquared = interpolatorComponentAttractionWithDistSquared;
		this.spinAroundLine = spinAroundLine;
	}


	@Override
	public int getNumInternalLines () {
		return 1 +
		       firstPositionInterpolator3Chunk.getNumLines () +
		       secondPositionInterpolator3Chunk.getNumLines () +
		       interpolatorComponentAttractionConst.getNumLines () +
		       interpolatorComponentAttractionWithDist.getNumLines () +
		       interpolatorComponentAttractionWithDistSquared.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		firstPositionInterpolator3Chunk = new Chunk3 (scanner);
		secondPositionInterpolator3Chunk = new Chunk3 (scanner);
		interpolatorComponentAttractionConst = new Chunk (scanner);
		interpolatorComponentAttractionWithDist = new Chunk (scanner);
		interpolatorComponentAttractionWithDistSquared = new Chunk (scanner);
		spinAroundLine = readBoolean (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		return new StringBuilder (3000)
		        .append (ind).append ("RenderInfoType String 'AttractionLineChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" RenderInfo")
		        .append (ind).append ('{')
		        .append (firstPositionInterpolator3Chunk.toCode (nextInd))
		        .append (secondPositionInterpolator3Chunk.toCode (nextInd))
		        .append (interpolatorComponentAttractionConst.toCode (nextInd))
		        .append (interpolatorComponentAttractionWithDist.toCode (nextInd))
		        .append (interpolatorComponentAttractionWithDistSquared.toCode (nextInd))
		        .append (indentationsN[nextInd]).append ("SpinAroundLine Bool ").append (toBool (spinAroundLine))
		        .append (ind).append ('}');
	}


}
