/**
 *
 */
package data.effects.movementInfo;

import interfaces.Constructor;


/**
 * MovementInfoType
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum MovementInfoType {

	AttractionChunk (data.effects.movementInfo.AttractionChunk::new),
	AttractionLineChunk (data.effects.movementInfo.AttractionLineChunk::new),
	ExpirationChunk (data.effects.movementInfo.ExpirationChunk::new),
	FrictionChunk (data.effects.movementInfo.FrictionChunk::new),
	;

	/** Constructor for the relative MovementInfo */
	public Constructor <MovementInfo> constructor;

	/**
	 * Creates a new {@link MovementInfoType}
	 * 
	 * @param constructor
	 */
	private MovementInfoType (Constructor <MovementInfo> constructor) {
		this.constructor = constructor;
	}



}
