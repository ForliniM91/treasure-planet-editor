/**
 *
 */
package data.effects.movementInfo;

import java.util.Scanner;

import data.effects.elements.Chunk;
import data.effects.elements.Chunk3;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * AttractionChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class AttractionChunk implements MovementInfo {

	public Chunk3	positionInterpolator3Chunk;
	public Chunk	interpolatorComponentAttractionConst;
	public Chunk	interpolatorComponentAttractionWithDist;
	public Chunk	interpolatorComponentAttractionWithDistSquared;




	/**
	 * Creates a new {@link AttractionChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public AttractionChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link AttractionChunk}
	 *
	 * @param positionInterpolator3Chunk
	 * @param interpolatorComponentAttractionConst
	 * @param interpolatorComponentAttractionWithDist
	 * @param interpolatorComponentAttractionWithDistSquared
	 */
	public AttractionChunk (Chunk3 positionInterpolator3Chunk, Chunk interpolatorComponentAttractionConst,
	                        Chunk interpolatorComponentAttractionWithDist, Chunk interpolatorComponentAttractionWithDistSquared) {
		this.positionInterpolator3Chunk = positionInterpolator3Chunk;
		this.interpolatorComponentAttractionConst = interpolatorComponentAttractionConst;
		this.interpolatorComponentAttractionWithDist = interpolatorComponentAttractionWithDist;
		this.interpolatorComponentAttractionWithDistSquared = interpolatorComponentAttractionWithDistSquared;
	}


	@Override
	public int getNumInternalLines () {
		return positionInterpolator3Chunk.getNumLines () +
		       interpolatorComponentAttractionConst.getNumLines () +
		       interpolatorComponentAttractionWithDist.getNumLines () +
		       interpolatorComponentAttractionWithDistSquared.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		positionInterpolator3Chunk = new Chunk3 (scanner);
		interpolatorComponentAttractionConst = new Chunk (scanner);
		interpolatorComponentAttractionWithDist = new Chunk (scanner);
		interpolatorComponentAttractionWithDistSquared = new Chunk (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		return new StringBuilder (2000)
		        .append (ind).append ("RenderInfoType String 'ExpirationChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" RenderInfo")
		        .append (ind).append ('{')
		        .append (positionInterpolator3Chunk.toCode (nextInd))
		        .append (interpolatorComponentAttractionConst.toCode (nextInd))
		        .append (interpolatorComponentAttractionWithDist.toCode (nextInd))
		        .append (interpolatorComponentAttractionWithDistSquared.toCode (nextInd))
		        .append (ind).append ('}');
	}


}
