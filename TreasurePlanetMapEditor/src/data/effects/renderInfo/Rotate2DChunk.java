/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.effects.elements.Chunk;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * Rotate2dChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class Rotate2DChunk implements RenderInfo {

	public Chunk rotationInterpolatorChunk;





	/**
	 * Creates a new {@link Rotate2DChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Rotate2DChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link Rotate2DChunk}
	 *
	 * @param rotationInterpolatorChunk
	 */
	public Rotate2DChunk (Chunk rotationInterpolatorChunk) {
		this.rotationInterpolatorChunk = rotationInterpolatorChunk;
	}


	@Override
	public int getNumInternalLines () {
		return rotationInterpolatorChunk.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		rotationInterpolatorChunk = new Chunk (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		return new StringBuilder (2000)
		        .append (ind).append ("RenderInfoType String 'Rotate2dChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" RenderInfo")
		        .append (ind).append ('{')
		        .append (rotationInterpolatorChunk.toCode (nextInd))
		        .append (ind).append ('}');
	}


}
