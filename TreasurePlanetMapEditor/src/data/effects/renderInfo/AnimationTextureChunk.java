/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * AnimationTextureChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class AnimationTextureChunk implements RenderInfo {

	public String	animationFiles;
	public int		numberFrames;
	public String	animationType;




	/**
	 * Creates a new {@link AnimationTextureChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public AnimationTextureChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link AnimationTextureChunk}
	 *
	 * @param name
	 * @param animationFiles
	 */
	public AnimationTextureChunk (String name, String animationFiles, int numberFrames, String animationType) {
		this.animationFiles = animationFiles;
		this.numberFrames = numberFrames;
		this.animationType = animationType;
	}


	@Override
	public int getNumInternalLines () {
		return 3;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);
		if (n != 3) {
			throw new UnexpectedNumLines (Integer.toString (n));
		}

		scanner.nextLine (); // {
		animationFiles = readString (scanner);
		numberFrames = readInt (scanner);
		animationType = readString (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation + 1];
		return new StringBuilder (250)
		        .append (ind).append ("RenderInfoType String 'AnimationTextureChunk'")
		        .append (ind).append ("00000003 RenderInfo")
		        .append (ind).append ('{')
		        .append (ind__).append ("AnimationFiles String '").append (animationFiles).append ('\'')
		        .append (ind__).append ("NumberFrames Int ").append (numberFrames)
		        .append (ind__).append ("AnimationType String '").append (animationType).append ('\'')
		        .append (ind).append ('}');
	}


}
