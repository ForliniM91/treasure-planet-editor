/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * DistanceSizeScaling
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class DistanceSizeScaling implements RenderInfo {

	public float	minDistanceScaling;
	public float	maxDistanceScaling;
	public float	minDistanceSizeMultiply;




	/**
	 * Creates a new {@link DistanceSizeScaling}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public DistanceSizeScaling (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link DistanceSizeScaling}
	 *
	 * @param textureName
	 */
	public DistanceSizeScaling (float minDistanceScaling, float maxDistanceScaling, float minDistanceSizeMultiply) {
		this.minDistanceScaling = minDistanceScaling;
		this.maxDistanceScaling = maxDistanceScaling;
		this.minDistanceSizeMultiply = minDistanceSizeMultiply;
	}


	@Override
	public int getNumInternalLines () {
		return 3;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);
		if (n != 3) {
			throw new UnexpectedNumLines (Integer.toString (n));
		}

		scanner.nextLine (); // {
		minDistanceScaling = readFloat (scanner);
		maxDistanceScaling = readFloat (scanner);
		minDistanceSizeMultiply = readFloat (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation + 1];
		return new StringBuilder (250)
		        .append (ind).append ("RenderInfoType String 'DistanceSizeScaling'")
		        .append (ind).append ("00000003 RenderInfo")
		        .append (ind).append ('{')
		        .append (ind__).append ("Min distance scaling Float ").append (toFloat6 (minDistanceScaling))
		        .append (ind__).append ("Max distance scaling Float ").append (toFloat6 (maxDistanceScaling))
		        .append (ind__).append ("min distance size multiply Float ").append (toFloat6 (minDistanceSizeMultiply))
		        .append (ind).append ('}');
	}


}
