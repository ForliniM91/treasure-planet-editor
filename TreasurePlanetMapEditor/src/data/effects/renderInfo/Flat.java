/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * Flat
 *
 * @author MarcoForlini
 */
public class Flat implements RenderInfo {

	/**
	 * Creates a new {@link Flat}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Flat (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link Flat}
	 */
	public Flat () {}



	@Override
	public int getNumInternalLines () {
		return 0;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);
		if (n != 0) {
			throw new UnexpectedNumLines (Integer.toString (n));
		}

		scanner.nextLine (); // {
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		return new StringBuilder (100)
		        .append (ind).append ("RenderInfoType String 'Flat'")
		        .append (ind).append ("00000000 RenderInfo")
		        .append (ind).append ('{')
		        .append (ind).append ('}');
	}


}
