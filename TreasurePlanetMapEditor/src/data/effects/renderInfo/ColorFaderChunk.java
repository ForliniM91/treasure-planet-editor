/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.effects.elements.Chunk;
import data.effects.elements.Chunk3;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * ColorFaderChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class ColorFaderChunk implements RenderInfo {

	public Chunk3	colorInterpolator3Chunk;
	public Chunk	alphaComponentChunk;




	/**
	 * Creates a new {@link BlendChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public ColorFaderChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link ColorFaderChunk}
	 *
	 * @param colorInterpolator3Chunk
	 * @param alphaComponentChunk
	 */
	public ColorFaderChunk (Chunk3 colorInterpolator3Chunk, Chunk alphaComponentChunk) {
		this.colorInterpolator3Chunk = colorInterpolator3Chunk;
		this.alphaComponentChunk = alphaComponentChunk;
	}


	@Override
	public int getNumInternalLines () {
		return colorInterpolator3Chunk.getNumLines () + alphaComponentChunk.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		colorInterpolator3Chunk = new Chunk3 (scanner);
		alphaComponentChunk = new Chunk (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		return new StringBuilder (2000)
		        .append (ind).append ("RenderInfoType String 'ColorFaderChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" RenderInfo")
		        .append (ind).append ('{')
		        .append (colorInterpolator3Chunk.toCode (nextInd))
		        .append (alphaComponentChunk.toCode (nextInd))
		        .append (ind).append ('}');
	}


}
