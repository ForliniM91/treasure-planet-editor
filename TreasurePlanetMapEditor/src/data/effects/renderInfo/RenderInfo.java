/**
 *
 */
package data.effects.renderInfo;

import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * RenderInfo
 *
 * @author MarcoForlini
 */
public interface RenderInfo extends ReadCode, WriteCode {
	// Nothing
}
