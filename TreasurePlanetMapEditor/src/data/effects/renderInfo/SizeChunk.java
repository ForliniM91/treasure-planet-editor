/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.effects.elements.Chunk;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * SizeChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class SizeChunk implements RenderInfo {

	public Chunk sizeInterpolatorChunk;




	/**
	 * Creates a new {@link SizeChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public SizeChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link SizeChunk}
	 *
	 * @param sizeInterpolatorChunk
	 */
	public SizeChunk (Chunk sizeInterpolatorChunk) {
		this.sizeInterpolatorChunk = sizeInterpolatorChunk;
	}


	@Override
	public int getNumInternalLines () {
		return sizeInterpolatorChunk.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		sizeInterpolatorChunk = new Chunk (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		return new StringBuilder (2000)
		        .append (ind).append ("RenderInfoType String 'SizeChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" RenderInfo")
		        .append (ind).append ('{')
		        .append (sizeInterpolatorChunk.toCode (nextInd))
		        .append (ind).append ('}');
	}


}
