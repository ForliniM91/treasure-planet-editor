/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * SimpleTextureChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class SimpleTextureChunk implements RenderInfo {

	public String textureName;




	/**
	 * Creates a new {@link SimpleTextureChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public SimpleTextureChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link SimpleTextureChunk}
	 *
	 * @param textureName
	 */
	public SimpleTextureChunk (String textureName) {
		this.textureName = textureName;
	}


	@Override
	public int getNumInternalLines () {
		return 1;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);
		if (n != 1) {
			throw new UnexpectedNumLines (Integer.toString (n));
		}

		scanner.nextLine (); // {
		textureName = readString (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation + 1];
		return new StringBuilder (250)
		        .append (ind).append ("RenderInfoType String 'SimpleTextureChunk'")
		        .append (ind).append ("00000001 RenderInfo")
		        .append (ind).append ('{')
		        .append (ind__).append ("TextureName String '").append (textureName).append ('\'')
		        .append (ind).append ('}');
	}


}
