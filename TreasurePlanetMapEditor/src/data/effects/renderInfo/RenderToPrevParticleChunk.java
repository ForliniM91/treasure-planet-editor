/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * RenderToPrevParticleChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class RenderToPrevParticleChunk implements RenderInfo {

	public boolean pointToPrev;




	/**
	 * Creates a new {@link RenderToPrevParticleChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public RenderToPrevParticleChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link RenderToPrevParticleChunk}
	 *
	 * @param pointToPrev
	 */
	public RenderToPrevParticleChunk (boolean pointToPrev) {
		this.pointToPrev = pointToPrev;
	}


	@Override
	public int getNumInternalLines () {
		return 1;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);
		if (n != 1) {
			throw new UnexpectedNumLines (Integer.toString (n));
		}

		scanner.nextLine (); // {
		pointToPrev = readBoolean (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation + 1];
		return new StringBuilder (250)
		        .append (ind).append ("RenderInfoType String 'RenderToPrevParticleChunk'")
		        .append (ind).append ("00000001 RenderInfo")
		        .append (ind).append ('{')
		        .append (ind__).append ("PointToPrev Bool ").append (toBool (pointToPrev))
		        .append (ind).append ('}');
	}


}
