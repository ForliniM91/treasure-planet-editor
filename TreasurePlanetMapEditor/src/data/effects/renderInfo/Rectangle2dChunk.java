/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.effects.elements.Chunk;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * Rectangle2dChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class Rectangle2dChunk implements RenderInfo {

	public Chunk	topLeftXInterpolatorChunk;
	public Chunk	topLeftYInterpolatorChunk;
	public Chunk	bottomRightXInterpolatorChunk;
	public Chunk	bottomRightYInterpolatorChunk;





	/**
	 * Creates a new {@link Rectangle2dChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Rectangle2dChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link Rectangle2dChunk}
	 *
	 * @param topLeftXInterpolatorChunk
	 * @param topLeftYInterpolatorChunk
	 * @param bottomRightXInterpolatorChunk
	 * @param bottomRightYInterpolatorChunk
	 */
	public Rectangle2dChunk (Chunk topLeftXInterpolatorChunk, Chunk topLeftYInterpolatorChunk, Chunk bottomRightXInterpolatorChunk, Chunk bottomRightYInterpolatorChunk) {
		this.topLeftXInterpolatorChunk = topLeftXInterpolatorChunk;
		this.topLeftYInterpolatorChunk = topLeftYInterpolatorChunk;
		this.bottomRightXInterpolatorChunk = bottomRightXInterpolatorChunk;
		this.bottomRightYInterpolatorChunk = bottomRightYInterpolatorChunk;
	}


	@Override
	public int getNumInternalLines () {
		return topLeftXInterpolatorChunk.getNumLines () +
		       topLeftYInterpolatorChunk.getNumLines () +
		       bottomRightXInterpolatorChunk.getNumLines () +
		       bottomRightYInterpolatorChunk.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		topLeftXInterpolatorChunk = new Chunk (scanner);
		topLeftYInterpolatorChunk = new Chunk (scanner);
		bottomRightXInterpolatorChunk = new Chunk (scanner);
		bottomRightYInterpolatorChunk = new Chunk (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		return new StringBuilder (2000)
		        .append (ind).append ("RenderInfoType String 'Rectangle2dChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" RenderInfo")
		        .append (ind).append ('{')
		        .append (topLeftXInterpolatorChunk.toCode (nextInd))
		        .append (topLeftYInterpolatorChunk.toCode (nextInd))
		        .append (bottomRightXInterpolatorChunk.toCode (nextInd))
		        .append (bottomRightYInterpolatorChunk.toCode (nextInd))
		        .append (ind).append ('}');
	}


}
