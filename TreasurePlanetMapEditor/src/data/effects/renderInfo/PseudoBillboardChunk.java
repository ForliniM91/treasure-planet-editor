/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.effects.elements.Chunk3;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * PseudoBillboardChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class PseudoBillboardChunk implements RenderInfo {

	public Chunk3	directionInterpolator3Chunk;
	public boolean	pseudoDirectionUseVelocity;



	/**
	 * Creates a new {@link PseudoBillboardChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public PseudoBillboardChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link PseudoBillboardChunk}
	 *
	 * @param directionInterpolator3Chunk
	 * @param pseudoDirectionUseVelocity
	 */
	public PseudoBillboardChunk (Chunk3 directionInterpolator3Chunk, boolean pseudoDirectionUseVelocity) {
		this.directionInterpolator3Chunk = directionInterpolator3Chunk;
		this.pseudoDirectionUseVelocity = pseudoDirectionUseVelocity;
	}


	@Override
	public int getNumInternalLines () {
		return directionInterpolator3Chunk.getNumLines () + 1;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		directionInterpolator3Chunk = new Chunk3 (scanner);
		pseudoDirectionUseVelocity = readBoolean (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[nextInd];
		return new StringBuilder (2000)
		        .append (ind).append ("RenderInfoType String 'PseudoBillboardChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" RenderInfo")
		        .append (ind).append ('{')
		        .append (directionInterpolator3Chunk.toCode (nextInd))
		        .append (ind__).append ("PseudoDirectionUseVelocity Bool ").append (toBool (pseudoDirectionUseVelocity))
		        .append (ind).append ('}');
	}


}
