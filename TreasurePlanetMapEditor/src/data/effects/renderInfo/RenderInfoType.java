/**
 *
 */
package data.effects.renderInfo;

import data.effects.primitive.PrimitiveType;
import interfaces.Constructor;


/**
 * RenderInfoType
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum RenderInfoType {

	AnimationTextureChunk (data.effects.renderInfo.AnimationTextureChunk::new),
	BlendChunk (data.effects.renderInfo.BlendChunk::new),
	ColorFaderChunk (data.effects.renderInfo.ColorFaderChunk::new),
	DistanceSizeScaling (data.effects.renderInfo.DistanceSizeScaling::new),
	Flat (data.effects.renderInfo.Flat::new),
	MeshChunk (data.effects.renderInfo.MeshChunk::new),
	PseudoBillboardChunk (data.effects.renderInfo.PseudoBillboardChunk::new),
	Rectangle2dChunk (data.effects.renderInfo.Rectangle2dChunk::new),
	Rectangle2DChunk (data.effects.renderInfo.Rectangle2dChunk::new),
	RenderToPrevParticleChunk (data.effects.renderInfo.RenderToPrevParticleChunk::new),
	Rotate2dChunk (data.effects.renderInfo.Rotate2DChunk::new),
	Rotate2DChunk (data.effects.renderInfo.Rotate2DChunk::new),
	SimpleTextureChunk (data.effects.renderInfo.SimpleTextureChunk::new),
	SizeChunk (data.effects.renderInfo.SizeChunk::new),
	;



	/** The constructor for the relative Primitive */
	public Constructor <RenderInfo> constructor;

	/**
	 * Creates a new {@link PrimitiveType}
	 *
	 * @param constructor
	 */
	private RenderInfoType (Constructor <RenderInfo> constructor) {
		this.constructor = constructor;
	}

}
