/**
 *
 */
package data.effects.renderInfo;

import java.util.Scanner;

import data.effects.elements.Chunk3;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * MeshChunk
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class MeshChunk implements RenderInfo {

	public Chunk3	meshWeightInterpolator;
	public boolean	useMorph;
	public String	sourceMeshName;
	public String	destinationMeshName;
	public float	morphTime;
	public float	timeChangeTexture;
	public boolean	FADE_EDGES;
	public boolean	USE_SHIFTING;





	/**
	 * Creates a new {@link MeshChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public MeshChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link MeshChunk}
	 *
	 * @param meshWeightInterpolator
	 * @param useMorph
	 */
	public MeshChunk (Chunk3 meshWeightInterpolator, boolean useMorph, String sourceMeshName, String destinationMeshName,
	                  float morphTime, float timeChangeTexture, boolean FADE_EDGES, boolean USE_SHIFTING) {
		this.meshWeightInterpolator = meshWeightInterpolator;
		this.useMorph = useMorph;
		this.sourceMeshName = sourceMeshName;
		this.destinationMeshName = destinationMeshName;
		this.morphTime = morphTime;
		this.timeChangeTexture = timeChangeTexture;
		this.FADE_EDGES = FADE_EDGES;
		this.USE_SHIFTING = USE_SHIFTING;
	}


	@Override
	public int getNumInternalLines () {
		return meshWeightInterpolator.getNumLines () + 1;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		meshWeightInterpolator = new Chunk3 (scanner);
		useMorph = readBoolean (scanner);
		sourceMeshName = readString (scanner);
		destinationMeshName = readString (scanner);
		morphTime = readFloat (scanner);
		timeChangeTexture = readFloat (scanner);
		FADE_EDGES = readBoolean (scanner);
		USE_SHIFTING = readBoolean (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[nextInd];
		return new StringBuilder (2000)
		        .append (ind).append ("RenderInfoType String 'MeshChunk'")
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" MeshChunk")
		        .append (ind).append ('{')
		        .append (meshWeightInterpolator.toCode (nextInd))
		        .append (ind__).append ("UseMorph Bool ").append (toBool (useMorph))
		        .append (ind__).append ("SourceMeshName String '").append (sourceMeshName).append ('\'')
		        .append (ind__).append ("DestinationMeshName String '").append (destinationMeshName).append ('\'')
		        .append (ind__).append ("MorphTime Float ").append (toFloat6 (morphTime))
		        .append (ind__).append ("TimeChangeTexture Float ").append (toFloat6 (timeChangeTexture))
		        .append (ind__).append ("FADE_EDGES Bool ").append (toBool (FADE_EDGES))
		        .append (ind__).append ("USE_SHIFTING Bool ").append (toBool (USE_SHIFTING))
		        .append (ind).append ('}');
	}


}
