/**
 *
 */
package data.effects.elements;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * Time-Value pair
 *
 * @author MarcoForlini
 */
public class TimeValue implements ReadCode, WriteCode {

	/** Time */
	public float	time;

	/** Value */
	public float	value;


	/**
	 * Creates a new {@link TimeValue}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public TimeValue (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link TimeValue}
	 *
	 * @param time Time
	 * @param value Value
	 */
	public TimeValue (float time, float value) {
		this.time = time;
		this.value = value;
	}

	@Override
	public int getNumLines () {
		return 2;
	}

	@Override
	public int getNumInternalLines () {
		return 0;
	}

	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		time = readFloat (scanner);
		value = readFloat (scanner);
	}

	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		return ind + "t Float " + toFloat6 (time) +
		       ind + "value Float " + toFloat6 (value);
	}

	@Override
	public String toString () {
		return "Time: " + time + " -> Value: " + value;
	}

}
