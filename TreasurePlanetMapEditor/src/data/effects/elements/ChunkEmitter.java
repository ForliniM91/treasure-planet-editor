/**
 *
 */
package data.effects.elements;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * Position emitter
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class ChunkEmitter implements ReadCode, WriteCode {


	public String	name;

	public Chunk3	emitterTargetPositionInterpolator3;

	public Chunk	radiusInterpolator;

	public Chunk	radiusErrorInterpolator;

	public Chunk	lengthInterpolator;

	public Chunk	lengthErrorInterpolator;




	/**
	 * Creates a new {@link ChunkEmitter}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public ChunkEmitter (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link ChunkEmitter}
	 *
	 * @param emitterTargetPositionInterpolator3
	 * @param radiusInterpolator
	 * @param radiusErrorInterpolator
	 * @param lengthInterpolator
	 * @param lengthErrorInterpolator
	 */
	public ChunkEmitter (Chunk3 emitterTargetPositionInterpolator3, Chunk radiusInterpolator, Chunk radiusErrorInterpolator, Chunk lengthInterpolator, Chunk lengthErrorInterpolator) {
		this.emitterTargetPositionInterpolator3 = emitterTargetPositionInterpolator3;
		this.radiusInterpolator = radiusInterpolator;
		this.radiusErrorInterpolator = radiusErrorInterpolator;
		this.lengthInterpolator = lengthInterpolator;
		this.lengthErrorInterpolator = lengthErrorInterpolator;
	}


	@Override
	public int getNumInternalLines () {
		return emitterTargetPositionInterpolator3.getNumLines () +
		       radiusInterpolator.getNumLines () +
		       radiusErrorInterpolator.getNumLines () +
		       lengthInterpolator.getNumLines () +
		       lengthErrorInterpolator.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		String line = scanner.nextLine ().trim ();
		int n = Integer.parseInt (line.substring (0, 8));

		name = line.substring (9);
		scanner.nextLine (); // {
		emitterTargetPositionInterpolator3 = new Chunk3 (scanner);
		radiusInterpolator = new Chunk (scanner);
		radiusErrorInterpolator = new Chunk (scanner);
		lengthInterpolator = new Chunk (scanner);
		lengthErrorInterpolator = new Chunk (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		return new StringBuilder (5000)
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (' ').append (name)
		        .append (ind).append ('{')
		        .append (emitterTargetPositionInterpolator3.toCode (nextInd))
		        .append (radiusInterpolator.toCode (nextInd))
		        .append (radiusErrorInterpolator.toCode (nextInd))
		        .append (lengthInterpolator.toCode (nextInd))
		        .append (lengthErrorInterpolator.toCode (nextInd))
		        .append (ind).append ('}');
	}


	@Override
	public String toString () {
		return name;
	}

}
