package data.effects.elements;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * CHUNKDATA_SOURCE_EMITTER
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class ChunkSourceEmitter implements ReadCode, WriteCode {

	public int		index;
	public boolean	enabled;


	/**
	 * Creates a new {@link ChunkSourceEmitter}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public ChunkSourceEmitter (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	@Override
	public final int getNumInternalLines () {
		return 2;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedNumLines {
		readNumLines (scanner, 2); // <number of lines> ConstData

		scanner.nextLine (); // {
		index = readInt (scanner);
		enabled = readBoolean (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation + 1];
		return new StringBuilder (200)
		        .append ("00000002 ConstData")
		        .append (ind).append ('{')
		        .append (ind__).append ("CHUNKDATA_PARTICLE_SYSTEM_SOURCE_EMITTER_INDEX Int ").append (index)
		        .append (ind__).append ("CHUNKDATA_PARTICLE_SYSTEM_SOURCE_EMITTER_ENABLEd Bool ").append (toBool (enabled))
		        .append (ind).append ('}');
	}

}
