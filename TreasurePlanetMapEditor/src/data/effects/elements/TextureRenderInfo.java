/**
 *
 */
package data.effects.elements;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * TextureRenderInfo
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class TextureRenderInfo implements ReadCode, WriteCode {

	public String textureName;




	/**
	 * Creates a new {@link TextureRenderInfo}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public TextureRenderInfo (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link TextureRenderInfo}
	 *
	 * @param name
	 * @param textureName
	 */
	public TextureRenderInfo (String textureName) {
		this.textureName = textureName;
	}


	@Override
	public int getNumInternalLines () {
		return 1;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);
		if (n != 1) {
			throw new UnexpectedNumLines (Integer.toString (n));
		}

		scanner.nextLine (); // {
		textureName = readString (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation + 1];
		return new StringBuilder (150)
		        .append (ind).append ("00000001 TEXTURE_RENDER_INFO")
		        .append (ind).append ('{')
		        .append (ind__).append ("TextureName String '").append (textureName).append ('\'')
		        .append (ind).append ('}');
	}


}
