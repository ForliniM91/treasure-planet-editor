/**
 *
 */
package data.effects.elements;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * ColorInterpolator3Chunk
 *
 * @author MarcoForlini
 */
public class Chunk3 implements ReadCode, WriteCode {

	/** Name */
	public String	name;

	/** Chunk 1 */
	public Chunk	chunk1;
	/** Chunk 2 */
	public Chunk	chunk2;
	/** Chunk 3 */
	public Chunk	chunk3;





	/**
	 * Creates a new {@link Chunk3}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Chunk3 (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link Chunk3}
	 *
	 * @param name Name
	 * @param chunk1 the chunk1
	 * @param chunk2 the chunk2
	 * @param chunk3 the chunk3
	 */
	public Chunk3 (String name, Chunk chunk1, Chunk chunk2, Chunk chunk3) {
		this.name = name;
		this.chunk1 = chunk1;
		this.chunk2 = chunk2;
		this.chunk3 = chunk3;
	}


	@Override
	public int getNumInternalLines () {
		return chunk1.getNumLines () + chunk2.getNumLines () + chunk3.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		String line = scanner.nextLine ().trim ();
		int n = Integer.parseInt (line.substring (0, 8));

		name = line.substring (9);
		scanner.nextLine (); // {
		chunk1 = new Chunk (scanner);
		chunk2 = new Chunk (scanner);
		chunk3 = new Chunk (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		int nextInd = indentation + 1;
		return new StringBuilder (1500)
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (' ').append (name)
		        .append (ind).append ('{')
		        .append (chunk1.toCode (nextInd))
		        .append (chunk2.toCode (nextInd))
		        .append (chunk3.toCode (nextInd))
		        .append (ind).append ('}');
	}


	@Override
	public String toString () {
		return name;
	}

}
