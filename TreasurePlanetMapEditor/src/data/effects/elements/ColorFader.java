/**
 *
 */
package data.effects.elements;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * Position emitter
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class ColorFader implements ReadCode, WriteCode {

	public Chunk3	colorInterpolator3Chunk;

	public Chunk3	alphaComponentChunk;




	/**
	 * Creates a new {@link ColorFader}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public ColorFader (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link ColorFader}
	 *
	 * @param colorInterpolator3Chunk
	 * @param alphaComponentChunk
	 */
	public ColorFader (Chunk3 colorInterpolator3Chunk, Chunk3 alphaComponentChunk) {
		this.colorInterpolator3Chunk = colorInterpolator3Chunk;
		this.alphaComponentChunk = alphaComponentChunk;
	}


	@Override
	public int getNumInternalLines () {
		return colorInterpolator3Chunk.getNumLines () +
		       alphaComponentChunk.getNumLines ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);

		scanner.nextLine (); // {
		colorInterpolator3Chunk = new Chunk3 (scanner);
		alphaComponentChunk = new Chunk3 (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		return new StringBuilder (5000)
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (" ColorFader")
		        .append (ind).append ('{')
		        .append (colorInterpolator3Chunk.toCode (nextInd))
		        .append (alphaComponentChunk.toCode (nextInd))
		        .append (ind).append ('}');
	}


}
