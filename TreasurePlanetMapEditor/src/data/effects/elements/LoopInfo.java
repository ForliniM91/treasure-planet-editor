/**
 *
 */
package data.effects.elements;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * @author MarcoForlini
 */
public class LoopInfo implements ReadCode, WriteCode {

	/** Start time */
	public float	startLoopingT;
	/** End time */
	public float	endLoopingT;
	/** Number of loops */
	public int		numberOfLoops;




	/**
	 * Creates a new {@link LoopInfo}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException
	 */
	public LoopInfo (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link LoopInfo}
	 *
	 * @param startLoopingT Start time
	 * @param endLoopingT End time
	 * @param numberOfLoops Number of loops
	 */
	public LoopInfo (float startLoopingT, float endLoopingT, int numberOfLoops) {
		super ();
		this.startLoopingT = startLoopingT;
		this.endLoopingT = endLoopingT;
		this.numberOfLoops = numberOfLoops;
	}


	@Override
	public final int getNumInternalLines () {
		return 3;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		readNumLines (scanner, 3); // <number of lines> ConstData
		scanner.nextLine (); // {
		startLoopingT = readFloat (scanner);
		endLoopingT = readFloat (scanner);
		numberOfLoops = readInt (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation + 1];
		return new StringBuilder (150)
		        .append (ind).append ("00000003 LoopInfo")
		        .append (ind).append ("{")
		        .append (ind__).append ("StartLoopingT Float ").append (toFloat6 (startLoopingT))
		        .append (ind__).append ("EndLoopingT Float ").append (toFloat6 (endLoopingT))
		        .append (ind__).append ("NumberOfLoops Int ").append (numberOfLoops)
		        .append (ind).append ("}");
	}


	@Override
	public String toString () {
		return "Loop: " + numberOfLoops + "x [" + startLoopingT + ',' + endLoopingT + ']';
	}

}
