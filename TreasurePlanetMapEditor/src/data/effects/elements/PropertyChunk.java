/**
 *
 */
package data.effects.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import data.effects.movementInfo.MovementInfo;
import data.effects.movementInfo.MovementInfoType;
import data.effects.renderInfo.RenderInfo;
import data.effects.renderInfo.RenderInfoType;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * Position emitter
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class PropertyChunk implements ReadCode, WriteCode {


	public String				name;

	public List <RenderInfo>	renderInfo;

	public List <MovementInfo>	movementInfo;




	/**
	 * Creates a new {@link PropertyChunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public PropertyChunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}




	@Override
	public int getNumInternalLines () {
		return 2 +
		       renderInfo.parallelStream ().mapToInt (RenderInfo::getNumLines).sum () +
		       movementInfo.parallelStream ().mapToInt (MovementInfo::getNumLines).sum ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		String line = scanner.nextLine ().trim ();
		int n = Integer.parseInt (line.substring (0, 8));

		name = line.substring (9);
		scanner.nextLine (); // {

		RenderInfoType renderInfoType;
		int size = readInt (scanner);
		renderInfo = new ArrayList<> (size);
		for (int i = 0; i < size; i++) {
			renderInfoType = RenderInfoType.valueOf (readString (scanner));
			renderInfo.add (renderInfoType.constructor.create (scanner));
		}

		MovementInfoType movementInfoType;
		size = readInt (scanner);
		movementInfo = new ArrayList<> (size);
		for (int i = 0; i < size; i++) {
			movementInfoType = MovementInfoType.valueOf (readString (scanner));
			movementInfo.add (movementInfoType.constructor.create (scanner));
		}

		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[nextInd];
		StringBuilder sb = new StringBuilder (5000)
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (' ').append (name)
		        .append (ind).append ('{');
		sb.append (ind__).append ("RenderNumInfos Int ").append (renderInfo.size ());
		for (RenderInfo ri : renderInfo) {
			sb.append (ri.toCode (nextInd));
		}
		sb.append (ind__).append ("MovementNumInfos Int ").append (movementInfo.size ());
		for (MovementInfo mi : movementInfo) {
			sb.append (mi.toCode (nextInd));
		}
		return sb.append (ind).append ('}');
	}


	@Override
	public String toString () {
		return name;
	}

}
