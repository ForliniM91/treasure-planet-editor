/**
 *
 */
package data.effects.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * @author MarcoForlini
 */
public class Chunk implements ReadCode, WriteCode {

	/** Chunk name */
	public String			name;

	/** Loop info */
	public LoopInfo			loopInfo;

	/** Use value at keys only */
	public boolean			useValuesAtKeysOnly;

	/** List of time-value pairs */
	public List <TimeValue>	timeValues;




	/**
	 * Creates a new {@link Chunk}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Chunk (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link Chunk}
	 *
	 * @param name
	 * @param loopInfo
	 * @param useValuesAtKeysOnly
	 * @param timeValues
	 */
	public Chunk (String name, LoopInfo loopInfo, boolean useValuesAtKeysOnly, List <TimeValue> timeValues) {
		this.name = name;
		this.loopInfo = loopInfo;
		this.useValuesAtKeysOnly = useValuesAtKeysOnly;
		this.timeValues = timeValues;
	}


	@Override
	public final int getNumInternalLines () {
		return loopInfo.getNumLines () + 2 + 2 * timeValues.size ();
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		String line = scanner.nextLine ().trim ();
		int n = Integer.parseInt (line.substring (0, 8));

		name = line.substring (9);
		scanner.nextLine (); // {
		loopInfo = new LoopInfo (scanner);
		useValuesAtKeysOnly = readBoolean (scanner);
		int nTV = readInt (scanner);
		timeValues = new ArrayList<> (nTV);
		for (int i = 0; i < nTV; i++) {
			timeValues.add (new TimeValue (scanner));
		}
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	@Override
	public CharSequence toCode (int indentation) {
		int nextInd = indentation + 1;
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[nextInd];
		StringBuilder sb = new StringBuilder (500)
		        .append (ind).append (toChars8 (getNumInternalLines ())).append (' ').append (name)
		        .append (ind).append ('{')
		        .append (loopInfo.toCode (indentation + 1))
		        .append (ind__).append ("UseValuesAtKeysOnly Bool ").append (toBool (useValuesAtKeysOnly))
		        .append (ind__).append ("NumKeys Int ").append (timeValues.size ());
		for (TimeValue tv : timeValues) {
			sb.append (tv.toCode (nextInd));
		}
		return sb.append (ind).append ('}');
	}


	@Override
	public String toString () {
		return name;
	}

}
