/**
 *
 */
package data.effects.elements;

import java.util.Scanner;

import data.effects.renderInfo.RenderInfo;
import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * BlendRenderInfo
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class BlendRenderInfo implements RenderInfo {

	public String	blendSource;
	public String	blendDestination;



	/**
	 * Creates a new {@link BlendRenderInfo}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public BlendRenderInfo (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link BlendRenderInfo}
	 *
	 * @param blendSource
	 * @param blendDestination
	 */
	public BlendRenderInfo (String blendSource, String blendDestination) {
		this.blendSource = blendSource;
		this.blendDestination = blendDestination;
	}



	@Override
	public int getNumInternalLines () {
		return 2;
	}


	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);
		if (n != 2) {
			throw new UnexpectedNumLines (Integer.toString (n));
		}

		scanner.nextLine (); // {
		blendSource = readString (scanner);
		blendDestination = readString (scanner);
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		String ind = indentationsN[indentation];
		String ind__ = indentationsN[indentation + 1];
		return new StringBuilder (150)
		        .append (ind).append ("00000002 BlendRenderInfo")
		        .append (ind).append ('{')
		        .append (ind__).append ("BlendSource String '").append (blendSource).append ('\'')
		        .append (ind__).append ("BlendDestination String '").append (blendDestination).append ('\'')
		        .append (ind).append ('}');
	}


}
