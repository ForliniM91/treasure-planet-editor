package data.effects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import data.effects.primitive.Primitive;
import data.effects.primitive.PrimitiveType;
import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * A graphic effect
 *
 * @author MarcoForlini
 */
public class Effect implements ReadCode, WriteCode {

	public static final Effect none = new Effect ("none");


	/** The map of all effects */
	public static final Map <String, Effect> effects = new HashMap<> ();
	static {
		effects.put ("none", none);
	}



	private String				name;
	private float				maxRadius;
	private List <Primitive>	primitives;


	/**
	 * Creates a new {@link Effect}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException
	 */
	public Effect (Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}

	/**
	 * Creates a new {@link Effect}
	 *
	 * @param name The name
	 */
	public Effect (String name) {
		this.name = name;
	}

	@Override
	public int getNumInternalLines () {
		return 3 + primitives.parallelStream ().mapToInt (Primitive::getNumLines).sum ();
	}

	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner);
		name = readString (scanner);
		maxRadius = readFloat (scanner);
		int size = readInt (scanner);
		primitives = new ArrayList<> (size);

		PrimitiveType primitiveType;
		for (int i = 0; i < size; i++) {
			primitiveType = PrimitiveType.valueOf (readString (scanner));
			primitives.add (primitiveType.constructor.create (scanner));
		}
		scanner.nextLine (); // }
	}


	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (toChars8 (getNumInternalLines ())).append (" EffectStream")
		        .append ("\n{")
		        .append ("\n\tEffectName String '").append (name).append ('\'')
		        .append ("\n\tEffectMaxRadius Float ").append (toFloat6 (maxRadius))
		        .append ("\n\tNumPrimitives Int ").append (primitives.size ());
		for (Primitive primitive : primitives) {
			sb.append (primitive.toCode (1));
		}
		return sb.append ("\n}");
	}

	@Override
	public String toString () {
		return name;
	}

}
