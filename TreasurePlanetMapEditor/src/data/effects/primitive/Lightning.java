/**
 *
 */
package data.effects.primitive;

import java.util.List;
import java.util.Scanner;

import data.effects.elements.BlendRenderInfo;
import data.effects.elements.ColorFader;
import data.effects.elements.TextureRenderInfo;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * Lightning
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class Lightning extends Primitive {

	public float				beamLength;
	public float				segmentLength;
	public float				segmentWidth;
	public float				segmentOverlap;
	public float				noise;
	public float				previousPositionWeight;
	public ColorFader			colorFader;
	public BlendRenderInfo		blendRenderInfo;
	public TextureRenderInfo	textureRenderInfo;




	/**
	 * Creates a new {@link Lightning}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Lightning (Scanner scanner) throws UnexpectedTokenException {
		super ("Lightning", scanner);
	}


	/**
	 * Creates a new {@link Lightning}
	 *
	 * @param name
	 * @param duration
	 * @param startDelay
	 * @param useWorldCoordInternaly
	 * @param primitiveDescription
	 * @param variables
	 * @param renderPassNumber
	 * @param everlasting
	 * @param updateWhenFaded
	 * @param doneWhenFaded
	 * @param beamLength
	 * @param segmentLength
	 * @param segmentWidth
	 * @param segmentOverlap
	 * @param noise
	 * @param previousPositionWeight
	 * @param colorFader
	 * @param blendRenderInfo
	 * @param textureRenderInfo
	 */
	public Lightning (String name, float duration, float startDelay, boolean useWorldCoordInternaly, String primitiveDescription, List <String> variables, int renderPassNumber, boolean everlasting, boolean updateWhenFaded, boolean doneWhenFaded,
	                  float beamLength, float segmentLength, float segmentWidth, float segmentOverlap, float noise, float previousPositionWeight, ColorFader colorFader, BlendRenderInfo blendRenderInfo, TextureRenderInfo textureRenderInfo) {
		super (name, duration, startDelay, useWorldCoordInternaly, primitiveDescription, variables, renderPassNumber, everlasting, updateWhenFaded, doneWhenFaded);
		this.beamLength = beamLength;
		this.segmentLength = segmentLength;
		this.segmentWidth = segmentWidth;
		this.segmentOverlap = segmentOverlap;
		this.noise = noise;
		this.previousPositionWeight = previousPositionWeight;
		this.colorFader = colorFader;
		this.blendRenderInfo = blendRenderInfo;
		this.textureRenderInfo = textureRenderInfo;
	}


	@Override
	public void readCodeSub (Scanner scanner) throws UnexpectedTokenException {
		beamLength = readFloat (scanner);
		segmentLength = readFloat (scanner);
		segmentWidth = readFloat (scanner);
		segmentOverlap = readFloat (scanner);
		noise = readFloat (scanner);
		previousPositionWeight = readFloat (scanner);
		colorFader = new ColorFader (scanner);
		blendRenderInfo = new BlendRenderInfo (scanner);
		textureRenderInfo = new TextureRenderInfo (scanner);
	}

	@Override
	public final int getNumInternalLinesSub () {
		return 6 +
		       colorFader.getNumLines () +
		       blendRenderInfo.getNumLines () +
		       textureRenderInfo.getNumLines ();
	}

	@Override
	public void toCodeSub (StringBuilder sb, int indentation) {
		String ind = indentationsN[indentation];
		sb
		        .append (ind).append ("BeamLength Float ").append (toFloat6 (beamLength))
		        .append (ind).append ("SegmentLength Float ").append (toFloat6 (segmentLength))
		        .append (ind).append ("SegmentWidth Float ").append (toFloat6 (segmentWidth))
		        .append (ind).append ("SegmentOverlap Float ").append (toFloat6 (segmentOverlap))
		        .append (ind).append ("Noise Float ").append (toFloat6 (noise))
		        .append (ind).append ("PreviousPositionWeight Float ").append (toFloat6 (previousPositionWeight))
		        .append (colorFader.toCode (indentation))
		        .append (blendRenderInfo.toCode (indentation))
		        .append (textureRenderInfo.toCode (indentation));
	}

}
