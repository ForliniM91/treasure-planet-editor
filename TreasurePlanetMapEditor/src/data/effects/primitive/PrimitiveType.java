package data.effects.primitive;

import interfaces.Constructor;


/**
 * The primitive types
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public enum PrimitiveType {

	GenericParticleSystem (data.effects.primitive.GenericParticleSystem::new),
	Lightning (data.effects.primitive.Lightning::new),
	River (data.effects.primitive.River::new),
	Trail (data.effects.primitive.Trail::new),
	Vortex (data.effects.primitive.Vortex::new),
	;


	/** The constructor for the relative Primitive */
	public Constructor <Primitive> constructor;

	/**
	 * Creates a new {@link PrimitiveType}
	 *
	 * @param constructor
	 */
	private PrimitiveType (Constructor <Primitive> constructor) {
		this.constructor = constructor;
	}

}
