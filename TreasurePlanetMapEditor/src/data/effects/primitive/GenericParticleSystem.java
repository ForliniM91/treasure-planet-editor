/**
 *
 */
package data.effects.primitive;

import java.util.List;
import java.util.Scanner;

import data.effects.elements.Chunk;
import data.effects.elements.ChunkEmitter;
import data.effects.elements.ChunkSourceEmitter;
import data.effects.elements.PropertyChunk;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * GenericParticleSystem
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class GenericParticleSystem extends Primitive {

	public ChunkEmitter			positionEmitter;
	public ChunkEmitter			velocityEmitter;
	public ChunkEmitter			CHUNKDATA_POSITION_ANGLE_EMITTER;
	public ChunkEmitter			CHUNKDATA_VELOCITY_ANGLE_EMITTER;
	public ChunkSourceEmitter	CHUNKDATA_SOURCE_EMITTER;
	public Chunk				particleRateInterpolator;

	public int					numberParticlesAtStart;
	public boolean				CHUNKDATA_ENABLE_TARGET_EMITTER;
	public boolean				CHUNKDATA_TARGET_EMITTER_USE_POSITION_FOR_VELOCITY;
	public boolean				CHUNKDATA_ENABLE_ANGLE_EMITTER;
	public boolean				CHUNKDATA_ANGLE_EMITTER_USE_POSITION_FOR_VELOCITY;
	public float				lowDetailParticleRateMultiply;
	public int					lowDetailSpawnInfosUpdate;
	public int					hightDetailSpawnInfosUpdate;

	public PropertyChunk		spawnedEachTimePropertiesChunk;
	public PropertyChunk		commonPropertiesChunk;





	/**
	 * Creates a new {@link GenericParticleSystem}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public GenericParticleSystem (Scanner scanner) throws UnexpectedTokenException {
		super ("GenericParticleSystem", scanner);
	}


	/**
	 * Creates a new {@link GenericParticleSystem}
	 *
	 * @param name
	 * @param duration
	 * @param startDelay
	 * @param useWorldCoordInternaly
	 * @param primitiveDescription
	 * @param variables
	 * @param renderPassNumber
	 * @param everlasting
	 * @param updateWhenFaded
	 * @param doneWhenFaded
	 * @param positionEmitter
	 * @param velocityEmitter
	 * @param CHUNKDATA_POSITION_ANGLE_EMITTER
	 * @param CHUNKDATA_VELOCITY_ANGLE_EMITTER
	 * @param CHUNKDATA_SOURCE_EMITTER
	 * @param particleRateInterpolator
	 * @param numberParticlesAtStart
	 * @param CHUNKDATA_ENABLE_TARGET_EMITTER
	 * @param CHUNKDATA_TARGET_EMITTER_USE_POSITION_FOR_VELOCITY
	 * @param CHUNKDATA_ENABLE_ANGLE_EMITTER
	 * @param CHUNKDATA_ANGLE_EMITTER_USE_POSITION_FOR_VELOCITY
	 * @param lowDetailParticleRateMultiply
	 * @param lowDetailSpawnInfosUpdate
	 * @param hightDetailSpawnInfosUpdate
	 * @param spawnedEachTimePropertiesChunk
	 * @param commonPropertiesChunk
	 */
	public GenericParticleSystem (String name, float duration, float startDelay, boolean useWorldCoordInternaly, String primitiveDescription, List <String> variables, int renderPassNumber, boolean everlasting, boolean updateWhenFaded, boolean doneWhenFaded,
	                              ChunkEmitter positionEmitter, ChunkEmitter velocityEmitter, ChunkEmitter CHUNKDATA_POSITION_ANGLE_EMITTER, ChunkEmitter CHUNKDATA_VELOCITY_ANGLE_EMITTER, ChunkSourceEmitter CHUNKDATA_SOURCE_EMITTER, Chunk particleRateInterpolator,
	                              int numberParticlesAtStart, boolean CHUNKDATA_ENABLE_TARGET_EMITTER, boolean CHUNKDATA_TARGET_EMITTER_USE_POSITION_FOR_VELOCITY, boolean CHUNKDATA_ENABLE_ANGLE_EMITTER, boolean CHUNKDATA_ANGLE_EMITTER_USE_POSITION_FOR_VELOCITY,
	                              float lowDetailParticleRateMultiply, int lowDetailSpawnInfosUpdate, int hightDetailSpawnInfosUpdate, PropertyChunk spawnedEachTimePropertiesChunk, PropertyChunk commonPropertiesChunk) {
		super (name, duration, startDelay, useWorldCoordInternaly, primitiveDescription, variables, renderPassNumber, everlasting, updateWhenFaded, doneWhenFaded);
		this.positionEmitter = positionEmitter;
		this.velocityEmitter = velocityEmitter;
		this.CHUNKDATA_POSITION_ANGLE_EMITTER = CHUNKDATA_POSITION_ANGLE_EMITTER;
		this.CHUNKDATA_VELOCITY_ANGLE_EMITTER = CHUNKDATA_VELOCITY_ANGLE_EMITTER;
		this.CHUNKDATA_SOURCE_EMITTER = CHUNKDATA_SOURCE_EMITTER;
		this.particleRateInterpolator = particleRateInterpolator;
		this.numberParticlesAtStart = numberParticlesAtStart;
		this.CHUNKDATA_ENABLE_TARGET_EMITTER = CHUNKDATA_ENABLE_TARGET_EMITTER;
		this.CHUNKDATA_TARGET_EMITTER_USE_POSITION_FOR_VELOCITY = CHUNKDATA_TARGET_EMITTER_USE_POSITION_FOR_VELOCITY;
		this.CHUNKDATA_ENABLE_ANGLE_EMITTER = CHUNKDATA_ENABLE_ANGLE_EMITTER;
		this.CHUNKDATA_ANGLE_EMITTER_USE_POSITION_FOR_VELOCITY = CHUNKDATA_ANGLE_EMITTER_USE_POSITION_FOR_VELOCITY;
		this.lowDetailParticleRateMultiply = lowDetailParticleRateMultiply;
		this.lowDetailSpawnInfosUpdate = lowDetailSpawnInfosUpdate;
		this.hightDetailSpawnInfosUpdate = hightDetailSpawnInfosUpdate;
		this.spawnedEachTimePropertiesChunk = spawnedEachTimePropertiesChunk;
		this.commonPropertiesChunk = commonPropertiesChunk;
	}





	@Override
	public void readCodeSub (Scanner scanner) throws UnexpectedTokenException {
		positionEmitter = new ChunkEmitter (scanner);
		velocityEmitter = new ChunkEmitter (scanner);
		CHUNKDATA_POSITION_ANGLE_EMITTER = new ChunkEmitter (scanner);
		CHUNKDATA_VELOCITY_ANGLE_EMITTER = new ChunkEmitter (scanner);
		CHUNKDATA_SOURCE_EMITTER = new ChunkSourceEmitter (scanner);
		particleRateInterpolator = new Chunk (scanner);
		numberParticlesAtStart = readInt (scanner);
		CHUNKDATA_ENABLE_TARGET_EMITTER = readBoolean (scanner);
		CHUNKDATA_TARGET_EMITTER_USE_POSITION_FOR_VELOCITY = readBoolean (scanner);
		CHUNKDATA_ENABLE_ANGLE_EMITTER = readBoolean (scanner);
		CHUNKDATA_ANGLE_EMITTER_USE_POSITION_FOR_VELOCITY = readBoolean (scanner);
		lowDetailParticleRateMultiply = readFloat (scanner);
		lowDetailSpawnInfosUpdate = readInt (scanner);
		hightDetailSpawnInfosUpdate = readInt (scanner);
		spawnedEachTimePropertiesChunk = new PropertyChunk (scanner);
		commonPropertiesChunk = new PropertyChunk (scanner);
	}

	@Override
	public int getNumInternalLinesSub () {
		return 8 + positionEmitter.getNumLines () + velocityEmitter.getNumLines () +
		       CHUNKDATA_POSITION_ANGLE_EMITTER.getNumLines () + CHUNKDATA_VELOCITY_ANGLE_EMITTER.getNumLines () +
		       CHUNKDATA_SOURCE_EMITTER.getNumLines () + particleRateInterpolator.getNumLines () +
		       spawnedEachTimePropertiesChunk.getNumLines () + commonPropertiesChunk.getNumLines ();
	}

	@Override
	public void toCodeSub (StringBuilder sb, int indentation) {
		String ind = indentationsN[indentation];
		sb
		        .append (positionEmitter.toCode (indentation))
		        .append (velocityEmitter.toCode (indentation))
		        .append (CHUNKDATA_POSITION_ANGLE_EMITTER.toCode (indentation))
		        .append (CHUNKDATA_VELOCITY_ANGLE_EMITTER.toCode (indentation))
		        .append (CHUNKDATA_SOURCE_EMITTER.toCode (indentation))
		        .append (particleRateInterpolator.toCode (indentation))
		        .append (ind).append ("NumberParticlesAtStart Int ").append (numberParticlesAtStart)
		        .append (ind).append ("CHUNKDATA_ENABLE_TARGET_EMITTER Bool ").append (toBool (CHUNKDATA_ENABLE_TARGET_EMITTER))
		        .append (ind).append ("CHUNKDATA_TARGET_EMITTER_USE_POSITION_FOR_VELOCITY Bool ").append (toBool (CHUNKDATA_TARGET_EMITTER_USE_POSITION_FOR_VELOCITY))
		        .append (ind).append ("CHUNKDATA_ENABLE_ANGLE_EMITTER Bool ").append (toBool (CHUNKDATA_ENABLE_ANGLE_EMITTER))
		        .append (ind).append ("CHUNKDATA_ANGLE_EMITTER_USE_POSITION_FOR_VELOCITY Bool ").append (toBool (CHUNKDATA_ANGLE_EMITTER_USE_POSITION_FOR_VELOCITY))
		        .append (ind).append ("LowDetailParticleRateMultiply Float ").append (toFloat6 (lowDetailParticleRateMultiply))
		        .append (ind).append ("LowDetailSpawnInfosUpdate Int ").append (lowDetailSpawnInfosUpdate)
		        .append (ind).append ("HightDetailSpawnInfosUpdate Int ").append (hightDetailSpawnInfosUpdate)
		        .append (spawnedEachTimePropertiesChunk.toCode (indentation))
		        .append (commonPropertiesChunk.toCode (indentation));
	}

}
