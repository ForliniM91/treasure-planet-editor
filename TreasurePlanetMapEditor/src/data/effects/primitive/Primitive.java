/**
 *
 */
package data.effects.primitive;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import data.worldObjects.elements.UnexpectedTokenException;
import interfaces.ReadCode;
import interfaces.WriteCode;


/**
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public abstract class Primitive implements ReadCode, WriteCode {

	public String			name;
	public float			duration;
	public float			startDelay;
	public boolean			useWorldCoordInternaly;
	public String			primitiveDescription;
	public List <String>	variables;
	public int				renderPassNumber;
	public boolean			everlasting;
	public boolean			updateWhenFaded;
	public boolean			doneWhenFaded;


	/**
	 * Creates a new {@link Primitive}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Primitive (String name, Scanner scanner) throws UnexpectedTokenException {
		readCode (scanner);
	}


	/**
	 * Creates a new {@link Primitive}
	 *
	 * @param name
	 * @param duration
	 * @param startDelay
	 * @param useWorldCoordInternaly
	 * @param primitiveDescription
	 * @param variables
	 * @param renderPassNumber
	 * @param everlasting
	 * @param updateWhenFaded
	 * @param doneWhenFaded
	 */
	public Primitive (String name, float duration, float startDelay, boolean useWorldCoordInternaly, String primitiveDescription,
	                  List <String> variables, int renderPassNumber, boolean everlasting, boolean updateWhenFaded, boolean doneWhenFaded) {
		this.name = name;
		this.duration = duration;
		this.startDelay = startDelay;
		this.useWorldCoordInternaly = useWorldCoordInternaly;
		this.primitiveDescription = primitiveDescription;
		this.variables = variables;
		this.renderPassNumber = renderPassNumber;
		this.everlasting = everlasting;
		this.updateWhenFaded = updateWhenFaded;
		this.doneWhenFaded = doneWhenFaded;
	}



	@Override
	public void readCode (Scanner scanner) throws UnexpectedTokenException {
		int n = readNumLines (scanner); // <number of lines> ConstData

		scanner.nextLine (); // {
		duration = readFloat (scanner);
		startDelay = readFloat (scanner);
		useWorldCoordInternaly = readBoolean (scanner);
		primitiveDescription = readString (scanner);
		int size = readInt (scanner);
		variables = new ArrayList<> (size);
		for (int i = 0; i < size; i++) {
			variables.add (readString (scanner));
		}
		renderPassNumber = readInt (scanner);
		everlasting = readBoolean (scanner);
		updateWhenFaded = readBoolean (scanner);
		doneWhenFaded = readBoolean (scanner);
		readCodeSub (scanner);
		scanner.nextLine (); // }

		checkNumLines (n);
	}


	public abstract void readCodeSub (Scanner scanner) throws UnexpectedTokenException;


	@Override
	public int getNumInternalLines () {
		return 9 + variables.size () + getNumInternalLinesSub ();
	}

	public abstract int getNumInternalLinesSub ();



	@Override
	public CharSequence toCode (int indentation) {
		StringBuilder sb = new StringBuilder (20000)
		        .append ("\n\t").append (getNumInternalLines ()).append (" Primitive")
		        .append ("\n\t{")
		        .append ("\n\tDuration Float ").append (toFloat6 (duration))
		        .append ("\n\tStartDelay Float ").append (toFloat6 (startDelay))
		        .append ("\n\tUseWorldCoordInternaly Bool ").append (toBool (useWorldCoordInternaly))
		        .append ("\n\tPrimitiveDescription String '").append (primitiveDescription).append ('\'')
		        .append ("\n\tCHUNKDATA_CONST_ACCEPTS_EFFECT_VARIABLES_STRING_VECTOR - Size Int ").append (variables.size ());
		for (String variable : variables) {
			sb.append ("\n\tCHUNKDATA_CONST_ACCEPTS_EFFECT_VARIABLES_STRING_VECTOR - Element String '").append (variable).append ('\'');
		}
		sb.append ("\n\tRenderPassNumber Int ").append (renderPassNumber)
		        .append ("\n\tEverlasting Bool ").append (toBool (everlasting))
		        .append ("\n\tUpdateWhenFaded Bool ").append (toBool (updateWhenFaded))
		        .append ("\n\tDoneWhenFaded Bool ").append (toBool (doneWhenFaded));
		toCodeSub (sb, indentation);
		return sb.append ("\n\t}");
	}

	public abstract void toCodeSub (StringBuilder sb, int indentation);

	@Override
	public String toString () {
		return name;
	}

}
