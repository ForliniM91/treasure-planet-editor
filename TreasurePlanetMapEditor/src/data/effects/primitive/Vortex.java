/**
 *
 */
package data.effects.primitive;

import java.util.List;
import java.util.Scanner;

import data.effects.elements.TextureRenderInfo;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * Vortex
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class Vortex extends Primitive {

	public TextureRenderInfo	TEXTURE_RENDER_INFO;
	public String				VORTEX_POINT_EFFECT_NAME;
	public float				SPAWN_PROBABILITY;
	public float				START_RADIUS;
	public float				KILL_RADIUS;
	public float				FADE_RADIUS;
	public float				RADIUS_DELTA;
	public float				ANGLE_DELTA_START;
	public float				ANGLE_DELTA_CHANGE;
	public float				CENTER_Z_OFFSET;
	public float				Z_DELTA_DELTA_ADD;
	public float				UP_CHANGE;
	public float				HIGHT_POINT;




	/**
	 * Creates a new {@link Vortex}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Vortex (Scanner scanner) throws UnexpectedTokenException {
		super ("Vortex", scanner);
	}


	/**
	 * Creates a new {@link Vortex}
	 *
	 * @param name
	 * @param duration
	 * @param startDelay
	 * @param useWorldCoordInternaly
	 * @param primitiveDescription
	 * @param variables
	 * @param renderPassNumber
	 * @param everlasting
	 * @param updateWhenFaded
	 * @param doneWhenFaded
	 * @param TEXTURE_RENDER_INFO
	 * @param VORTEX_POINT_EFFECT_NAME
	 * @param SPAWN_PROBABILITY
	 * @param sTART_RADIUS
	 * @param kILL_RADIUS
	 * @param fADE_RADIUS
	 * @param rADIUS_DELTA
	 * @param aNGLE_DELTA_START
	 * @param aNGLE_DELTA_CHANGE
	 * @param cENTER_Z_OFFSET
	 * @param z_DELTA_DELTA_ADD
	 * @param uP_CHANGE
	 * @param hIGHT_POINT
	 */
	public Vortex (String name, float duration, float startDelay, boolean useWorldCoordInternaly, String primitiveDescription, List <String> variables, int renderPassNumber, boolean everlasting, boolean updateWhenFaded, boolean doneWhenFaded,
	               TextureRenderInfo TEXTURE_RENDER_INFO, String VORTEX_POINT_EFFECT_NAME, float SPAWN_PROBABILITY, float START_RADIUS, float KILL_RADIUS, float FADE_RADIUS, float RADIUS_DELTA, float ANGLE_DELTA_START, float ANGLE_DELTA_CHANGE, float CENTER_Z_OFFSET, float Z_DELTA_DELTA_ADD, float UP_CHANGE, float HIGHT_POINT) {
		super (name, duration, startDelay, useWorldCoordInternaly, primitiveDescription, variables, renderPassNumber, everlasting, updateWhenFaded, doneWhenFaded);
		this.TEXTURE_RENDER_INFO = TEXTURE_RENDER_INFO;
		this.VORTEX_POINT_EFFECT_NAME = VORTEX_POINT_EFFECT_NAME;
		this.SPAWN_PROBABILITY = SPAWN_PROBABILITY;
		this.START_RADIUS = START_RADIUS;
		this.KILL_RADIUS = KILL_RADIUS;
		this.FADE_RADIUS = FADE_RADIUS;
		this.RADIUS_DELTA = RADIUS_DELTA;
		this.ANGLE_DELTA_START = ANGLE_DELTA_START;
		this.ANGLE_DELTA_CHANGE = ANGLE_DELTA_CHANGE;
		this.CENTER_Z_OFFSET = CENTER_Z_OFFSET;
		this.Z_DELTA_DELTA_ADD = Z_DELTA_DELTA_ADD;
		this.UP_CHANGE = UP_CHANGE;
		this.HIGHT_POINT = HIGHT_POINT;
	}


	@Override
	public void readCodeSub (Scanner scanner) throws UnexpectedTokenException {
		TEXTURE_RENDER_INFO = new TextureRenderInfo (scanner);
		VORTEX_POINT_EFFECT_NAME = readString (scanner);
		SPAWN_PROBABILITY = readFloat (scanner);
		START_RADIUS = readFloat (scanner);
		KILL_RADIUS = readFloat (scanner);
		FADE_RADIUS = readFloat (scanner);
		RADIUS_DELTA = readFloat (scanner);
		ANGLE_DELTA_START = readFloat (scanner);
		ANGLE_DELTA_CHANGE = readFloat (scanner);
		CENTER_Z_OFFSET = readFloat (scanner);
		Z_DELTA_DELTA_ADD = readFloat (scanner);
		UP_CHANGE = readFloat (scanner);
		HIGHT_POINT = readFloat (scanner);
	}

	@Override
	public final int getNumInternalLinesSub () {
		return 12 + TEXTURE_RENDER_INFO.getNumLines ();
	}

	@Override
	public void toCodeSub (StringBuilder sb, int indentation) {
		String ind = indentationsN[indentation];
		sb
		        .append (TEXTURE_RENDER_INFO.toCode (indentation))
		        .append (ind).append ("VORTEX_POINT_EFFECT_NAME String '").append (VORTEX_POINT_EFFECT_NAME).append ('\'')
		        .append (ind).append ("SPAWN_PROBABILITY Float ").append (toFloat6 (SPAWN_PROBABILITY))
		        .append (ind).append ("START_RADIUS Float ").append (toFloat6 (START_RADIUS))
		        .append (ind).append ("KILL_RADIUS Float ").append (toFloat6 (KILL_RADIUS))
		        .append (ind).append ("FADE_RADIUS Float ").append (toFloat6 (FADE_RADIUS))
		        .append (ind).append ("RADIUS_DELTA Float ").append (toFloat6 (RADIUS_DELTA))
		        .append (ind).append ("ANGLE_DELTA_START Float ").append (toFloat6 (ANGLE_DELTA_START))
		        .append (ind).append ("ANGLE_DELTA_CHANGE Float ").append (toFloat6 (ANGLE_DELTA_CHANGE))
		        .append (ind).append ("CENTER_Z_OFFSET Float ").append (toFloat6 (CENTER_Z_OFFSET))
		        .append (ind).append ("Z_DELTA_DELTA_ADD Float ").append (toFloat6 (Z_DELTA_DELTA_ADD))
		        .append (ind).append ("UP_CHANGE Float ").append (toFloat6 (UP_CHANGE))
		        .append (ind).append ("HIGHT_POINT Float ").append (toFloat6 (HIGHT_POINT));
	}

}
