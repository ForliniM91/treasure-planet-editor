/**
 *
 */
package data.effects.primitive;

import java.util.List;
import java.util.Scanner;

import data.effects.elements.TextureRenderInfo;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * River
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class River extends Primitive {

	public TextureRenderInfo TEXTURE_RENDER_INFO;




	/**
	 * Creates a new {@link River}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public River (Scanner scanner) throws UnexpectedTokenException {
		super ("River", scanner);
	}


	/**
	 * Creates a new {@link River}
	 *
	 * @param name
	 * @param duration
	 * @param startDelay
	 * @param useWorldCoordInternaly
	 * @param primitiveDescription
	 * @param variables
	 * @param renderPassNumber
	 * @param everlasting
	 * @param updateWhenFaded
	 * @param doneWhenFaded
	 * @param TEXTURE_RENDER_INFO
	 */
	public River (String name, float duration, float startDelay, boolean useWorldCoordInternaly, String primitiveDescription, List <String> variables, int renderPassNumber, boolean everlasting, boolean updateWhenFaded, boolean doneWhenFaded,
	              TextureRenderInfo TEXTURE_RENDER_INFO) {
		super (name, duration, startDelay, useWorldCoordInternaly, primitiveDescription, variables, renderPassNumber, everlasting, updateWhenFaded, doneWhenFaded);
		this.TEXTURE_RENDER_INFO = TEXTURE_RENDER_INFO;
	}


	@Override
	public void readCodeSub (Scanner scanner) throws UnexpectedTokenException {
		TEXTURE_RENDER_INFO = new TextureRenderInfo (scanner);
	}

	@Override
	public final int getNumInternalLinesSub () {
		return TEXTURE_RENDER_INFO.getNumLines ();
	}

	@Override
	public void toCodeSub (StringBuilder sb, int indentation) {
		sb.append (TEXTURE_RENDER_INFO.toCode (indentation));
	}

}
