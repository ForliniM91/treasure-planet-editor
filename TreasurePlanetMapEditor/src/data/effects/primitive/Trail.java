/**
 *
 */
package data.effects.primitive;

import java.util.List;
import java.util.Scanner;

import data.effects.elements.TextureRenderInfo;
import data.worldObjects.elements.UnexpectedTokenException;


/**
 * Trail
 *
 * @author MarcoForlini
 */
@SuppressWarnings ("javadoc")
public class Trail extends Primitive {

	public TextureRenderInfo	TEXTURE_RENDER_INFO;
	public float				backDrift;
	public float				outDrift;
	public float				initialWidth;
	public boolean				textureFlowBack;
	public boolean				generateEvenWhenFaded;



	/**
	 * Creates a new {@link Trail}
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	public Trail (Scanner scanner) throws UnexpectedTokenException {
		super ("Trail", scanner);
	}


	/**
	 * Creates a new {@link Trail}
	 *
	 * @param name
	 * @param duration
	 * @param startDelay
	 * @param useWorldCoordInternaly
	 * @param primitiveDescription
	 * @param variables
	 * @param renderPassNumber
	 * @param everlasting
	 * @param updateWhenFaded
	 * @param doneWhenFaded
	 * @param TEXTURE_RENDER_INFO
	 */
	public Trail (String name, float duration, float startDelay, boolean useWorldCoordInternaly, String primitiveDescription, List <String> variables, int renderPassNumber, boolean everlasting, boolean updateWhenFaded, boolean doneWhenFaded,
	              TextureRenderInfo TEXTURE_RENDER_INFO, float backDrift, float outDrift, float initialWidth, boolean textureFlowBack, boolean generateEvenWhenFaded) {
		super (name, duration, startDelay, useWorldCoordInternaly, primitiveDescription, variables, renderPassNumber, everlasting, updateWhenFaded, doneWhenFaded);
		this.TEXTURE_RENDER_INFO = TEXTURE_RENDER_INFO;
		this.backDrift = backDrift;
		this.outDrift = outDrift;
		this.initialWidth = initialWidth;
		this.textureFlowBack = textureFlowBack;
		this.generateEvenWhenFaded = generateEvenWhenFaded;
	}


	@Override
	public void readCodeSub (Scanner scanner) throws UnexpectedTokenException {
		TEXTURE_RENDER_INFO = new TextureRenderInfo (scanner);
		backDrift = readFloat (scanner);
		outDrift = readFloat (scanner);
		initialWidth = readFloat (scanner);
		textureFlowBack = readBoolean (scanner);
		generateEvenWhenFaded = readBoolean (scanner);
	}


	@Override
	public final int getNumInternalLinesSub () {
		return 5 + TEXTURE_RENDER_INFO.getNumLines ();
	}


	@Override
	public void toCodeSub (StringBuilder sb, int indentation) {
		String ind = indentationsN[indentation];
		sb
		        .append (TEXTURE_RENDER_INFO.toCode (indentation))
		        .append (ind).append ("BackDrift Float ").append (toFloat6 (backDrift))
		        .append (ind).append ("OutDrift Float ").append (toFloat6 (outDrift))
		        .append (ind).append ("InitialWidth Float ").append (toFloat6 (initialWidth))
		        .append (ind).append ("TextureFlowBack Bool ").append (toBool (textureFlowBack))
		        .append (ind).append ("GenerateEvenWhenFaded Bool ").append (toBool (generateEvenWhenFaded));
	}

}
