package interfaces;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedNumLines;
import data.worldObjects.elements.UnexpectedTokenException;
import entities.Vector3;


/**
 * An object which can read a line of code
 *
 * @author MarcoForlini
 */
public interface ReadCode extends Codeline {

	/**
	 * Initialize the ConstData from the given scanner
	 *
	 * @param scanner The scanner to read
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	void readCode (Scanner scanner) throws UnexpectedTokenException;




	/**
	 * Check the given number of lines against the actual number of lines of the object.
	 * Throw an exception if the numbers don't match
	 *
	 * @param numLines The expected number of lines
	 * @throws UnexpectedNumLines if the expected number of lines doesn't match the actual number of lines
	 */
	default void checkNumLines (int numLines) throws UnexpectedNumLines {
		if (numLines != getNumInternalLines ()) {
			throw new UnexpectedNumLines (Integer.toString (numLines));
		}
	}

	/**
	 * Return the number of lines in the following section of the scanner
	 *
	 * @param scanner The scanner
	 * @return The number of lines in the following section
	 */
	default int readNumLines (Scanner scanner) {
		String line = scanner.nextLine ();
		return Integer.parseInt (line.trim ().substring (0, 8), 10);
	}

	/**
	 * Check if the number of lines in the following section of the scanner match the expected number
	 *
	 * @param scanner The scanner
	 * @param expected The expected number of lines
	 * @throws UnexpectedNumLines If the number of lines doesn't match the expected value
	 */
	default void readNumLines (Scanner scanner, int expected) throws UnexpectedNumLines {
		int n = readNumLines (scanner);
		if (n != expected) {
			throw new UnexpectedNumLines (Integer.toString (n));
		}
	}

	/**
	 * Extract a string from the next line
	 *
	 * @param scanner The scanner
	 * @return The string
	 */
	default String readString (Scanner scanner) {
		String line = scanner.nextLine ();
		int begin = line.indexOf ('\'');
		return line.substring (begin + 1, line.length () - 1);
	}

	/**
	 * Extract an integer value from the next line
	 *
	 * @param scanner The scanner
	 * @return The integer value
	 */
	default int readInt (Scanner scanner) {
		String line = scanner.nextLine ();
		int begin = line.lastIndexOf (' ');
		return Integer.parseInt (line.substring (begin + 1), 10);
	}

	/**
	 * Extract a float value from the next line
	 *
	 * @param scanner The scanner
	 * @return The float value
	 */
	default float readFloat (Scanner scanner) {
		String line = scanner.nextLine ();
		int begin = line.lastIndexOf (' ');
		return Float.parseFloat (line.substring (begin + 1));
	}

	/**
	 * Extract a boolean value from the next line
	 *
	 * @param scanner The scanner
	 * @return The boolean value
	 */
	default boolean readBoolean (Scanner scanner) {
		String line = scanner.nextLine ();
		int begin = line.lastIndexOf (' ', line.length () - 2); // -2, because value False ends with a whitespace: "... Bool False ", and we must avoid that
		return line.substring (begin + 1).equals ("True");
	}

	/**
	 * Extract a Vector3 from the next line
	 *
	 * @param scanner The scanner
	 * @return The Vector3
	 */
	default Vector3 readVector (Scanner scanner) {
		String line = scanner.nextLine ();
		int min = line.indexOf ("Vector3") + 9;
		int max = line.indexOf (',', min + 8);
		float x0 = Float.parseFloat (line.substring (min, max));
		min = max + 2;
		max = line.indexOf (',', min + 8);
		float x1 = Float.parseFloat (line.substring (min, max));
		min = max + 2;
		max = line.indexOf (',', min + 8);
		float x2 = Float.parseFloat (line.substring (min, max));
		return new Vector3 (x0, x1, x2);
	}

}
