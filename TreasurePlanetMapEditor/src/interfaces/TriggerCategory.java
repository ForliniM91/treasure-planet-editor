package interfaces;

public interface TriggerCategory <T extends TriggerElementType> {

	T[] getTypes ();

}
