/**
 *
 */
package interfaces;

import java.util.Scanner;

import data.worldObjects.elements.UnexpectedTokenException;


/**
 * A constructor which accept a scanner and create an object of type T
 *
 * @author MarcoForlini
 * @param <T> The type of object to build
 */
public interface Constructor <T> {

	/**
	 * Create a new T
	 *
	 * @param scanner The scanner to read
	 * @return a new T
	 * @throws UnexpectedTokenException If the scanner contains an unexpected token
	 */
	T create (Scanner scanner) throws UnexpectedTokenException;

}
