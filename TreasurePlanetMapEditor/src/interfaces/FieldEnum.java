package interfaces;

/**
 * An enumeration for a fixed list of values which can be assigned to a variable in the files
 *
 * @author MarcoForlini
 */
public interface FieldEnum extends WriteCode {

}
