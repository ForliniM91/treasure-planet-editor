package interfaces;

import entities.Field;


/**
 * A trigger element type
 *
 * @author MarcoForlini
 */
public interface TriggerElementType extends WriteCode {

	/** None string */
	String NONE = "";

	/**
	 * Get the index for the element type
	 *
	 * @return The index for the element type
	 */
	int getIndex ();

	/**
	 * Get the game code for the element type.
	 *
	 * @return The game code for the element type.
	 */
	String getCode ();

	/**
	 * Get the displayed name for the element type.
	 *
	 * @return The displayed name for the element type.
	 */
	String getName ();

	/**
	 * Get the description for the element type.
	 *
	 * @return The description for the element type.
	 */
	String getDesc ();

	/**
	 * Get the required fields for this element type.
	 *
	 * @return The required fields for this element type.
	 */
	Field[] getFields ();

}
