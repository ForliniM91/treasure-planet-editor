/**
 *
 */
package interfaces;

/**
 * @author MarcoForlini
 */
public interface Codeline {

	/**
	 * Gets the number of internal lines
	 *
	 * @return the number of internal lines
	 */
	int getNumInternalLines ();

	/**
	 * Gets the total number of lines
	 *
	 * @return the total number of lines
	 */
	default int getNumLines () {
		return 3 + getNumInternalLines ();
	}

}
