package interfaces;

import java.text.NumberFormat;


/**
 * An object which can write a line of code
 *
 * @author MarcoForlini
 */
public interface WriteCode {

	/** Indentations */
	String indentations[] = {
	                          "",
	                          "\t", "\t\t", "\t\t\t",
	                          "\t\t\t\t", "\t\t\t\t\t", "\t\t\t\t\t\t",
	                          "\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t\t",
	                          "\t\t\t\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t\t\t\t\t",
	                          "\t\t\t\t\t\t\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	};


	/** Indentations starting with \n */
	String			indentationsN[]	= {
	                                    "\n",
	                                    "\n\t", "\n\t\t", "\n\t\t\t",
	                                    "\n\t\t\t\t", "\n\t\t\t\t\t", "\n\t\t\t\t\t\t",
	                                    "\n\t\t\t\t\t\t\t", "\n\t\t\t\t\t\t\t\t", "\n\t\t\t\t\t\t\t\t\t",
	                                    "\n\t\t\t\t\t\t\t\t\t\t", "\n\t\t\t\t\t\t\t\t\t\t\t", "\n\t\t\t\t\t\t\t\t\t\t\t\t",
	                                    "\n\t\t\t\t\t\t\t\t\t\t\t\t\t", "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t", "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	};

	NumberFormat	floatFormat3	= NumberFormat.getInstance ();

	NumberFormat	floatFormat6	= NumberFormat.getInstance ();

	NumberFormat	numLineFormat	= NumberFormat.getInstance ();


	/**
	 * Convert the element to game code
	 *
	 * @param indentation TODO
	 * @return the code
	 */
	CharSequence toCode (int indentation);


	/**
	 * Convert an integer to an 8-char String, filling with 0
	 *
	 * @param num The integer
	 * @return The 8-char String representation of the ID.
	 */
	default String toChars8 (int num) {
		return numLineFormat.format (num);
	}


	/**
	 * Format a number with 6 decimals and return a string containing it
	 *
	 * @param number The number to format
	 * @return A string containing the formatted number
	 */
	default String toFloat6 (double number) {
		return WriteCode.floatFormat6.format (number);
	}


	/**
	 * Format a number with 3 decimals and return a string containing it
	 *
	 * @param number The number to format
	 * @return A string containing the formatted number
	 */
	default String getFloat3 (double number) {
		return WriteCode.floatFormat3.format (number);
	}


	/**
	 * Convert the Java boolean to the game boolean string representation
	 *
	 * @param value The boolean
	 * @return The string representation
	 */
	default String toBool (boolean value) {
		return (value ? "True" : "False ");
	}

}
