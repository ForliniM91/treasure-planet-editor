package interfaces;

import entities.GameColor;


public interface Colorable {

	GameColor getColor ();

	void setColor (GameColor color);

}