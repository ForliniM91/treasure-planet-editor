package interfaces;

/**
 * Interface for an entity
 *
 * @author MarcoForlini
 * @param <T>
 */
public interface EntityInterface <T> extends Comparable <T>, WriteCode {

	/**
	 * Checks if the entry is "special" (it can't be moved/edited/cloned/deleted)
	 *
	 * @return <code>true</code> if special, false othwerise
	 */
	boolean isSpecial ();

	/**
	 * Clone this entity
	 *
	 * @return the new entity
	 */
	EntityInterface <T> clone ();

}
