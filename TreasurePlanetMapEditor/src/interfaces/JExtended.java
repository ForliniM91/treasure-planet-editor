package interfaces;

import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

import javax.accessibility.Accessible;

/**
 * An extended version of the JList/JComboBox
 *
 * @author MarcoForlini
 * @param <E> The type of elements
 */
public interface JExtended <E extends Comparable <? super E>> extends Accessible {

	/**
	 * Gets the elements
	 *
	 * @return the elements
	 */
	Vector <E> getVector ();

	/**
	 * Sets the elements
	 *
	 * @param newElements the elements
	 */
	default void setVector (E[] newElements) {
		setVector (new Vector<> (Arrays.asList (newElements)), false);
	}

	/**
	 * Sets the elements
	 *
	 * @param newElements the elements
	 */
	default void setVector (Vector <E> newElements) {
		setVector (newElements, false);
	}

	/**
	 * Sets the elements
	 *
	 * @param newElements the elements
	 * @param sorted if <code>true</code>, the elements will be sorted
	 */
	default void setVector (E[] newElements, boolean sorted) {
		setVector (new Vector<> (Arrays.asList (newElements)), sorted);
	}

	/**
	 * Sets the elements
	 *
	 * @param newElements the elements
	 * @param sorted if <code>true</code>, the elements will be sorted
	 */
	void setVector (Vector <E> newElements, boolean sorted);

	/**
	 * Gets the number of elements
	 *
	 * @return the number of elements
	 */
	default int getLength () {
		return getVector ().size ();
	}

	/**
	 * Returns the selected element
	 *
	 * @return the selected element
	 */
	E getSelectedElement ();

	/**
	 * Returns the selected index
	 *
	 * @return the selected index
	 */
	int getSelectedIndex ();

	/**
	 * Sets the given element
	 *
	 * @param element the element to select
	 */
	default void setSelectedElement (E element) {
		setSelectedElement (element, false);
	}

	/**
	 * Sets the given element
	 *
	 * @param element the element to select
	 * @param showElement if <code>true</code>, roll the scrollbars to show the element
	 */
	void setSelectedElement (E element, boolean showElement);

	/**
	 * Sets the given index
	 *
	 * @param index the index to select
	 */
	void setSelectedIndex (int index);

	/**
	 * Sets the given index
	 *
	 * @param index the index to select
	 * @param showElement if <code>true</code>, roll the scrollbars to show the element
	 */
	void setSelectedIndex (int index, boolean showElement);

	/**
	 * Gets the element at the given index
	 *
	 * @param index the index
	 * @return the element at the given index
	 */
	default E get (int index) {
		return getVector ().get (index);
	}

	/**
	 * Gets the index of the given element
	 *
	 * @param element the element
	 * @return the index of the given index
	 */
	default int indexOf (E element) {
		return getVector ().indexOf (element);
	}

	/**
	 * Assign a the given element at the given index
	 *
	 * @param index the index
	 * @param element the element to assign
	 */
	default void set (int index, E element) {
		getVector ().set (index, element);
	}

	/**
	 * Add the given element at the bottom
	 *
	 * @param element the element to add
	 * @return the index of the element
	 */
	default int add (E element) {
		return add (element, -1, false);
	}

	/**
	 * Add the given element at the given position
	 *
	 * @param element the element to add
	 * @param index the index of the element
	 * @return the index of the element
	 */
	default int add (E element, int index) {
		return add (element, index, false);
	}

	/**
	 * Add the given element at the bottom
	 *
	 * @param element the element to add
	 * @param sorted if true, the list will be sorted
	 * @return the index of the element
	 */
	default int add (E element, boolean sorted) {
		return add (element, -1, sorted);
	}

	/**
	 * Add the given element at the given position
	 *
	 * @param element the element to add
	 * @param index the index of the element
	 * @param sorted if true, the list will be sorted
	 * @return the index of the element
	 */
	default int add (E element, int index, boolean sorted) {
		if (index < 0) {
			getVector ().add (element);
		} else {
			getVector ().add (index, element);
		}
		if (sorted) {
			sort ();
		}
		refresh ();
		setSelectedElement (element, true);
		if (index < 0) {
			return getSelectedIndex ();
		} else {
			return index;
		}
	}

	/**
	 * Erase the given element from the list
	 *
	 * @param element the element
	 * @return the index of the erased element
	 */
	default int erase (E element) {
		int index = indexOf (element);
		if (index == -1) {
			return -1;
		}
		erase (index);
		return index;
	}

	/**
	 * Erase the element at the given index
	 *
	 * @param index the index
	 * @return the erased element
	 */
	default E erase (int index) {
		Vector <E> elements = getVector ();
		E element = elements.remove (index);
		refresh ();
		if (index < elements.size ()) {
			setSelectedIndex (index);
		} else {
			setSelectedIndex (elements.size () - 1);
		}
		return element;
	}

	/**
	 * Add all elements from the other list
	 *
	 * @param otherList the other list
	 */
	default void addAll (JExtended <E> otherList) {
		addAll (otherList.getVector (), false);
	}

	/**
	 * Add all elements from the other list
	 *
	 * @param otherList the other list
	 * @param sorted if <code>true</code>, sort the elements
	 */
	default void addAll (JExtended <E> otherList, boolean sorted) {
		addAll (otherList.getVector (), sorted);
	}

	/**
	 * Add all elements from the other vector
	 *
	 * @param otherElements the other vector
	 */
	default void addAll (Vector <E> otherElements) {
		addAll (otherElements, false);
	}

	/**
	 * Add all elements from the other vector
	 *
	 * @param otherElements the other vector
	 * @param sorted if <code>true</code>, sort the elements
	 */
	default void addAll (Vector <E> otherElements, boolean sorted) {
		getVector ().addAll (otherElements);
		if (sorted) {
			sort ();
		}
		refresh ();
	}

	/**
	 * Remove from this list all elements contained in the other vector
	 *
	 * @param otherElements the other vector
	 */
	default void eraseAll (Vector <E> otherElements) {
		getVector ().retainAll (otherElements);
		refresh ();
	}

	/**
	 * Move the given element to the given list
	 *
	 * @param element the element to move
	 * @param otherList the other list
	 * @return the new position of the element
	 */
	default int transferTo (E element, JExtended <E> otherList) {
		return transferTo (getVector ().indexOf (element), otherList, false);
	}

	/**
	 * Move the given element to the given list
	 *
	 * @param element the element to move
	 * @param otherList the other list
	 * @param sorted if <code>true</code>, the other list will be sorted
	 * @return the new position of the element
	 */
	default int transferTo (E element, JExtended <E> otherList, boolean sorted) {
		return transferTo (getVector ().indexOf (element), otherList, sorted);
	}

	/**
	 * Move the element at the given index to the given list
	 *
	 * @param element the index of the element to move
	 * @param otherList the other list
	 * @return the new position of the element
	 */
	default int transferTo (int element, JExtended <E> otherList) {
		return transferTo (element, otherList, false);
	}

	/**
	 * Move the element at the given index to the given list
	 *
	 * @param element the index of the element to move
	 * @param otherList the other list
	 * @param sorted if <code>true</code>, the other list will be sorted
	 * @return the new position of the element
	 */
	default int transferTo (int element, JExtended <E> otherList, boolean sorted) {
		if (element == -1) {
			return -1;
		}
		E transfered = erase (element);
		if (transfered == null) {
			return -2;
		}

		otherList.add (transfered, sorted);
		otherList.setSelectedElement (transfered, true);
		Vector <E> elements = getVector ();
		if (element < elements.size ()) {
			setSelectedIndex (element);
			return element;
		} else {
			setSelectedIndex (elements.size () - 1);
			return elements.size () - 1;
		}
	}

	/**
	 * Transfer all elements to the given list
	 *
	 * @param otherList the other list
	 * @param sorted if <code>true</code>, the other list will be sorted
	 */
	default void transferAll (JExtended <E> otherList, boolean sorted) {
		Vector <E> elements = getVector ();
		otherList.addAll (elements, sorted);
		elements.clear ();
		refresh ();
	}

	/**
	 * Move up the given element
	 *
	 * @param element the element to move
	 * @return the switched element
	 */
	default E moveUp (E element) {
		return moveUp (indexOf (element));
	}

	/**
	 * Move up the element at the given index
	 *
	 * @param element the index of the element to move
	 * @return the switched element
	 */
	default E moveUp (int element) {
		if (element < 1) {
			return null;
		}
		switchElements (element, element - 1);
		return getVector ().get (element); // return the other element
	}

	/**
	 * Move down the given element
	 *
	 * @param element the element to move
	 * @return the switched element
	 */
	default E moveDown (E element) {
		return moveDown (indexOf (element));
	}

	/**
	 * Move down the element at the given index
	 *
	 * @param element the index of the element to move
	 * @return the switched element
	 */
	default E moveDown (int element) {
		Vector <E> elements = getVector ();
		if (element == elements.size () - 1) {
			return null;
		}
		switchElements (element, element + 1);
		return elements.get (element); // return the other element
	}

	/**
	 * Switch the position of the elements at the given positions
	 *
	 * @param indexA index of an element
	 * @param indexB index of another element
	 */
	default void switchElements (int indexA, int indexB) {
		E elemA = get (indexA);
		E elemB = get (indexB);
		set (indexB, elemA);
		set (indexA, elemB);
		refresh ();
	}

	/**
	 * Switch the position of the given elements
	 *
	 * @param elemA an element
	 * @param elemB another element
	 */
	default void switchElements (E elemA, E elemB) {
		int indexA = indexOf (elemA);
		int indexB = indexOf (elemB);
		set (indexB, elemA);
		set (indexA, elemB);
		refresh ();
	}

	/**
	 * Sort the elements
	 */
	default void sort () {
		sort (false);
	}

	/**
	 * Sort the elements
	 *
	 * @param refresh if true, refresh the list
	 */
	default void sort (boolean refresh) {
		Vector <E> elements = getVector ();
		Collections.sort (elements);
		if (refresh) {
			refresh (elements);
		}
	}

	/**
	 * Refresh the list
	 */
	default void refresh () {
		refresh (getVector ());
	}

	/**
	 * Refresh the list with a temporary vector
	 *
	 * @param elements the temporary vector
	 */
	void refresh (Vector <E> elements);

}
