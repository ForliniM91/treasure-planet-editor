package interfaces;

public interface TriggerElement <T extends TriggerElementType> extends WriteCode {

	/**
	 * Get the elementType.
	 * 
	 * @return The element type.
	 */
	T getElementType ();

	/**
	 * Set the element type.
	 * 
	 * @param type The element type.
	 */
	void setElementType (T type);

	/**
	 * Get the values assigned to the fields.
	 * 
	 * @return The values assigned to the fields.
	 */
	Object[] getValues ();

	/**
	 * Set the values assigned to the fields.
	 * 
	 * @param fields The values assigned to the fields.
	 */
	void setValues (Object[] fields);

}
