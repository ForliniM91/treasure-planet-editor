package logic;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import data.world.Alliance;
import data.world.Dialog;
import data.world.Entity;
import data.world.Player;
import data.world.Point2D;
import data.world.Team;
import gui.MainMenu;
import interfaces.Colorable;
import interfaces.JExtended;
import interfaces.WriteCode;


public class Core {

	public static final String	REGISTER_PATH	= "HKCU\\Software\\Microsoft\\IntelliPoint\\AppSpecific\\TP_Win32.exe";
	public static final String	GAME_PATH		= getGamePath ();
	public static final Random	RANDOM			= new Random ();

	static {
		WriteCode.numLineFormat.setMinimumIntegerDigits (8);
		WriteCode.numLineFormat.setGroupingUsed (false);
		WriteCode.floatFormat6.setMaximumFractionDigits (3);
		WriteCode.floatFormat6.setGroupingUsed (false);
		WriteCode.floatFormat6.setRoundingMode (RoundingMode.HALF_UP);
		WriteCode.floatFormat6.setMaximumFractionDigits (6);
		WriteCode.floatFormat6.setGroupingUsed (false);
		WriteCode.floatFormat6.setRoundingMode (RoundingMode.HALF_UP);
	}



	// GAME DATA: DEFINED BY THE GAME. Loaded in "loadResources()"
	/** Available map names. */
	public static String[]				mapNames		= { "Map1", "Map2", "Map3", "Map4", "Map5", "Map6", "Map7", "Map8" };
	/** Available map descriptions. */
	public static String[]				mapDesc			= { "Desc1", "Desc2", "Desc3", "Desc4", "Desc5", "Desc6", "Desc7", "Desc8" };
	/** Available backgrounds. */
	public static String[]				backgrounds		= { "Background 1", "Background 2", "Background 3", "Background 4" };
	/** Available starmaps. */
	public static String[]				starmaps		= { "Starmap 1", "Starmap 2", "Starmap 3", "Starmap 4" };
	/** Available task names. */
	public static String[]				taskNames		= { "Distruggi Tizio", "Cattura Caio", "Salva Sempronio", "Proteggi Pinco Pallino", "Vai a fanculo" };
	/** Available dialogue speakers. */
	public static String[]				dialogSpeaker	= { "Tizio 1", "Tizio 2", "Tizio 3", "Tizio 4", "Tizio 5" };
	/** Available dialogue voices. */
	public static String[]				dialogVoices	= { "Voice 1", "Voice 2", "Voice 3", "Voice 4", "Voice 5", "Voice 6", "Voice 7", "Voice 8" };
	/** Available dialogue textures. */
	public static String[]				dialogTextures	= { "Texture 1", "Texture 2", "Texture 3", "Texture 4", "Texture 5", "Texture 6", "Texture 7", "Texture 8" };
	/** Available dialogue texts. */
	public static String[]				dialogTexts		= { "Text 1", "Text 2", "Text", "Text 4", "Text 5", "Text 6", "Text 7", "Text 8" };
	/** Available worlds. */
	public static final Vector <String>	worlds			= new Vector<> ();
	/** Available winner messages. */
	public static final Vector <String>	winnerMessages	= new Vector<> ();
	/** Available loser messages. */
	public static final Vector <String>	loserMessage	= new Vector<> ();
	/** Available ship names. */
	public static final Vector <String>	shipNames		= new Vector<> ();
	/** Available flag images. */
	public static final Vector <String>	flagImages		= new Vector<> ();
	/** Available banner types. */
	public static final Vector <String>	bannerType		= new Vector<> ();
	/** Available music files. */
	public static final Vector <String>	musicFiles		= new Vector<> ();
	/** Available hud textures. */
	public static final Vector <String>	hudTextures		= new Vector<> ();
	/** Available starmap text. */
	public static final Vector <String>	mapText			= new Vector<> ();
	/** Available effects. */
	public static final Vector <String>	effectNames		= new Vector<> ();
	/** Team names used in the map. */
	public static final Vector <String>	usedTeamsNames	= new Vector<> ();

	public static MainMenu				mainMenu		= new MainMenu ();




	public static void main (String[] args) {}


	public static String getGamePath () {
		try {
			Process p = Runtime.getRuntime ().exec ("REG QUERY " + REGISTER_PATH);
			try {
				p.waitFor ();
			} catch (InterruptedException e) {
				e.printStackTrace ();
			}
			try (InputStream in = p.getInputStream ()) {
				byte[] buffer = new byte[5000];
				in.read (buffer);
				String s = new String (buffer);
				int i = s.indexOf ("Path    REG_SZ    ");
				int j = s.lastIndexOf ("TP_Win32.exe");
				return s.substring (i + 18, j);
			} finally {
				p.destroy ();
			}
		} catch (IOException e) {
			e.printStackTrace ();
			System.exit (0);
		}
		return null; // UNREACHABLE, BUT REQUIRED TO COMPILE
	}


	/**
	 * Check for deleted teams, and update every object pointing to it. If no team is passed, it check for all teams.
	 *
	 * @param deleted Teams to check for. If empty, check for all teams.
	 */
	public static void checkTeams (Team... deleted) {
		Team[] check;
		if (deleted != null && deleted.length > 0) {
			check = deleted;
		} else if (Team.teams.size () > 0) {
			check = Team.teams.toArray (new Team[Team.teams.size ()]);
		} else { // No teams, go for everyone
			for (Player p : Player.players) {
				p.setTeam (Team.teams.get (0));
			}
			return;
		}

		PCICLE: for (Player p : Player.players) {
			for (Team t : check) {
				if (p.getTeam () == t) {
					p.setTeam (Team.teams.get (0));
					continue PCICLE;
				}
			}
		}
	}

	/**
	 * Check for deleted players, and update every object pointing to it. If no team is passed, it check for all teams.
	 *
	 * @param deleted Teams to check for. If empty, check for all teams.
	 */
	public static void checkPlayers (Colorable... deleted) {
		Colorable[] check;
		if (deleted != null && deleted.length > 0) {
			check = deleted;
		} else if (Player.players.size () > 0) {
			check = Player.players.toArray (new Player[Player.players.size ()]);
		} else { // No players, no alliances
			Alliance.alliances.clear ();
			return;
		}

		int j;
		for (int i = 0; i < check.length; i++) {
			j = 0;
			while (j < Alliance.alliances.size ()) {
				if (deleted[i] == Alliance.alliances.get (j).getPlayerA () || deleted[i] == Alliance.alliances.get (j).getPlayerB ()) {
					Alliance.alliances.remove (j);
				} else {
					j++;
				}
			}
		}
	}




	/**
	 * Convert the first letter to upper case and all other chars to lower case
	 *
	 * @param s The string to convert
	 * @return The new string
	 */
	public static String firstUpper (String s) {
		return Character.toUpperCase (s.charAt (0)) + s.substring (1).toLowerCase ();
	}

	/**
	 * Parse a String as an Integer. Return null if the String is not an Integer.
	 *
	 * @param s The string
	 * @return The Integer in the String
	 */
	public static Integer getInt (String s) {
		try {
			return Integer.valueOf (s);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Retrieve the string in the JTextField and parse it as an Integer, Return null if the String is not an Integer.
	 *
	 * @param textField The JTextField
	 * @return The Integer in the JTextField
	 */
	public static Integer getInt (JTextField textField) {
		return getInt (textField.getText ());
	}

	/**
	 * Parse a String as a Float. Return null if the String is not a Float.
	 *
	 * @param s The string
	 * @return The Integer in the String
	 */
	public static Float getFloat (String s) {
		try {
			return Float.parseFloat (s);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Retrieve the string in the JTextField and parse it as a Float, Return null if the String is not a Float.
	 *
	 * @param textField The JTextField
	 * @return The Integer in the JTextField
	 */
	public static Float getFloat (JTextField textField) {
		return getFloat (textField.getText ());
	}

	/**
	 * Parse a String as a Double. Return null if the String is not a Double.
	 *
	 * @param s The string
	 * @return The Integer in the String
	 */
	public static Double getDouble (String s) {
		try {
			return Double.parseDouble (s);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Retrieve the string in the JTextField and parse it as a Double, Return null if the String is not a Double.
	 *
	 * @param textField The JTextField
	 * @return The Integer in the JTextField
	 */
	public static Double getDouble (JTextField textField) {
		return getDouble (textField.getText ());
	}

	/**
	 * Retrieve the number of digits in a number.
	 *
	 * @param number The number
	 * @return The number of digits.
	 */
	public static int getNumDigits (double number) {
		return 1 + (int) Math.log (number);
	}


	public static boolean checkID (String ID, Entity element, boolean msgOk) {
		if (ID.length () < 2) {
			JOptionPane.showMessageDialog (null, "The ID must be at least 2 characthers", "Check the ID", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (element != null && !ID.equalsIgnoreCase (element.getID ())) {
			for (Dialog p : Dialog.dialogs) {
				if (ID.equalsIgnoreCase (p.getID ())) {
					JOptionPane.showMessageDialog (null, "This ID is already used!", "ID already used", JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}
		if (msgOk) {
			JOptionPane.showMessageDialog (null, "This ID is fine!", "Ok", JOptionPane.INFORMATION_MESSAGE);
		}
		return true;
	}

	public static boolean checkString (String str, Entity element, JExtended <? extends Entity> listItems) {
		if (element == null || !str.equals (element.getID ())) {
			for (Entity t : listItems.getVector ()) {
				if (str.equals (t.getID ())) {
					JOptionPane.showMessageDialog (null, "This strings is already used!", "String already used", JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Check the ID and calculate the next index, if any is present. Else assign the index 2;
	 *
	 * @param ID The ID to check
	 * @param list The list where to search for this ID.
	 * @return The ID with a new index
	 * @throws MaxIndex If there the maximum amount of index has been reached.
	 */
	public static String getCloneID (String ID, List <? extends Entity> list) throws MaxIndex {
		String base = "", copy;
		int i;
		String a, b = "";
		boolean jump = false;

		for (i = ID.length () - 1; i >= 0; i--) {
			if (!Character.isDigit (ID.charAt (i))) {
				if (i == ID.length () - 1) { // First (or maybe last!) char can be ignored if not a number
					jump = true;
				} else {
					if (i < ID.length () - 2 | jump == false) {
						base = ID.substring (0, i); // The first (last) char was surely a number, so let's take the base up to this char.
					} else {
						base = ID; // First (last) char was jumped and the second is not a number too. Don't expect a number, so let's take the whole ID as base
					}
					break;
				}
			}
		}

		a = "" + ID.charAt (i);
		switch (a) {
			case "(":
				b = ")";
				break;
			case "[":
				b = "]";
				break;
			case "{":
				b = "}";
				break;
			case "<":
				b = ">";
				break;
			default:
		}

		WHILE: while (i <= 64) {
			copy = base + a + i + b;
			for (Entity iter : list) {
				if (iter.getID ().equals (copy)) {
					i++;
					continue WHILE;
				}
			}
			return copy;
		}
		throw new MaxIndex ();
	}

	/**
	 * Convert a game Race ID to the editor Race ID.
	 *
	 * @param ind The game Race ID
	 * @return The editor Race ID
	 * @throws IllegalArgumentException If there's no race with that ID.
	 */
	public static int getRaceEditorIndex (int ind) throws IllegalArgumentException {
		if (ind == 1 || ind < 0 || ind > 4) {
			throw new IllegalArgumentException ("There's no race with this index");
		}
		return (ind == 0 ? 0 : ind - 1);
	}

	/**
	 * Convert an editor Race ID to the game Race ID.
	 *
	 * @param ind The editor Race ID
	 * @return The game Race ID
	 * @throws IllegalArgumentException If there's no race with that ID.
	 */
	public static int getRaceGameIndex (int ind) throws IllegalArgumentException {
		if (ind < 0 || ind > 3) {
			throw new IllegalArgumentException ("There's no race with this index");
		}
		return (ind == 0 ? 0 : ind + 1);
	}

	/**
	 * Check for the typed character and the resulting number: only allows digits, and only allow numbers in the defined range.
	 *
	 * @param field The JTextField containing the string
	 * @param e The KeyTiped fired
	 * @param isFloat Does the number is a float?
	 * @param min Minimum number allowed. If null, the minimum Integer value is used.
	 * @param max Maximum number allowes. If null, the maximum Integer value is used.
	 * @return True if the character is valid and the resulting number is between min and max. False otherwise.
	 */
	public static boolean isValidNumber (JTextField field, KeyEvent e, boolean isFloat, Float min, Float max) {
		char c = e.getKeyChar ();
		CHECKKEY: {
			if (Character.isDigit (c)) {
				break CHECKKEY;
			}
			if (c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE) {
				break CHECKKEY;
			}
			if (c == KeyEvent.VK_MINUS && field.getCaretPosition () == 0 && field.getText ().indexOf ('-') == -1) {
				break CHECKKEY;
			}
			if (isFloat && c == KeyEvent.VK_PERIOD && field.getCaretPosition () != 0 && field.getText ().indexOf ('.') == -1) {
				break CHECKKEY;
			}
			return false;
		}

		String s = field.getText ();
		if (s.isEmpty ()) {
			return true;
		}
		APPLYEVENT: {
			String sel = field.getSelectedText ();
			if (sel != null) {
				int i = field.getSelectionStart (), j = field.getSelectionEnd ();
				s = s.substring (0, i) + c + s.substring (j);
			} else {
				int i = field.getCaretPosition ();
				if (c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE) {
					break APPLYEVENT;
				} else if (i < s.length ()) {
					s = s.substring (0, i) + c + s.substring (i);
				} else {
					s += c;
				}
			}
		}

		// CHECK RESULT:
		Float f;
		try {
			if (isFloat) {
				f = Float.valueOf (s);
			} else {
				f = (float) Integer.valueOf (s);
			}
		} catch (NumberFormatException e1) {
			if (min == null) {
				min = (float) Integer.MIN_VALUE;
			}
			if (max == null) {
				max = (float) Integer.MAX_VALUE;
			}
			JOptionPane.showMessageDialog (field, "This value is not valid: " + s + "\nThe value must be a number between " + min.intValue () + " and " + max.intValue (), "The value is not valid", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (min != null && f < min) {
			JOptionPane.showMessageDialog (field, "This number is not valid: " + f + "\nThe minimum value is " + min, "The number is not valid", JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (max != null && max < f) {
			JOptionPane.showMessageDialog (field, "This number is not valid: " + f + "\nThe maximum value is " + max, "The number is not valid", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	@SuppressWarnings ("unchecked")
	public static <T extends Entity> List <T> cloneListE (List <T> list) {
		List <T> copy;
		if (list instanceof ArrayList) {
			copy = new ArrayList<> ();
		} else if (list instanceof Vector) {
			copy = new Vector<> ();
		} else {
			throw new InternalError ();
		}
		for (int i = 0; i < list.size (); i++) {
			copy.add ((T) list.get (i).clone ());
		}
		return copy;
	}

	@SuppressWarnings ("unchecked")
	public static <T extends Point2D> List <T> cloneListP (List <T> list) {
		List <T> copy;
		if (list instanceof ArrayList) {
			copy = new ArrayList<> ();
		} else if (list instanceof Vector) {
			copy = new Vector<> ();
		} else {
			throw new InternalError ();
		}
		for (int i = 0; i < list.size (); i++) {
			copy.add ((T) list.get (i).clone ());
		}
		return copy;
	}

	public static Image getImage (String path, String image) {
		path = GAME_PATH + path + image;
		try {
			return TargaReader.getImage (path);
		} catch (IOException e) {
			return null;
		}
	}




	/**
	 * Convert a throwable's stack trace to String
	 *
	 * @param e The throwable
	 * @return Its stack trace
	 */
	public static String buildStackTrace (Throwable e) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream ();
		PrintStream ps = new PrintStream (baos);
		e.printStackTrace (ps);
		return new String (baos.toByteArray (), StandardCharsets.UTF_8);
	}


	/**
	 * Show a message about an error and ask the user to see the stack trace of the given exception
	 *
	 * @param parent The parent component
	 * @param e The exception
	 * @param message Message to display
	 * @param title Title of the message
	 * @param alsoPrintToConsole if <code>true</code>, also print the stack trace to console
	 */
	public static void printException (Component parent, Throwable e, String message, String title, boolean alsoPrintToConsole) {
		String[] exceptionChoices = { "Close", "Show stack trace" };
		if (JOptionPane.showOptionDialog (parent, message, title, 0, JOptionPane.ERROR_MESSAGE, null, exceptionChoices, exceptionChoices[0]) == 1) {
			printException (parent, e, alsoPrintToConsole);
		}
	}

	/**
	 * Show a message with the stack trace of the given exception
	 *
	 * @param parent The parent component
	 * @param e The exception
	 * @param alsoPrintToConsole if <code>true</code>, also print the stack trace to console
	 */
	public static void printException (Component parent, Throwable e, boolean alsoPrintToConsole) {
		if (alsoPrintToConsole) {
			e.printStackTrace ();
		}
		JTextArea area = new JTextArea (buildStackTrace (e));
		area.setForeground (Color.RED);
		JScrollPane scrollPane = new JScrollPane (area);
		scrollPane.setPreferredSize (new Dimension (800, 600));
		printError (parent, scrollPane, "Exception: stack trace");
	}

	/**
	 * Show a message about an error
	 *
	 * @param parent The parent component
	 * @param message Message to display
	 * @param title Title of the message
	 */
	public static void printError (Component parent, Object message, String title) {
		JOptionPane.showMessageDialog (parent, message, title, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Show a message about a warning
	 *
	 * @param parent The parent component
	 * @param message Message to display
	 * @param title Title of the message
	 */
	public static void printWarning (Component parent, Object message, String title) {
		JOptionPane.showMessageDialog (parent, message, title, JOptionPane.WARNING_MESSAGE);
	}




}
