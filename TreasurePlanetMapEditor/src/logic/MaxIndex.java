package logic;

public class MaxIndex extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6692802326866171999L;

	public MaxIndex (){
		super("Can't duplicate, since there are too many duplicates! Rename some of them and retry.");
	}
	
}
